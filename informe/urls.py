from django.conf.urls import url
from django.urls import path
from . import views
from .views import *
from django.contrib.auth.decorators import login_required

app_name = 'informe'


informe_patterns = ([
    path('list_pendientes/', login_required(InformeListar.as_view()), name='inf_listar'),
    path('list_pendientes_medico/', login_required(InformeListarMedico.as_view()), name='inf_listarmedico'),
    path('list_firmar/', login_required(InformeFirmar.as_view()), name='inf_listarfirmar'),
    path('informe_generar/', views.informe_medico_pdf, name='inf_informegenerar'),
    url(r'^informe_crear/(?P<pk>\d+)/$', login_required(InformeCrear.as_view()), name='inf_crear'),
    url(r'^informe_crearmedico/(?P<pk>\d+)/$', login_required(InformeCrearMedico.as_view()), name='inf_crearmedico'),
    url(r'^informe_subirfirmado/(?P<pk>\d+)/$', login_required(InformeSubirFirmado.as_view()),
        name='inf_subirfirmado'),
    url(r'^preinforme/(?P<pk>\d+)/$', login_required(PreInformeCrear.as_view()), name='inf_preinforme'),
    path('ajax/medicomatricula/', views.medicomatricula, name='ajaxmedicomatricula'),
    path('batch/exportarinformes/', views.exportar_informes, name='exportarinformes'),

    path('ajax/grabaraudio/', views.grabaraudio, name='ajaxgrabaraudio'),
    path('informe/corregirpaciente/', views.corregirpaciente, name='corregirpaciente'),
    path('informe/corregirtexto/', views.traer_texto_corregir, name='traertexto'),
    path('informe/corregirestudio/', views.corregirestudio, name='corregirestudio'),
    path('informe/corregirtextoestudio/', views.traer_texto_corregir_estudio, name='traertextoestudio'),
    path('informe/cambiarfirmadodigital/', views.finalizar_sinfirma, name='finalizar_sinfirma'),
    path('informe/estadisticamensual/', views.estadisticas_excel, name='estadisticas_excel'),
    path('informe/imprimiretiqueta/', views.imprimir_etiqueta, name='imprimir_etiqueta'),
    path('consultarplantillas/', views.consultar_plantillas, name='consultar_plantillas'),
    path('informeteccorregido/', views.informe_tec_correccion_finalizada, name='informe_tec_corregido'),
    path('medicoscountmes/', views.medicos_count_mes, name='medicos_count_mes'),
    path('informecorecciontexto/', views.InformeCorregir, name='informe_corregir'),
    path('tipeoenuso/', views.tipeo_marcar_enuso, name='tipeo_en_uso'),
    path('informecerrarexpediente/', login_required(CerrarExpediente.as_view()), name='cerrar_expediente'),
    path('unificarPaciente/', views.unificar_paciente_index, name='unificar_paciente'),
    path('unificarPacienteHC/', views.unificar_paciente_hc, name='unificar_paciente_hc'),
    path('cerrarexpediente/', views.cerrar_expediente_sin_informe, name='cerrar_expediente_sin_informe'),
    ], 'informe')
