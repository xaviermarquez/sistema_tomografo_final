from django.contrib import admin
from .models import *

admin.site.register(InformePlanilla)
admin.site.register(InformePlanillaDBTCSE)
admin.site.register(UnificarPaciente)
