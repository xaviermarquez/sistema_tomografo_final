import ast
import subprocess
import threading

import pytz
import json
import io
import os
import re

from .forms import *
from .models import InformePlanilla, UnificarPaciente
from turno.models import *
from usuario.models import UserProfile
from tecnico.models import TecnicoInsumo
from utils.patient_functions import patiient_identity_label
from django.db import transaction
from django.shortcuts import redirect, reverse, render
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, TemplateView
from django.db.models import Q
from django.contrib import messages
from turno.views import generar_linkimagenes
from django.core.mail import EmailMessage
from pydub import AudioSegment
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, timezone, date
import textwrap
from reportlab.lib.pagesizes import A4
from django.http import FileResponse
from reportlab.platypus import Image
from django.http import HttpResponse, JsonResponse
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
import html2text
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from reportlab.pdfgen.canvas import Canvas
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache


class NeverCacheMixin(object):
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(NeverCacheMixin, self).dispatch(*args, **kwargs)


def is_ip_private(ip):
    priv_lo = re.compile("^127\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    priv_24 = re.compile("^10\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    priv_20 = re.compile("^192\.168\.\d{1,3}.\d{1,3}$")
    priv_16 = re.compile("^172.(1[6-9]|2[0-9]|3[0-1]).[0-9]{1,3}.[0-9]{1,3}$")

    res = priv_lo.match(ip) or priv_24.match(ip) or priv_20.match(ip) or priv_16.match(ip)
    return res is not None


class NumberedPageCanvas(Canvas):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)

        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            super().showPage()

        super().save()

    def draw_page_number(self, page_count):
        if self._pageNumber == 1:
            pass
        else:
            page = "Página %s de %s" % (self._pageNumber - 1, page_count - 1)
            self.setFont("Helvetica", 11)
            self.drawString(450, A4[1] - 125, page)


class InformeListar(ListView):
    model = Turno
    template_name = 'informe/informe_list.html'

    def get_context_data(self, **kwargs):
        ahora = datetime.now(timezone.utc)
        ahora_menos48hours = ahora - timedelta(hours=48)
        ahora_menos24hours = ahora - timedelta(hours=24)
        servicios = Servicio.objects.filter(habilitado=True)

        filtro_pasado_facturacion = self.request.GET.get('facturacion', False)

        for servicio in servicios:
            q_pendientes_todos = Turno.objects.filter(
                habilitado=True,
                servicio=servicio,
                turno_recepcion_suspendido=False,
                informe_cerrado=False,
                estudio_terminado=True, ) \
                .exclude(informe_firmadodigital=True) \
                .exclude(turno_relacionado_id__isnull=False) \
                .exclude(estudio_cancelado=True) \
                .exclude(estudio_reprograma=True)

            if filtro_pasado_facturacion:
                q_pendientes_todos = q_pendientes_todos\
                    .filter(informe_finalizado=True, informe_no_firmadigital=True, informe_facturado=False)
            else:
                q_pendientes_todos = q_pendientes_todos\
                    .exclude(informe_finalizado=True, informe_no_firmadigital=True)

            q_atrasados_normal = q_pendientes_todos.filter(
                urgencia=False,
                fechahora_inicio__lte=ahora_menos48hours)

            q_atrasados_urgencia = q_pendientes_todos.filter(
                urgencia=True,
                fechahora_inicio__lte=ahora_menos24hours)

            q_atrasados_todos = q_atrasados_normal | q_atrasados_urgencia

            turnos_en_partes_atra = q_atrasados_todos.filter(tipomodalidad="En partes")
            if turnos_en_partes_atra:
                for tur_p in turnos_en_partes_atra:
                    if not tur_p.estudio_partes_finalizado:
                        q_atrasados_todos = q_atrasados_todos.exclude(id=tur_p.id)

            atrasados_todos_count = q_atrasados_todos.exclude(informe_finalizado=True).count()
            for atrasado in q_atrasados_todos:
                if atrasado.informe_finalizado:
                    continue
                if atrasado.tipomodalidad == 'En partes':
                    ultimo_turno_hijo = Turno.objects.filter(turno_relacionado=atrasado,
                                                             habilitado=True).order_by(
                        "turno__fechahora_inicio").last()
                    if ultimo_turno_hijo:
                        fechaini_turno = ultimo_turno_hijo.fechahora_inicio
                    else:
                        fechaini_turno = atrasado.fechahora_inicio
                else:
                    fechaini_turno = atrasado.fechahora_inicio

                if atrasado.urgencia:
                    tiempo_atraso = ahora - (fechaini_turno + timedelta(hours=24))
                else:
                    tiempo_atraso = ahora - (fechaini_turno + timedelta(hours=48))

                horas_atraso = int(tiempo_atraso.seconds / 3600)
                dias_atraso = tiempo_atraso.days
                if dias_atraso == 0:
                    string_atraso = str(horas_atraso) + ' hrs.'
                else:
                    string_atraso = str(dias_atraso) + ' dias ' + str(horas_atraso) + ' hrs.'
                atrasado.tiempo_atraso = string_atraso
                atrasado.horas_atraso = dias_atraso + ( horas_atraso / 100)

            turnos_en_partes_pend = q_pendientes_todos.filter(tipomodalidad="En partes")
            if turnos_en_partes_pend:
                for tur_p in turnos_en_partes_pend:
                    if not tur_p.estudio_partes_finalizado:
                        q_pendientes_todos = q_pendientes_todos.exclude(id=tur_p.id)

            pendientes_todos_count = q_pendientes_todos.exclude(informe_finalizado=True).count()

            q_pend_intersec_atrasados = q_pendientes_todos & q_atrasados_todos
            q_pend_diff_atrasados = q_pendientes_todos.difference(q_pend_intersec_atrasados).order_by(
                'fechahora_inicio')

            q_pendientes_urgencia_vencenhoy = q_pend_diff_atrasados.exclude(urgencia=False)
            for turno_urgencia in q_pendientes_urgencia_vencenhoy:
                if turno_urgencia.urgencia:
                    turno_urgencia.vence_hoy = True

            counter_vencehoy = 0
            counter_vence_despues = 0
            for turno in q_pend_diff_atrasados:
                if turno.urgencia:
                    turno.vence_hoy = True
                    counter_vencehoy += 1
                else:
                    if turno.tipomodalidad == 'En partes':
                        ultimo_turno_hijo = Turno.objects.filter(turno_relacionado=turno,
                                                                 habilitado=True).order_by(
                            "turno__fechahora_inicio").last()
                        if ultimo_turno_hijo:
                            fechaini_turno = ultimo_turno_hijo.fechahora_inicio
                        else:
                            fechaini_turno = turno.fechahora_inicio
                    else:
                        fechaini_turno = turno.fechahora_inicio
                    tiempo_atraso = ahora - fechaini_turno
                    horas_atraso = int(tiempo_atraso.seconds / 3600) + int(tiempo_atraso.days) * 24
                    if horas_atraso >= 24:
                        vencehoy = True
                        counter_vencehoy = + 1
                    else:
                        vencehoy = False
                        counter_vence_despues += 1
                    turno.vence_hoy = vencehoy

            servicio.pendientes_todos_count = pendientes_todos_count
            servicio.atrasados_count = atrasados_todos_count
            servicio.list_atrasados = q_atrasados_todos
            servicio.list_pendientes = q_pend_diff_atrasados
            servicio.vencehoy_count = counter_vencehoy
            servicio.vencedespues_count = counter_vence_despues

        servicio_select_num = self.request.GET.get('q_servicio', '')
        if servicio_select_num != '':
            servicio_select_nombre = Servicio.objects.filter(id=servicio_select_num).first().nombre
        else:
            servicio_select_nombre = 'Todos'

        context = {'servicios': servicios, 'servicio_select_num': servicio_select_num,
                   'servicio_select_nombre': servicio_select_nombre}
        return context


class InformeListarMedico(ListView):
    model = Turno
    template_name = 'informe_medico/informe_medico_list.html'

    def get_context_data(self, **kwargs):
        session = self.request.session
        if self.request.user.userprofile.perfil == 'Medico':
            session.set_expiry(60 * 40)
        user_medico = self.request.user
        profile_user_medico = UserProfile.objects.get(user=user_medico)
        session = self.request.session
        modo_vista = self.request.GET.get('modovista', 'normal')
        session['modo_vista'] = modo_vista
        servicios_externos = Servicio.objects.filter(is_servicio_externo=True)
        if modo_vista == 'informestodos':
            q_pendientes = Turno.objects.filter(
                habilitado=True,
                informe_cerrado=False,
                estudio_finalizado=True,
                informe_finalizado=True) \
                .order_by('-fechahora_inicio') \
                .exclude(turno_relacionado__isnull=False)
        elif modo_vista == 'informesyo':
            q_pendientes = Turno.objects.filter(
                habilitado=True,
                informe_cerrado=False,
                estudio_finalizado=True,
                usuario_medico=user_medico,
                informe_finalizado=True) \
                .order_by('-fechahora_inicio') \
                .exclude(turno_relacionado__isnull=False)
        elif modo_vista == 'normal':
            q_pendientes = Turno.objects.filter(
                Q(habilitado=True, informe_cerrado=False, estudio_finalizado=True, usuario_medico=user_medico) |
                Q(habilitado=True, informe_cerrado=False, estudio_partes_finalizado=True, usuario_medico=user_medico)) \
                .order_by('-fechahora_inicio') \
                .exclude(informe_firmadodigital=True) \
                .exclude(informe_finalizado=True) \
                .exclude(turno_relacionado__isnull=False) \
                .exclude(servicio__in=servicios_externos)
        elif modo_vista == 'guardia':
            servicio_reso = Servicio.objects.get(codigo=2)
            q_pendientes_guardia = Turno.objects.filter(
                habilitado=True,
                urgencia=True,
                informe_cerrado=False,
                estudio_finalizado=True,
                tipomodalidad='Normal') \
                .exclude(informe_finalizado=True) \
                .exclude(turno_relacionado__isnull=False) \
                .exclude(servicio=servicio_reso) \
                .exclude(usuario_medico__isnull=False) \
                .exclude(servicio__in=servicios_externos)

            list_ids_turnosguardia = []
            for turpend in q_pendientes_guardia:
                turno_hora_guardia = turpend.fechahora_inicio.astimezone(
                    pytz.timezone("America/Argentina/Salta")).time()
                is_time_guadia = turpend.servicio.hora_guardiafin >= turno_hora_guardia or turno_hora_guardia >= turpend.servicio.hora_guardiaini
                turno_fecha_guardia = turpend.fechahora_inicio.astimezone(
                    pytz.timezone("America/Argentina/Salta")).date()
                is_weekend = turno_fecha_guardia.weekday() >= 5
                hollidays = [feriado.fecha for feriado in Feriados.objects.all()]
                is_holliday = turno_fecha_guardia in hollidays

                if is_holliday or is_weekend or is_time_guadia:
                    list_ids_turnosguardia.append(turpend.id)

            q_pendientes = Turno.objects.filter(id__in=list_ids_turnosguardia)
        else:
            q_pendientes = Turno.objects.filter(
                Q(habilitado=True, informe_cerrado=False, estudio_finalizado=True) |
                Q(habilitado=True, informe_cerrado=False, estudio_partes_finalizado=True)) \
                .order_by('-fechahora_inicio') \
                .exclude(informe_firmadodigital=True) \
                .exclude(informe_finalizado=True) \
                .exclude(turno_relacionado__isnull=False) \
                .exclude(estudio_partes_finalizado=None, tipomodalidad='En partes') \
                .exclude(estudio_partes_finalizado=False, tipomodalidad='En partes') \
                .exclude(servicio__in=servicios_externos)
        context = {'list_pendientes': q_pendientes, 'medico_matricula': profile_user_medico.matricula,
                   'medico': user_medico}
        return context


class InformeFirmar(ListView):
    model = Turno
    template_name = 'informe_medico/informe_listado_finalizados.html'

    def get_context_data(self, **kwargs):
        turnos = Turno.objects.filter(habilitado=True, estudio_terminado=True, informe_finalizado=True,
                                      informe_firmadodigital=False).order_by('fechahora_inicio')
        context = {'object_list': turnos}
        return context


class InformeCrear(NeverCacheMixin, UpdateView):
    model = Turno
    form_class = InformeCrearForm
    template_name = 'informe/informe_crear.html'
    success_url = reverse_lazy('informe:inf_listar')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turno_id = self.kwargs['pk']
        turno_editar = Turno.objects.get(pk=turno_id)
        form = InformeCrearForm(request.POST, instance=turno_editar)

        finalizartipeo = request.GET.get("finalizartipeo", "")
        if form.is_valid() and not turno_editar.informe_finalizado:
            form.save()
            if not turno_editar.usuario_tipeo_registra:
                turno_editar.usuario_tipeo_registra = request.user
                turno_editar.save()

            if finalizartipeo == '1':
                turno_editar.informe_finalizado_tipeo = True
                turno_editar.usuario_tipeo = request.user
                turno_editar.informe_fecha_finalizacion_t = datetime.now()
                turno_editar.save()
                websocket_actualizarinforme(turno_editar.id, 'tipeo_finalizado')
            else:
                turno_editar.usuario_tipeo_ultimaedicion = request.user
                turno_editar.save()

            return redirect(reverse('informe:inf_preinforme', kwargs={'pk': turno_editar.id}) + "?tipo=1")
        else:
            return redirect(reverse('informe:inf_preinforme', kwargs={'pk': turno_editar.id}) + "?tipo=1")

    def get_context_data(self, **kwargs):
        context = super(InformeCrear, self).get_context_data(**kwargs)
        ip_request = self.request.META['REMOTE_ADDR']
        return update_contexdata_informecrear(context, ip_request)


def generar_codigos_turno(turno):
    estudios_solicitados = EstudioSolicitado.objects.filter(turno=turno)
    codigos = "Códigos de estudios: \n"
    for est in estudios_solicitados:
        codigo_sufix = est.servicio_estudio.codigo.zfill(2)
        codigo_sol = turno.servicio.protocolo_prefix + codigo_sufix + ' - ' + est.servicio_estudio.nombre
        codigos += codigo_sol + '\n'

    if turno.tipomodalidad == "En partes":
        turnos_hijos = Turno.objects.filter(turno_relacionado=turno, estudio_terminado=True, habilitado=True)
        for t_hijo in turnos_hijos:
            estudios_solicitados_h = EstudioSolicitado.objects.filter(turno=t_hijo)
            for est in estudios_solicitados_h:
                codigo_sufix = est.servicio_estudio.codigo.zfill(2)
                codigo_sol = t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - ' + est.servicio_estudio.nombre
                codigos += codigo_sol + '\n'
    if turno.anestesia:
        obj_servicio_anestesia = ServicioAnestesia.objects.filter(codigo=turno.anestesia_codigo).first()
        if obj_servicio_anestesia:
            anestesia_description = obj_servicio_anestesia.codigo + ' - ' + obj_servicio_anestesia.nombre
            codigos += anestesia_description + '\n'

    if turno.tipomodalidad == "En partes":
        turnos_hijos = Turno.objects.filter(turno_relacionado=turno, estudio_terminado=True, habilitado=True)
        for t_hijo in turnos_hijos:
            if turno.anestesia:
                obj_servicio_anestesia = ServicioAnestesia.objects.filter(codigo=t_hijo.anestesia_codigo).first()
                if obj_servicio_anestesia:
                    anestesia_description = obj_servicio_anestesia.codigo + ' - ' + obj_servicio_anestesia.nombre
                    codigos += anestesia_description + '\n'

    return codigos


class InformeCrearMedico(UpdateView):
    model = Turno
    form_class = InformeCrearForm
    template_name = 'informe_medico/informe_medico_crear.html'
    success_url = reverse_lazy('informe:inf_listarmedico')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turnoid = self.kwargs['pk']
        turno = Turno.objects.get(pk=turnoid)
        form = InformeCrearForm(request.POST, instance=turno)
        tiposubmit = request.POST.get('submittipo', '')
        estudio_finalizado = turno.estudio_finalizado or turno.estudio_partes_finalizado

        if form.is_valid() \
                and request.user.userprofile.perfil == 'Medico' \
                and not turno.informe_finalizado \
                and estudio_finalizado:

            form.save()
            message_success = 'Informe guardado como borrador'
            if tiposubmit == '1' or tiposubmit == 1:
                password_correct = request.user.check_password(request.POST['submitted_password'])
                if password_correct:
                    turno.usuario_medico = request.user
                    turno.informe_finalizado = True
                    turno.informe_fecha_finalizacion = datetime.now()
                    turno.informe_tec_corregir = False

                    # save statics fields
                    paciente_id = turno.paciente.identificacion_tipo
                    paciente_numero = turno.paciente.identificacion_numero
                    paciente_apellido = turno.paciente.apellido if turno.paciente.apellido else ' '
                    paciente_nombre = turno.paciente.nombre if turno.paciente.nombre else ' '
                    paciente_nn = str(turno.paciente.paciente_nn)
                    paciente_nn_label = turno.paciente.paciente_nnlabel if turno.paciente.paciente_nnlabel else '---'
                    paciente_fecnac = turno.paciente.fecnac.strftime('%d-%m-%Y') if turno.paciente.fecnac else '---'
                    paciente_localidad = turno.paciente.localidad.nombre if turno.paciente.localidad else '---'
                    paciente_provincia = turno.paciente.provincia.nombre if turno.paciente.provincia else '---'
                    paciente_sexo = turno.paciente.sexo if turno.paciente.sexo else '---'
                    medico_derivante = turno.medico_derivante.apellido if turno.medico_derivante else '---'
                    establecimiento = turno.establecimiento_derivador.nombre
                    codigos = generar_codigos_turno(turno)
                    datos_informe_finalizado = [
                        paciente_id,
                        paciente_numero,
                        paciente_apellido,
                        paciente_nombre,
                        paciente_nn,
                        paciente_nn_label,
                        paciente_sexo,
                        paciente_fecnac,
                        paciente_provincia,
                        paciente_localidad,
                        medico_derivante,
                        establecimiento,
                        codigos,
                    ]
                    turno.informe_datos_finalizado = datos_informe_finalizado
                    turno.save()

                    # treat send mail
                    thread = threading.Thread(target=enviar_mail_informe, args=([turno]))
                    thread.daemon = True
                    thread.start()

                    websocket_actualizarinforme(turno.id, 'informe_finalizado')
                    message_success = 'Informe finalizado con éxito'
                else:
                    messages.error(request,
                                   'Operación no permitida: Contraseña Incorrecta, el texto fue guardado como borrador')
                    return redirect('../../informe_crearmedico/' + str(turno.pk))

            elif tiposubmit == '2' or tiposubmit == 2:
                turno.informe_para_tipear = True
                turno.informe_finalizado_tipeo = False
                turno.informe_tec_corregir = False
                turno.informe_tec_corregir_completo = False
                turno.save()
                message_success = 'Informe marcado para Tipeo'
            elif tiposubmit == '3' or tiposubmit == 3:
                turno.informe_tec_corregir = True
                if not turno.informe_tec_corregir_ini:
                    turno.informe_tec_corregir_ini = datetime.now()
                turno.save()
                message_success = 'Alerta de corrección registrada'

            messages.success(request, message_success)
            return redirect('../../informe_crearmedico/' + str(turno.pk))
        else:
            messages.error(request, 'Error: Operación no permitida')
            return redirect('../../list_pendientes_medico')

    def get_context_data(self, **kwargs):
        session = self.request.session
        if self.request.user.userprofile.perfil == 'Medico':
            session.set_expiry(60 * 40)
        context = super(InformeCrearMedico, self).get_context_data(**kwargs)
        ip_request = self.request.META['REMOTE_ADDR']

        turno = context['object']
        turno_en_partes = False
        turno_padre = False
        if turno.tipomodalidad == 'En partes':
            turno_padre = True
            turno_en_partes = True

        if turno.turno_relacionado:
            turno_en_partes = True
        turnos_hijos = {}
        if turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(Q(turno_relacionado=turno) | Q(id=turno.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")
        elif not turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(
                Q(turno_relacionado=turno.turno_relacionado) | Q(id=turno.turno_relacionado.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")
        else:
            turnos_hijos = Turno.objects.filter(id=turno.id)

        configuracion_general = ConfiguracionGeneral.objects.all().first()
        if configuracion_general.is_db_production:
            if turnos_hijos and turno.servicio.tipo == 'MR':
                generar_link_turnos_hijos(turnos_hijos)
            else:
                generar_link_turno_normal(turno)

        return update_contexdata_informecrear(context, ip_request)


@login_required
def medicomatricula(request):
    texto_json = {'error': '404', 'informe_finalizado': False}
    matricula = request.GET.get('matricula', '')

    turnoid = request.GET.get('turnoid', '')
    if turnoid == '':
        return JsonResponse(texto_json)

    obj_turno = Turno.objects.get(id=turnoid)
    if obj_turno.informe_finalizado and obj_turno.usuario_medico:
        texto_json['informe_finalizado'] = True
        texto_json['matricula_actual'] = str(obj_turno.usuario_medico.userprofile.matricula)
        return JsonResponse(texto_json)

    if matricula == '':
        matricula_borrada = obj_turno.informe_medicomatricula
        obj_turno.informe_medicomatricula = ''
        medico_borrado = obj_turno.usuario_medico
        obj_turno.usuario_medico = None
        obj_turno.save()
        texto_json['limpiar_matricula'] = True

        paciente_nombre = obj_turno.paciente.nombre if obj_turno.paciente.nombre else ''
        paciente_apellido = obj_turno.paciente.apellido if obj_turno.paciente.apellido else ''
        ape_nomb = paciente_apellido + ', ' + paciente_nombre

        turno_fecha = obj_turno.fechahora_inicio.strftime('%d/%m/%y %H:%M')
        turno_servicio = obj_turno.servicio.nombre
        paciente_id = str(obj_turno.paciente.identificacion_numero)
        if obj_turno.urgencia:
            turno_atencion = '<strong class="text-red">Urgente</strong>'
        elif obj_turno.internado:
            turno_atencion = '<strong class="text-yellow">Internado</strong>'
        else:
            turno_atencion = '<strong>Programada</strong>'

        medico_iniciales = medico_borrado.userprofile.iniciales if medico_borrado else ''
        websocket_actualizarinforme(obj_turno.id, 'remover_medico', ape_nomb, turno_fecha, turno_atencion,
                                    paciente_id, turno_servicio, matricula_borrada, medico_iniciales)

    elif matricula != '':
        profile_medico = UserProfile.objects.filter(matricula=matricula).first()
        if profile_medico:
            obj_turno.informe_medicomatricula = matricula
            obj_turno.usuario_medico = profile_medico.user
            medico_iniciales = profile_medico.iniciales
            texto_json['medico_inciales'] = medico_iniciales
            paciente_nombre = obj_turno.paciente.nombre if obj_turno.paciente.nombre else ''
            paciente_apellido = obj_turno.paciente.apellido if obj_turno.paciente.apellido else ''
            ape_nomb = paciente_apellido + ', ' + paciente_nombre

            turno_fecha = obj_turno.fechahora_inicio.strftime('%d/%m/%y %H:%M')
            turno_servicio = obj_turno.servicio.nombre
            paciente_id = str(obj_turno.paciente.identificacion_numero)
            if obj_turno.urgencia:
                turno_atencion = '<strong class="text-red">Urgente</strong>'
            elif obj_turno.internado:
                turno_atencion = '<strong class="text-yellow">Internado</strong>'
            else:
                turno_atencion = '<strong>Programada</strong>'

            obj_turno.save()
            websocket_actualizarinforme(obj_turno.id, 'asigna_medico', ape_nomb, turno_fecha, turno_atencion,
                                        paciente_id, turno_servicio, matricula, medico_iniciales)

    texto_json['error'] = ''
    return JsonResponse(texto_json)


def exportar_informes(request):
    turnos = Turno.objects.filter(informe_finalizado=True, informe_entregado=False, fechahora_inicio__gt='2023-01-01').order_by('-id')

    for turno in turnos:
        try:
            informe = informe_medico_generar_pdf(turno)
            if informe:
                paciente_nombre = turno.paciente.nombre.upper() if turno.paciente.nombre else ''
                paciente_apellido = turno.paciente.apellido.upper() if turno.paciente.apellido else ''

                nombre_informe_pdf = 'media/informes_generados/' + paciente_nombre + ', ' + paciente_apellido + '_' + 'ORD-' + str(turno.id) + '_' + 'DNI-' + str(turno.paciente.identificacion_numero)+ '_' + 'INF-' +  str(turno.informe_numero)+ '.pdf'
                with open(nombre_informe_pdf, "wb") as outfile:
                    outfile.write(informe.getbuffer())

                turno.informe_entregado = True
                turno.save()
        except:
            continue

    return JsonResponse({'success': True})



@login_required
@transaction.atomic()
def grabaraudio(request):
    texto_json = {}
    texto_json['success'] = False
    audio_file = request.FILES.get('audio_data')
    turnoid = request.POST.get('turnoid', '')
    obj_turno = Turno.objects.get(id=turnoid)

    if obj_turno.informe_audio:
        audio_inicial = obj_turno.informe_audio
        sound1 = AudioSegment.from_mp3(audio_inicial)
        sound2 = AudioSegment.from_wav(audio_file)
        combined_sounds = sound1 + sound2
        path_audio = "audios/uid_" + str(obj_turno.id) + "/informe_audio_" + str(obj_turno.id) + ".mp3"
        combined_sounds.export("media/" + path_audio, format="mp3", bitrate="16k")
        obj_turno.informe_audio = path_audio
    else:
        sound = AudioSegment.from_wav(audio_file)
        path_audio_folder = "media/audios/uid_" + str(obj_turno.id) + "/"
        if not os.path.exists(path_audio_folder):
            os.makedirs(path_audio_folder)

        path_audio = "audios/uid_" + str(obj_turno.id) + "/informe_audio_" + str(obj_turno.id) + ".mp3"
        sound.export("media/" + path_audio, format="mp3", bitrate="16k")
        obj_turno.informe_audio = path_audio

    obj_turno.informe_fecha_finalizacion_a = datetime.now()
    obj_turno.save()
    if obj_turno.informe_audio:
        websocket_actualizarinforme(obj_turno.id, 'audio_finalizado')

    texto_json['success'] = True
    data_json = json.dumps(texto_json)
    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


class PreInformeCrear(NeverCacheMixin, UpdateView):
    model = Turno
    form_class = InformeAltaForm
    template_name = 'informe/informe_preinforme.html'
    success_url = reverse_lazy('informe:inf_listar')

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turnoid = self.kwargs['pk']
        turno = Turno.objects.get(pk=turnoid)
        form = InformeAltaForm(request.POST, instance=turno)

        if form.is_valid():
            if turno.servicio.is_servicio_externo:
                EstudioSolicitado.objects.filter(turno=turno).delete()
                selectcount = int(request.POST.get('selectcount', ''))
                estudiossollist = []
                for i in range(1, selectcount + 1):
                    selectname = 'serestudio_' + str(i)
                    estudioselect = request.POST.get(selectname, '')
                    if estudioselect != '':
                        obj_estudio_servicio = ServicioEstudio.objects.get(id=estudioselect)
                    else:
                        continue

                    obj_est = EstudioSolicitado(turno=turno, servicio=turno.servicio,
                                                servicio_estudio=obj_estudio_servicio,
                                                numero_protocolo=str(i))
                    estudiossollist.append(obj_estudio_servicio.codigo)
                    obj_est.save()

                string_estdios_sol = ''
                string_estdios_sol_codigos = ''
                estudio_prefix = turno.servicio.protocolo_prefix

                for estudi in estudiossollist:
                    if estudi != '':
                        string_estdios_sol += estudio_prefix + estudi + ' '
                        string_estdios_sol_codigos += 'cKw' + estudi + 'x '

                turno.estudios_solaux = string_estdios_sol
                turno.estudios_solaux_codigos = string_estdios_sol_codigos
            turno.save()
            form.save()

            return redirect(reverse('informe:inf_preinforme', kwargs={'pk': turno.id}) + "?tipo=1")
        else:
            return redirect(reverse('informe:inf_preinforme', kwargs={'pk': turno.id}) + "?tipo=1")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        turno = context['turno']
        tiposdeestudios = ServicioEstudio.objects.filter(servicio=turno.servicio)
        context['estudios'] = tiposdeestudios

        turno = self.get_object()
        turnoinsumo = TurnoInsumo.objects.filter(turno=turno)
        paciente = Paciente.objects.get(id=turno.paciente.id)
        hoy = datetime.now(tz=pytz.utc)
        edad = relativedelta(hoy.date(), paciente.fecnac)
        edadyear = edad.years
        edadmonth = edad.months
        edadday = edad.days
        if edadmonth < 10:
            edadmonth = '0' + str(edadmonth)
        if edadyear < 10:
            edadyear = '0' + str(edadyear)
        if edadday < 10:
            edadday = '0' + str(edadday)

        anestesias = ServicioAnestesia.objects.filter(servicio=turno.servicio)
        context['turnoinsumo'] = turnoinsumo
        context['paciente'] = paciente
        context['edadyear'] = edadyear
        context['edadmonth'] = edadmonth
        context['edadday'] = edadday
        context['anestesias'] = anestesias

        turno_en_partes = False
        turno_padre = False
        if turno.tipomodalidad == 'En partes':
            turno_padre = True
            turno_en_partes = True

        if turno.turno_relacionado:
            turno_en_partes = True
        turnos_hijos = {}
        if turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(Q(turno_relacionado=turno) | Q(id=turno.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")
        elif not turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(
                Q(turno_relacionado=turno.turno_relacionado) | Q(id=turno.turno_relacionado.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")
        else:
            turnos_hijos = Turno.objects.filter(id=turno.id)
        configuracion_general = ConfiguracionGeneral.objects.all().first()
        if configuracion_general.is_db_production:
            if turnos_hijos and turno.servicio.tipo == 'MR':
                generar_link_turnos_hijos(turnos_hijos)
            else:
                generar_link_turno_normal(turno)

        context['turno_padre'] = turno_padre
        context['turnos_hijos'] = turnos_hijos
        context['turno_en_partes'] = turno_en_partes
        return context


def informe_medico_generar_pdf(obj_turno, estipeo='0'):

    buffer = io.BytesIO()
    h = A4[1]
    w = A4[0]
    ps = (w, h)
    c = NumberedPageCanvas(buffer, pagesize=ps)
    # convert statics field save to list
    if obj_turno.informe_datos_finalizado:
        informe_datos_finalizado = ast.literal_eval(str(obj_turno.informe_datos_finalizado))
        if len(informe_datos_finalizado) != 13:
            informe_datos_finalizado = []
    else:
        informe_datos_finalizado = []

    # paciente
    if informe_datos_finalizado:
        paciente_nn_al_finalizar_informe = informe_datos_finalizado[4] == 'True'
        if paciente_nn_al_finalizar_informe:
            paciente = informe_datos_finalizado[5]
        else:
            pac_apellido = informe_datos_finalizado[2]
            pac_nombre = informe_datos_finalizado[3]
            paciente = pac_apellido + ', ' + pac_nombre
    else:
        if obj_turno.paciente.paciente_nn:
            paciente = "NN - " + obj_turno.paciente.paciente_nnlabel
        else:
            pac_apellido = obj_turno.paciente.apellido.upper() if obj_turno.paciente.apellido else ''
            pac_nombre = obj_turno.paciente.nombre.upper() if obj_turno.paciente.nombre else ''
            paciente = pac_apellido + ', ' + pac_nombre

    # servicio label
    servicio_label_tipo = ' '
    if obj_turno.servicio.tipo == 'CT':
        servicio_label_tipo = 'TOMOGRAFÍA COMPUTADA'
    elif obj_turno.servicio.tipo == 'MR':
        servicio_label_tipo = 'RESONANCIA NUCLEAR MAGNÉTICA'
    elif obj_turno.servicio.tipo == 'CR':
        servicio_label_tipo = 'MAMOGRAFÍA'
    c.setFont('Helvetica-Bold', 22)
    c.drawCentredString(w / 2.0, h - 620, servicio_label_tipo)

    # fecha
    months_dict = {'January': 'Enero',
                   'February': 'Febrero',
                   'March': 'Marzo',
                   'April': 'Abril',
                   'May': 'Mayo',
                   'June': 'Junio',
                   'July': 'Julio',
                   'August': 'Agosto',
                   'September': 'Septiembre',
                   'October': 'Octubre',
                   'November': 'Noviembre',
                   'December': 'Diciembre'}

    turno_date_coloquial = obj_turno.fechahora_inicio.strftime("%d de ") \
                           + months_dict[obj_turno.fechahora_inicio.strftime("%B")] \
                           + ' ' \
                           + obj_turno.fechahora_inicio.strftime("del %Y")

    c.setFont('Helvetica', 12)
    c.drawCentredString(w / 2.0, h - 655, turno_date_coloquial)

    # insert first page
    logofirst = 'informe/img/imageninforme1.png'
    im = Image(logofirst, 595, 840)
    im.drawOn(c, 0, 0)

    c.setFont('Helvetica', 18)
    c.drawCentredString(w / 2.0, h - 585, paciente)
    c.showPage()

    # other pages
    logo = 'informe/img/imageninforme2.png'
    im = Image(logo, 595, 840)
    im.drawOn(c, 0, 0)

    # paciente DNI
    if informe_datos_finalizado:
        paciente_nn_al_finalizar_informe = informe_datos_finalizado[4] == 'True'
        if paciente_nn_al_finalizar_informe:
            pac_dni1 = informe_datos_finalizado[4]
            pac_dni2 = ""
        else:
            pac_dni1 = informe_datos_finalizado[0] + ": "
            pac_dni2 = informe_datos_finalizado[1]
    else:
        if obj_turno.paciente.paciente_nn:
            pac_dni1 = obj_turno.paciente.identificacion_tipo + ": "
            pac_dni2 = "---"
        else:
            pac_dni1 = obj_turno.paciente.identificacion_tipo + ": "
            pac_dni2 = obj_turno.paciente.identificacion_numero

    dni = pac_dni1 + pac_dni2
    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 165, pac_dni1)
    c.setFont('Helvetica', 11)
    c.drawString(50, h - 165, pac_dni2)

    # Protocoo label
    protocolo = ""
    estudios_solicitados = EstudioSolicitado.objects.filter(turno=obj_turno)
    aux_counter = 0
    for est in estudios_solicitados:
        if est.servicio_estudio.codigo == '99':
            continue
        aux_counter += 1
        protocolo_prefix = obj_turno.estudio_protocoloprefix
        protocolo_counter = aux_counter
        protocolo_sufix = int(protocolo_counter) + int(obj_turno.estudio_protocolocount)
        nro_protocolo = protocolo_prefix + str(protocolo_sufix).zfill(4)

        if estudios_solicitados.last() == est:
            protocolo += nro_protocolo
        else:
            protocolo += nro_protocolo + ' - '
    if obj_turno.tipomodalidad == "En partes":
        turnos_hijos = Turno.objects.filter(turno_relacionado=obj_turno, estudio_terminado=True, habilitado=True)

        for t_hijo in turnos_hijos:
            estudios_solicitados_h = EstudioSolicitado.objects.filter(turno=t_hijo)
            aux_counter = 0
            for est in estudios_solicitados_h:
                aux_counter += 1
                protocolo_prefix = t_hijo.estudio_protocoloprefix
                protocolo_counter = aux_counter
                protocolo_sufix = int(protocolo_counter) + int(t_hijo.estudio_protocolocount)
                nro_protocolo = protocolo_prefix + str(protocolo_sufix).zfill(4)
                if estudios_solicitados_h.first() == est:
                    protocolo += ' - ' + nro_protocolo + ' - '
                elif estudios_solicitados_h.last() == est:
                    protocolo += nro_protocolo
                else:
                    protocolo += ' - ' + nro_protocolo + ' - '

    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 195, 'Protocolo Nº: ')
    c.setFont('Helvetica', 11)
    counter_label = ' (+' + str(aux_counter) + ')' if aux_counter > 1 else ''
    c.drawString(100, h - 195, protocolo[:11] + counter_label)

    # Informe label
    num_inf = obj_turno.informe_numero
    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 140, 'INFORME Nº: ')
    c.setFont('Helvetica', 11)
    c.drawString(100, h - 140, num_inf)

    # Paciente Nombres
    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 125, 'PACIENTE: ')
    c.setFont('Helvetica', 11)
    c.drawString(90, h - 125, paciente)

    # Paciente Fecha Nacimiento
    if informe_datos_finalizado:
        fecha_nac_paciente = informe_datos_finalizado[7]
    else:
        fecha_nac_paciente = obj_turno.paciente.fecnac.strftime('%d-%m-%Y') if obj_turno.paciente.fecnac else '---'

    c.setFont('Helvetica-Bold', 11)
    c.drawString(180, h - 165, 'Fecha Nac: ')
    c.setFont('Helvetica', 11)
    c.drawString(240, h - 165, fecha_nac_paciente)

    # Paciente Sexo
    if informe_datos_finalizado:
        sexo = informe_datos_finalizado[6]
    else:
        sexo = obj_turno.paciente.sexo

    c.setFont('Helvetica-Bold', 11)
    c.drawString(390, h - 165, 'Sexo: ')
    c.setFont('Helvetica', 11)
    c.drawString(422, h - 165, sexo)

    # Fecha y Hora del estudio
    fecha = obj_turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%d-%m-%Y')
    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 180, 'Fecha del Estudio: ')
    c.setFont('Helvetica', 11)
    c.drawString(128, h - 180, fecha)

    hora = obj_turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%H:%M')
    c.setFont('Helvetica-Bold', 11)
    c.drawString(240, h - 180, 'Hora: ')
    c.setFont('Helvetica', 11)
    c.drawString(270, h - 180, hora)

    # nro de orden
    nro_orden = str(obj_turno.id)
    c.setFont('Helvetica-Bold', 11)
    c.drawString(390, h - 180, 'Nº Orden: ')
    c.setFont('Helvetica', 11)
    c.drawString(445, h - 180, nro_orden)

    # Modo de acceso
    if obj_turno.internado:
        if obj_turno.urgencia:
            estado = 'Urgencia'
        else:
            estado = 'Internado'
    else:
        estado = 'Ambulatorio'

    c.setFont('Helvetica-Bold', 11)
    c.drawString(240, h - 195, 'Mod. Acceso: ')
    c.setFont('Helvetica', 11)
    c.drawString(315, h - 195, estado)

    # Establecimiento
    if informe_datos_finalizado:
        establ = informe_datos_finalizado[11]
    else:
        establ = obj_turno.establecimiento_derivador.nombre

    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 235, 'Establecimiento: ')
    c.setFont('Helvetica', 11)
    c.drawString(120, h - 235, establ)

    # Medico Derivante
    if informe_datos_finalizado:
        medico_sol = informe_datos_finalizado[10] if informe_datos_finalizado[10] != 'Médico sin definir' else '---'
    else:
        if obj_turno.medico_derivante and obj_turno.medico_derivante.matricula != '1':
            medico_sol = obj_turno.medico_derivante.apellido
        else:
            medico_sol = '---'

    c.setFont('Helvetica-Bold', 11)
    c.drawString(28, h - 220, 'MÉDICO SOLICITANTE: ')
    c.setFont('Helvetica', 11)
    c.drawString(158, h - 220, medico_sol)

    c.setFont('Helvetica-Bold', 11)
    codigos = "CÓDIGOS: "
    c.drawString(28, h - 250, codigos)
    c.setFont('Helvetica', 11)
    linejump_codigos = 0
    linehorizontal_codigos = 0
    codigo_line_start = 87

    if informe_datos_finalizado and informe_datos_finalizado[12]:
        estudios_solicitados = informe_datos_finalizado[12].replace('Códigos de estudios: \n', '')
        estudios_solicitados = estudios_solicitados.split('\n')
        if not estudios_solicitados[-1]:
            estudios_solicitados.pop()

        for est in estudios_solicitados:
            if linejump_codigos >= 55:
                linehorizontal_codigos += 170
                linejump_codigos = 0
            c.drawString(codigo_line_start + linehorizontal_codigos, h - 250 - linejump_codigos, est)
            linejump_codigos += 11
    else:
        for est in estudios_solicitados:
            if linejump_codigos >= 55:
                linehorizontal_codigos += 170
                linejump_codigos = 0
            codigo_sufix = est.servicio_estudio.codigo.zfill(2)
            codigo_sol = obj_turno.servicio.protocolo_prefix + codigo_sufix + ' - ' + est.servicio_estudio.nombre
            c.drawString(codigo_line_start + linehorizontal_codigos, h - 250 - linejump_codigos, codigo_sol)
            linejump_codigos += 11

        if obj_turno.tipomodalidad == "En partes":
            turnos_hijos = Turno.objects.filter(turno_relacionado=obj_turno, estudio_terminado=True, habilitado=True)
            for t_hijo in turnos_hijos:
                estudios_solicitados_h = EstudioSolicitado.objects.filter(turno=t_hijo)

                for est in estudios_solicitados_h:
                    if linejump_codigos >= 55:
                        linehorizontal_codigos += 170
                        linejump_codigos = 0
                    codigo_sufix = est.servicio_estudio.codigo.zfill(2)
                    codigo_sol = t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - ' + est.servicio_estudio.nombre
                    c.drawString(codigo_line_start + linehorizontal_codigos, h - 250 - linejump_codigos, codigo_sol)
                    linejump_codigos += 11

        if obj_turno.anestesia:
            obj_servicio_anestesia = ServicioAnestesia.objects.filter(codigo=obj_turno.anestesia_codigo).first()
            if obj_servicio_anestesia:
                if linejump_codigos >= 55:
                    linehorizontal_codigos += 170
                    linejump_codigos = 0
                anestesia_description = obj_servicio_anestesia.codigo + ' - ' + obj_servicio_anestesia.nombre
                c.drawString(codigo_line_start + linehorizontal_codigos, h - 250 - linejump_codigos, anestesia_description.title())
                linejump_codigos += 11

        if obj_turno.tipomodalidad == "En partes":
            turnos_hijos = Turno.objects.filter(turno_relacionado=obj_turno, estudio_terminado=True, habilitado=True)
            for t_hijo in turnos_hijos:
                if obj_turno.anestesia:
                    obj_servicio_anestesia = ServicioAnestesia.objects.filter(codigo=t_hijo.anestesia_codigo).first()
                    if obj_servicio_anestesia:
                        if linejump_codigos >= 55:
                            linehorizontal_codigos += 170
                            linejump_codigos = 0
                        anestesia_description = obj_servicio_anestesia.codigo + ' - ' + obj_servicio_anestesia.nombre
                        c.drawString(codigo_line_start + linehorizontal_codigos, h - 250 - linejump_codigos, anestesia_description.title())
                        linejump_codigos += 11

    linejump_codigos = 0
    c.line(28,  h - 300, 565,  h - 300)

    c.setFont('Helvetica', 11)
    if obj_turno.informe_texto:
        textoinfhtml = obj_turno.informe_texto
    else:
        textoinfhtml = ""

    h1 = html2text.HTML2Text()
    h1.body_width = 90
    h1.strong_mark = ''
    h1.emphasis_mark = ''
    h1.unicode_snob = True
    textoinfo = h1.handle(textoinfhtml)
    arraylines_raw = textoinfo.splitlines()

    final_array = []
    for line in arraylines_raw:
        if len(line) > 90:
            currentline_wraped = textwrap.wrap(line, 90)
            final_array += currentline_wraped
        else:
            final_array.append(line)
    arraylines = final_array
    textopageone = ''
    if obj_turno.usuario_medico.userprofile.foto_firma:
        firma_foto_path = 'media/fotosfirmas/uid_' + str(obj_turno.usuario_medico.id) + '/firma.png'
    medico_informante_2 = obj_turno.usuario_medico.userprofile.sexo_prefix + ' ' + obj_turno.usuario_medico.last_name + \
                          ', ' + obj_turno.usuario_medico.first_name + ' - MP ' + obj_turno.usuario_medico.userprofile.matricula
    medico_informante_1 = 'Esp. Diagnóstico por Imágenes'
    texto_firma_electronica = 'El presente informe medico a sido firmado por el profesional responsable mediante firma electrónica.'
    x_texto_firma_electronica = 189
    y_texto_firma_electronica = 70

    if len(arraylines) > 30:
        # primera paginA si hay mas de una
        for i in range(0, 30):
            textopageone = textopageone + "\n" + arraylines[i]
        t = c.beginText(28, 525 - linejump_codigos)
        t.setLeading(12)
        t.textLines(textopageone.replace("\\-", "-"))
        c.drawText(t)
        c.setFont('Helvetica-Bold', 10)
        c.drawString(350, 90, medico_informante_1)
        c.drawString(350, 100, medico_informante_2)
        # texto firma ELECTRONICA
        c.setFont('Helvetica', 8)
        c.drawString(x_texto_firma_electronica, y_texto_firma_electronica, texto_firma_electronica)

        c.showPage()
        textoall = ''
        linecount = 0

        # paginas medio
        for i in range(30, len(arraylines)):
            linecount += 1
            textoall = textoall + "\n" + arraylines[i]
            if linecount % 41 == 0:
                im = Image(logo, 595, 840)
                im.drawOn(c, 0, 0)
                c.setFont('Helvetica-Bold', 11)
                c.drawString(28, A4[1] - 128, 'INFORME Nº: ' + num_inf)
                fecha = "Fecha del Estudio: " + obj_turno.fechahora_inicio.strftime('%d-%m-%Y')
                c.drawString(28, A4[1] - 145, fecha)
                fecha = "Hora: " + obj_turno.fechahora_inicio.strftime('%H:%M')
                c.drawString(250, A4[1] - 145, fecha)
                c.drawString(450, A4[1] - 162, dni)
                c.drawString(28, A4[1] - 162, 'Paciente: ' + paciente)

                c.line(25, A4[1] - 115, 565, A4[1] - 115)
                c.line(25, A4[1] - 165, 565, A4[1] - 165)

                c.line(25, A4[1] - 115, 25, A4[1] - 165)
                c.line(565, A4[1] - 115, 565, A4[1] - 165)

                # texto informe
                c.setFont('Helvetica', 11)
                t = c.beginText(28, A4[1] - 190)
                t.setLeading(12)
                t.textLines(textoall.replace("\\-", "-"))
                c.drawText(t)
                c.setFont('Helvetica-Bold', 10)
                c.drawString(350, 90, medico_informante_1)
                c.drawString(350, 100, medico_informante_2)

                # texto firma ELECTRONICA
                c.setFont('Helvetica', 8)
                c.drawString(x_texto_firma_electronica, y_texto_firma_electronica, texto_firma_electronica)

                c.showPage()
                textoall = ''
        textopagefin = ''
        resto = (len(arraylines) - 30) % 41
        linecontlast_init = len(arraylines) - resto

        # ultima pagina mas de una
        for i in range(0, resto):
            textopagefin = textopagefin + "\n" + arraylines[linecontlast_init]
            linecontlast_init += 1
        im = Image(logo, 595, 840)
        im.drawOn(c, 0, 0)
        c.setFont('Helvetica-Bold', 11)

        c.drawString(28, A4[1] - 128, 'INFORME Nº: ' + num_inf)

        fecha = "Fecha del Estudio: " + obj_turno.fechahora_inicio.strftime('%d-%m-%Y')
        c.drawString(28, A4[1] - 145, fecha)
        fecha = "Hora: " + obj_turno.fechahora_inicio.strftime('%H:%M')
        c.drawString(250, A4[1] - 145, fecha)
        c.drawString(450, A4[1] - 162, dni)
        c.drawString(28, A4[1] - 162, 'Paciente: ' + paciente)

        c.line(25, A4[1] - 115, 565, A4[1] - 115)
        c.line(25, A4[1] - 165, 565, A4[1] - 165)

        c.line(25, A4[1] - 115, 25, A4[1] - 165)
        c.line(565, A4[1] - 115, 565, A4[1] - 165)

        # texto informe
        c.setFont('Helvetica', 11)
        t3 = c.beginText(28, A4[1] - 190)
        t3.setLeading(12)
        t3.textLines(textopagefin.replace("\\-", "-"))
        c.drawText(t3)
        c.setFont('Helvetica-Bold', 10)

        line_final = h - 12 * cm - resto * 12
        if line_final < 73:
            line_final = 73
        img_desfasage = 0  # antes 135
        if obj_turno.usuario_medico.userprofile.foto_firma:
            im = Image(firma_foto_path, 200, 150)
            im.drawOn(c, 350, line_final)
            img_desfasage = 0
        texto_firma_nombre = obj_turno.usuario_medico.userprofile.sexo_prefix + ' ' + obj_turno.usuario_medico.last_name + \
                             ', ' + obj_turno.usuario_medico.first_name + ' - MP ' + obj_turno.usuario_medico.userprofile.matricula
        c.drawString(350, line_final + img_desfasage, texto_firma_nombre)
        c.drawString(350, line_final - 12 + img_desfasage, medico_informante_1)
        # texto firma ELECTRONICA
        c.setFont('Helvetica', 8)
        c.drawString(x_texto_firma_electronica, y_texto_firma_electronica, texto_firma_electronica)
        c.showPage()

    else:
        # Una sola pagina
        textopageone = ''
        for i in range(0, len(arraylines)):
            textopageone = textopageone + "\n" + arraylines[i]

        t = c.beginText(28, 525 - linejump_codigos)
        t.setLeading(12)
        t.textLines(textopageone.replace("\\-", "-"))
        c.drawText(t)
        c.setFont('Helvetica-Bold', 10)
        line_final = 375 - linejump_codigos - len(arraylines) * 12
        if line_final < 73:
            line_final = 73
        img_desfasage = 17
        if obj_turno.usuario_medico.userprofile.foto_firma:
            im = Image(firma_foto_path, 200, 150)
            im.drawOn(c, 350, line_final)

        texto_firma_nombre = obj_turno.usuario_medico.userprofile.sexo_prefix + ' ' + obj_turno.usuario_medico.last_name + \
                             ', ' + obj_turno.usuario_medico.first_name + ' - MP ' + obj_turno.usuario_medico.userprofile.matricula
        c.drawString(350, line_final + img_desfasage, texto_firma_nombre)
        c.drawString(350, line_final + img_desfasage - 12, medico_informante_1)

        # texto firma ELECTRONICA
        c.setFont('Helvetica', 8)
        c.drawString(x_texto_firma_electronica, y_texto_firma_electronica, texto_firma_electronica)

        c.showPage()
    c.save()
    buffer.seek(0)
    return buffer


@login_required
def informe_medico_pdf(request):
    turnoid = request.GET.get('turnoid', '')
    obj_turno = Turno.objects.get(id=turnoid)
    estipeo = str(request.GET.get('tipeo', ''))
    buffer = informe_medico_generar_pdf(obj_turno, estipeo)
    nombre_informe_pdf = str(obj_turno.id) + '_' + str(obj_turno.paciente.identificacion_numero) + '_' \
                         + obj_turno.paciente.nombrecompleto.title().replace(" ", "") + '_.pdf'
    openpdfwindow = request.GET.get('openpdf', '')
    if openpdfwindow == '1':
        return FileResponse(buffer, filename=nombre_informe_pdf)
    else:
        return FileResponse(buffer, as_attachment=True, filename=nombre_informe_pdf)


def generar_numero_informe(turno):
    nro_informe_serv_prefix = turno.servicio.nro_informe_prefix
    nro_informe_serv_count = str(turno.servicio.nro_informe_count + 1)
    nro_informe = nro_informe_serv_prefix + nro_informe_serv_count
    if len(nro_informe) > 8:
        len_diff = (len(nro_informe) - 8) * -1
        nro_informe = nro_informe_serv_prefix[:len_diff] + nro_informe_serv_count
    elif len(nro_informe) < 8:
        len_diff = ((len(nro_informe) - 8) * - 1) + len(nro_informe_serv_count)
        nro_informe = nro_informe_serv_prefix + nro_informe_serv_count.zfill(len_diff)
    return nro_informe


class InformeSubirFirmado(UpdateView):
    model = Turno
    form_class = InformeSubirFirmadoForm
    template_name = 'informe_medico/informe_firmar.html'

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turnoid = self.kwargs['pk']
        turno = Turno.objects.get(pk=turnoid)
        form = InformeSubirFirmadoForm(request.POST, request.FILES, instance=turno)

        if form.is_valid():
            if turno.usuario_medico:
                form.save()
                if turno.informe_firmadopdf:
                    turno.informe_finalizado = True
                else:
                    turno.informe_finalizado = False

                if turno.informe_numero == 'No asignado':
                    turno.informe_numero = generar_numero_informe(turno)
                    servicio = turno.servicio
                    servicio.nro_informe_count = turno.servicio.nro_informe_count + 1
                    servicio.save()

                turno.save()
            else:
                messages.error(request,
                               "Error: No se puede agregar un Informe Externo, sin definir un médico informante.")

        else:
            messages.error(request, "Tipo de archivo invalido.")

        return redirect(reverse('informe:inf_preinforme', kwargs={'pk': turnoid}) + '?tipo=1')


@login_required
def corregirpaciente(request):
    try:
        obs_corregir = request.GET.get('observacion', '')
        corregir = request.GET.get('corregir', '')
        paciente = request.GET.get('paciente', '')
        pac = Paciente.objects.get(id=paciente)
        pac.corregir = True
        pac.corregir_texto = obs_corregir
        pac.save()
        response_data = {'corregirpaciente_exito': True}
        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'corregirpaciente_exito': False}
        return JsonResponse(response_data)


@login_required
def traer_texto_corregir(request):
    try:
        paciente = request.GET.get('paciente', '')
        pac = Paciente.objects.get(id=paciente)
        response_data = {'texto_corregir': pac.corregir_texto}
        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'corregirtexto_exito': False}
        return JsonResponse(response_data)


@login_required
def traer_texto_corregir_estudio(request):
    try:
        turno = request.GET.get('turno', '')
        tur = Turno.objects.get(id=turno)
        response_data = {'texto_corregir_estudio': tur.corregir_texto}
        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'corregirtexto_exito': False}
        return JsonResponse(response_data)


@login_required
def corregirestudio(request):
    try:
        obs_corregir = request.GET.get('observacion', '')
        corregir = request.GET.get('corregir', '')
        turno = request.GET.get('turno', '')
        tur = Turno.objects.get(id=turno)
        tur.estudio_corregir = True
        tur.estudio_corregir_texto = obs_corregir
        tur.save()
        response_data = {'corregirestudio_exito': True}
        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'corregiresstudio_exito': False}
        return JsonResponse(response_data)


def websocket_actualizarinforme(turnoid, tipo_actualizacion, ape_nombre='',
                                fecha='', atencion='', id_numero='', servicio='', medico_matricula='',
                                medico_iniciales=''):
    tipeo_finalizado_tipo = ''
    if tipo_actualizacion == 'tipeo_finalizado':
        turno = Turno.objects.get(id=turnoid)
        tipeo_finalizado_tipo = 'audio' if turno.informe_audio else 'texto'
    layer = get_channel_layer()
    async_to_sync(layer.group_send)('chat_informes',
                                    {
                                        'type': 'mensaje_turno',
                                        'turno_id': turnoid,
                                        'tipo_actualizacion': tipo_actualizacion,
                                        'apellido_nombre': ape_nombre,
                                        'fecha': fecha,
                                        'identificacion_numero': id_numero,
                                        'servicio': servicio,
                                        'atencion': atencion,
                                        'medico_matricula': medico_matricula,
                                        'medico_iniciales': medico_iniciales,
                                        'tipeo_finalizado_tipo': tipeo_finalizado_tipo
                                    })
    return True


@login_required
@transaction.atomic()
def finalizar_sinfirma(request):
    try:
        turnoid = request.GET.get('turnoid', '')
        turno = Turno.objects.get(id=turnoid)

        if turno.informe_finalizado:
            turno.informe_no_firmadigital = True
            turno.save()
            msg = 'El Informe ya no aparecera en pendientes.'
            estado = True
        else:
            msg = 'Se requiere que el informe este finalizado por el médico.'
            estado = False

        response_data = {
            'estado': estado,
            'msg': msg,
            'success': True}

        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'success': False}
        return JsonResponse(response_data)


@login_required
def estadisticas_excel(request):
    fecha_ini = request.GET.get('fechaini', '')
    fecha_fin = request.GET.get('fechafin', '')
    if fecha_ini != '' and fecha_fin != '':
        fecha_ini = datetime.strptime(fecha_ini + ' 00:00:00', '%d-%m-%Y %H:%M:%S')
        fecha_fin = datetime.strptime(fecha_fin + ' 23:59:00', '%d-%m-%Y %H:%M:%S')

    servicios_codigos = []
    if 'resonadorboedo' in request.GET:
        servicios_codigos.append(2)
    if 'tomografoboedo' in request.GET:
        servicios_codigos.append(1)
    if 'oran' in request.GET:
        servicios_codigos.append(4)
    if 'materno' in request.GET:
        servicios_codigos.append(3)
    if 'tartagal' in request.GET:
        servicios_codigos.append(5)
    if 'embarcacion' in request.GET:
        servicios_codigos.append(6)
    if 'rosariof' in request.GET:
        servicios_codigos.append(8)
    if 'cafayate' in request.GET:
        servicios_codigos.append(7)
    if 'rosariofm' in request.GET:
        servicios_codigos.append(12)
    if 'embarcacionm' in request.GET:
        servicios_codigos.append(11)
    if 'tomografoexterno' in request.GET:
        servicios_codigos.append(9)
    if 'resonadorexterno' in request.GET:
        servicios_codigos.append(10)

    sevicios_selected = Servicio.objects.filter(codigo__in=servicios_codigos)
    result = []

    if sevicios_selected:
        for servicio in sevicios_selected:
            turnos_entre_fechas = Turno.objects. \
                filter(servicio=servicio). \
                filter(fechahora_inicio__gte=fecha_ini). \
                filter(fechahora_inicio__lte=fecha_fin)

            turnos_estadistica = turnos_entre_fechas.filter(
                Q(estudio_finalizado=True) |
                Q(estudio_cancelado=True) |
                Q(estudio_reprograma=True) |
                Q(estudio_suspendido_completar=True) |
                Q(estudio_suspendido_paciente=True)) \
                .exclude(habilitado=False) \
                .exclude(estudio_finalizado_sistema=True)\
                .exclude(turno_recepcion_suspendido=True)

            turnos_estadistica_order = turnos_estadistica.order_by('-fechahora_inicio')
            for t in turnos_estadistica_order:
                count = 1
                estudiossol = EstudioSolicitado.objects.filter(turno=t)
                for est in estudiossol:
                    turno_hora_guardia = t.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).time()
                    is_time_guadia = t.servicio.hora_guardiafin >= turno_hora_guardia or turno_hora_guardia >= t.servicio.hora_guardiaini
                    turno_fecha_guardia = t.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).date()
                    is_weekend = turno_fecha_guardia.weekday() >= 5
                    hollidays = [feriado.fecha for feriado in Feriados.objects.all()]
                    is_holliday = turno_fecha_guardia in hollidays
                    if is_holliday:
                        tipoturno = 'Guardia'
                        tipoguardia = 'Feriado'
                    elif is_weekend:
                        tipoturno = 'Guardia'
                        tipoguardia = 'Fin de Semana'
                    elif is_time_guadia:
                        tipoturno = 'Guardia'
                        tipoguardia = 'Horario Guardia'
                    else:
                        tipoturno = 'Normal'
                        tipoguardia = '---'
                    if t.servicio.tipo == 'MR' or t.servicio.tipo == 'CR' or not t.urgencia:
                        tipoturno = 'Normal'
                        tipoguardia = '---'

                    if t.turno_relacionado:
                        if t.estudio_suspendido_completar or t.estudio_suspendido_paciente or t.estudio_cancelado or t.estudio_reprograma:
                            medico = '---'
                        else:
                            if t.turno_relacionado.usuario_medico and not t.turno_relacionado.informe_cerrado:
                                medico = t.turno_relacionado.usuario_medico.last_name + ', ' + t.turno_relacionado.usuario_medico.first_name
                            else:
                                medico = '---'
                        nroinforme = t.turno_relacionado.informe_numero
                    else:
                        nroinforme = t.informe_numero
                        if t.usuario_medico and not t.informe_cerrado:
                            medico = t.usuario_medico.last_name + ', ' + t.usuario_medico.first_name
                        else:
                            medico = '---'
                    if t.medico_derivante.nombre:
                        medicoder = t.medico_derivante.apellido + ' ' + t.medico_derivante.nombre
                    else:
                        medicoder = t.medico_derivante.apellido

                    if t.servicio.is_servicio_externo:
                        if t.informe_finalizado:
                            estado = 'Finalizado'
                        elif t.informe_cerrado:
                            estado = 'Finalizado sin Informe'
                        else:
                            estado = 'Tipeo'

                    elif t.estudio_finalizado:
                        if t.turno_relacionado:
                            if t.turno_relacionado.informe_finalizado:
                                estado = 'Finalizado'
                            elif t.informe_cerrado:
                                estado = 'Finalizado sin Informe'
                            else:
                                if t.estudio_finalizado:
                                    estado = 'Tipeo'
                                elif t.estudio_cancelado or t.estudio_suspendido_completar or t.estudio_suspendido_completar:
                                    estado = 'Suspendido'
                                elif t.estudio_reprograma:
                                    estado = 'Reprogramado'
                                else:
                                    estado = 'No realizado'
                        else:
                            if t.informe_finalizado:
                                estado = 'Finalizado'
                            elif t.informe_cerrado:
                                estado = 'Finalizado sin Informe'
                            else:
                                estado = 'En Proceso'
                    else:
                        if t.servicio.is_servicio_externo:
                            estado = '-'
                        elif t.estudio_cancelado or t.estudio_suspendido_completar or t.estudio_suspendido_completar:
                            estado = 'Suspendido'
                        elif t.estudio_reprograma:
                            estado = 'Reprogramado'
                        else:
                            estado = 'No realizado'
                    pacnombre = ''
                    if t.paciente.apellido:
                        pacnombre += t.paciente.apellido + ', '
                    if t.paciente.nombre:
                        pacnombre += t.paciente.nombre

                    if est.servicio_estudio.codigo == '99':
                        count -= 1
                        protocolo = nro_protocolo = '---'
                    elif not est.servicio.is_servicio_externo:
                        protocolo = t.estudio_protocolocount + count
                        nro_protocolo = t.estudio_protocoloprefix + str(protocolo).zfill(4)
                    else:
                        protocolo = '-'
                        nro_protocolo = '-'

                    if t.informe_facturado and t.informe_facturado_fecha:
                        facturacion_fecha = t.informe_facturado_fecha.strftime('%d/%m/%Y')
                    else:
                        facturacion_fecha = '---'

                    tecnico =  t.usuario_tecnico.last_name + ', ' + t.usuario_tecnico.first_name if t.usuario_tecnico else '-'

                    if t.estudio_fechahora_ingreso:
                        estudio_fec_ing = t.estudio_fechahora_ingreso.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%d/%m/%Y')
                        estudio_hora_ing = t.estudio_fechahora_ingreso.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%H:%M')
                    else:
                        estudio_fec_ing = '-'
                        estudio_hora_ing = '-'

                    current_dict = {}
                    current_dict['count'] = count
                    current_dict['turnoid'] = t.id
                    current_dict['serviciocod'] = t.servicio.codigo_sistema_ant
                    current_dict['servicio'] = t.servicio.nombre
                    current_dict['nroinforme'] = nroinforme
                    current_dict['fecha'] = t.fechahora_inicio.astimezone(
                        pytz.timezone("America/Argentina/Salta")).strftime('%d/%m/%Y')
                    current_dict['hora'] = t.fechahora_inicio.astimezone(
                        pytz.timezone("America/Argentina/Salta")).strftime('%H:%M')
                    dni = t.paciente.identificacion_numero if not t.paciente.paciente_nn else '---'
                    current_dict['dni'] = dni
                    pacientenomb = pacnombre if not t.paciente.paciente_nn else t.paciente.paciente_nnlabel
                    current_dict['paciente'] = pacientenomb
                    current_dict['protocolo'] = protocolo
                    current_dict['nroprotocolo'] = nro_protocolo
                    current_dict['prestacion'] = est.servicio.protocolo_prefix + str(est.servicio_estudio.codigo).zfill(
                        2)
                    current_dict['obrasocial'] = t.obrasocial.nombre
                    current_dict['estder'] = t.establecimiento_derivador.nombre
                    current_dict['medicoder'] = medicoder
                    current_dict['informante'] = medico
                    current_dict['tecnico'] = tecnico
                    current_dict['tecnico_matricula'] = t.usuario_tecnico.userprofile.matricula if t.usuario_tecnico else '---'
                    current_dict['fecha2'] =estudio_fec_ing
                    current_dict['hora2'] = estudio_hora_ing
                    current_dict['tipoturno'] = tipoturno
                    current_dict['estado'] = estado
                    current_dict['contraste'] = 'Si' if t.contrasteev else 'No'
                    current_dict['anestesia'] = 'Si' if t.anestesia else 'No'
                    current_dict['email'] = t.email_entregadigital if t.email_entregadigital else '---'
                    current_dict['contraste_oral'] = 'Si' if t.contrastevo else 'No'
                    current_dict['anestesia_codigo'] = t.anestesia_codigo if t.anestesia_codigo else '---'
                    current_dict['tipo_guardia'] = tipoguardia
                    current_dict['facturacion_fecha'] = facturacion_fecha
                    current_dict['cantidad'] = est.cantidad
                    if t.usuario_tipeo:
                        current_dict['tipeo'] = t.usuario_tipeo.last_name + ', ' + t.usuario_tipeo.first_name
                    else:
                        current_dict['tipeo'] = '---'
                    result.append(current_dict)
                    count += 1

    return render(request, 'informe_estadisticas/estadisticas_temporal.html', {'turnos': json.dumps(result)})


def imprimir_etiqueta(request):
    turnoid = request.GET.get('turnoid', '')
    if turnoid != '':
        turno = Turno.objects.get(id=turnoid)
        apellido_paciente = turno.paciente.apellido.upper()
        nombre_paciente = ''
        if turno.paciente.nombre:
            nombre_paciente = turno.paciente.nombre.upper()
        paciente_nombrecompleto = apellido_paciente + ' ' + nombre_paciente
        fecha = 'FECHA: ' + turno.fechahora_inicio.strftime('%d/%m/%Y')
        nroinforme = 'Nº INFORME: ' + turno.informe_numero
        idpaciente = turno.paciente.identificacion_tipo + ': ' + turno.paciente.identificacion_numero
        buffer = io.BytesIO()
        pagesize = (10 * cm, 8 * cm)
        c = canvas.Canvas(buffer, pagesize=pagesize)
        logo = 'static/dist/img/tc.png'
        im = Image(logo, 225, 70)
        x_imagen = 1.1 * cm
        y_imagen = 5.5 * cm
        im.drawOn(c, x_imagen, y_imagen)
        c.setFont('Helvetica-Bold', 14)
        c.drawString(0.2 * cm, 4.5 * cm, paciente_nombrecompleto)
        c.setFont('Helvetica-Bold', 12)
        c.drawString(0.2 * cm, 3.5 * cm, idpaciente)
        c.setFont('Helvetica', 12)
        c.drawString(0.2 * cm, 2.5 * cm, nroinforme)
        c.setFont('Helvetica', 12)
        c.drawString(0.2 * cm, 1.5 * cm, fecha)
        c.showPage()
        c.save()
        pdf = buffer.getvalue()
        printer_name = 'tipe'
        cmd = '/usr/bin/lpr -P {}'.format(printer_name)
        lpr = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        lpr.stdin.write(pdf)
        buffer.close()
        lpr.communicate()

        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


@login_required
def consultar_plantillas(request):
    try:
        medico_id = request.GET.get('medicoid', '')
        servicio_id = request.GET.get('servicioid', '')
        response = {'success': False}
        if medico_id == '' or servicio_id == '':
            return JsonResponse(response)

        medico = User.objects.get(id=medico_id)
        servicio = Servicio.objects.get(id=servicio_id)
        plantillas = InformePlanilla.objects.filter(medico=medico, servicio_tipo=servicio.tipo).order_by(
            'prestacion_codigo').order_by('texto_numero')
        plantillas_dict = {}
        if plantillas:
            for plantilla in plantillas:
                new_plant = (plantilla.texto_numero,
                             plantilla.texto.replace('<br />', '').replace('<br>', '').replace('</p>', '').replace(
                                 '<p>', ''), plantilla.id)
                if str(plantilla.prestacion_codigo) in plantillas_dict:
                    plantillas_dict[str(plantilla.prestacion_codigo)].append(new_plant)
                else:
                    plantillas_dict[str(plantilla.prestacion_codigo)] = [new_plant]

        response['success'] = True
        response['plantillas'] = plantillas_dict
        return JsonResponse(response)

    except:
        response = {'success': False}
        return JsonResponse(response)


@transaction.atomic
def generar_link_turnos_hijos(turnos_hijos):
    if turnos_hijos.count() >= 1:
        for t_h in turnos_hijos:
            imagenes_generadas = False
            if t_h.estudio_finalizado and not t_h.estudio_link_imagenes_generado:
                estudios_solicitado_turno_hijo_count = EstudioSolicitado.objects.filter(turno=t_h).count()
                for index_estudio in range(1, estudios_solicitado_turno_hijo_count + 1):
                    result = generar_linkimagenes(t_h.id, index_estudio)
                    if result['success']:
                        link = TurnoLinkImagen(turno=t_h,
                                               estudio_imagenes_url=result['url_interna'],
                                               estudio_imagenes_url_externa=result['url_externa'],
                                               numero_orden=index_estudio,
                                               descripcion=result['estudio_descripcion'])
                        link.save()
                        imagenes_generadas = True
            if imagenes_generadas:
                t_h.estudio_link_imagenes_generado = True
                t_h.save()


@transaction.atomic
def generar_link_turno_normal(turno):
    if turno.estudio_finalizado and not turno.estudio_link_imagenes_generado:
        result = generar_linkimagenes(turno.id, 0)
        if result['success']:
            link = TurnoLinkImagen(turno=turno,
                                   estudio_imagenes_url=result['url_interna'],
                                   estudio_imagenes_url_externa=result['url_externa'],
                                   numero_orden=1,
                                   descripcion=result['estudio_descripcion'])
            link.save()
            turno.estudio_link_imagenes_generado = True
            turno.save()


def turno_generar_codigos_estudios_solicitados(turno):
    codigos_realizados = ''
    codigos_turno_actual_padre = ''
    if turno.estudio_finalizado:
        estudios_solicitados = EstudioSolicitado.objects.filter(turno=turno)
        for est in estudios_solicitados:
            codigo_sufix = est.servicio_estudio.codigo.zfill(2)
            codigos_turno_actual_padre += turno.servicio.protocolo_prefix + codigo_sufix + ' - '
            codigos_realizados += turno.servicio.protocolo_prefix + codigo_sufix + ' - '

    if turno.tipomodalidad == "En partes":
        turnos_hijos = Turno.objects.filter(turno_relacionado=turno, estudio_finalizado=True, habilitado=True).order_by(
            'fechahora_inicio')
        for t_hijo in turnos_hijos:
            estudios_solicitados_h = EstudioSolicitado.objects.filter(turno=t_hijo)
            codigos_turno_actual = ''
            for est in estudios_solicitados_h:
                codigo_sufix = est.servicio_estudio.codigo.zfill(2)
                codigos_turno_actual += t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - '
                codigos_realizados += t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - '
    return codigos_realizados


def consultar_todos_los_turnos_relacionados(turno):
    todos_los_turnos = Turno.objects.filter(
        turno_relacionado=turno,
        estudio_terminado=True,
        habilitado=True) \
        .order_by('fechahora_inicio')
    queryset_turno = Turno.objects.filter(id=turno.id)
    return queryset_turno | todos_los_turnos


def get_historialpaciente_del_turnoactual(turno, ip_request):
    is_url_local = is_ip_private(ip_request)
    turnos_anteriores = Turno.objects.filter(
        paciente=turno.paciente,
        informe_finalizado=True) \
        .exclude(id=turno.id) \
        .exclude(habilitado=False)

    for turno_anterior in turnos_anteriores:
        codigos_solicitados = turno_generar_codigos_estudios_solicitados(turno_anterior)
        turno_anterior.codigos_solicitados_todos = codigos_solicitados
        link_imagenes = ''
        for turno_rel in consultar_todos_los_turnos_relacionados(turno_anterior):
            if link_imagenes:
                link_imagenes = link_imagenes | TurnoLinkImagen.objects.filter(turno=turno_rel)
            else:
                link_imagenes = TurnoLinkImagen.objects.filter(turno=turno_rel)
        turno_anterior.link_imagenes = link_imagenes
        turno_anterior.is_request_local = is_url_local
    return turnos_anteriores


def get_all_medicos_con_plantilas_del_servicio(servicio):
    medicos_profiles = UserProfile.objects.filter(perfil='Medico')
    medicos = {}
    for med_prof in medicos_profiles:
        medico = User.objects.get(id=med_prof.user.id)
        plantillas = InformePlanilla.objects.filter(medico=medico, servicio_tipo=servicio.tipo)
        if plantillas:
            medicos[medico.id] = medico.last_name + ', ' + medico.first_name

    return medicos


def get_codigos_linksimages_obs(turno, ip):
    url_local = is_ip_private(ip)
    dict_ulrs_imagenes = {}
    estudios_solicitados = EstudioSolicitado.objects.filter(turno=turno)
    codigos_realizados = ''
    codigos_turno_actual_padre = ''
    for est in estudios_solicitados:
        codigo_sufix = est.servicio_estudio.codigo.zfill(2)
        codigos_turno_actual_padre += turno.servicio.protocolo_prefix + codigo_sufix + ' - '
        codigos_realizados += turno.servicio.protocolo_prefix + codigo_sufix + ' - '

    obs_medicas = ''
    if turno.estudio_observacion_medico:
        obs_medicas += '• ' + codigos_realizados[:-2] + ': ' + turno.estudio_observacion_medico + '\n'

    if turno.tipomodalidad == 'Normal':
        links_turno = TurnoLinkImagen.objects.filter(turno=turno)
        if links_turno:
            for link in links_turno:
                url = link.estudio_imagenes_url if url_local else link.estudio_imagenes_url_externa
                dict_index_url = str(turno.id) + '-' + str(link.numero_orden)
                dict_ulrs_imagenes[dict_index_url] = (codigos_turno_actual_padre[:-2], url, link.descripcion)

    elif turno.tipomodalidad == "En partes":
        turnos_hijos = Turno.objects.filter(turno_relacionado=turno, estudio_terminado=True, habilitado=True).order_by(
            'fechahora_inicio')
        queryset_turno = Turno.objects.filter(id=turno.id)
        turnos_hijos = queryset_turno | turnos_hijos
        for t_hijo in turnos_hijos:
            estudios_solicitados_h = EstudioSolicitado.objects.filter(turno=t_hijo)
            codigos_turno_actual = ''
            for est in estudios_solicitados_h:
                codigo_sufix = est.servicio_estudio.codigo.zfill(2)
                codigos_turno_actual += t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - '
                codigos_realizados += t_hijo.servicio.protocolo_prefix + codigo_sufix + ' - '
            if t_hijo.estudio_observacion_medico:
                obs_medicas += '• ' + codigos_turno_actual[:-2] + ': ' + t_hijo.estudio_observacion_medico + '\n'
            turno_hijo_urls = TurnoLinkImagen.objects.filter(turno=t_hijo)
            if turno_hijo_urls:
                for link in turno_hijo_urls:
                    url = link.estudio_imagenes_url if url_local else link.estudio_imagenes_url_externa
                    dict_index_url = str(t_hijo.id) + '-' + str(link.numero_orden)
                    dict_ulrs_imagenes[dict_index_url] = (codigos_turno_actual[:-2], url, link.descripcion)
    return dict_ulrs_imagenes, codigos_realizados[:-2], obs_medicas


def update_contexdata_informecrear(context, ip_request):
    turno = context['object']
    context['turnos_anteriores'] = get_historialpaciente_del_turnoactual(turno, ip_request)
    context['estudios'] = ServicioEstudio.objects.filter(servicio=turno.servicio)
    context['medicos'] = get_all_medicos_con_plantilas_del_servicio(turno.servicio)
    context['urls_imagenes'], context['codigos_realizados_links'], context['obs_tecnico'] = \
        get_codigos_linksimages_obs(turno, ip_request)
    context['codigos_realizados'] = turno_generar_codigos_estudios_solicitados(turno)
    context['edad_year'], context['edad_months'], context['edad_days'] = caclular_edad(turno.paciente.fecnac)
    return context


@login_required
@transaction.atomic
def informe_tec_correccion_finalizada(request):
    turno_id = request.GET.get('turnoid', '')
    response = {'success': False}
    if not turno_id:
        return JsonResponse(response)
    turno = Turno.objects.get(id=turno_id)
    turno.informe_tec_corregir = False
    turno.informe_tec_corregir_completo = True
    turno.informe_tec_corregir_fin = datetime.now()
    turno.save()
    response['success'] = True
    return JsonResponse(response)


@login_required
def medicos_count_mes(request):
    fecha_hoy = datetime.now()
    mes_actual = fecha_hoy.strftime('%B') + ' del ' + str(fecha_hoy.year)
    fecha_inicio = datetime(fecha_hoy.year, fecha_hoy.month, 1, 0, 0, 0)

    fecha_fin = fecha_inicio + relativedelta(months=1)
    medicos_profile = UserProfile.objects.filter(perfil='Medico')
    for medico in medicos_profile:
        cantidad_estudios = 0
        turnos_medico = Turno.objects.filter(habilitado=True, usuario_medico=medico.user, informe_finalizado=True,
                                             fechahora_inicio__gte=fecha_inicio, fechahora_inicio__lt=fecha_fin)
        for turno in turnos_medico:
            cantidad_estudios += EstudioSolicitado.objects.filter(turno=turno).count()
            if turno.tipomodalidad == 'En partes' and turno.servicio.tipo == 'MR':
                turnos_hijos = Turno.objects.filter(estudio_finalizado=True, turno_relacionado=turno)
                for t_h in turnos_hijos:
                    cantidad_estudios += EstudioSolicitado.objects.filter(turno=t_h).count()
        medico.cantidad_regiones = cantidad_estudios

    session = request.session
    if request.user.userprofile.perfil == 'Medico':
        session.set_expiry(60 * 40)
    return render(request, 'informe_estadisticas/estadisticas_mensual_medicos.html',
                  {'medicos': medicos_profile, 'mes_estadistica': mes_actual})


def caclular_edad(fecha_nacimiento):
    edadyear = edadmonth = edaddays = ''
    if isinstance(fecha_nacimiento, date):
        edad = relativedelta(datetime.now().date(), fecha_nacimiento)
        edadyear = edad.years
        edadmonth = edad.months
        edaddays = edad.days
        if not (edadyear >= 0 and edadmonth >= 0 and edaddays >= 0):
            edadyear = edadmonth = edaddays = ''
    return edadyear, edadmonth, edaddays


@login_required
@transaction.atomic
def InformeCorregir(request):
    turno_id = request.POST.get('turnoid', '')
    informe_texto_corregir = request.POST.get('texto_corregir', '')
    response = {'success': False, 'password_error': False}
    if not turno_id or not informe_texto_corregir:
        return JsonResponse(response)
    turno = Turno.objects.get(id=turno_id)
    if not turno.informe_finalizado:
        return JsonResponse(response)
    success = request.user.check_password(request.POST['submitted_password'])
    if not success:
        response['password_error'] = True
        return JsonResponse(response)
    fecha = datetime.now().strftime('%d/%m/%Y %H:%M')
    titulo_correccion = '<br><br><strong>' + 'REVISIÓN - ' + fecha + '</strong><br><br>'
    turno.informe_texto += titulo_correccion
    turno.informe_texto += informe_texto_corregir
    turno.save()
    response['success'] = True

    return JsonResponse(response)


@login_required
def tipeo_marcar_enuso(request):
    turno_id = request.POST.get('turnoid', '')
    response = {'success': False}
    turno = Turno.objects.get(id=turno_id)
    turno.usuario_tipeo_actual = request.user
    turno.save()
    response['success'] = True
    return JsonResponse(response)


class CerrarExpediente(TemplateView):
    template_name = 'informe/informe_cerrar_expediente.html'


@login_required
@transaction.atomic
def cerrar_expediente_sin_informe(request):
    if request.method == 'POST':
        response_data = {'expediente_cerrado': False}
        turno_id = request.POST.get('turno_id', '')
        motivo_cierre = request.POST.get('motivo_cierre', '')
        if turno_id == '':
            return JsonResponse(response_data)
        turno = Turno.objects.filter(id=turno_id).first()
        if not turno:
            return JsonResponse(response_data)
        turno.informe_cerrado = True
        turno.informe_cerrado_fecha = datetime.now()
        turno.informe_cerrado_usuario = request.user
        turno.informe_cerrado_observaciones = motivo_cierre
        turno.save()
        response_data['expediente_cerrado'] = True
    else:
        response_data = {'expediente_cerrado': False}

    return JsonResponse(response_data)


def enviar_mail_informe(turno):
    try:
        config_generals = ConfiguracionGeneral.objects.all().first()
        if (turno and turno.informe_finalizado and turno.email_entregadigital and turno.entrega_digital
                and config_generals.informe_email_envio_automatico and not turno.paciente.paciente_nn):
            paciente_apellido = turno.paciente.apellido.upper() if turno.paciente.apellido else ''
            paciente_nombre = turno.paciente.nombre.upper() if turno.paciente.nombre else ''

            email_asunto = paciente_apellido + ' ' + paciente_nombre + ' – SU INFORME DE DIAGNOSTICO REALIZADO EN T.C.S.E.'
            turno_links = TurnoLinkImagen.objects.filter(turno=turno)
            links_urls = ''
            for link in turno_links:
                links_urls += link.estudio_imagenes_url_externa + '\n'

            email_body = config_generals.informe_email_body.replace('{link_imagenes}', links_urls)
            email_to = turno.email_entregadigital
            email_from = config_generals.informe_email_from
            email = EmailMessage(
                email_asunto,
                email_body,
                email_from,
                [email_to])

            informe_pdf_buffer = informe_medico_generar_pdf(turno)
            informe_pdf_bytes = informe_pdf_buffer.getvalue()
            turno_pdf_filename = patiient_identity_label(turno.paciente) + '.pdf'

            email.attach(turno_pdf_filename, informe_pdf_bytes, 'application/pdf')
            email.send()
            return

    except Exception as e:
        print('Error: {}'.format(e))
        return


@login_required
def unificar_paciente_index(request):
    is_authz_user = request.user.userprofile.user_unificar_hc
    context = {}

    if request.method == 'GET' and is_authz_user:
        paciente_id_permanece = request.GET.get('pacienteIdPermanece', '')
        paciente_id_borrar = request.GET.get('pacienteIdBorrar', '')
        paciente_id_same = paciente_id_borrar == paciente_id_permanece

        if paciente_id_borrar == '' or paciente_id_permanece == '' or paciente_id_same:
            return render(request, 'informe/informe_unificar_paciente.html', context)

        paciente_permanece = Paciente.objects.filter(id=paciente_id_permanece).first()
        paciente_borrar = Paciente.objects.filter(id=paciente_id_borrar).first()

        if not paciente_permanece or not paciente_id_borrar:
            return render(request, 'informe/informe_unificar_paciente.html', context)

        context['paciente_id_permanece'] = paciente_id_permanece
        context['paciente_permanece_nombres'] = paciente_permanece.nombre if paciente_permanece.nombre else '---'
        context['paciente_permanece_apellidos'] = paciente_permanece.apellido if paciente_permanece.apellido else '---'
        context['paciente_permanece_sexo'] = paciente_permanece.sexo
        context['paciente_permanece_fechanac'] = paciente_permanece.fecnac if paciente_permanece.fecnac else '---'
        pac_permanece_dni_tipo = paciente_permanece.identificacion_tipo if paciente_permanece.identificacion_tipo else '---'
        pac_permanece_dni = paciente_permanece.identificacion_numero if paciente_permanece.identificacion_numero else '---'
        context['paciente_permanece_dni'] = pac_permanece_dni_tipo + ': ' + pac_permanece_dni
        context['paciente_permanece_nnlabel'] = paciente_permanece.paciente_nnlabel if paciente_permanece.paciente_nn else '---'

        context['paciente_id_borrar'] = paciente_id_borrar
        context['paciente_borrar_nombres'] = paciente_borrar.nombre if paciente_borrar.nombre else '---'
        context['paciente_borrar_apellidos'] = paciente_borrar.apellido if paciente_borrar.apellido else '---'
        context['paciente_borrar_sexo'] = paciente_borrar.sexo
        context['paciente_borrar_fechanac'] = paciente_borrar.fecnac if paciente_borrar.fecnac else '---'
        pac_borrar_dni_tipo = paciente_borrar.identificacion_tipo if paciente_borrar.identificacion_tipo else '---'
        pac_borrar_dni = paciente_borrar.identificacion_numero if paciente_borrar.identificacion_numero else '---'
        context['paciente_borrar_dni'] = pac_borrar_dni_tipo + ': ' + pac_borrar_dni
        context['paciente_borrar_nnlabel'] = paciente_borrar.paciente_nnlabel if paciente_borrar.paciente_nn else '---'

        context['turnos_paciente_borrar'] = Turno.objects.filter(paciente=paciente_borrar, habilitado=True).order_by('fechahora_inicio')
        context['turnos_paciente_permanece'] = Turno.objects.filter(paciente=paciente_permanece, habilitado=True).order_by('fechahora_inicio')

    return render(request, 'informe/informe_unificar_paciente.html', context)


@login_required
@transaction.atomic
def unificar_paciente_hc(request):
    response_data = {'unificar_finalizado': False}
    is_authz_user = request.user.userprofile.user_unificar_hc

    if request.method == 'POST' and is_authz_user:
        paciente_id_permanece = request.POST.get('pacienteIdPermanece', '')
        paciente_id_borrar = request.POST.get('pacienteIdBorrar', '')
        motivo_unificar = request.POST.get('motivo', '')
        paciente_id_same = paciente_id_borrar == paciente_id_permanece

        if paciente_id_borrar == '' or paciente_id_permanece == '' or motivo_unificar == '' or paciente_id_same:
            return JsonResponse(response_data)

        paciente_permanece = Paciente.objects.get(id=paciente_id_permanece)
        paciente_borrar = Paciente.objects.get(id=paciente_id_borrar)

        turnos_paciente_borrar = Turno.objects.filter(paciente=paciente_borrar)
        listado_nro_orden = ''
        for turno_pac_borrar in turnos_paciente_borrar:
            listado_nro_orden += str(turno_pac_borrar.id) + ', '
            turno_pac_borrar.paciente = paciente_permanece
            turno_pac_borrar.save()

        unificar_registro = UnificarPaciente(
            listado_nro_orden=listado_nro_orden,
            paciente_borrado_id_label=patiient_identity_label(paciente_borrar),
            paciente_borrado_nn=paciente_borrar.paciente_nn,
            motivo=motivo_unificar,
            usuario=request.user)

        unificar_registro.save()
        paciente_borrar.delete()
        response_data['unificar_finalizado'] = True

    return JsonResponse(response_data)
