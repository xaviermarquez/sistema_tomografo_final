from django import forms
from turno.models import Turno


class InformeAltaForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = [
            'paciente',
            'medico_derivante',
            'obrasocial',
            'contrasteev',
            'contrastevo',
            'anestesia',
            'urgencia',
            'internado',
            'edadregistro_manual',
            'informe_pre_observaciones',
            'establecimiento_derivador'
        ]
        widgets = {
            'paciente': forms.TextInput(attrs={'class': 'form-control', 'id': 'f_pacienteid'}),
            'urgencia': forms.CheckboxInput(attrs={'id': 'f_urgencia', }),
            'internado': forms.CheckboxInput(attrs={'id': 'f_internado', }),
            'contrastevo': forms.CheckboxInput(attrs={'id': 'f_contrastevo', }),
            'contrasteev': forms.CheckboxInput(attrs={'id': 'f_contrasteev', }),
            'anestesia': forms.CheckboxInput(attrs={'id': 'f_anestesia', 'disabled': ''}),
            'obrasocial': forms.Select(attrs={'class': 'form-control', 'id': 'f_obrasocial', 'disabled':''}),
            'edadregistro_manual': forms.TextInput(
                attrs={'class': 'form-control', 'readonly': '', 'id': 'f_edadregistro', 'data-inputmask': "'alias': 'age'", 'data-mask': ''}),
            'informe_pre_observaciones': forms.Textarea(
                attrs={'class': 'form-control', 'style': 'resize: none;', 'rows': '2'}),
            'medico_derivante': forms.Select(attrs={'class': 'form-control', 'id': 'f_medico_derivante'}),
            'establecimiento_derivador': forms.Select(attrs={'class': 'form-control', 'id': 'f_establecimiento_derivador'}),
        }


class InformeCrearForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = [
            'informe_texto',
            'informe_observaciones',
            'informe_observaciones_tipeo'
        ]
        widgets = {
            'informe_texto': forms.Textarea(attrs={'class': 'form-control', 'id': 'textoinforme'}),
            'informe_observaciones': forms.Textarea(attrs={'class': 'form-control', 'style': 'resize: none;', 'rows': '3',}),
            'informe_observaciones_tipeo': forms.Textarea(
                attrs={'class': 'form-control', 'style': 'resize: none;', 'rows': '3', }),
        }


class InformeSubirFirmadoForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = [
            'informe_firmadopdf',
        ]
