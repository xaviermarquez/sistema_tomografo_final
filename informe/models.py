from django.db import models
from django.contrib.auth.models import User
from turno.models import Servicio
from ckeditor.fields import RichTextField

TYPES = {
    ('CT', 'CT'),
    ('MR', 'MR'),
    ('CR', 'CR')
}


class InformePlanilla(models.Model):
    class Meta:
        ordering = ['medico', 'prestacion_codigo', 'texto_numero']
    servicio_tipo = models.CharField(choices=TYPES, max_length=40, verbose_name='Tipo de Equipo', null=True)
    medico = models.ForeignKey(User, on_delete=models.PROTECT)
    prestacion_codigo = models.CharField(max_length=60)
    texto_titulo = models.CharField(max_length=120, blank=True, null=True)
    habilitada = models.BooleanField(default=True)
    texto = RichTextField(null=True, blank=True)
    texto_numero = models.IntegerField()

    def __str__(self):
        return self.medico.username + ' - ' + self.prestacion_codigo + ' - ' + 'Nº: ' + str(self.texto_numero)


class UnificarPaciente(models.Model):
    listado_nro_orden = models.CharField(max_length=2000, null=True, blank=True)
    paciente_borrado_id_label = models.CharField(max_length=500,null=True, blank=True)
    paciente_borrado_nn = models.BooleanField(default=False)
    motivo = models.CharField(max_length=1000, null=True, blank=True)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    fecha_registro = models.DateTimeField(auto_now_add=True)


class InformePlanillaDBTCSE(models.Model):
    prestacion_codigo = models.CharField(max_length=60, null=True, blank=True)
    medico_matricula = models.CharField(max_length=60, null=True, blank=True)
    habilitada = models.CharField(max_length=20, null=True, blank=True)
    texto = RichTextField(null=True, blank=True)
    texto_numero = models.CharField(max_length=10, null=True, blank=True)

