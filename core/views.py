from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


class Home(LoginRequiredMixin, TemplateView):
    template_name = "core/home.html"

    def get_context_data(self, **kwargs):
        session = self.request.session
        if self.request.user.userprofile.perfil == 'Medico':
            session.set_expiry(60*40)
        else:
            cambiarsucursal = self.request.GET.get('cambiarsucursal', '0')
            if cambiarsucursal == '1':
                session['location'] = 'Default'
                session.set_expiry(0)
                session.save()

            if 'location' not in session or session['location'] == 'Default':
                location = self.request.GET.get('location', 'Default')
                session['location'] = location
                session.set_expiry(0)
                session.save()


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Cambio de contraseña con éxito.')

            return redirect('core:login')
        else:
            messages.error(request, 'Error no se pudo realizar la operación.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })
