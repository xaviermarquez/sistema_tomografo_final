from django.conf.urls import url
from django.urls import include
from .views import Home, change_password
from django.contrib.auth.decorators import login_required

app_name = 'core'

urlpatterns = [
    url(r'^$', login_required(Home.as_view()), name='home'),
    url(r'', include('django.contrib.auth.urls')),
    url(r'^password/$', change_password, name='change_password'),
]
