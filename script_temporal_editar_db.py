import os
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sistema_ris.settings')
import django
django.setup()
from turno.models import *
from informe.models import *
from usuario.models import *
from tecnico.models import *
import csv
from django.db.models import Q


# def fix_servicios_movimientos():
#     """ script fix insumos ajuste"""
#     print('Script INIT SERVICIOS AJUSTE')
#     servicio_insumo = InsumoServicio.objects.get(id=24)
#     valor_ajuste = 12637
#     id_inicio = 4807
#     movimientos_insumo_servicio = InsumoMovimientosServicio.objects.filter(servicio_insumo=servicio_insumo, id__gte=id_inicio)
#     for det in movimientos_insumo_servicio:
#         det.existencia = det.existencia + valor_ajuste
#         det.save()
#         print(det.id)
#     print('Script END')
#
#
# def fix_tecnicos_movimientos():
#     """ script fix insumos ajuste para tecnicos"""
#     print('Script INIT TECNICOS AJUSTE')
#     tecnico_insumo = TecnicoInsumo.objects.get(id=23)
#     valor_ajuste = -15
#     id_inicio = 3
#     movimientos_insumo_tecnico = InsumoDetalle.objects.filter(tecnico_insumo=tecnico_insumo, id__gte=id_inicio)
#     for det in movimientos_insumo_tecnico:
#         det.existencia = det.existencia + valor_ajuste
#         det.save()
#         print(det.id)
#     print('Script END')

def habilitar_tecinsumo():
    print('Script INIT TECNICOS AJUSTE')
    tecnico_ins = TecnicoInsumo.objects.all()
    insumos_constraste = Insumo.objects.filter(grupo=3).exclude(codigo='30301')
    for tecins in tecnico_ins:
        if tecins.insumo in insumos_constraste:
            tecins.habilitado_listado = True
        else:
            tecins.habilitado_listado = False

        tecins.save()
        print(tecins.id)
    print('Script END')


# __main__
habilitar_tecinsumo()
