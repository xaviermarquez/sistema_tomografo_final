/*
 Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
*/
CKEDITOR.addTemplates("default",
    {
        imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),
        templates:[
            {
            title:"Plantilla TAC 4",image:"template1.gif",description:"Plantilla para Cerebro Reforzado",
                html:'<p>Texto de plantilla de Tomografia Ejemplo 1 asdasdsa asdasd asd asd asdsadasdasdasdasdas as asd asd as das das das as das das as das das as dasd asd asd a ......</p>' +
                    '  <h3>TOMOGRAFIA COMPUTADA DE ABDOMEN Y PELVIS CON CONTRASTE </h3>' +
                    '<p>Se realizó estudio helicoidal con contraste oral y endovenoso.' +
                    'Hígado y bazo homogéneos sin refuerzos patológicos luego de la inyección del contraste endovenoso.</p>' +
                    '<p>Vesícula biliar y páncreas sin particularidades.</p>' +
                    '<p>Ambos riñones de características normales.</p>' +
                    '<p>Retroperitoneo libre de adenomegalias.</p>' +
                    '<p>Resto del estudio sin particularidades.</p>' +
                    '<p>Lo saluda atentamente.</p>'
            },
            {title:"Plantilla TAC 2",image:"template2.gif",description:"Plantilla para TAC Pelvis + Abdomen",
            html:'Texto de plantilla de Tomografia Ejemplo 2 ......'},
            {title:"Plantilla TAC 3",image:"template3.gif",description:"Plantilla TAC Columna Cervical",
            html:'\x3cdiv style\x3d"width: 80%"\x3e\x3ch3\x3eTitle goes here\x3c/h3\x3e\x3ctable style\x3d"width:150px;float: right" cellspacing\x3d"0" cellpadding\x3d"0" border\x3d"1"\x3e\x3ccaption style\x3d"border:solid 1px black"\x3e\x3cstrong\x3eTable title\x3c/strong\x3e\x3c/caption\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3ctr\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3ctd\x3e\x26nbsp;\x3c/td\x3e\x3c/tr\x3e\x3c/table\x3e\x3cp\x3eType the text here\x3c/p\x3e\x3c/div\x3e'}
            ]
    });