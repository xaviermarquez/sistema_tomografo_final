

!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                    details: {
                    type: 'inline',
                    target: 1
                    }
                },
            dom:    "<'row'<'col-sm-4'B><'col-sm-4 text-center'><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12 mx-10'tr>>" + "<'row'<'col-sm-3'><'col-sm-4 text-center'><'col-sm-5 text-right 'p>>",
            pageLength: 20,
            language: {

                    "sProcessing":     "Procesando...",
                	"sLengthMenu":     "Ver: _MENU_",
                    "searchPlaceholder": 'Buscar...',
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "",
                        "sLast":     "",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    select: {
                        rows: ""
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                     }
                  },
            buttons: [

            ],
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);
