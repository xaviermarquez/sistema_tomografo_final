
!function($) {
    "use strict";
    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full-paciente")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                    details: {
                    type: 'inline',
                    target: 1
                    }
                },
            columnDefs: [
                { className: 'text-center', targets: "_all"}
            ],
            dom:    "",
            paging: false,
            language: {
                    "sEmptyTable":     "---",
                    select: {
                        rows: ""
                    },
                  },
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);
