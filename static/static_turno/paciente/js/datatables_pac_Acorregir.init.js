
!function($) {
    "use strict";
    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                    details: {
                    type: 'inline',
                    target: 1
                    }
                },
            columnDefs: [
                { className: 'text-center', targets: "_all"},
                {"width": "5%", "targets": 0},
                {"width": "15%", "targets": 1},
                {"width": "15%", "targets": 2},
                {"width": "10%", "targets": 3},
                {"width": "10%", "targets": 4},
                {"width": "40%", "targets": 5},
                {"width": "5%", "targets": 6},
            ],
            dom:    "",
            paging: false,
            language: {
                    "sEmptyTable":     "---",
                    select: {
                        rows: ""
                    },
                  },
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);
