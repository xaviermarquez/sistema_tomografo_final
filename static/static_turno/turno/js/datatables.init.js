
!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                    details: {
                    type: 'inline',
                    target: 1
                    }
                },
            dom:    "",
            paging: false,
            columnDefs: [
                { className: 'text-center', targets: "_all"},
                {
                    data: {
                        _: '1.display',
                        sort: '1.@data-sort',
                        order: '1.@data-sort'
                    },
                    targets: 1
                }
            ],
            language: {
                    "sZeroRecords":    "---",
                  },
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);
