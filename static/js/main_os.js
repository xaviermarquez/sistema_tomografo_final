

!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#obrasocial-table")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
                    responsive: {
            details: {
            type: 'inline',
            target: 1
            }
        },
            dom: "Bfrtip",
            pageLength: 20,

            columnDefs: [
            {
                targets: 1,
                className: 'noVis'
            }
                ],
            language: {
            paginate: {
                first:      "Primera",
                last:       "Ultima",
                next:       "Siguiente",
                previous:   "Anterior"
                                },
            info: "Mostrando Pagina _PAGE_ de _PAGES_",
            search: 'Busqueda Rapida'
        },
            buttons: [
                {
                extend: 'colvis', text: "Filtro",
                columns: ':not(.noVis)'
            },
                {

                extend: "excel", text: "Excel"
            }, {
                extend: "pdf", text: "PDF"
            }, {
                extend: "print", text: "Imprimir",
                    exportOptions: {
                        rows: function (idx, data, node) {
                            var dt = new $.fn.dataTable.Api('#example');
                            var selected = dt.rows({selected: true}).indexes().toArray();

                            if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                return true;


                            return false;
                        }
                    }
            },
            ],
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
    document.getElementById("obrasocial-table").style["opacity"] = "1";
}(window.jQuery);



$(function () {

  /* Functions */

  var loadForm = function () {

    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-book .modal-content").html("");
        $("#modal-book").modal("show");
      },
      success: function (data) {
        $("#modal-book .modal-content").html(data.html_form);
      }

    });

  };

  var saveForm = function () {

    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
            document.getElementById("obrasocial-table").style["opacity"] = "0";
          $("#obrasocial-table tbody").html(data.html_obrasocial_list);
          $("#modal-book").modal("hide");
          window.location.reload()
        }
        else {
          $("#modal-book .modal-content").html(data.html_form);
        }
      }
    });

    return false;

  };




  /* Binding */

  // Create book
  $(".js-create-book").click(loadForm);
  $("#modal-book").on("submit", ".js-book-create-form", saveForm);

  // Update book
  $("#obrasocial-table").on("click", ".js-update-book", loadForm);
  $("#modal-book").on("submit", ".js-book-update-form", saveForm);

  // Delete book
  $("#obrasocial-table").on("click", ".js-delete-book", loadForm);
  $("#modal-book").on("submit", ".js-book-delete-form", saveForm);


});


/*add dinamically imput*/

