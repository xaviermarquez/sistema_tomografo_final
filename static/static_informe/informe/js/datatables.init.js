!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                details: {
                    type: 'inline',
                    target: 1
                }
            },
            dom:    "f",
            pageLength: 1000,
    language: {

                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Ver: _MENU_",

                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Sin registros pendientes",
                "sInfo":           "Mostrando registros [_START_ - _END_] de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros [ 0 - 0 ] de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "",
                'searchPlaceholder': "Buscar...",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },

            },
            select: true
        });
    },
        DataTable.prototype.init = function() {
            //creating table with button
            this.createDataTableButtons();
        },
        //init
        $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.DataTable.init();
    }(window.jQuery);
