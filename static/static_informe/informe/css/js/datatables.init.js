

!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#data-table-full")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            responsive: {
                    details: {
                    type: 'inline',
                    target: 1
                    }
                },
            dom:    "<'row'<'col-sm-4'B><'col-sm-4 text-center'><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12 mx-10'tr>>" + "<'row'<'col-sm-3'i><'col-sm-4 text-center'p><'col-sm-3 '><'col-sm-2 text-right 'l>>",
            pageLength: 15,


            language: {

                    "sProcessing":     "Procesando...",
                	"sLengthMenu":     "Ver: _MENU_",

                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros [_START_ - _END_] de _TOTAL_",
                    "sInfoEmpty":      "Mostrando registros [ 0 - 0 ] de 0",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    select: {
                        rows: ""
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                     }
                  },
            buttons: [

                  {
                    text: '<i class="fa fa-plus" style="color: white"></i>',
                      className: 'btn-success',
                    action: function ( e, dt, node, config ) {
                        window.location.href = "../pacientecrear";

                    }
                  },


                {
                extend: 'colvis',
                    text:      '<i class="fa fa-columns"></i>',
                    titleAttr: 'Ocultar Columnas'
                },

                {

                extend: "excel",
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                        exportOptions: {
                        rows: function (idx, data, node) {
                            var dt = new $.fn.dataTable.Api('#data-table-full');
                            var selected = dt.rows({selected: true}).indexes().toArray();

                            if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                return true;


                            return false;
                        },
                        columns: [ 0, 1, 2, 3, 4, 5, 6]
                    }
            }, {
                extend: "pdf",
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                        rows: function (idx, data, node) {
                            var dt = new $.fn.dataTable.Api('#data-table-full');
                            var selected = dt.rows({selected: true}).indexes().toArray();

                            if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                return true;
                            return false;
                        },

                        columns: [ 0, 1, 2, 3, 4, 5, 6],
                    },

                    customize: function (doc) {
                    doc.defaultStyle.fontSize = 8;}
            }, {
                extend: "print",
                    text:      '<i class="fa fa-print"></i>',
                    titleAttr: 'Imprimir',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '8pt' );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );


                        var last = null;
                        var current = null;
                        var bod = [];

                        var css = '@page { size: landscape; }',
                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                            style = win.document.createElement('style');

                        style.type = 'text/css';
                        style.media = 'print';

                        if (style.styleSheet)
                        {
                          style.styleSheet.cssText = css;
                        }
                        else
                        {
                          style.appendChild(win.document.createTextNode(css));
                        }

                        head.appendChild(style);
                    },
                    exportOptions: {
                        rows: function (idx, data, node) {
                            var dt = new $.fn.dataTable.Api('#data-table-full');
                            var selected = dt.rows({selected: true}).indexes().toArray();

                            if (selected.length === 0 || $.inArray(idx, selected) !== -1)
                                return true;


                            return false;
                        },
                        columns: [0, 1, 2, 3, 4, 5,6]
                    }
            },
            ],
            select: true
        });
    },
    DataTable.prototype.init = function() {
        //creating table with button
        this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);
