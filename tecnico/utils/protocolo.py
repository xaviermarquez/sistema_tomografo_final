"""
    Main Protocolo Functions
"""
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta

from turno.models import Turno, EstudioSolicitado


def consecutive_month1_less_than_month2(month1, month2):
    """
        Compare two consecutive months and returns True if month1 is less than month2,
        considering the case where December is less than January, because we considering January bigger than December.

    :param month1: month1 int representation
    :param month2: month2 int representation
    :return: True if month1 is less than month2 else False
    """
    if month1 == 12 and month2 == 1:
        return True

    if month2 - month1 >= 1:
        return True
    return False


def get_turno_protocolo_prefix(turno):
    protocolo_prefix_aniomes = turno.fechahora_inicio.strftime('%y') + turno.fechahora_inicio.strftime('%m')
    return protocolo_prefix_aniomes + turno.servicio.protocolo_label


def update_protocolo_servicio_month_counter(servicio, month):
    servicio.protocolo_mesactual = month
    servicio.protocolo_count = 0
    servicio.save()


def current_month_equal_turno_month(turno):
    current_month = datetime.now().astimezone(pytz.timezone("America/Argentina/Salta")).month
    turno_month = turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).month
    return current_month == turno_month


def check_valid_turno_date_protocolo(turno):
    current_date = datetime.now().astimezone(pytz.timezone("America/Argentina/Salta"))
    last_date_of_month = datetime(current_date.year, current_date.month, 1) + relativedelta(months=1, seconds=-1)
    last_date_of_month_aware = last_date_of_month.astimezone(pytz.timezone("America/Argentina/Salta"))
    turno_date_aware = turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta"))
    return last_date_of_month_aware > turno_date_aware


def update_protocolo_servicio(turno):
    servicio = turno.servicio
    current_month = datetime.now().astimezone(pytz.timezone("America/Argentina/Salta")).month
    servicio_protocolo_month = turno.servicio.protocolo_mesactual
    turno_month = turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).month

    if current_month != servicio_protocolo_month \
            and consecutive_month1_less_than_month2(servicio_protocolo_month, turno_month):
        update_protocolo_servicio_month_counter(servicio, current_month)


def get_current_protocolo_count(turno):
    update_protocolo_servicio(turno)
    protocolo_count = turno.servicio.protocolo_count
    
    if not current_month_equal_turno_month(turno):
        nro_protocolo_prefix = get_turno_protocolo_prefix(turno)
        last_protocolo = Turno.objects.filter(estudio_protocoloprefix=nro_protocolo_prefix)\
            .order_by('estudio_protocolocount')\
            .last()
        if last_protocolo:
            cantidad_estudios = EstudioSolicitado.objects.filter(turno=last_protocolo).count()
            protocolo_count = int(last_protocolo.estudio_protocolocount) + cantidad_estudios

    return protocolo_count


def update_protocolo_servicio_current_count(turno, estudio_count):
    servicio = turno.servicio

    if current_month_equal_turno_month(turno):
        servicio.protocolo_count += estudio_count
        servicio.save()
