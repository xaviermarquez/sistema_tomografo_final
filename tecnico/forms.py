from django import forms
from turno.models import Turno


class ProtocoloAltaForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = [
            'usuario_creacion',
            'paciente',
            'medico_derivante',
            'obrasocial',
            'contrasteev',
            'contrastevo',
            'anestesia',
            'urgencia',
            'internado',
            'fechahora_inicio',
            'estudio_fechahora_ingreso',
            'estudio_fechahora_manual',
            'estudio_medicoanestesia',
            'estudio_observacion_nota',
            'estudio_observacion_medico',

        ]
        widgets = {
            'estudio_fechahora_ingreso': forms.TextInput(
                attrs={'class': 'form-control', 'id': 'fechahoraingreso', 'data-date-format': 'YYYY-MM-DD hh:mm', 'readonly': 'readonly', }),
            'fechahora_inicio': forms.TextInput(
                attrs={'class': 'form-control', 'id': 'fechahorainicio', 'data-date-format': 'YYYY-MM-DD HH:mm', 'readonly': 'readonly'}),
            'estudio_fechahora_manual': forms.TextInput(
                attrs={'class': 'form-control', 'id': 'datetimepicker4', 'data-date-format': 'YYYY-MM-DD hh:mm'}),
            'urgencia': forms.CheckboxInput(),
            'internado': forms.CheckboxInput(),
            'contrastevo': forms.CheckboxInput(),
            'contrasteev': forms.CheckboxInput(),
            'anestesia': forms.CheckboxInput(),
            'estudio_medicoanestesia': forms.Select(attrs={'class': 'form-control', }),
            'estudio_observacion_nota': forms.Textarea(
                attrs={'class': 'form-control',  'id': 'estudio_observacion', 'rows': '3', 'style': 'resize: none;'}),
            'estudio_observacion_medico': forms.Textarea(
                attrs={'class': 'form-control',  'id': 'estudio_observacion_medico', 'rows': '3',
                       'style': 'resize: none;'}),
            'paciente': forms.TextInput(attrs={'class': 'form-control hidden', 'id': 'f_pacienteid'}),
            'medico_derivante': forms.TextInput(attrs={'class': 'form-control hidden', 'id': 'f_medicoderivante'}),
            'obrasocial': forms.TextInput(attrs={'class': 'form-control hidden', 'id': 'f_obrasocial'}),
            'usuario_creacion': forms.TextInput(attrs={'class': 'form-control hidden', 'id': 'f_usuariocreacion'}),
        }
