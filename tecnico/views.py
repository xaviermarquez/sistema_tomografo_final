import datetime
import io
import json
from lib2to3.pgen2.pgen import DFAState

import pytz
import xlsxwriter
from PIL import Image
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Q, Sum
from django.http import FileResponse
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, inch
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, Paragraph, TableStyle, Image, BaseDocTemplate, Frame, PageTemplate

from informe.views import get_codigos_linksimages_obs
from informe.views import websocket_actualizarinforme
from turno.models import *
from usuario.models import UserProfile
from .forms import *
from .models import TecnicoInsumo, InsumoDetalle, RemitoIngreso, Proveedor, InsumoRemitoIngreso, RemitoEntregaTecnico, \
    InsumoRemitoEntrega
from utils.protocolo_functions import get_protocolo_estado
from .utils.protocolo import update_protocolo_servicio, get_turno_protocolo_prefix, get_current_protocolo_count, \
    update_protocolo_servicio_current_count, check_valid_turno_date_protocolo

styles = getSampleStyleSheet()
styleN = styles['Normal']
styleH = styles['Heading1']
styleH5 = styles['Heading5']
styleH3 = styles['Heading3']


class NeverCacheMixin(object):
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(NeverCacheMixin, self).dispatch(*args, **kwargs)


@login_required
def index_insumos(request):
    insumos = Insumo.objects.order_by('grupo', 'codigo')
    return render(request, 'insumo/abm_insumos.html', {'insumos': insumos})


insumo_fields_form = (
    'nombre', 'codigo', 'abreviatura', 'descripcion', 'grupo', 'unidad', 'unidad_tipo', 'habilitado', 'marca')


class InsumoCreate(CreateView):
    model = Insumo
    fields = insumo_fields_form
    template_name = 'insumo/insumo_form.html'
    success_url = reverse_lazy('tecnico:index_insumo')


class InsumoUpdate(UpdateView):
    model = Insumo
    fields = insumo_fields_form
    template_name = 'insumo/insumo_form.html'
    success_url = reverse_lazy('tecnico:index_insumo')


class InsumoDelete(DeleteView):
    model = Insumo
    template_name = 'insumo/insumo_confirm_delete.html'
    success_url = reverse_lazy('tecnico:index_insumo')


@login_required
def index_tecxins(request):
    context = {}
    tecnico_id = request.GET.get("tecnicoid", "")
    ajuste = request.GET.get("ajuste", "0")

    usuarios_tecnico_profile = UserProfile.objects.filter(perfil__contains='Tecnico', registra_stock=True)
    usuarios_tecnico = [userprofile.user for userprofile in usuarios_tecnico_profile]

    if tecnico_id != "" and tecnico_id != '1':
        tecnico = User.objects.get(id=tecnico_id)
        if tecnico:
            insumo_tecnico = TecnicoInsumo.objects.filter(tecnico=tecnico, insumo__grupo="3")
            context['tecnico_selected'] = tecnico

        else:
            insumo_tecnico = TecnicoInsumo.objects.filter(tecnico__in=usuarios_tecnico,
                                                          insumo__grupo="3").order_by('tecnico__last_name',
                                                                                      'tecnico__first_name')
    else:
        insumo_tecnico = TecnicoInsumo.objects.filter(tecnico__in=usuarios_tecnico, insumo__grupo="3").order_by(
            'tecnico__last_name', 'tecnico__first_name')

    list_tecnicos = {}
    profiles_tecnico = UserProfile.objects.filter(Q(perfil='Tecnico') | Q(perfil='Tecnico Recepcion'),
                                                  registra_stock=True).exclude(user__username='tecnico').order_by('user__last_name', 'user__first_name')
    for pt in profiles_tecnico:
        list_tecnicos[pt.user.id] = (pt.matricula, pt.user.last_name + ' ' + pt.user.first_name)

    insumos = Insumo.objects.filter(seregistrastock_tecnico=True).order_by('nombre')

    context['tecnicos'] = list_tecnicos
    context['insumo_tecnico'] = insumo_tecnico
    context['insumos'] = insumos

    template = 'insumo/index_tecxins.html'
    if ajuste == '1':
        template = 'insumo/index_tecxins_ajuste.html'

    return render(request, template, context)


@login_required
def insumo_tecnico_detalle(request):
    context = {}
    tecnico_id = request.GET.get("tecnicoid", "")
    insumo_id = request.GET.get("insumoid", "")

    if tecnico_id != "" and insumo_id != "":
        tecnico = User.objects.get(id=tecnico_id)
        insumo = Insumo.objects.get(id=insumo_id)
        insumo_tecnico = TecnicoInsumo.objects.filter(tecnico=tecnico, insumo=insumo).first()
        detalle_stock = InsumoDetalle.objects.filter(tecnico_insumo=insumo_tecnico).order_by('-fechahora')
        context = {"detalle_stock": detalle_stock, "insumo": insumo, "tecnico": tecnico,
                   "insumo_detalle_id": insumo_tecnico.id}

    return render(request, 'insumo/insumo_tecnico_detalle.html', context)


@login_required
@transaction.atomic
def tec_ins_crear(request):
    tecnicos = User.objects.all()
    insumos = Insumo.objects.filter(stock_almacen__gte=0)
    tecins = TecnicoInsumo.objects.all()
    if request.method == "POST":
        id_tec = request.POST['sele_tec']
        id_ins = {x: y for x, y in request.POST.items() if x.startswith('ins_')}
        tec = User.objects.get(pk=id_tec)
        ids = []
        for key in id_ins.keys():
            ids = key.replace("ins_", "")
        ins = Insumo.objects.filter(id__in=ids)
        for i in ins:
            tins = TecnicoInsumo(
                tecnico=tec,
                insumo=i,
                stock=request.POST['cant_ins_' + str(i.id)]
            )
            Insumo.objects.filter(id=i.id).update(
                stock_almacen=i.stock_almacen - int(request.POST['cant_ins_' + str(i.id)])
            )
            tins.save()
        return HttpResponseRedirect(reverse('tecnico:index_tecxins'))
    else:
        return render(request, 'insumo/tecxins_ingresar.html',
                      {"tecnicos": tecnicos, "insumos": insumos, "tecins": tecins})


@login_required
@transaction.atomic
def tec_ins_mod(request, pk_ti):
    tecins = TecnicoInsumo.objects.get(pk=pk_ti)
    tecnico = User.objects.get(pk=tecins.tecnico_id)
    ins = Insumo.objects.get(pk=tecins.insumo_id)
    max_stock = ins.stock_almacen + tecins.stock
    if request.method == "POST":
        nuevo_valor = int(request.POST["nuevo_stock"])
        if nuevo_valor > tecins.stock:
            Insumo.objects.filter(id=tecins.insumo_id).update(
                stock_almacen=ins.stock_almacen - (nuevo_valor - tecins.stock)
            )
        elif nuevo_valor < tecins.stock:
            Insumo.objects.filter(id=tecins.insumo_id).update(
                stock_almacen=ins.stock_almacen + (tecins.stock - nuevo_valor)
            )
        TecnicoInsumo.objects.filter(id=pk_ti).update(stock=nuevo_valor)
        return HttpResponseRedirect(reverse('tecnico:index_tecxins'))
    else:
        return render(request, 'insumo/tecxins_mod.html',
                      {"tecnico": tecnico, "tecins": tecins, "max": max_stock})


@login_required
@transaction.atomic
def IngresarInsumo(request):
    insumos = Insumo.objects.all()
    return render(request, 'protocolo/insumos.html', {'insumosxtecnico': insumos})


@login_required
@transaction.atomic
def tec_ins_eli(request, pk_ti):
    tecins = TecnicoInsumo.objects.get(pk=pk_ti)
    tecnico = User.objects.get(pk=tecins.tecnico_id)
    ins = Insumo.objects.get(pk=tecins.insumo_id)
    cantidad = tecins.stock
    if request.method == "POST":
        Insumo.objects.filter(id=tecins.insumo_id).update(
            stock_almacen=ins.stock_almacen + cantidad
        )
        TecnicoInsumo.objects.filter(id=pk_ti).delete()
        return HttpResponseRedirect(reverse('tecnico:index_tecxins'))
    else:
        return render(request, 'insumo/tecxins_eli.html',
                      {"tecnico": tecnico, "insumo": ins, "cantidad": cantidad})


@login_required
def listado_remito_ingreso(request):
    remitos_ingreso = RemitoIngreso.objects.all().order_by('-fecha')
    insumos = Insumo.objects.all()
    servicios = Servicio.objects.all().exclude(is_servicio_externo=True)
    tecnicos = UserProfile.objects.filter(perfil__contains='Tecnico').order_by('user__last_name', 'user__first_name')

    return render(request, 'insumo_remito/remitos_ingreso_listado.html',
                  {'remitos': remitos_ingreso, "insumos": insumos, "servicios": servicios, "tecnicos": tecnicos})


@login_required
def listado_remito_egreso(request):
    remitos_egreso_tecnico = RemitoEntregaTecnico.objects.all().order_by('-fecha')
    remitos_egreso_servicio = RemitoEntregaServicio.objects.all().order_by('-fecha')
    context = {'remitos_egreso_tecnico': remitos_egreso_tecnico, "remitos_egreso_servicio": remitos_egreso_servicio}
    return render(request, 'insumo_remito/remitos_egreso_listado.html', context)


@login_required
@transaction.atomic
def remito_ingreso_nuevo(request):
    response = {'success': False}
    servicio_tecnico_id = request.POST.get('select-servicios', '')
    remito_fecha = request.POST.get('remitoingreso-fecha', '')
    tipo_ingreso = request.POST.get('select-tipoingreso', '')
    orden_compra_numero = request.POST.get('ordencompra-numero', '')
    proveedor_codigo = request.POST.get('proveedor-codigo', '')
    proveedor_remito_numero = request.POST.get('proveedor-remito-numero', '')
    proveedor_remito_fecha = request.POST.get('proveedor-remito-fecha', '')
    factura_numero = request.POST.get('factura-numero', '')
    factura_fecha = request.POST.get('factura-fecha', '')
    factura_importe = request.POST.get('factura-importe', '')
    remito_obs = request.POST.get('remito-obs', '')
    remito_fecha = datetime.datetime.strptime(remito_fecha, '%Y-%m-%d').date() if remito_fecha else None
    remito_prov_fecha = datetime.datetime.strptime(proveedor_remito_fecha,
                                                   '%Y-%m-%d').date() if proveedor_remito_fecha else None
    remito_factura_fecha = datetime.datetime.strptime(factura_fecha, '%Y-%m-%d').date() if factura_fecha else None
    proveedor = Proveedor.objects.filter(codigo=proveedor_codigo).first() if Proveedor.objects.filter(
        codigo=proveedor_codigo) else None
    remito_fact_importe = float(factura_importe) if factura_importe else float(0)

    if tipo_ingreso == 'Reposición-t':
        tipo_ingreso = 'Reposición'

    new_remito_ingreso = RemitoIngreso(
        fecha=remito_fecha,
        observaciones=remito_obs,
        tipo_ingreso=tipo_ingreso,
    )

    if tipo_ingreso == 'Orden de Compra':
        new_remito_ingreso.proveedor = proveedor
        new_remito_ingreso.remito_proveedor_numero = proveedor_remito_numero
        new_remito_ingreso.remito_proveedor_fecha = remito_prov_fecha
        new_remito_ingreso.factura_numero = factura_numero
        new_remito_ingreso.factura_fecha = remito_factura_fecha
        new_remito_ingreso.factura_importe = remito_fact_importe
        new_remito_ingreso.orden_compra_numero = orden_compra_numero

    new_remito_ingreso.save()

    cantidad_insumos = request.POST.get('cantidad-total-insumos', '')
    for i in range(1, int(cantidad_insumos) + 1):
        insumo_codigo = request.POST.get('insumo-' + str(i), '')
        insumo_cantidad = request.POST.get('insumo-cantidad-' + str(i), '')
        insumo_costounitario = request.POST.get('insumo-costounit-' + str(i), '')
        insumo_importe = request.POST.get('insumo-importe-' + str(i), '')
        insumo_lote = request.POST.get('insumo-lote-' + str(i), '')
        insumo_lote_vencimiento = request.POST.get('insumo-lotevencimiento-' + str(i), '')
        insumo_laboratorio = request.POST.get('insumo-laboratorio-' + str(i), '')
        insumo_nombrecomercial = request.POST.get('insumo-nombrecomercial-' + str(i), '')
        cantidad = float(insumo_cantidad)
        insumo = Insumo.objects.get(codigo=insumo_codigo)

        insumo_importe = float(cantidad) * float(insumo_costounitario)

        lote_fecha_ven = datetime.datetime.strptime(insumo_lote_vencimiento,
                                                    '%d/%m/%Y').date() if insumo_lote_vencimiento else None
        insumo_remito_ingreso = InsumoRemitoIngreso(
            insumo=insumo,
            remito=new_remito_ingreso,
            fecha_vencimiento=lote_fecha_ven,
            lote=insumo_lote,
            importe=insumo_importe,
            costo_unitario=insumo_costounitario,
            unidades=cantidad,
            nombre_comercial=insumo_nombrecomercial,
            laboratorio=insumo_laboratorio
        )
        insumo_remito_ingreso.save()

        if insumo_lote:
            insumo_lote = InsumoLote(
                insumo=insumo,
                insumo_remito=insumo_remito_ingreso,
                fecha_vencimiento=lote_fecha_ven,
                lote=insumo_lote,
                ingresos=cantidad,
                egresos=float(0),
                saldo=cantidad,
                stock_inicial=cantidad
            )
            insumo_lote.save()

        ins_detalle_movimiento = InsumoDetalle(
            insumo=insumo,
            remito_ingreso=new_remito_ingreso,
            is_turno=False,
            ingreso=cantidad,
            egreso=float(0),
            existencia=float(0),
            observaciones='Ingreso Insumo Remito '
        )
        ins_detalle_movimiento.save()

        insumo_stock_final = insumo.stock_almacen + cantidad if insumo.stock_almacen else cantidad
        insumo.stock_almacen = insumo_stock_final
        insumo.save()

        if tipo_ingreso == 'Reposición':
            cantidad = float(cantidad)
            cantidad_x_unidad = float(cantidad) * float(insumo.unidad)
            select_tipo = request.POST.get('select-tipo', '')
            if select_tipo == 'servicio':
                servicio = Servicio.objects.get(id=servicio_tecnico_id)
                insumo_servicio_ajustar, created = InsumoServicio.objects.get_or_create(insumo=insumo, servicio=servicio)
                nueva_cantidad = insumo_servicio_ajustar.stock - cantidad_x_unidad
                insumo_servicio_ajustar.stock = nueva_cantidad
                insumo_servicio_ajustar.save()
                InsumoMovimientosServicio.objects.create(
                    servicio_insumo=insumo_servicio_ajustar,
                    observaciones='Reposición a Farmacia: Remito Nº ' + str(new_remito_ingreso.id),
                    ingreso=0,
                    egreso=cantidad_x_unidad,
                    existencia=nueva_cantidad)

            elif select_tipo == 'tecnico':
                tecnico = User.objects.get(id=servicio_tecnico_id)
                insumo_tecnico_ajustar, created = TecnicoInsumo.objects.get_or_create(insumo=insumo, tecnico=tecnico)
                nueva_cantidad = insumo_tecnico_ajustar.stock - cantidad_x_unidad
                insumo_tecnico_ajustar.stock = nueva_cantidad
                insumo_tecnico_ajustar.save()

                InsumoDetalle.objects.create(
                    tecnico_insumo=insumo_tecnico_ajustar,
                    observaciones='Reposición a Farmacia: Remito Nº ' + str(new_remito_ingreso.id),
                    ingreso=0,
                    egreso=cantidad_x_unidad,
                    existencia=nueva_cantidad)
    response['success'] = True
    return JsonResponse(response)


@login_required
def listado_estudiossolicitados(request, serviciocod):
    selected_date = request.GET.get('date', '')
    if selected_date:
        hoy = datetime.datetime.strptime(selected_date, "%d-%m-%Y")
    else:
        hoy = datetime.date.today()

    if serviciocod == '2':
        iniciodeldia = datetime.datetime.combine(hoy, datetime.time(4, 00, 00))
        findeldia = datetime.datetime.combine(hoy, datetime.time(3, 59, 59)) + datetime.timedelta(days=1)
    else:
        iniciodeldia = datetime.datetime.combine(hoy, datetime.time(00, 00, 00))
        findeldia = datetime.datetime.combine(hoy, datetime.time(23, 59, 59))

    intervalo = (iniciodeldia, findeldia)
    servicio = Servicio.objects.get(codigo=int(serviciocod))
    turnos = Turno.objects.filter(
        fechahora_inicio__range=intervalo,
        turno_recepcion_suspendido=False,
        habilitado=True, servicio=servicio) \
        .order_by('fechahora_inicio')

    context = {
        'turnos': turnos,
        'servicio': servicio,
        'fecha_filtro': hoy
    }
    return render(request, 'protocolo/protocolo_listado_estudiossolicitados.html', context)


class ProtocoloAlta(NeverCacheMixin, UpdateView):
    model = Turno
    form_class = ProtocoloAltaForm
    template_name = 'protocolo/protocolo_alta.html'

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turnoid = self.kwargs['pk']
        turno = Turno.objects.get(pk=turnoid)
        form = ProtocoloAltaForm(request.POST, instance=turno)
        user = request.user
        estudio_tipoterminacion = request.POST.get("submittipo", "")
        if not ('Tecnico' in user.userprofile.perfil) and user.userprofile.perfil != 'Administrador':
            urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo)
            return redirect(urlred)

        if turno.estudio_protocolocount and not estudio_tipoterminacion == '6':
            urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo) + "/?error=estudiofinalizado"
            return redirect(urlred)

        delta_turnodate_now =  datetime.datetime.now().astimezone(pytz.utc) - turno.fechahora_inicio
        if estudio_tipoterminacion in ('6', '7') and delta_turnodate_now.days > 7:
            urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo) + "/?error=estudiovencidoeditar"
            return redirect(urlred)

        if form.is_valid():
            form.save()
            url_imagen = request.POST.get('url-imagen', '')
            url_imagen_desc = request.POST.get('url-imagen-desc', '')

            if url_imagen and url_imagen_desc:
                url_interna = url_imagen if '172.16.5' in url_imagen else url_imagen.replace('181.199.159.254',
                                                                                             '172.16.5.1')
                url_externa = url_imagen if '181.199.159.254' in url_imagen else url_imagen.replace('172.16.5.1',
                                                                                                    '181.199.159.254')
                TurnoLinkImagen.objects.filter(turno=turno).delete()
                new_link = TurnoLinkImagen(
                    turno=turno,
                    numero_orden=1,
                    descripcion=url_imagen_desc,
                    estudio_imagenes_url_externa=url_externa,
                    estudio_imagenes_url=url_interna
                )
                new_link.save()

            accessnumber_alt = request.POST.get('accessnumber_alt', '')
            if accessnumber_alt != '' and turno.estudio_accessnumber and turno.estudio_accessnumber != accessnumber_alt:
                turno.estudio_link_imagenes_generado = False
            turno.estudio_accessnumber = accessnumber_alt
            turno.save()

            dict_insumos_modificados = {}
            turno_insumos_existen = TurnoInsumo.objects.filter(turno=turno)
            for tur_ins in turno_insumos_existen:
                insumo_tecnico_ajustar = TecnicoInsumo.objects.filter(insumo=tur_ins.insumo,
                                                                      tecnico=turno.usuario_tecnico).first()
                if insumo_tecnico_ajustar:
                    insumo_tecnico_ajustar.stock = insumo_tecnico_ajustar.stock + tur_ins.cantidad_usada
                    insumo_tecnico_ajustar.save()

                insumo_servicio_ajustar = InsumoServicio.objects.filter(insumo=tur_ins.insumo,
                                                                        servicio=turno.servicio).first()
                if insumo_servicio_ajustar:
                    insumo_servicio_ajustar.stock = insumo_servicio_ajustar.stock + tur_ins.cantidad_usada
                    insumo_servicio_ajustar.save()

                dict_insumos_modificados[tur_ins.insumo.id] = tur_ins.cantidad_usada
                tur_ins.delete()

            selectcount_insumo = int(request.POST.get('insumocount', ''))
            insumos_cargados = {}
            tecnico = user

            for i in range(1, selectcount_insumo + 1):
                nameinsumo = 'insumo' + str(i)
                insumoid = request.POST.get(nameinsumo, '')
                if insumoid != '':
                    obj_insumo = Insumo.objects.get(codigo=int(insumoid))
                else:
                    continue

                cantinsumo = 'cant_ins_' + insumoid
                cantidadusada = request.POST.get(cantinsumo, '')
                if cantidadusada != '' and insumoid not in insumos_cargados:
                    insumos_cargados[insumoid] = cantidadusada
                    turnoinsumo = TurnoInsumo(turno=turno, insumo=obj_insumo, cantidad_usada=cantidadusada)
                    turnoinsumo.save()

                    tecnicoxinsumo, created = TecnicoInsumo.objects.get_or_create(tecnico=tecnico, insumo=obj_insumo)
                    tecnicoxinsumo.stock = tecnicoxinsumo.stock - float(cantidadusada)
                    tecnicoxinsumo.save()

                    pac_apellido = turno.paciente.apellido.upper() if turno.paciente.apellido else ""
                    pac_nombre = turno.paciente.nombre.upper() if turno.paciente.nombre else ""
                    obs = pac_apellido + ' ' + pac_nombre

                    if obj_insumo.id in dict_insumos_modificados:

                        ajuste = float(cantidadusada) - float(dict_insumos_modificados[obj_insumo.id])
                        if ajuste >= 0:
                            ins_ingreso = 0
                            ins_egreso = abs(ajuste)
                        else:
                            ins_ingreso = abs(ajuste)
                            ins_egreso = 0
                        del dict_insumos_modificados[obj_insumo.id]

                    else:
                        ins_egreso = float(cantidadusada)
                        ins_ingreso = float(0)

                    last_detalle_tecnico = InsumoDetalle.objects.filter(tecnico_insumo=tecnicoxinsumo).order_by(
                        'fechahora').last()
                    if last_detalle_tecnico:
                        existencia = last_detalle_tecnico.existencia + ins_ingreso - ins_egreso
                    else:
                        existencia = ins_ingreso - ins_egreso

                    if not (ins_ingreso == 0 and ins_egreso == 0):
                        tecnicoxdetalle, tecins_created = InsumoDetalle.objects.get_or_create(
                            tecnico_insumo=tecnicoxinsumo, is_turno=True, comprobante=turno.id)

                        if tecins_created:
                            tecnicoxdetalle.existencia = existencia
                            tecnicoxdetalle.observaciones = obs
                            tecnicoxdetalle.egreso += ins_egreso - ins_ingreso
                        else:
                            tecnicoxdetalle.egreso += ins_egreso - ins_ingreso
                            tecnicoxdetalle.existencia = tecnicoxdetalle.existencia - ins_egreso + ins_ingreso

                        tecnicoxdetalle.save()

                        tecnicoxdetalles_siguientes = InsumoDetalle.objects.filter(
                            fechahora__gt=tecnicoxdetalle.fechahora, tecnico_insumo=tecnicoxinsumo)
                        for detalle_ajustar in tecnicoxdetalles_siguientes:
                            detalle_ajustar.existencia = detalle_ajustar.existencia - ins_egreso + ins_ingreso
                            detalle_ajustar.save()

                    servicioxinsumo, created = InsumoServicio.objects.get_or_create(servicio=turno.servicio,
                                                                                    insumo=obj_insumo)
                    servicioxinsumo.stock = servicioxinsumo.stock - float(cantidadusada)
                    servicioxinsumo.save()

                    last_detalle_servicio = InsumoMovimientosServicio.objects.filter(
                        servicio_insumo=servicioxinsumo).order_by('fechahora').last()
                    if last_detalle_servicio:
                        existencia = last_detalle_servicio.existencia + ins_ingreso - ins_egreso
                    else:
                        existencia = ins_ingreso - ins_egreso

                    if not (ins_ingreso == 0 and ins_egreso == 0):
                        servicioxdetalle, insumoserv_created = InsumoMovimientosServicio.objects.get_or_create(
                            servicio_insumo=servicioxinsumo, comprobante=turno.id)

                        if insumoserv_created:
                            servicioxdetalle.existencia = existencia
                            servicioxdetalle.observaciones = obs
                            servicioxdetalle.egreso += ins_egreso - ins_ingreso
                        else:
                            servicioxdetalle.egreso += ins_egreso - ins_ingreso
                            servicioxdetalle.existencia = servicioxdetalle.existencia - ins_egreso + ins_ingreso

                        servicioxdetalle.save()

                        servicioxdetalles_siguientes = InsumoMovimientosServicio.objects.filter(
                            fechahora__gt=servicioxdetalle.fechahora, servicio_insumo=servicioxinsumo)
                        for detalle_ajustar in servicioxdetalles_siguientes:
                            detalle_ajustar.existencia = detalle_ajustar.existencia - ins_egreso + ins_ingreso
                            detalle_ajustar.save()
                else:
                    continue

            # deleted insumos
            for additional_insumo_k, additional_insumo_v in dict_insumos_modificados.items():
                # servicio
                servicioxinsumo, created = InsumoServicio.objects.get_or_create(servicio=turno.servicio,
                                                                                insumo=additional_insumo_k)

                servicioxdetalle = InsumoMovimientosServicio.objects.filter(servicio_insumo=servicioxinsumo,
                                                                            comprobante=turno.id).first()

                if servicioxdetalle:
                    servicioxdetalles_siguientes = InsumoMovimientosServicio.objects.filter(
                        fechahora__gt=servicioxdetalle.fechahora, servicio_insumo=servicioxinsumo)
                    for detalle_ajustar in servicioxdetalles_siguientes:
                        detalle_ajustar.existencia = detalle_ajustar.existencia + additional_insumo_v
                        detalle_ajustar.save()

                servicioxdetalle.delete()

                # tecnico
                tecnicoxinsumo = TecnicoInsumo.objects.get(tecnico=tecnico, insumo=additional_insumo_k)

                tecnicoxdetalle = InsumoDetalle.objects \
                    .filter(tecnico_insumo=tecnicoxinsumo,
                            is_turno=True,
                            comprobante=turno.id) \
                    .first()

                if tecnicoxdetalle:
                    tecnicoxdetalles_siguientes = InsumoDetalle.objects\
                        .filter(fechahora__gt=tecnicoxdetalle.fechahora, tecnico_insumo=tecnicoxinsumo)

                    for detalle_ajustar in tecnicoxdetalles_siguientes:
                        detalle_ajustar.existencia = detalle_ajustar.existencia + additional_insumo_v
                        detalle_ajustar.save()

                    tecnicoxdetalle.delete()

            EstudioSolicitado.objects.filter(turno=turno).delete()
            selectcount = int(request.POST.get('selectcount', ''))
            estudiossollist = []
            estudio_count = 0
            protocolo_cantidad = 1
            for i in range(1, selectcount + 1):
                selectname = 'serestudio_' + str(i)
                estudioselect = request.POST.get(selectname, '')
                if estudioselect != '':
                    obj_estudio_servicio = ServicioEstudio.objects.get(id=estudioselect)
                else:
                    continue

                if turno.servicio.tipo == 'CR':
                    protocolo_cantidad = request.POST.get('protocolo_cantidad' + str(i), 2)

                obj_est = EstudioSolicitado(turno=turno, servicio=turno.servicio, servicio_estudio=obj_estudio_servicio,
                                            numero_protocolo=str(i), cantidad=protocolo_cantidad)
                estudiossollist.append(obj_estudio_servicio.codigo)
                obj_est.save()

                if obj_estudio_servicio.codigo != '99':
                    estudio_count += 1
            string_estdios_sol = ''
            string_estdios_sol_codigos = ''
            estudio_prefix = turno.servicio.protocolo_prefix

            for estudi in estudiossollist:
                if estudi != '':
                    string_estdios_sol += estudio_prefix + estudi + ' '
                    string_estdios_sol_codigos += 'cKw' + estudi + 'x '

            turno.estudios_solaux = string_estdios_sol
            turno.estudios_solaux_codigos = string_estdios_sol_codigos
            if estudio_tipoterminacion == '6' or estudio_tipoterminacion == 6:
                urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo)
                return redirect(urlred)

            elif estudio_tipoterminacion == '7' or estudio_tipoterminacion == 7:
                urlred = '../../estudiocorregir/'
                return redirect(urlred)

            # PROTOCOLO PART
            turno.estudio_protocolocount = get_current_protocolo_count(turno)
            turno.estudio_protocoloprefix = get_turno_protocolo_prefix(turno)
            turno.estudio_count = estudio_count
            update_protocolo_servicio_current_count(turno, estudio_count)

            # informe part
            nro_informe_serv_prefix = turno.servicio.nro_informe_prefix
            nro_informe_serv_count = str(turno.servicio.nro_informe_count + 1)
            nro_informe = nro_informe_serv_prefix + nro_informe_serv_count

            if len(nro_informe) == 8:
                turno.informe_numero = nro_informe
            elif len(nro_informe) > 8:
                len_diff = (len(nro_informe) - 8) * -1
                nro_informe = nro_informe_serv_prefix[:len_diff] + nro_informe_serv_count
                turno.informe_numero = nro_informe
            else:
                len_diff = ((len(nro_informe) - 8) * - 1) + len(nro_informe_serv_count)
                nro_informe = nro_informe_serv_prefix + nro_informe_serv_count.zfill(len_diff)
                turno.informe_numero = nro_informe

            turno.save()
            servicio = turno.servicio
            servicio.nro_informe_count = turno.servicio.nro_informe_count + 1
            servicio.save()

            codigo_anestesia = request.POST.get("select_anestesia", '')
            if codigo_anestesia:
                turno.anestesia_codigo = codigo_anestesia
            usuario_tecnico = request.user
            turno.usuario_tecnico = usuario_tecnico
            turno.save()

            paciente_nombre = turno.paciente.nombre if turno.paciente.nombre else ''
            paciente_apellido = turno.paciente.apellido if turno.paciente.apellido else ''

            if estudio_tipoterminacion == '1' or estudio_tipoterminacion == 1:
                turno.estudio_finalizado = True
                turno.estudio_terminado = True
                paciente_apenombre = paciente_apellido + ', ' + paciente_nombre
                paciente_id = str(turno.paciente.identificacion_numero)
                turno_fecha = turno.fechahora_inicio.strftime('%d/%m/%y %H:%M')
                turno_servicio = turno.servicio.nombre
                if turno.urgencia:
                    turno_atencion = 'Urgente'
                else:
                    turno_atencion = 'Programada'
                websocket_actualizarinforme(turno.id, 'estudio_tecnico_finalizado', paciente_apenombre, turno_fecha,
                                            turno_atencion, paciente_id, turno_servicio)
            elif estudio_tipoterminacion == '2' or estudio_tipoterminacion == 2:
                turno.estudio_reprograma = True
                turno.estudio_terminado = True
            elif estudio_tipoterminacion == '3' or estudio_tipoterminacion == 3:
                turno.estudio_cancelado = True
                turno.estudio_terminado = True
            elif estudio_tipoterminacion == '4' or estudio_tipoterminacion == 4:
                turno.estudio_suspendido_paciente = True
                turno.estudio_terminado = False
            elif estudio_tipoterminacion == '5' or estudio_tipoterminacion == 5:
                turno.estudio_suspendido_completar = True
                turno.estudio_terminado = False
            estudio_partes_finalizado = request.POST.get('turno_partes_finalizado', '')
            if estudio_partes_finalizado in (1, '1'):
                if turno.turno_enpartes_primero:
                    turno.estudio_partes_finalizado = True
                elif turno.turno_relacionado:
                    turno_padre = Turno.objects.get(id=turno.turno_relacionado.id)
                    turno_padre.estudio_partes_finalizado = True
                    est_sin_realizar = not turno_padre.estudio_finalizado and \
                                       not turno_padre.estudio_suspendido_completar and \
                                       not turno_padre.estudio_suspendido_paciente
                    if est_sin_realizar:
                        turno_padre.habilitado = True
                        turno_padre.turno_recepcion_suspendido = False
                        turno_padre.estudio_finalizado = True
                        turno_padre.estudio_terminado = True
                        turno_padre.informe_numero = turno.informe_numero
                        turno_padre.usuario_tecnico = turno.usuario_tecnico
                        turno_padre.estudio_observacion_nota = turno.estudio_observacion_nota
                        turno_padre.estudio_observacion_medico = turno.estudio_observacion_medico

                        estudios_sol = EstudioSolicitado.objects.filter(turno=turno_padre)
                        if estudios_sol:
                            estudios_sol.delete()

                        turno_padre.estudios_solaux = ''
                        turno_padre.estudios_solaux_codigos = ''
                        turno_padre.estudio_finalizado_sistema = True

                    turno_padre.save()

            turno.save()
            fechafin_ajustetimezone = turno.fechahora_fin.astimezone(pytz.timezone("America/Argentina/Salta"))
            layer = get_channel_layer()
            async_to_sync(layer.group_send)('chat_turnobrightspeed', {
                'type': 'mensaje_turno',
                'message': turno.id,
                'message_type': 'mover',
                'message_user': turno.usuario_creacion.username,
                'message_servicio': turno.servicio.codigo,
                'message_body': 'na',
                'turno_id': turno.id,
                'turno_resofix': 'na',
                'turno_fechaini2': 'na',
                'turno_fechafin2': 'na',
                'turno_fechaini': turno.fechahora_inicio.strftime('%Y-%m-%d %H:%M'),
                'turno_fechafin': fechafin_ajustetimezone.strftime('%Y-%m-%d %H:%M'),
                'turno_presente': turno.presente,
                'turno_confirmacion': turno.confirmacion,
                'turno_pacientearm': 'na',
                'turno_estudio_terminado': turno.estudio_terminado,
                'turno_whatsapp': turno.whatsapp,
                'turno_anestesia': turno.anestesia,
                'turno_cev': turno.contrasteev,
                'turno_cvo': turno.contrastevo,
                'turno_agua': turno.tomaagua,
                'turno_urgencia': turno.urgencia,
                'turno_internado': turno.internado,
                'turno_suspendido': turno.turno_recepcion_suspendido,
                'turno_nombre': paciente_nombre.upper(),
                'turno_apellido': paciente_apellido.upper(),
                'turno_documento': turno.paciente.identificacion_numero,
                'turno_establecimiento': turno.establecimiento_derivador.abreviatura,
                'turno_codigos': turno.estudios_solaux_codigos,
                'turno_codigoscompleto': 'na',
                'turno_sexo': 'na',
                'turno_fotopm': 'na',
                'turno_fotodoc1': 'na',
                'turno_fotodoc2': 'na',
                'turno_pacienteid': 'na'
            })
            urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo)

            return redirect(urlred)
        else:
            urlred = '../listadoestudiossolicitados/' + str(turno.servicio.codigo) + "/?error=1"
            return redirect(urlred)

    def get_context_data(self, **kwargs):
        context = super(ProtocoloAlta, self).get_context_data(**kwargs)
        ip_request = self.request.META['REMOTE_ADDR']
        turno = context['turno']

        context['nro_protocolo_prefix'] = get_turno_protocolo_prefix(turno)
        context['nro_protocolo_count'] = get_current_protocolo_count(turno)
        context['protocolo_estado'] = get_protocolo_estado(turno)
        context['protocolo_valid_date'] = check_valid_turno_date_protocolo(turno)
        context['estudios_corregir'] = Turno.objects.filter(informe_tec_corregir=True, servicio=turno.servicio)
        context['anestesias'] = ServicioAnestesia.objects.filter(servicio=turno.servicio)
        context['estudios'] = ServicioEstudio.objects.filter(servicio=turno.servicio)

        turno_en_partes = False
        turno_padre = False

        if turno.tipomodalidad == 'En partes':
            turno_padre = True

        if turno.turno_relacionado or turno.tipomodalidad == 'En partes':
            turno_en_partes = True

        turnos_hijos = {}
        if turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(Q(turno_relacionado=turno) | Q(id=turno.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")
        elif not turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(
                Q(turno_relacionado=turno.turno_relacionado) | Q(id=turno.turno_relacionado.id)).exclude(
                habilitado=False).order_by("fechahora_inicio")

        context['turno_padre'] = turno_padre
        context['turnos_hijos'] = turnos_hijos
        context['turno_en_partes'] = turno_en_partes

        urls_imagenes, codigos_links, obs_tecnico = get_codigos_linksimages_obs(turno, ip_request)
        context['urls_imagenes'] = urls_imagenes
        context['codigos_realizados_links'] = codigos_links
        context['obs_tecnico'] = obs_tecnico

        return context


@login_required
@transaction.atomic
def protocolo_ajuste_numeracion(request):
    if request.method == 'POST':
        response_data = {'ajuste_realizado': False}
        ajuste_valor = request.POST.get('ajuste_valor', '')
        turno_id = request.POST.get('turnoid', '')

        if turno_id == '' or ajuste_valor == '':
            return JsonResponse(response_data)

        turno = Turno.objects.get(id=turno_id)
        turno.estudio_protocolocount += int(ajuste_valor)
        turno.save()
        response_data['ajuste_realizado'] = True
    else:
        response_data = {'ajuste_realizado': False}

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def protocolo_ajuste_agregar_codigo(request):
    if request.method == 'POST':
        response_data = {'ajuste_realizado': False}
        codigo_estudio_id = request.POST.get('codigo_estudio_id', '')
        turno_id = request.POST.get('turnoid', '')

        if turno_id == '' or codigo_estudio_id == '':
            return JsonResponse(response_data)

        turno = Turno.objects.get(id=turno_id)
        servicio = turno.servicio
        estudio_servicio = ServicioEstudio.objects.get(id=codigo_estudio_id)
        obj_estudio_solicitado = EstudioSolicitado(turno=turno, servicio=servicio, servicio_estudio=estudio_servicio)
        obj_estudio_solicitado.save()
        string_estdios_sol = servicio.protocolo_prefix + estudio_servicio.codigo.zfill(2) + ' '
        turno.estudios_solaux += string_estdios_sol
        string_estdios_sol_codigos = 'cKw' + estudio_servicio.codigo + 'x '
        turno.estudios_solaux_codigos += string_estdios_sol_codigos
        turno.save()

        response_data['ajuste_realizado'] = True
    else:
        response_data = {'ajuste_realizado': False}

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def protocolo_ajuste_remover_codigo(request):
    if request.method == 'POST':
        response_data = {'ajuste_realizado': False}
        codigo_estudio_id = request.POST.get('codigo_estudio_id', '')
        turno_id = request.POST.get('turnoid', '')

        if turno_id == '' or codigo_estudio_id == '':
            return JsonResponse(response_data)

        turno = Turno.objects.get(id=turno_id)
        servicio = turno.servicio
        estudio_servicio = ServicioEstudio.objects.get(id=codigo_estudio_id)
        obj_estudio_solicitado = EstudioSolicitado.objects.filter(turno=turno, servicio=servicio,
                                                                  servicio_estudio=estudio_servicio).first()
        if not obj_estudio_solicitado:
            return JsonResponse(response_data)

        obj_estudio_solicitado.delete()
        string_estdios_sol = servicio.protocolo_prefix + estudio_servicio.codigo.zfill(2) + ' '
        turno.estudios_solaux = turno.estudios_solaux.replace(string_estdios_sol, '', 1)
        string_estdios_sol_codigos = 'cKw' + estudio_servicio.codigo + 'x '
        turno.estudios_solaux_codigos = turno.estudios_solaux_codigos.replace(string_estdios_sol_codigos, '', 1)
        turno.save()

        response_data['ajuste_realizado'] = True
    else:
        response_data = {'ajuste_realizado': False}

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def protocolo_ajuste_numeracion_servicio(request):
    if request.method == 'POST':
        response_data = {'ajuste_realizado': False}
        ajuste_valor = request.POST.get('ajuste_valor', '')
        servicio_id = request.POST.get('servicio_id', '')

        if servicio_id == '' or ajuste_valor == '' or int(ajuste_valor) < 0:
            return JsonResponse(response_data)

        servicio = Servicio.objects.get(id=servicio_id)
        servicio.protocolo_count = ajuste_valor
        servicio.save()
        response_data['ajuste_realizado'] = True
    else:
        response_data = {'ajuste_realizado': False}

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def protocolo_cambiar_estado(request):
    """
        Updates the estudio protocolo state
        estado values:
            1: Finalizado
            2: Suspendido
            3: Cancelado

        :param request: turnoId and estado
        :return: Json operation results
    """
    response_data = {
        'estado_cambiado': False,
        'message': 'Error: No se pudo completar la acción solicitada.'
    }
    is_authz_user = request.user.userprofile.user_jefe_tecnico or request.user.userprofile.perfil == 'Administrador'

    if request.method == 'POST' and is_authz_user:
        turno_id = request.POST.get('turnoId', '')
        estado = request.POST.get('estado', '')

        if turno_id == '' or estado not in ('1', '2', '3', '4', '5'):
            response_data['message'] = 'Error: Parámetros Inválidos.'
            return JsonResponse(response_data)
        turno = Turno.objects.filter(id=turno_id).first()

        if not turno and not turno.informe_finalizado:
            response_data['message'] = 'Error: No se puede cambiar el estado de un estudio con informe finalizado.'
            return JsonResponse(response_data)

        turno.estudio_finalizado = False
        turno.estudio_suspendido_paciente = False
        turno.estudio_suspendido_completar = False
        turno.estudio_reprograma = False
        turno.estudio_cancelado = False
        turno.estudio_terminado = False

        if estado == '1':
            turno.estudio_finalizado = True
            turno.estudio_terminado = True
        elif estado == '2':
            turno.estudio_suspendido_paciente = True
        elif estado == '3':
            turno.estudio_suspendido_completar = True
        elif estado == '4':
            turno.estudio_reprograma = True
            turno.estudio_terminado = True
        elif estado == '5':
            turno.estudio_cancelado = True
            turno.estudio_terminado = True

        turno.save()

        response_data['estado_cambiado'] = True
        response_data['message'] = ''

    return JsonResponse(response_data)


def imprimir_fichatecnica(turno_id):
    turno = Turno.objects.get(id=turno_id)

    if turno.turno_relacionado and turno.servicio.tipo == 'MR' and turno.estudio_finalizado:
        turno = turno.turno_relacionado
        turnos_todos = Turno.objects.filter(turno_relacionado=turno, estudio_finalizado=True).order_by(
            'fechahora_inicio') | Turno.objects.filter(id=turno.id, estudio_finalizado=True)
    elif turno.tipomodalidad == 'En partes' and turno.servicio.tipo == 'MR' and turno.estudio_finalizado:
        turnos_todos = Turno.objects.filter(turno_relacionado=turno, estudio_finalizado=True).order_by(
            'fechahora_inicio') | Turno.objects.filter(id=turno.id, estudio_finalizado=True)
    else:
        turnos_todos = Turno.objects.filter(id=turno.id)

    turno_fecha_last = turnos_todos.last().fechahora_inicio
    turno_fecha_ingreso_last = turnos_todos.last().estudio_fechahora_ingreso.astimezone(
        pytz.timezone("America/Argentina/Salta")).strftime('%H:%M')
    buffer = io.BytesIO()
    h = A4[1]
    w = A4[0]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/static_tecnico/protocolo/img/inf_tec_j.jpg'
    im = Image(logo, 595, 840)
    im.drawOn(c, 0, 0)
    c.setFont('Helvetica-Bold', 9)
    servicio = 'Servicio: ' + turno.servicio.nombre
    c.drawString(16, h - 65, servicio)
    c.setFont('Helvetica', 9)
    if turno.estudio_fechahora_ingreso:
        hora_manual = turno_fecha_ingreso_last
    else:
        hora_manual = ''
    hora_realiz = 'Hora de realización: ' + hora_manual
    c.drawString(228, h - 61, hora_realiz)
    nros_orden = ''
    for tur in turnos_todos:
        nros_orden += str(tur.id) + ' - '
    num_ord = 'Nº Orden: ' + nros_orden
    c.setFont('Helvetica-Bold', 9)
    c.drawString(16, h - 55, num_ord)
    c.setFont('Helvetica', 8)
    num_inf = 'Nº Informe: ' + turno.informe_numero
    c.setFont('Helvetica-Bold', 12)
    c.drawString(425, h - 65, num_inf)
    c.setFont('Helvetica', 8)
    fec = 'Fecha: ' + turno_fecha_last.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%d/%m/%Y')
    c.drawString(w - 80, h - 36, fec)
    hora = 'Hora: ' + turno_fecha_last.astimezone(pytz.timezone("America/Argentina/Salta")).strftime('%H:%M')
    c.drawString(w - 80, h - 46, hora)
    if turno.paciente.paciente_nn:
        pacnomb = turno.paciente.paciente_nnlabel
        dni = turno.paciente.identificacion_tipo + ': ---'

    else:
        pacnomb = turno.paciente.nombrecompleto.upper()
        dni = turno.paciente.identificacion_tipo + ': ' + turno.paciente.identificacion_numero

    pac = 'Paciente: ' + pacnomb
    c.drawString(18, h - 79, pac)
    c.drawString(270, h - 79, dni)
    pacfec = turno.paciente.fecnac.strftime('%d/%m/%Y') if turno.paciente.fecnac else "---"
    fec_nac = 'Fecha nacimiento: ' + pacfec
    c.drawString(385, h - 79, fec_nac)
    edad = relativedelta(datetime.datetime.now().date(), turno.paciente.fecnac)
    edadyear = edad.years
    edadmonth = edad.months
    edad = 'Edad(A-M): ' + str(edadyear) + '-' + str(edadmonth)
    c.drawString(w - 93, h - 79, edad)
    pacdom = turno.paciente.domicilio if turno.paciente.domicilio else '---'
    dom = 'Domicilio: ' + pacdom
    c.drawString(18, h - 94, dom)
    pactec = turno.paciente.telefono if turno.paciente.telefono else '---'
    tel = 'Teléfono: ' + pactec
    c.drawString(w - 153, h - 94, tel)
    obra = 'Obra social: (' + str(turno.obrasocial.codigo) + ') - ' + turno.obrasocial.nombre
    c.drawString(18, h - 108, obra)
    if turno.establecimiento_derivador.nombre == 'AMBULATORIO':
        procedencia = '---'
    else:
        procedencia = turno.establecimiento_derivador.nombre
    proced = "Procedencia: " + procedencia
    c.drawString(18, h - 125, proced)
    nombre_completo = ''
    if turno.medico_derivante.apellido:
        nombre_completo += turno.medico_derivante.apellido + ', '
    if turno.medico_derivante.nombre:
        nombre_completo += turno.medico_derivante.nombre

    med_sol = 'Médico solicitante: ' + turno.medico_derivante.matricula + ' - ' + nombre_completo
    c.drawString(18, h - 140, med_sol)
    if turno.internado:
        estado = 'Internado'
    else:
        estado = 'Ambulatorio'

    estado = "Estado: " + estado
    c.drawString(18, h - 157, estado)
    if turno.urgencia:
        atencion = 'Urgencia'
    else:
        atencion = 'Programada'
    ai = 'Atenc.: ' + atencion
    c.drawString(170, h - 157, ai)
    tec = 'Técnico: ' + turno.usuario_tecnico.last_name.capitalize() + ', ' + turno.usuario_tecnico.first_name.capitalize()
    c.drawString(245, h - 157, tec)
    tec = 'Observaciones: ' + turno.estudio_observacion_nota
    c.drawString(18, h - 184, tec)
    if turno.estudio_finalizado:
        finalizacion = 'ESTUDIO FINALIZADO'
    elif turno.estudio_suspendido_completar:
        finalizacion = 'ESTUDIO SUSPENDIDO'
    elif turno.estudio_suspendido_paciente:
        finalizacion = 'ESTUDIO SUSPENDIDO'
    elif turno.estudio_terminado:
        finalizacion = 'ESTUDIO CANCELADO'
    else:
        finalizacion = ''

    c.setFont('Helvetica-Bold', 12)
    c.drawString(410, h - 170, finalizacion)
    baseline = 215
    linejump = 0
    c.setFont('Helvetica', 8)

    for turno_session in turnos_todos:
        estudios_sol = EstudioSolicitado.objects.filter(turno=turno_session)
        aux_counter = 0
        for est in estudios_sol:
            aux_counter += 1
            protocolo_prefix = turno_session.estudio_protocoloprefix
            protocolo_counter = aux_counter
            protocolo_sufix = int(protocolo_counter) + int(turno_session.estudio_protocolocount)
            if est.servicio_estudio.codigo == '99':
                nro_protocolo = '---'
                aux_counter -= 1
            else:
                nro_protocolo = protocolo_prefix + str(protocolo_sufix).zfill(4)

            c.drawString(18, h - baseline - linejump, nro_protocolo)
            estudio_codigo = est.servicio_estudio.codigo.zfill(2)
            estudio_prefix = est.servicio_estudio.servicio.protocolo_prefix
            estudio_nombre = est.servicio_estudio.nombre
            estudio_descripcion = estudio_prefix + estudio_codigo + ' - ' + estudio_nombre
            c.drawString(75, h - baseline - linejump, estudio_descripcion)
            c.drawString(323, h - baseline - linejump, str(est.cantidad))
            linejump += 10

        if turno_session.anestesia:
            c.drawString(18, h - baseline - linejump, 'Anestesia')
            obj_servicio_anestesia = ServicioAnestesia.objects.filter(codigo=turno_session.anestesia_codigo).first()
            if obj_servicio_anestesia:
                anestesia_description = obj_servicio_anestesia.codigo + ' - ' + obj_servicio_anestesia.nombre
                c.drawString(75, h - baseline - linejump, anestesia_description.title())
            c.drawString(323, h - baseline - linejump, '1')
            linejump += 10

    insumos_utilizados = TurnoInsumo.objects.filter(turno=turno)
    baseline_ins = 215
    linejump_ins = 0
    for insumoxturno in insumos_utilizados:
        insumo_codigo = insumoxturno.insumo.codigo
        c.drawString(340, h - baseline_ins - linejump_ins, insumo_codigo)
        insumo_nombre = insumoxturno.insumo.nombre
        c.drawString(370, h - baseline_ins - linejump_ins, insumo_nombre)
        insumo_cantidad = str(insumoxturno.cantidad_usada)
        c.drawString(536, h - baseline_ins - linejump_ins, insumo_cantidad)
        linejump_ins += 10
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename='test'),
                            content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename=' + 'test' + '.pdf'
    return response


@login_required
def solicitar_imprimirficha(request):
    turno_id = request.GET.get('turnoid', '')
    response = imprimir_fichatecnica(turno_id)

    return response


class EstudioCorregir(ListView):
    model = Turno
    template_name = 'protocolo/protocolo_list_corregir.html'

    def get_queryset(self):
        queryset = Turno.objects.filter(estudio_corregir=True)
        return queryset


@login_required
def marcar_estudio_corregido(request):
    try:
        turno_id = request.GET['turno_id']
        turno = Turno.objects.get(id=turno_id)
        turno.estudio_corregir = False
        turno.estudio_corregir_texto = ""
        turno.save()
        response_data = {"exito", "Marcado como corregido"}
        return JsonResponse(response_data)
    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def detalle_insumos_imprimir(request):
    tecnicoid = request.GET.get('tecnicoid', '')
    tecnico = User.objects.get(id=tecnicoid)

    nom_tecnico = tecnico.last_name + ', ' + tecnico.first_name
    filename = datetime.datetime.now().strftime('%Y/%m/%d') + '_' + nom_tecnico + '.pdf'
    buffer = io.BytesIO()
    w = A4[0]
    h = A4[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)
    c.setFont('Helvetica', 14)
    c.drawString((w / 2) - 130, h - 1.2 * cm, "DETALLE DE EXISTENCIAS DE INSUMO POR TÉCNICO")
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm
    c.line(x, y + 2, xfin, y + 2)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 28, "Técnico:")
    c.setFont('Helvetica', 18)
    c.drawString(x_imagen + 61, y_imagen - 28, nom_tecnico)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 250, y_imagen - 28, "Fecha: ")
    c.setFont('Helvetica', 15)
    server_timezone = pytz.timezone("America/Argentina/Salta")
    f = datetime.datetime.now().astimezone(server_timezone)
    dias = {"Monday": "Lunes", "Tuesday": "Martes", "Wednesday": "Miércoles", "Thursday": "Jueves", "Friday": "Viernes",
            "Saturday": "Sábado", "Sunday": "Domingo"}

    fe_str = dias[f.strftime("%A")] + ", " + f.strftime("%d/%m/%Y")
    c.drawString(w - 204, y_imagen - 28, fe_str)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_imagen - 49, "3: Medios de contraste")
    styles = getSampleStyleSheet()
    styleBH = styles["Heading4"]
    styleBH.alignment = TA_CENTER
    cod = Paragraph('''CÓD.''', styleBH)
    desc = Paragraph('''DESCRIPCIÓN''', styleBH)
    unid = Paragraph('''UNID.''', styleBH)
    ingresos = Paragraph('''INGRESOS''', styleBH)
    egresos = Paragraph('''EGRESOS''', styleBH)
    saldo = Paragraph('''SALDO''', styleBH)
    contraste_data = [[cod, desc, unid, ingresos, egresos, saldo]]
    normal_data = [[cod, desc, unid, ingresos, egresos, saldo]]
    y_tabla_contraste, y_tabla_normal = 720, 0

    insumo = Insumo.objects.all()
    for ins in insumo:
        tecnico_insumo = TecnicoInsumo.objects.filter(tecnico=tecnico, insumo=ins)
        for ti in tecnico_insumo:
            egresos, ingresos = 0, 0
            detalle = InsumoDetalle.objects.filter(tecnico_insumo=ti)
            for det in detalle:
                egresos -= det.egreso
                ingresos += det.ingreso
            if ins.grupo == '3':
                contraste_data.append([ins.codigo, ins.nombre, ins.unidad_tipo, float("%.2f"%ingresos), float("%.2f"%egresos), float("%.2f"%ti.stock)])
                y_tabla_contraste -= 18
            else:
                normal_data.append([ins.codigo, ins.nombre, ins.unidad_tipo, float("%.2f"%ingresos), float("%.2f"%egresos), float("%.2f"%ti.stock)])
                y_tabla_normal += 18

    width, height = A4
    contraste_table = Table(contraste_data, colWidths=[1.5 * cm, 9 * cm, 2 * cm, 2.5 * cm, 2.5 * cm, 2 * cm],
                            repeatRows=1)
    contraste_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    contraste_table.wrapOn(c, width, height)
    contraste_table.drawOn(c, 30, y_tabla_contraste)

    y_tabla_contraste -= 25
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_tabla_contraste, "6: Accesorios y descartables")
    y_tabla_contraste -= 25

    normal_table = Table(normal_data, colWidths=[1.5 * cm, 9 * cm, 2 * cm, 2.5 * cm, 2.5 * cm, 2 * cm], repeatRows=1)
    normal_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    normal_table.wrapOn(c, width, height)
    normal_table.drawOn(c, 30, y_tabla_contraste - y_tabla_normal)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=filename),
                            content_type='application/pdf')
    return response


@login_required
@transaction.atomic
def entregar_insumo(request):
    response_data = {'success': False}

    tecnicoid = request.GET.get('tecnico-id', '')
    tecnico = User.objects.get(id=tecnicoid)
    cantidad_total_insumos = request.GET.get('cantidad-total-insumos', '')
    obs = ', observación: ' + request.GET.get('entrega-obs', '') if request.GET.get('entrega-obs', '') else ''
    obs_raw = request.GET.get('entrega-obs', '')

    config_generales = ConfiguracionGeneral.objects.all().first()
    config_generales.remito_egreso_numeracion += 1
    config_generales.save()

    remito_ingreso_tecnico = RemitoEntregaTecnico(
        tecnico=tecnico,
        observaciones=obs_raw,
        comprobante_numero=config_generales.remito_egreso_numeracion
    )
    remito_ingreso_tecnico.save()

    for i in range(1, int(cantidad_total_insumos) + 1):
        insumocodigo = request.GET.get('insumo-' + str(i), '')
        cantidad = request.GET.get('cantidad-' + str(i), '')
        lote_id = request.GET.get('lote-' + str(i), '')

        if tecnicoid and insumocodigo and cantidad:
            insumo = Insumo.objects.get(codigo=insumocodigo)
            cantidad = float(cantidad)
            cantidad_x_unidad = float(cantidad) * float(insumo.unidad)

            insumo_tecnico, created = TecnicoInsumo.objects.get_or_create(insumo=insumo, tecnico=tecnico)
            cantidad_nuevo = insumo_tecnico.stock + cantidad_x_unidad
            insumo_tecnico.stock = cantidad_nuevo
            insumo_tecnico.save()
            if lote_id:
                lote = InsumoLote.objects.get(id=lote_id)
                lote.egresos += float(cantidad)
                lote.saldo -= float(cantidad)
                lote.save()
            else:
                lote = None

            detalle_remito = InsumoRemitoEntrega(
                remito_entrega=remito_ingreso_tecnico,
                insumo_tecnico=insumo_tecnico,
                lote=lote,
                cantidad=cantidad
            )
            detalle_remito.save()

            ins_detalle_movimiento = InsumoDetalle(
                insumo_remito_entrega=detalle_remito,
                tecnico_insumo=insumo_tecnico,
                is_turno=False,
                is_entrega_ins_tecnico=True,
                ingreso=cantidad_x_unidad,
                egreso=float(0),
                existencia=cantidad_nuevo,
                fechahora=datetime.datetime.now(),
                observaciones='Entrega de Insumo por ' + request.user.username + obs
            )
            ins_detalle_movimiento.save()
            insumo_stock_final = insumo.stock_almacen - cantidad if insumo.stock_almacen else cantidad * -1
            insumo.stock_almacen = insumo_stock_final
            insumo.save()

    response_data["success"] = True
    response_data["remito_entrega_id"] = remito_ingreso_tecnico.id

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def ajustar_insumo(request):
    response_data = {'success': False}

    if not ('Stock' in request.user.userprofile.perfil or 'Administrador' in request.user.userprofile.perfil):
        return JsonResponse(response_data)

    tecnicoid = request.GET.get('tecnico-id', '')
    tecnico = User.objects.get(id=tecnicoid)
    cantidad_total_insumos = request.GET.get('cantidad-total-insumos', '')
    obs = ', observación: ' + request.GET.get('entrega-obs', '') if request.GET.get('entrega-obs', '') else ''

    for i in range(1, int(cantidad_total_insumos) + 1):
        insumocodigo = request.GET.get('insumo-' + str(i), '')
        cantidad = request.GET.get('cantidad-' + str(i), '')

        if tecnicoid and insumocodigo and cantidad:
            insumo = Insumo.objects.get(codigo=insumocodigo)
            cantidad = float(cantidad)

            insumo_tecnico, created = TecnicoInsumo.objects.get_or_create(insumo=insumo, tecnico=tecnico)
            diferencia_stock = cantidad - insumo_tecnico.stock
            insumo_tecnico.stock = cantidad
            insumo_tecnico.save()
            ingreso = float(0)
            egreso = float(0)

            if diferencia_stock > 0:
                ingreso = diferencia_stock
            elif diferencia_stock < 0:
                egreso = diferencia_stock * -1

            ins_detalle_movimiento = InsumoDetalle(
                tecnico_insumo=insumo_tecnico,
                is_turno=False,
                is_entrega_ins_tecnico=False,
                ingreso=ingreso,
                egreso=egreso,
                existencia=cantidad,
                fechahora=datetime.datetime.now(),
                observaciones='Ajuste de Stock por ' + request.user.username + ' ' + obs
            )
            ins_detalle_movimiento.save()

    response_data["success"] = True

    return JsonResponse(response_data)

def remito_entrega_insumos_pdf(request):
    remito_entrega_id = request.GET.get('remitoentregaid', '')
    remito_entrega = RemitoEntregaTecnico.objects.get(id=remito_entrega_id)

    tecnico = remito_entrega.tecnico
    nom_tecnico = tecnico.last_name + ', ' + tecnico.first_name

    filename = datetime.datetime.now().strftime('%Y/%m/%d') + '_Entrega Insumos_' + nom_tecnico + '.pdf'
    buffer = io.BytesIO()
    w = A4[0]
    h = A4[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)
    c.setFont('Helvetica-Bold', 10)
    c.drawString(w - 150, h - 1.8 * cm, "RIS TCSE - Módulo Farmacia")
    c.setFont('Helvetica', 14)
    c.drawString((w / 2) - 130, h - 1.2 * cm, "REMITO INTERNO DE PROVISIÓN (TÉCNICO)")
    c.setFont('Helvetica', 12)
    num_remito = remito_entrega.comprobante_numero
    c.drawString((w / 2) - 130, h - 1.8 * cm, "Comprobante Nº: " + str(num_remito))
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm
    c.line(x, y + 2, xfin, y + 2)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 28, "Técnico:")
    c.setFont('Helvetica', 18)
    c.drawString(x_imagen + 61, y_imagen - 28, nom_tecnico)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 250, y_imagen - 28, "Fecha: ")
    c.drawString(w - 250, y_imagen - 45, "Fecha Entrega: ")

    c.setFont('Helvetica', 15)
    server_timezone = pytz.timezone("America/Argentina/Salta")
    f = remito_entrega.fecha.astimezone(server_timezone)
    dias = {"Monday": "Lunes", "Tuesday": "Martes", "Wednesday": "Miércoles", "Thursday": "Jueves", "Friday": "Viernes",
            "Saturday": "Sábado", "Sunday": "Domingo"}

    fe_str = dias[f.strftime("%A")] + ", " + f.strftime("%d/%m/%Y")
    c.drawString(w - 204, y_imagen - 28, fe_str)

    fe_entrega_str = dias[remito_entrega.fecha.strftime("%A")] + ", " + remito_entrega.fecha.strftime("%d/%m/%Y")
    c.drawString(w - 150, y_imagen - 45, fe_entrega_str)

    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_imagen - 64, "3: Medios de contraste")
    styles = getSampleStyleSheet()
    styleBH = styles["Heading4"]
    styleBH.alignment = TA_CENTER
    cod = Paragraph('''CÓD.''', styleBH)
    desc = Paragraph('''DESCRIPCIÓN''', styleBH)
    unid = Paragraph('''UNID.''', styleBH)
    lote = Paragraph('''LOTE''', styleBH)
    fec_vto = Paragraph('''VTO.''', styleBH)
    cantidad = Paragraph('''CANT.''', styleBH)
    contraste_data = [[cod, desc, unid, lote, fec_vto, cantidad]]
    normal_data = [[cod, desc, unid, lote, fec_vto, cantidad]]
    y_tabla_contraste, y_tabla_normal = 705, 0
    detalle_remito_entrega = InsumoRemitoEntrega.objects.filter(remito_entrega=remito_entrega)

    for item in detalle_remito_entrega:
        lote = item.lote.lote if item.lote else '---'
        lote_fecha_venc = item.lote.fecha_vencimiento.strftime(
            '%d/%m/%Y') if lote != '---' and item.lote.fecha_vencimiento else '---'
        if item.insumo_tecnico.insumo.grupo == '3':
            contraste_data.append([item.insumo_tecnico.insumo.codigo, item.insumo_tecnico.insumo.nombre,
                                   item.insumo_tecnico.insumo.unidad_tipo, lote, lote_fecha_venc, item.cantidad])
            y_tabla_contraste -= 18
        else:
            normal_data.append([item.insumo_tecnico.insumo.codigo, item.insumo_tecnico.insumo.nombre,
                                item.insumo_tecnico.insumo.unidad_tipo, lote, lote_fecha_venc, item.cantidad])
            y_tabla_normal += 18
    width, height = A4
    contraste_table = Table(contraste_data, colWidths=[1.5 * cm, 9 * cm, 1.5 * cm, 3.5 * cm, 2 * cm, 2 * cm],
                            repeatRows=1)
    contraste_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    contraste_table.wrapOn(c, width, height)
    contraste_table.drawOn(c, 30, y_tabla_contraste)

    y_tabla_contraste -= 25
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_tabla_contraste, "6: Accesorios y descartables")
    y_tabla_contraste -= 25

    normal_table = Table(normal_data, colWidths=[1.5 * cm, 9 * cm, 1.5 * cm, 3.5 * cm, 2 * cm, 2 * cm], repeatRows=1)
    normal_table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    normal_table.wrapOn(c, width, height)
    normal_table.drawOn(c, 30, y_tabla_contraste - y_tabla_normal)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=filename),
                            content_type='application/pdf')
    return response


@login_required
def remito_ingreso_proveedor_detalle(request):
    remito_id = request.GET.get('remitoid', '')
    remito = RemitoIngreso.objects.get(id=remito_id)
    filename = 'Remito_ingreso_' + str(remito.id) + '.pdf'
    buffer = io.BytesIO()
    w = A4[0]
    h = A4[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)
    titulo_comprobante = 'REMITO INGRESO DE INSUMOS'
    c.setFont('Helvetica', 14)
    c.drawString((w / 2) - 130, h - 1.2 * cm, titulo_comprobante)
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm
    c.line(x, y + 2, xfin, y + 2)
    line_y = 28
    linejump = 12
    c.setFont('Helvetica-Bold', 11)
    c.drawString(x_imagen + 4, y_imagen - line_y, "Comprobante Nº:")
    c.setFont('Helvetica', 11)
    c.drawString(x_imagen + 95, y_imagen - line_y, str(remito.id))
    c.setFont('Helvetica-Bold', 11)
    c.drawString(w - 250, y_imagen - line_y, "Fecha:")
    c.setFont('Helvetica', 11)
    comprobante_fecha = remito.fecha.strftime("%d/%m/%Y")
    c.drawString(w - 210, y_imagen - line_y, comprobante_fecha)
    line_y += linejump
    c.setFont('Helvetica-Bold', 11)
    c.drawString(x_imagen + 4, y_imagen - line_y, "Tipo Ingreso: ")
    c.setFont('Helvetica', 11)
    c.drawString(x_imagen + 75, y_imagen - line_y, remito.tipo_ingreso)

    if remito.tipo_ingreso == 'Orden de Compra':
        c.setFont('Helvetica-Bold', 11)
        c.drawString(w - 250, y_imagen - line_y, "Orden Compra Nº:")
        orden_compra = remito.orden_compra_numero if remito.orden_compra_numero else '---'
        c.setFont('Helvetica', 11)
        c.drawString(w - 151, y_imagen - line_y, orden_compra)
        line_y += linejump

        c.setFont('Helvetica-Bold', 11)
        c.drawString(x_imagen + 4, y_imagen - line_y, "Proveedor: ")
        c.setFont('Helvetica', 11)
        proveedor_razonsocial = Proveedor.objects.get(id=remito.proveedor.id).nombre
        c.drawString(x_imagen + 65, y_imagen - line_y, proveedor_razonsocial)

    if remito.tipo_ingreso == 'Orden de Compra':
        c.setFont('Helvetica-Bold', 11)
        c.drawString(w - 250, y_imagen - line_y, "Remito Prov. Nº:")
        remito_proveedor_numero = remito.remito_proveedor_numero if remito.remito_proveedor_numero else '---'
        c.setFont('Helvetica', 11)
        c.drawString(w - 161, y_imagen - line_y, remito_proveedor_numero)

    line_y += linejump
    c.setFont('Helvetica-Bold', 11)
    c.drawString(x_imagen + 4, y_imagen - line_y, "Obs.:")
    obs = remito.observaciones if remito.observaciones else '---'
    c.setFont('Helvetica', 11)
    c.drawString(x_imagen + 34, y_imagen - line_y, obs)
    line_y += linejump + 12
    c.setFont('Helvetica-Bold', 13)
    c.drawString(15, y_imagen - line_y, "Detalle de insumos")
    styles = getSampleStyleSheet()
    styleBH = styles["Heading4"]
    styleBH.alignment = TA_CENTER
    codigo = Paragraph('''COD.''', styleBH)
    insumo = Paragraph('''INSUMO''', styleBH)
    unidades = Paragraph('''UNID.''', styleBH)
    costo_unitario = Paragraph('''C/U.''', styleBH)
    importe = Paragraph('''IMPORTE''', styleBH)
    lote = Paragraph('''LOTE''', styleBH)
    detalle_data = [[codigo, insumo, unidades, costo_unitario, importe, lote]]
    items_lenght = 25
    remito_insumos = InsumoRemitoIngreso.objects.filter(remito=remito)
    for remito_insumo in remito_insumos:
        insumo = remito_insumo.insumo
        detalle_data.append(
            [insumo.codigo, insumo.nombre, remito_insumo.unidades, remito_insumo.costo_unitario, remito_insumo.importe,
             remito_insumo.lote])
        items_lenght += 18

    width, height = A4
    contraste_table = Table(detalle_data, colWidths=[1.5 * cm, 10 * cm, 1.5 * cm, 1.5 * cm, 2.2 * cm, 3 * cm],
                            repeatRows=1)
    contraste_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))
    contraste_table.wrapOn(c, width, height)
    contraste_table.drawOn(c, 15, y_imagen - line_y - items_lenght)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=filename),
                            content_type='application/pdf')
    return response


@login_required()
@transaction.atomic()
def cerrar_estudio_con_informe(request):
    response = {'estudio_cerrado': False}
    turnoid = request.GET.get('turnoid', '')
    if turnoid != '':
        turno = Turno.objects.get(id=turnoid)
        if turno.tipomodalidad == 'En partes':
            turno.estudio_partes_finalizado = True
            turno.estudio_finalizado = True
            turno.estudio_terminado = True
            turno.estudio_suspendido_completar = False
            turno.estudio_suspendido_paciente = False
            turno.estudio_cancelado = False
            turno.estudio_reprograma = False
            response['estudio_cerrado'] = True
        elif turno.turno_relacionado:
            turno_padre = turno.turno_relacionado
            turno_padre.estudio_partes_finalizado = True
            turno_padre.estudio_finalizado = True
            turno_padre.estudio_terminado = True
            turno_padre.estudio_suspendido_completar = False
            turno_padre.estudio_suspendido_paciente = False
            turno_padre.estudio_cancelado = False
            turno_padre.estudio_reprograma = False
            turno_padre.save()
            response['estudio_cerrado'] = True
        else:
            turno.estudio_finalizado = True
            turno.estudio_terminado = True
            turno.estudio_suspendido_completar = False
            turno.estudio_suspendido_paciente = False
            turno.estudio_cancelado = False
            turno.estudio_reprograma = False
            response['estudio_cerrado'] = True
        turno.save()

    return JsonResponse(response)


@login_required
def proveedor_listar(request):
    if request.is_ajax:
        search_term = request.GET.get('term', '')

        if search_term == '*':
            proveedores = Proveedor.objects.all().order_by('nombre')
        else:
            proveedores = Proveedor.objects.all()
            for term in search_term.split():
                proveedores = proveedores.filter(nombre__icontains=term).order_by('nombre')

        results = []
        for proveedor in proveedores:
            nombre = "(" + str(proveedor.codigo) + ") " + proveedor.nombre
            art_json = {'label': nombre, 'value': proveedor.codigo}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def proveedor_buscar(request):
    response = {"success": False}

    try:
        proveedor_codigo = request.GET['codigo']
        proveedor = Proveedor.objects.get(codigo=proveedor_codigo)
        response['proveedor_nombre'] = proveedor.nombre
        response['success'] = True
        return JsonResponse(response)
    except:
        return JsonResponse(response)


@login_required
def insumo_listar(request):
    if request.is_ajax:
        search_term = request.GET.get('term', '')

        if search_term == '*':
            insumos = Insumo.objects.all().order_by('nombre')
        else:
            insumos = Insumo.objects.all()
            for term in search_term.split():
                insumos = insumos.filter(nombre__icontains=term).order_by('nombre')

        results = []
        for insumo in insumos:
            nombre = "(" + str(insumo.codigo) + ") " + insumo.nombre + ' - ' + insumo.unidad_tipo
            art_json = {'label': nombre, 'value': insumo.codigo}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def lote_listar(request):
    if request.is_ajax:
        results = []
        insumo_codigo = request.GET.get('insumocod', '')
        if insumo_codigo:
            insumo = Insumo.objects.get(codigo=insumo_codigo)
            lotes = InsumoLote.objects.filter(insumo=insumo, saldo__gt=float(0))

            for lote in lotes:
                nombre = "- " + lote.lote + ": stock(" + str(lote.saldo) + ")"
                art_json = {'label': nombre, 'value': lote.id}
                results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def lote_buscar(request):
    response = {"success": False}
    try:
        lote_id = request.GET['lote_id']
        lote = InsumoLote.objects.get(id=lote_id)
        response['lote_nombre'] = lote.lote
        response['success'] = True
        return JsonResponse(response)

    except:
        return JsonResponse(response)


@login_required
def insumo_servicio_listado(request):
    context = {}
    servicio_id = request.GET.get("servicioid", "")
    ajuste = request.GET.get("ajuste", "0")

    if servicio_id != "":
        servicio = Servicio.objects.get(id=servicio_id)
        servicio_insumo = InsumoServicio.objects.filter(servicio=servicio)
        context['servicio_selected'] = servicio
    else:
        servicio_insumo = InsumoServicio.objects.all().order_by('servicio__nombre')

    context['servicios'] = Servicio.objects.all().exclude(is_servicio_externo=True)
    context['insumos'] = Insumo.objects.all().order_by('nombre')
    context['servicio_insumo'] = servicio_insumo

    template = 'insumo/insumo_servicio.html'
    if ajuste == '1':
        template = 'insumo/insumo_servicio_ajuste.html'

    return render(request, template, context)


@login_required
def insumo_servicio_detalle(request):
    context = {}
    servicio_id = request.GET.get("servicioid", "")
    insumo_id = request.GET.get("insumoid", "")

    detalle_stock = ""
    if servicio_id != "" and insumo_id != "":
        servicio = Servicio.objects.get(id=servicio_id)
        insumo = Insumo.objects.get(id=insumo_id)
        insumo_servicio = InsumoServicio.objects.filter(servicio=servicio, insumo=insumo).first()
        detalle_stock = InsumoMovimientosServicio.objects.filter(servicio_insumo=insumo_servicio).order_by('-fechahora')
        context = {"detalle_stock": detalle_stock, "insumo": insumo, "servicio": servicio,
                   "insumo_detalle_id": insumo_servicio.id}
    return render(request, 'insumo/insumo_tecnico_detalle.html', context)


@login_required
@transaction.atomic
def entregar_insumo_servicio(request):
    response_data = {'success': False}
    servicio_id = request.GET.get('servicio-id', '')
    servicio = Servicio.objects.get(id=servicio_id)
    cantidad_total_insumos = request.GET.get('cantidad-total-insumos', '')
    obs = ', observación: ' + request.GET.get('entrega-obs', '') if request.GET.get('entrega-obs', '') else ''
    obs_raw = request.GET.get('entrega-obs', '')
    config_generales = ConfiguracionGeneral.objects.all().first()
    config_generales.remito_egreso_numeracion += 1
    config_generales.save()
    remito_ingreso_servicio = RemitoEntregaServicio(
        servicio=servicio,
        observaciones=obs_raw,
        comprobante_numero=config_generales.remito_egreso_numeracion
    )
    remito_ingreso_servicio.save()

    for i in range(1, int(cantidad_total_insumos) + 1):
        insumocodigo = request.GET.get('insumo-' + str(i), '')
        cantidad = request.GET.get('cantidad-' + str(i), '')
        lote_id = request.GET.get('lote-' + str(i), '')

        if servicio and insumocodigo and cantidad:
            insumo = Insumo.objects.get(codigo=insumocodigo)
            cantidad = float(cantidad)
            cantidad_x_unidad = float(cantidad) * float(insumo.unidad)
            insumo_servicio, created = InsumoServicio.objects.get_or_create(insumo=insumo, servicio=servicio)
            cantidad_nuevo = insumo_servicio.stock + cantidad_x_unidad
            insumo_servicio.stock = cantidad_nuevo
            insumo_servicio.save()
            if lote_id:
                lote = InsumoLote.objects.get(id=lote_id)
                lote.egresos += float(cantidad)
                lote.saldo -= float(cantidad)
                lote.save()
            else:
                lote = None
            detalle_remito = InsumoRemitoEntregaServicio(
                remito_entrega_servicio=remito_ingreso_servicio,
                insumo_servicio=insumo_servicio,
                lote=lote,
                cantidad=cantidad
            )
            detalle_remito.save()
            ins_detalle_movimiento = InsumoMovimientosServicio(
                insumo_remito_entrega_serv=detalle_remito,
                servicio_insumo=insumo_servicio,
                ingreso=cantidad_x_unidad,
                egreso=float(0),
                existencia=cantidad_nuevo,
                fechahora=datetime.datetime.now(),
                observaciones='Entrega de Insumo por ' + request.user.username + obs
            )
            ins_detalle_movimiento.save()
            insumo_stock_final = insumo.stock_almacen - cantidad if insumo.stock_almacen else cantidad * -1
            insumo.stock_almacen = insumo_stock_final
            insumo.save()

    response_data["success"] = True
    response_data["remito_entrega_id"] = remito_ingreso_servicio.id

    return JsonResponse(response_data)


@login_required
@transaction.atomic
def ajustar_insumo_servicio(request):
    response_data = {'success': False}
    servicio_id = request.GET.get('servicio-id', '')
    servicio = Servicio.objects.get(id=servicio_id)
    cantidad_total_insumos = request.GET.get('cantidad-total-insumos', '')
    obs = ', observación: ' + request.GET.get('entrega-obs', '') if request.GET.get('entrega-obs', '') else ''

    for i in range(1, int(cantidad_total_insumos) + 1):
        insumocodigo = request.GET.get('insumo-' + str(i), '')
        cantidad = request.GET.get('cantidad-' + str(i), '')

        if servicio and insumocodigo and cantidad:
            insumo = Insumo.objects.get(codigo=insumocodigo)
            cantidad = float(cantidad)
            insumo_servicio, created = InsumoServicio.objects.get_or_create(insumo=insumo, servicio=servicio)
            diferencia_stock = cantidad - insumo_servicio.stock
            insumo_servicio.stock = cantidad
            insumo_servicio.save()

            ingreso = float(0)
            egreso = float(0)

            if diferencia_stock > 0:
                ingreso = diferencia_stock
            elif diferencia_stock < 0:
                egreso = diferencia_stock * -1


            ins_detalle_movimiento = InsumoMovimientosServicio(
                servicio_insumo=insumo_servicio,
                ingreso=ingreso,
                egreso=egreso,
                existencia=cantidad,
                fechahora=datetime.datetime.now(),
                observaciones='Ajuste de Stock por ' + request.user.username + obs
            )
            ins_detalle_movimiento.save()

    response_data["success"] = True

    return JsonResponse(response_data)



@login_required
def detalle_insumos_serv_imprimir(request):
    servicioid = request.GET.get('servicioid', '')
    servicio = Servicio.objects.get(id=servicioid)
    servicio_nombre = servicio.nombre
    filename = datetime.datetime.now().strftime('%Y/%m/%d') + '_' + servicio_nombre + '.pdf'
    buffer = io.BytesIO()
    w = A4[0]
    h = A4[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)
    c.setFont('Helvetica', 14)
    c.drawString((w / 2) - 130, h - 1.2 * cm, "DETALLE DE EXISTENCIAS DE INSUMO POR SERVICIO")
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm
    c.line(x, y + 2, xfin, y + 2)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 28, "Servicio: ")
    c.setFont('Helvetica', 18)
    c.drawString(x_imagen + 61, y_imagen - 28, servicio_nombre)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 250, y_imagen - 28, "Fecha: ")
    c.setFont('Helvetica', 15)
    server_timezone = pytz.timezone("America/Argentina/Salta")
    f = datetime.datetime.now().astimezone(server_timezone)
    dias = {"Monday": "Lunes", "Tuesday": "Martes", "Wednesday": "Miércoles", "Thursday": "Jueves", "Friday": "Viernes",
            "Saturday": "Sábado", "Sunday": "Domingo"}
    fe_str = dias[f.strftime("%A")] + ", " + f.strftime("%d/%m/%Y %H:%m")
    c.drawString(w - 204, y_imagen - 28, fe_str)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_imagen - 49, "3: Medios de contraste")
    styles = getSampleStyleSheet()
    styleBH = styles["Heading4"]
    styleBH.alignment = TA_CENTER
    cod = Paragraph('''CÓD.''', styleBH)
    desc = Paragraph('''DESCRIPCIÓN''', styleBH)
    unid = Paragraph('''UNID.''', styleBH)
    ingresos = Paragraph('''INGRESOS''', styleBH)
    egresos = Paragraph('''EGRESOS''', styleBH)
    saldo = Paragraph('''SALDO''', styleBH)
    contraste_data = [[cod, desc, unid, ingresos, egresos, saldo]]
    normal_data = [[cod, desc, unid, ingresos, egresos, saldo]]
    y_tabla_contraste, y_tabla_normal = 720, 0
    insumo = Insumo.objects.all()
    for ins in insumo:
        servicio_insumo = InsumoServicio.objects.filter(servicio=servicio, insumo=ins)
        for serv_ins in servicio_insumo:
            egresos, ingresos = 0, 0
            detalle = InsumoMovimientosServicio.objects.filter(servicio_insumo=serv_ins)
            for det in detalle:
                egresos -= det.egreso
                ingresos += det.ingreso
            if ins.grupo == '3':
                contraste_data.append([ins.codigo, ins.nombre, ins.unidad_tipo, float("%.2f"%ingresos), float("%.2f"%egresos), float("%.2f"%serv_ins.stock)])
                y_tabla_contraste -= 18
            else:
                normal_data.append([ins.codigo, ins.nombre, ins.unidad_tipo, float("%.2f"%ingresos), float("%.2f"%egresos), float("%.2f"%serv_ins.stock)])
                y_tabla_normal += 18

    width, height = A4
    contraste_table = Table(contraste_data, colWidths=[1.5 * cm, 9 * cm, 2 * cm, 2.5 * cm, 2.5 * cm, 2 * cm],
                            repeatRows=1)
    contraste_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    contraste_table.wrapOn(c, width, height)
    contraste_table.drawOn(c, 30, y_tabla_contraste)

    y_tabla_contraste -= 25
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_tabla_contraste, "6: Accesorios y descartables")
    y_tabla_contraste -= 25

    normal_table = Table(normal_data, colWidths=[1.5 * cm, 9 * cm, 2 * cm, 2.5 * cm, 2.5 * cm, 2 * cm], repeatRows=1)
    normal_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    normal_table.wrapOn(c, width, height)
    normal_table.drawOn(c, 30, y_tabla_contraste - y_tabla_normal)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=filename),
                            content_type='application/pdf')

    return response


@login_required
def remito_entrega_serv_insumos_pdf(request):
    remito_entrega_id = request.GET.get('remitoentregaid', '')
    remito_entrega = RemitoEntregaServicio.objects.get(id=remito_entrega_id)
    servicio = remito_entrega.servicio
    servicio_nombre = servicio.nombre
    filename = datetime.datetime.now().strftime('%Y/%m/%d') + '_Entrega Insumos_' + servicio_nombre + '.pdf'
    buffer = io.BytesIO()
    w = A4[0]
    h = A4[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)
    c.setFont('Helvetica-Bold', 10)
    c.drawString(w - 150, h - 1.8 * cm, "RIS TCSE - Módulo Farmacia")
    c.setFont('Helvetica', 14)
    c.drawString((w / 2) - 130, h - 1.2 * cm, "REMITO INTERNO DE PROVISIÓN (SERVICIO)")
    c.setFont('Helvetica', 12)
    num_remito = remito_entrega.comprobante_numero
    c.drawString((w / 2) - 130, h - 1.8 * cm, "Comprobante Nº: " + str(num_remito))
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm
    c.line(x, y + 2, xfin, y + 2)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 28, "Servicio: ")
    c.setFont('Helvetica', 18)
    c.drawString(x_imagen + 61, y_imagen - 28, servicio_nombre)
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 250, y_imagen - 28, "Fecha: ")
    c.drawString(w - 250, y_imagen - 45, "Fecha Entrega: ")
    c.setFont('Helvetica', 15)
    server_timezone = pytz.timezone("America/Argentina/Salta")
    f = datetime.datetime.now().astimezone(server_timezone)
    dias = {"Monday": "Lunes", "Tuesday": "Martes", "Wednesday": "Miércoles", "Thursday": "Jueves", "Friday": "Viernes",
            "Saturday": "Sábado", "Sunday": "Domingo"}

    fe_str = dias[f.strftime("%A")] + ", " + f.strftime("%d/%m/%Y")
    c.drawString(w - 204, y_imagen - 28, fe_str)

    fe_entrega_str = dias[remito_entrega.fecha.strftime("%A")] + ", " + remito_entrega.fecha.strftime("%d/%m/%Y")
    c.drawString(w - 150, y_imagen - 45, fe_entrega_str)

    c.setFont('Helvetica-Bold', 11)
    c.drawString(x_imagen + 4, y_imagen - 44, "Obs: ")
    c.setFont('Helvetica', 11)
    c.drawString(x_imagen + 30, y_imagen - 44, remito_entrega.observaciones)

    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_imagen - 64, "3: Medios de contraste")
    styles = getSampleStyleSheet()
    styleBH = styles["Heading4"]
    styleBH.alignment = TA_CENTER
    cod = Paragraph('''CÓD.''', styleBH)
    desc = Paragraph('''DESCRIPCIÓN''', styleBH)
    unid = Paragraph('''UNID.''', styleBH)
    lote = Paragraph('''LOTE''', styleBH)
    fec_vto = Paragraph('''VTO.''', styleBH)
    cantidad = Paragraph('''CANT.''', styleBH)
    contraste_data = [[cod, desc, unid, lote, fec_vto, cantidad]]
    normal_data = [[cod, desc, unid, lote, fec_vto, cantidad]]
    y_tabla_contraste, y_tabla_normal = 705, 0
    detalle_remito_entrega = InsumoRemitoEntregaServicio.objects.filter(remito_entrega_servicio=remito_entrega)

    for item in detalle_remito_entrega:
        lote = item.lote.lote if item.lote else '---'
        lote_fecha_venc = item.lote.fecha_vencimiento.strftime(
            '%d/%m/%Y') if lote != '---' and item.lote.fecha_vencimiento else '---'
        if item.insumo_servicio.insumo.grupo == '3':
            contraste_data.append([item.insumo_servicio.insumo.codigo, item.insumo_servicio.insumo.nombre,
                                   item.insumo_servicio.insumo.unidad_tipo, lote, lote_fecha_venc, item.cantidad])
            y_tabla_contraste -= 18
        else:
            normal_data.append([item.insumo_servicio.insumo.codigo, item.insumo_servicio.insumo.nombre,
                                item.insumo_servicio.insumo.unidad_tipo, lote, lote_fecha_venc, item.cantidad])
            y_tabla_normal += 18

    width, height = A4
    contraste_table = Table(contraste_data, colWidths=[1.5 * cm, 9 * cm, 1.5 * cm, 3.5 * cm, 2 * cm, 2 * cm],
                            repeatRows=1)
    contraste_table.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))

    contraste_table.wrapOn(c, width, height)
    contraste_table.drawOn(c, 30, y_tabla_contraste)
    y_tabla_contraste -= 25
    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 15, y_tabla_contraste, "6: Accesorios y descartables")
    y_tabla_contraste -= 25
    normal_table = Table(normal_data, colWidths=[1.5 * cm, 9 * cm, 1.5 * cm, 3.5 * cm, 2 * cm, 2 * cm], repeatRows=1)
    normal_table.setStyle(TableStyle([  # estilos de la tabla
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
    ]))
    normal_table.wrapOn(c, width, height)
    normal_table.drawOn(c, 30, y_tabla_contraste - y_tabla_normal)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=filename),
                            content_type='application/pdf')
    return response


@login_required
def listado_estudios_corregir(request):
    context = {}
    context['estudios_corregir'] = Turno.objects.filter(informe_tec_corregir=True)

    return render(request, 'protocolo/listado_estudios_corregir.html', context)


def header_listado_insumos_general(canvas, pdf):
    w = A4[0]
    h = A4[1]
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(canvas, x_imagen, y_imagen - 8)
    styleBH2 = styles["Heading2"]
    heading = Paragraph("EXISTENCIA DE INSUMOS", styleBH2)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 185, 800)
    styleBH5 = styles["Heading5"]
    today = "--/--/----"
    heading = Paragraph("Periodo: --/--/---- al " + today, styleBH5)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 185, 790)
    today = datetime.date.today().strftime('%d/%m/%Y')
    heading = Paragraph("Fecha: " + today, styleBH5)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 505, 805)


class NumberedPageCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)

        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            super().showPage()

        super().save()

    def draw_page_number(self, page_count):
        page = "Página %s de %s" % (self._pageNumber, page_count)
        self.setFont("Helvetica", 9)
        self.drawString(505, A4[1] - 25, page)


@login_required
def listado_insumos_gral_pdf(request):
    end_date = request.GET.get('fechafin')
    localtz = pytz.timezone('America/Argentina/Salta')
    date_end = localtz.localize(datetime.datetime.strptime(end_date, '%Y-%m-%d'))

    buffer = io.BytesIO()

    pdf = BaseDocTemplate(buffer, pagesize=A4)
    frame = Frame(
        pdf.leftMargin,
        pdf.bottomMargin,
        pdf.width,
        pdf.height)
    template = PageTemplate(id='all_pages', frames=frame, onPage=header_listado_insumos_general)
    pdf.addPageTemplates([template])
    styles = getSampleStyleSheet()
    styleBH = styles["Heading5"]
    styleBH.alignment = TA_CENTER

    cod = Paragraph('''Cód.''', styleBH)
    desc = Paragraph('''Descripción''', styleBH)
    unid = Paragraph('''Und.''', styleBH)
    rep = Paragraph('''R''', styleBH)
    costo = Paragraph('''Costo''', styleBH)
    inicial = Paragraph('''Inicial''', styleBH)
    ingresos = Paragraph('''Ingresos''', styleBH)
    egresos = Paragraph('''Egresos''', styleBH)
    existencia = Paragraph('''Exist.''', styleBH)

    insumo_data = [[cod, desc, unid, rep, costo, inicial, ingresos, egresos, existencia]]
    grupos_nombre = {
        '0': '0000 -',
        '1': '0001 - Placas Radiologicas',
        '2': '0002 - Reveladores',
        '3': '0003 - Medios de Contraste',
        '4': '0004 - Medicamentos',
        '5': '0005 - Anestesicos',
        '6': '0006 - Accesorios y Descartables',
    }
    insumos = Insumo.objects.filter(habilitado=True).order_by('grupo', 'codigo')

    grupo_actual = insumos.first().grupo
    groups = []
    for_count = 1

    insumo_data.append([grupos_nombre['0']])
    for insumo in insumos:
        for_count += 1
        if insumo.grupo != grupo_actual:
            groups.append(for_count)
            for_count += 1
            grupo_actual = insumo.grupo
            if not grupo_actual:
                grupo_actual = '0'
            insumo_data.append([grupos_nombre[grupo_actual]])

        # Entradas

        insumo_ingresos = InsumoRemitoIngreso.objects.filter(insumo=insumo, fecha__lte=date_end).aggregate(Sum('unidades'))
        total_ingresos = insumo.entradas
        if insumo_ingresos['unidades__sum']:
            total_ingresos += insumo_ingresos['unidades__sum']

        insumo_egresos_tecnico = InsumoRemitoEntrega.objects.filter(insumo_tecnico__insumo=insumo, remito_entrega__fecha__lte=date_end).aggregate(
            Sum('cantidad'))
        insumo_egresos_servicio = InsumoRemitoEntregaServicio.objects.filter(insumo_servicio__insumo=insumo, remito_entrega_servicio__fecha__lte=date_end).aggregate(
            Sum('cantidad'))
        
        total_egresos = insumo.salidas

        if insumo_egresos_servicio['cantidad__sum']:
            total_egresos += insumo_egresos_servicio['cantidad__sum']

        if insumo_egresos_tecnico['cantidad__sum']:
            total_egresos += insumo_egresos_tecnico['cantidad__sum']

        remito_detalle_costo = InsumoRemitoIngreso.objects.filter(insumo=insumo).order_by('fecha').last()
        costounit = '-'
        if remito_detalle_costo:
            costounit = str(remito_detalle_costo.costo_unitario)

        row_data = [insumo.codigo, insumo.nombre.capitalize(), insumo.unidad_tipo.lower(), '-', costounit, 0.0,
                    total_ingresos, total_egresos, total_ingresos - total_egresos]
        insumo_data.append(row_data)
    insumo_tabla = Table(insumo_data,
                         colWidths=[1.2 * cm, 7.4 * cm, 2.3 * cm, 0.8 * cm, 1.4 * cm, 1.8 * cm, 1.8 * cm, 1.8 * cm],
                         repeatRows=1)
    tabla_config_style = [
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('SPAN', (0, 1), (8, 1)),
        ('ALIGN', (0, 1), (8, 1), "LEFT"),
        ('FONTNAME', (0, 1), (8, 1), 'Helvetica-Bold')

    ]
    for index in groups:
        tabla_config_style.append(('SPAN', (0, index), (8, index)))
        tabla_config_style.append(('ALIGN', (0, index), (8, index), "LEFT"))
        tabla_config_style.append(('FONTNAME', (0, index), (8, index), 'Helvetica-Bold'))
    insumo_tabla.setStyle(TableStyle(tabla_config_style))
    pdf.build([insumo_tabla], canvasmaker=NumberedPageCanvas)
    buffer.seek(0)
    response = HttpResponse(FileResponse(as_attachment=True, filename='test'),
                            content_type='application/pdf')
    response.write(buffer.getvalue())
    return response


def header_lote(canvas, pdf):
    w = A4[0]
    h = A4[1]
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(canvas, x_imagen, y_imagen - 8)
    styleBH = styles["Heading2"]
    heading = Paragraph("DETALLE DE EXISTENCIAS DE INSUMOS POR LOTE", styleBH)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 135, 800)
    styleBH = styles["Heading5"]
    today = datetime.datetime.now().strftime('%d/%m/%Y %H:%M')
    heading = Paragraph("Fecha: " + today, styleBH)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 135, 790)


@login_required
def listado_lotes_pdf(request):
    buffer = io.BytesIO()

    pdf = BaseDocTemplate(buffer, pagesize=A4)
    frame = Frame(
        pdf.leftMargin,
        pdf.bottomMargin,
        pdf.width,
        pdf.height)
    template = PageTemplate(id='all_pages', frames=frame, onPage=header_lote)
    pdf.addPageTemplates([template])
    styles = getSampleStyleSheet()
    styleBH = styles["Heading5"]
    styleBH.alignment = TA_CENTER
    cod = Paragraph('''Cód''', styleBH)
    desc = Paragraph('''Descripción''', styleBH)
    unid = Paragraph('''Und.''', styleBH)
    punit = Paragraph('''P/Unt.''', styleBH)
    ingresos = Paragraph('''Ingresos''', styleBH)
    egresos = Paragraph('''Egresos''', styleBH)
    existencia = Paragraph('''Saldo''', styleBH)
    lote = Paragraph('''Lote/Part.''', styleBH)
    vencimiento = Paragraph('''Venc.''', styleBH)
    insumo_data = [[cod, desc, unid, lote, vencimiento, punit, ingresos, egresos, existencia]]
    lotes = InsumoLote.objects.all().exclude(saldo=0.0).order_by('insumo')
    bold_rows_index = [1]
    tabla_config_style = [
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTSIZE', (0, 0), (-1, -1), 9),
    ]

    if lotes:
        current_insumo = lotes.first().insumo
        insumo_data.append(
            [current_insumo.codigo, current_insumo.nombre.capitalize(), current_insumo.unidad_tipo.lower(), ' ', ' ',
             ' ', current_insumo.entradas, current_insumo.salidas, current_insumo.stock_almacen])
        forloop_counter = 0

        for lote in lotes:
            forloop_counter += 1
            if lote.insumo != current_insumo:
                forloop_counter += 1
                bold_rows_index.append(forloop_counter)
                current_insumo = lote.insumo
                insumo_data.append(
                    [lote.insumo.codigo, lote.insumo.nombre.capitalize(), lote.insumo.unidad_tipo.lower(), ' ', ' ',
                     ' ', lote.insumo.entradas, lote.insumo.salidas, lote.insumo.stock_almacen])
            lote_vecimiento = lote.fecha_vencimiento.strftime('%d/%m/%Y') if lote.fecha_vencimiento else '---'
            row_data = [' ', ' ', ' ', lote.lote, lote_vecimiento,
                        lote.insumo_remito.costo_unitario, lote.ingresos, lote.egresos, lote.saldo]
            insumo_data.append(row_data)

        for index in bold_rows_index:
            tabla_config_style.append(('FONTNAME', (0, index), (8, index), 'Helvetica-Bold'))
    insumo_tabla = Table(insumo_data,
                         colWidths=[1.1 * cm, 6.4 * cm, 1.5 * cm, 2.1 * cm, 1.7 * cm, 1.8 * cm, 1.8 * cm, 1.8 * cm],
                         repeatRows=1)
    insumo_tabla.setStyle(TableStyle(tabla_config_style))
    pdf.build([insumo_tabla])
    buffer.seek(0)
    response = HttpResponse(FileResponse(as_attachment=True, filename='test'),
                            content_type='application/pdf')
    response.write(buffer.getvalue())
    return response


def header_detalle_movimientos_insumo(canvas, pdf):
    w = A4[0]
    h = A4[1]
    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(canvas, x_imagen, y_imagen - 8)
    styleBH = styles["Heading2"]
    heading = Paragraph("REPORTE DE MOVIMIENTOS POR INSUMO", styleBH)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 135, 800)
    styleBH = styles["Heading5"]

    heading = Paragraph("RIS TCSE - Módulo Farmacia", styleBH)
    heading.wrap(pdf.width, inch * 0.5)
    heading.drawOn(canvas, 450, 790)


@login_required
def reporte_insumo_movimientos_farmacia(request):
    # fechahora
    start_date = request.GET.get('fechaini', '')
    end_date = request.GET.get('fechafin', '')
    localtz = pytz.timezone('America/Argentina/Salta')
    date_start = localtz.localize(datetime.datetime.strptime(start_date, '%d-%m-%Y'))
    date_end = localtz.localize(datetime.datetime.strptime(end_date, '%d-%m-%Y'))

    buffer = io.BytesIO()
    pdf = BaseDocTemplate(buffer, pagesize=A4)
    frame = Frame(
        pdf.leftMargin,
        pdf.bottomMargin,
        pdf.width,
        pdf.height)
    template = PageTemplate(id='all_pages', frames=frame, onPage=header_detalle_movimientos_insumo)
    pdf.addPageTemplates([template])
    styles = getSampleStyleSheet()
    styleBH = styles["Heading5"]
    styleBH.alignment = TA_CENTER

    header_codigo = Paragraph('''Fecha''', styleBH)
    header_tipomov = Paragraph('''Tipo Mov.''', styleBH)
    header_remito = Paragraph('''Remito''', styleBH)
    header_orgdest = Paragraph('''Origen/Destino''', styleBH)
    header_ingresos = Paragraph('''Ingresos''', styleBH)
    header_egresos = Paragraph('''Egresos''', styleBH)
    header_saldo = Paragraph('''Saldo''', styleBH)
    table_mov_headers = (header_codigo, header_tipomov, header_remito, header_orgdest, header_ingresos, header_egresos, header_saldo)
    bold_rows_index = [1]
    tabla_config_style = [
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTSIZE', (0, 0), (-1, -1), 9),
    ]

    insumo_movimientos_todos = []
    total_ingresos = 0
    total_egresos = 0

    insumo_codigo = request.GET.get('insumoCodigo', '')
    insumo = Insumo.objects.get(codigo=insumo_codigo)
    insumo_tecnicos = TecnicoInsumo.objects.filter(insumo=insumo)
    remito_entrega_insumos_tecnico = InsumoRemitoEntrega.objects.filter(insumo_tecnico__in=insumo_tecnicos)
    for mov_entrega_tec in remito_entrega_insumos_tecnico:
        insumo_movimientos_todos.append((mov_entrega_tec.remito_entrega.fecha,
                                         'Prov. a Técnico',
                                         mov_entrega_tec.remito_entrega.comprobante_numero,
                                         mov_entrega_tec.remito_entrega.tecnico.username,
                                         0,
                                         mov_entrega_tec.cantidad))

    insumo_servicios = InsumoServicio.objects.filter(insumo=insumo)
    remito_entrega_insumos_servicio = InsumoRemitoEntregaServicio.objects.filter(insumo_servicio__in=insumo_servicios)

    for mov_entrega_serv in remito_entrega_insumos_servicio:
        insumo_movimientos_todos.append((mov_entrega_serv.remito_entrega_servicio.fecha,
                                         'Prov. a Servicio',
                                         mov_entrega_serv.remito_entrega_servicio.comprobante_numero,
                                         mov_entrega_serv.remito_entrega_servicio.servicio.nombre,
                                         0,
                                         mov_entrega_serv.cantidad))

    remito_ingreso_insumos = InsumoRemitoIngreso.objects.filter(insumo=insumo)

    for mov_ingreso_prov in remito_ingreso_insumos:
        mov_concepto = '---'
        if mov_ingreso_prov.remito.proveedor:
            mov_concepto = mov_ingreso_prov.remito.proveedor.nombre

        insumo_movimientos_todos.append((mov_ingreso_prov.remito.fecha,
                                         mov_ingreso_prov.remito.tipo_ingreso,
                                         mov_ingreso_prov.remito.id,
                                         mov_concepto,
                                         mov_ingreso_prov.unidades,
                                         0))

    # sort list of tuples by first element of the tuple
    movimientos_short_by_date = sorted(insumo_movimientos_todos, key=lambda tup: tup[0])
    # filter by start and end date
    movimientos_filtered_by_date = [mov for mov in movimientos_short_by_date if date_start <= mov[0] <= date_end]

    table_data = []
    init = insumo.entradas - insumo.salidas
    mov_initial = None
    if init > 0:
        mov_initial = ('01/01/2021', 'Stock Inicial Sistema', '-', '-', init, 0, init)
        total_ingresos = init

    init_ing = init
    init_egr = 0
    for mov in movimientos_short_by_date:
        if date_start > mov[0]:
            init += mov[4] - mov[5]
            total_ingresos = init
            init_ing += mov[4]
            init_egr += mov[5]
            saldo_init = init_ing - init_egr
            mov_initial = ('--/--/----', 'Stock Inicial Periodo', '-', '-', init_ing, init_egr, saldo_init)

    saldo = 0
    if mov_initial is not None:
        saldo = mov_initial[6]
        table_data.append(mov_initial)

    for mov in movimientos_filtered_by_date:
        saldo += mov[4] - mov[5]
        new_item = (mov[0].strftime('%d/%m/%Y'), mov[1], mov[2], mov[3], mov[4], mov[5], saldo)
        total_ingresos += mov[4]
        total_egresos += mov[5]
        table_data.append(new_item)

    table_data.insert(0, table_mov_headers)
    insumo_tabla = Table(table_data, colWidths=[2.2 * cm, 2.8 * cm, 2 * cm, 8.7 * cm, 1.9 * cm, 1.9 * cm], repeatRows=1)
    insumo_tabla.setStyle(TableStyle(tabla_config_style))

    story = []
    # table title
    periodo_title = "Periodo: " + date_start.strftime('%d/%m/%Y') + ' al ' + date_end.strftime('%d/%m/%Y')
    insumo_title = "Insumo: " + insumo.nombre
    style_left = ParagraphStyle(name='left', alignment=TA_LEFT, leftIndent=0, rightIndent=0, parent=styleH5)
    story.append(Paragraph(insumo_title + ' - ' + periodo_title, style_left))

    # add end table results
    endtable_saldo = "SALDO INICIAL: " + str(init)
    endtable_ingresos = "TOTAL INGRESOS: " + str(total_ingresos - init)
    endtable_egresos = "TOTAL EGRESOS: " + str(total_egresos)
    style_right = ParagraphStyle(name='right', alignment=TA_RIGHT, leftIndent=0, rightIndent=0, parent=styleH3)

    story.append(Paragraph(endtable_saldo, style_right))
    story.append(Paragraph(endtable_ingresos, style_right))
    story.append(Paragraph(endtable_egresos, style_right))

    total_results = total_ingresos - total_egresos
    endtable_saldo = "EXISTENCIA ACTUAL: " + str(total_results)

    story.append(Paragraph(endtable_saldo, style_right))

    pdf.build([story[0], insumo_tabla, story[1], story[2], story[3], story[4]])

    buffer.seek(0)
    response = HttpResponse(FileResponse(as_attachment=True, filename='test'),
                            content_type='application/pdf')
    response.write(buffer.getvalue())
    return response


@login_required
def insumo_movimientos_farmacia(request):
    insumos = Insumo.objects.all()
    context = {'insumos': insumos}
    return render(request, 'insumo_movimientos/Insumo_detalle_movimientos.html', context)


GRUPO_INSUMO = {
    '0': '0. Sin Grupo',
    '1': '1. Placas Radiologicas',
    '2': '2. Reveladores',
    '3': '3. Medios de Contraste',
    '4': '4. Medicamentos',
    '5': '5. Anestesicos',
    '6': '6. Accesorios y Descartables',
}


@login_required
def reporte_farmacia_ingresos_excel(request):
    start_date = request.GET.get('fechaini', '')
    end_date = request.GET.get('fechafin', '')
    localtz = pytz.timezone('America/Argentina/Salta')
    date_start = localtz.localize(datetime.datetime.strptime(start_date, '%d-%m-%Y'))
    date_end = localtz.localize(datetime.datetime.strptime(end_date, '%d-%m-%Y'))

    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    row_num = 0

    columns = [
        'Remito Ingreso Nº',
        'Remito Fecha',
        'Tipo Operación',
        'Comprobante Nº',
        'Proveedor',
        'Insumo Nombre',
        'Insumo Código',
        'Insumo Grupo',
        'Insumo Unidad',
        'Insumo Cantidad',
        'Insumo Costo Unt.',
        'Insumo Importe'
    ]

    for col_num in range(len(columns)):
        worksheet.write(row_num, col_num, columns[col_num])

    remito_ingresos = RemitoIngreso.objects.filter(fecha__range=(date_start, date_end))
    for remito in remito_ingresos:
        remito_fecha = remito.fecha.strftime('%d-%m-%Y')
        proveedor_nombre = remito.proveedor.nombre if remito.proveedor else ''
        row_base = [str(remito.id), remito_fecha, remito.tipo_ingreso, remito.comprobante_numero, proveedor_nombre]
        remito_ingreso_detalles = InsumoRemitoIngreso.objects.filter(remito=remito)
        for remito_detalle in remito_ingreso_detalles:
            row_num += 1

            row_append = [
                remito_detalle.insumo.nombre,
                remito_detalle.insumo.codigo,
                GRUPO_INSUMO[remito_detalle.insumo.grupo],
                remito_detalle.insumo.unidad_tipo,
                remito_detalle.unidades,
                remito_detalle.costo_unitario,
                remito_detalle.importe
            ]
            row = row_base + row_append

            for col_num in range(len(row)):
                worksheet.write(row_num, col_num, row[col_num])

    workbook.close()

    xlsx_data = output.getvalue()

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=Report.xlsx'
    response.write(xlsx_data)

    return response


@login_required
def reporte_farmacia_egresos_excel(request):
    start_date = request.GET.get('fechaini', '')
    end_date = request.GET.get('fechafin', '')
    localtz = pytz.timezone('America/Argentina/Salta')
    date_start = localtz.localize(datetime.datetime.strptime(start_date, '%d-%m-%Y'))
    date_end = localtz.localize(datetime.datetime.strptime(end_date, '%d-%m-%Y'))

    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    row_num = 0

    columns = [
        'Remito Egreso Nº',
        'Remito Fecha',
        'Concepto',
        'Servicio/Técnico',
        'Insumo Nombre',
        'Insumo Código',
        'Insumo Grupo',
        'Insumo Unidad',
        'Insumo Cantidad',
    ]

    for col_num in range(len(columns)):
        worksheet.write(row_num, col_num, columns[col_num])

    remito_entrega_tecnicos = RemitoEntregaTecnico.objects.filter(fecha__range=(date_start, date_end))

    for remito_entrega_tecnico in remito_entrega_tecnicos:
        remito_fecha = remito_entrega_tecnico.fecha.strftime('%d-%m-%Y')

        row_base = [str(remito_entrega_tecnico.comprobante_numero), remito_fecha, 'Provisión Técnico',
                    remito_entrega_tecnico.tecnico.username]

        remito_entrega_detalles = InsumoRemitoEntrega.objects.filter(remito_entrega=remito_entrega_tecnico)
        for remito_detalle in remito_entrega_detalles:
            row_num += 1

            row_append = [
                remito_detalle.insumo_tecnico.insumo.nombre,
                remito_detalle.insumo_tecnico.insumo.codigo,
                GRUPO_INSUMO[remito_detalle.insumo_tecnico.insumo.grupo],
                remito_detalle.insumo_tecnico.insumo.unidad_tipo,
                remito_detalle.cantidad
            ]
            row = row_base + row_append

            for col_num in range(len(row)):
                worksheet.write(row_num, col_num, row[col_num])

    remito_entrega_servicios = RemitoEntregaServicio.objects.filter(fecha__range=(date_start, date_end))

    for remito_entrega_servicio in remito_entrega_servicios:
        remito_fecha = remito_entrega_servicio.fecha.strftime('%d-%m-%Y')

        row_base = [str(remito_entrega_servicio.comprobante_numero), remito_fecha, 'Provisión Servicio',
                    remito_entrega_servicio.servicio.nombre]

        remito_entrega_detalles = InsumoRemitoEntregaServicio.objects.filter(
            remito_entrega_servicio=remito_entrega_servicio)
        for remito_detalle in remito_entrega_detalles:
            row_num += 1

            row_append = [
                remito_detalle.insumo_servicio.insumo.nombre,
                remito_detalle.insumo_servicio.insumo.codigo,
                GRUPO_INSUMO[remito_detalle.insumo_servicio.insumo.grupo],
                remito_detalle.insumo_servicio.insumo.unidad_tipo,
                remito_detalle.cantidad
            ]
            row = row_base + row_append

            for col_num in range(len(row)):
                worksheet.write(row_num, col_num, row[col_num])

    workbook.close()

    xlsx_data = output.getvalue()

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=Report.xlsx'
    response.write(xlsx_data)

    return response
