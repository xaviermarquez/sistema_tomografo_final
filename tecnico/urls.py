from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views
from .views import imprimir_fichatecnica, EstudioCorregir, marcar_estudio_corregido, detalle_insumos_imprimir, \
    remito_entrega_insumos_pdf, cerrar_estudio_con_informe, proveedor_listar, proveedor_buscar, insumo_listar, \
    lote_listar, lote_buscar, insumo_servicio_listado, remito_ingreso_proveedor_detalle, detalle_insumos_serv_imprimir, \
    remito_entrega_serv_insumos_pdf, listado_estudios_corregir, listado_insumos_gral_pdf, listado_lotes_pdf

app_name = 'tecnico'

urlpatterns = [
    url(r'insumo/$', views.index_insumos, name='index_insumo'),
    url(r'insumo/crear/$', login_required(views.InsumoCreate.as_view()), name='insumo_crear'),
    url(r'insumo/mod/(?P<pk>\d+)/$', login_required(views.InsumoUpdate.as_view()), name='insumo_mod'),
    url(r'insumo/tecins/$', views.index_tecxins, name='index_tecxins'),
    url(r'insumo/tecinsajuste/$', views.index_tecxins, name='index_tecxins_ajuste'),
    url(r'insumo/tecins/crear/$', views.tec_ins_crear, name='tec_ins_crear'),
    url(r'insumo/tecins/mod/(?P<pk_ti>\d+)/$', views.tec_ins_mod, name='tec_ins_mod'),
    url(r'insumo/tecins/eli/(?P<pk_ti>\d+)/$', views.tec_ins_eli, name='tec_ins_eli'),
    url(r'insumo/tecins/detalle$', views.insumo_tecnico_detalle, name='tec_ins_detalle'),
    url(r'insumo/tecins/entregar$', views.entregar_insumo, name='tec_ins_entregar'),
    url(r'insumo/tecins/ajustar$', views.ajustar_insumo, name='tec_ins_ajustar'),

    url(r'insumo/servicio/entregar$', views.entregar_insumo_servicio, name='servicio_ins_entregar'),
    url(r'insumo/servicio/ajustar$', views.ajustar_insumo_servicio, name='servicio_ins_ajustar'),

    url(r'protocolo/insxpro/$', views.IngresarInsumo, name='ingresar_insumo'),
    url(r'listadoRemitosIngreso/$', views.listado_remito_ingreso, name='listado_remito_ingreso'),
    url(r'listadoRemitoEgreso/$', views.listado_remito_egreso, name='listado_remito_egreso'),
    url(r'insumoMovimientosReporte/$', views.reporte_insumo_movimientos_farmacia, name='insumo_movimientos_reporte'),
    url(r'insumoMovimientos/$', views.insumo_movimientos_farmacia, name='insumo_movimientos_farmacia'),
    url(r'remitoingresonuevo/$', views.remito_ingreso_nuevo, name='remito_ingreso_nuevo'),
    url(r'insumoservicio/', insumo_servicio_listado, name='insumo_servicio'),
    url(r'insumoservicioajuste/', insumo_servicio_listado, name='insumo_servicio_ajuste'),

    url(r'^insumolistar/', insumo_listar, name='insumo_listar'),
    url(r'^insumolotelistar/', lote_listar, name='insumo_lote_listar'),
    url(r'^lotebuscarid/', lote_buscar, name='insumo_lote_buscar'),
    url(r'^insumoslistadogralimprimir/', listado_insumos_gral_pdf, name='insumo_listadogral_imprimir'),
    url(r'^insumosloteimprimir/', listado_lotes_pdf, name='insumo_lotes_imprimir'),
    url(r'^proveedorlistar/', proveedor_listar, name='proveedor_listar'),
    url(r'^proveedorbuscarcodigo/', proveedor_buscar, name='proveedor_buscar'),
    url(r'listadoestudiossolicitados/(?P<serviciocod>\d+)/$', views.listado_estudiossolicitados,
        name='listado_estudiossolicitados'),
    url(r'protocoloalta/(?P<pk>\d+)/$', login_required(views.ProtocoloAlta.as_view()), name='protocolo_alta'),
    url(r'protocolo/imprimirficha$', views.solicitar_imprimirficha, name='imprimir_ficha' ),
    url(r'^inf_tec/', imprimir_fichatecnica, name='informe_tecnico'),
    url(r'^tecnicoestudioscorregir/', listado_estudios_corregir, name='listado_estudios_corregir'),
    url(r'protocoloCambiarEstado$', views.protocolo_cambiar_estado, name='protocolo_cambiar_estado'),
    url(r'ajustenumeracion$', views.protocolo_ajuste_numeracion, name='protocolo_ajuste_numeracion'),
    url(r'ajusteagregarcodigo', views.protocolo_ajuste_agregar_codigo, name='protocolo_ajuste_agregar_codigo'),
    url(r'ajusteremovercodigo', views.protocolo_ajuste_remover_codigo, name='protocolo_ajuste_remover_codigo'),
    url(r'ajustenumeracionservicio$', views.protocolo_ajuste_numeracion_servicio,
        name='protocolo_ajuste_numeracion_servicio'),
    url(r'^estudiocorregir/', login_required(EstudioCorregir.as_view()), name='est_corregir'),
    url(r'^estudiomarcarcorregido/', marcar_estudio_corregido, name='est_marcar_corregido'),
    url(r'^detalleimprimir/', detalle_insumos_imprimir, name='detalle_insumos_imprimir'),
    url(r'^detalleimprimirservicio/', detalle_insumos_serv_imprimir, name='detalle_insumos_serv_imprimir'),
    url(r'^remitoprovisionimprimir/', remito_entrega_insumos_pdf, name='remito_entrega_insumos'),
    url(r'^remitoprovisionimprimirservicio/', remito_entrega_serv_insumos_pdf, name='remito_entrega_insumos_serv'),
    url(r'insumo/servicio/detalle$', views.insumo_servicio_detalle, name='servicio_ins_detalle'),
    url(r'^estudiocerrarinforme/', cerrar_estudio_con_informe, name='estudio_cerrar'),
    url(r'^detalleremitoproveedorimprimir/', remito_ingreso_proveedor_detalle, name='remito_ingreso_proveedor_detalle'),

    url(r'insumoMovimientosReporteIngresosExcel/$', views.reporte_farmacia_ingresos_excel, name='insumo_movimientos_reporte_ingresos_excel'),
    url(r'insumoMovimientosReporteEgresosExcel/$', views.reporte_farmacia_egresos_excel, name='insumo_movimientos_reporte_egresos_excel'),

]
