from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Insumo)
admin.site.register(TecnicoInsumo)
admin.site.register(RemitoIngreso)
admin.site.register(InsumoRemitoIngreso)
admin.site.register(Proveedor)
admin.site.register(InsumoDetalle)
admin.site.register(RemitoEntregaTecnico)
admin.site.register(InsumoRemitoEntrega)
