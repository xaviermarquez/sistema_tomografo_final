from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords


class Insumo(models.Model):
    GRUPO = [
        ('0', '0. Sin Grupo'),
        ('1', '1. Placas Radiologicas'),
        ('2', '2. Reveladores'),
        ('3', '3. Medios de Contraste'),
        ('4', '4. Medicamentos'),
        ('5', '5. Anestesicos'),
        ('6', '6. Accesorios y Descartables'),
    ]

    class Meta:
        ordering = ['id']
    nombre = models.CharField(max_length=120, verbose_name='Nombre del Insumo')
    codigo = models.CharField(max_length=50, null=True, blank=True, unique=True, verbose_name='Código')
    abreviatura = models.CharField(max_length=5, verbose_name='Abreviatura', null=True, blank=True,)
    descripcion = models.TextField(verbose_name='Descripción', null=True, blank=True,)
    grupo = models.CharField(choices=GRUPO, max_length=30, verbose_name='Grupo', default='0')
    unidad = models.FloatField(verbose_name='Unidad Valor', default=1.0)
    unidad_tipo = models.CharField(max_length=20, verbose_name='Unidad Tipo', null=True, blank=True,)
    habilitado = models.BooleanField(default=True)
    marca = models.CharField(max_length=80, verbose_name='Laboratorio', null=True, blank=True,)
    usocompleto = models.BooleanField(verbose_name='Uso Completo', default=True, null=True, blank=True,)
    seregistrastock = models.BooleanField(verbose_name='Se registra Stock Almacen', default=True)
    seregistrastock_tecnico = models.BooleanField(verbose_name='Se registra Stock Tecnico', default=True,)
    seregistrastock_servicio = models.BooleanField(verbose_name='Se registra Stock Servicio', default=True,)
    botones_medidas = models.BooleanField( default=True)
    stock_almacen = models.FloatField(default=0, verbose_name='Saldo Actual')
    entradas = models.FloatField(default=0, verbose_name='Total Entradas')
    salidas = models.FloatField(default=0, verbose_name='Total Salidas')
    uso_minimo = models.FloatField(default=1, verbose_name='Uso minimo')
    uso_maximo = models.FloatField(default=999, verbose_name='Uso maximo')

    # history
    history = HistoricalRecords()


# registra el stock actual del insumo por tecnico
class TecnicoInsumo(models.Model):
    class Meta:
        ordering = ['id']
    tecnico = models.ForeignKey(User, on_delete=models.PROTECT)
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    habilitado_listado = models.BooleanField(default=False)
    stock = models.FloatField(default=0)
    # history
    history = HistoricalRecords()


class Proveedor(models.Model):
    nombre = models.CharField(max_length=200, null=True, blank=True)
    codigo = models.CharField(max_length=50, unique=True)


# Remito ingreso proveedor
class RemitoIngreso(models.Model):
    insumo = models.ManyToManyField(Insumo, through='InsumoRemitoIngreso')
    proveedor = models.ForeignKey(Proveedor, on_delete=models.PROTECT, null=True)
    fecha = models.DateTimeField()
    comprobante_numero = models.CharField(max_length=120, null=True, blank=True)
    orden_compra_numero = models.CharField(max_length=120, null=True, blank=True)
    remito_proveedor_numero = models.CharField(max_length=120, null=True, blank=True)
    remito_proveedor_fecha = models.DateField(null=True)
    factura_numero = models.CharField(max_length=120, null=True, blank=True)
    factura_fecha = models.DateField(null=True)
    factura_importe = models.DecimalField(max_digits=24, decimal_places=2, blank=True, null=True)
    observaciones = models.CharField(max_length=400, null=True, blank=True)
    tipo_ingreso = models.CharField(max_length=60, null=True, blank=True)


class InsumoRemitoIngreso(models.Model):
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    remito = models.ForeignKey(RemitoIngreso, on_delete=models.PROTECT)
    fecha = models.DateTimeField(auto_now_add=True)
    fecha_vencimiento = models.DateField(null=True)
    lote = models.CharField(max_length=120, null=True, blank=True)
    importe = models.DecimalField(max_digits=24, decimal_places=2, blank=True, null=True)
    costo_unitario = models.DecimalField(max_digits=24, decimal_places=2, blank=True, null=True)
    unidades = models.FloatField()
    nombre_comercial = models.CharField(max_length=120, null=True, blank=True)
    laboratorio = models.CharField(max_length=120, null=True, blank=True)


class InsumoLote(models.Model):
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    insumo_remito = models.ForeignKey(InsumoRemitoIngreso, on_delete=models.PROTECT)
    fecha = models.DateTimeField(auto_now_add=True)
    fecha_vencimiento = models.DateField(null=True)
    lote = models.CharField(max_length=120)
    stock_inicial = models.FloatField()
    ingresos = models.FloatField()
    egresos = models.FloatField()
    saldo = models.FloatField()

# remito entrega tecnico
class RemitoEntregaTecnico(models.Model):
    tecnico = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    observaciones = models.CharField(max_length=400, null=True, blank=True)
    comprobante_numero = models.CharField(max_length=120, default='---')

# detalle remito entrega insumo tecnicop
class InsumoRemitoEntrega(models.Model):
    remito_entrega = models.ForeignKey(RemitoEntregaTecnico, on_delete=models.PROTECT, null=True)
    insumo_tecnico = models.ForeignKey(TecnicoInsumo, on_delete=models.PROTECT)
    lote = models.ForeignKey(InsumoLote, on_delete=models.PROTECT, null=True)
    cantidad = models.FloatField()


# detalle de mov. de stock
class InsumoDetalle(models.Model):
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT, null=True, blank=True)
    lote = models.ForeignKey(InsumoLote, on_delete=models.PROTECT, null=True, blank=True)
    tecnico_insumo = models.ForeignKey(TecnicoInsumo, on_delete=models.PROTECT, null=True, blank=True)
    remito_ingreso = models.ForeignKey(RemitoIngreso, on_delete=models.PROTECT, null=True, blank=True)
    insumo_remito_entrega = models.ForeignKey(InsumoRemitoEntrega, on_delete=models.PROTECT, null=True, blank=True)
    is_turno = models.BooleanField(default=True)
    is_entrega_ins_tecnico = models.BooleanField(default=False)
    comprobante = models.CharField(max_length=120, null=True, blank=True)
    ingreso = models.FloatField(default=0, null=True, blank=True)
    egreso = models.FloatField(default=0, null=True, blank=True)
    existencia = models.FloatField(default=0, null=True, blank=True)
    fechahora = models.DateTimeField(auto_now_add=True)
    observaciones = models.CharField(max_length=350, null=True, blank=True)


class ProtocoloBook(models.Model):
    """
        Model for "Libro de Protocolos"
    """

    class Meta:
        ordering = ['id']

    user_tecnico_start = models.ForeignKey(User, on_delete=models.PROTECT, related_name="user_tecnico_start")
    user_tecnico_end = models.ForeignKey(User, on_delete=models.PROTECT, related_name="user_tecnico_end")
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    description = models.CharField(max_length=1000, null=True, blank=True)

