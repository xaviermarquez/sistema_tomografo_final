from django.contrib.auth.models import User
from django.db import models


def user_directory_path_fotofirma(instance, filename):
    return 'fotosfirmas/uid_{0}/{1}'.format(instance.user.id, filename)

PERFILES = (
    ('Tecnico', 'Tecnico'),
    ('Medico', 'Medico'),
    ('Recepcion', 'Recepcion'),
    ('Tecnico Recepcion', 'Tecnico Recepcion'),
    ('Tipeo', 'Tipeo'),
    ('Tipeo Recepcion', 'Tipeo Recepcion'),
    ('Farmacia', 'Farmacia'),
    ('Stock', 'Stock'),
    ('Administrador', 'Administrador'),
)

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    perfil = models.CharField(max_length=50, choices=PERFILES)
    user_guardia = models.BooleanField(default=False)
    user_jefe_tecnico = models.BooleanField(default=False)
    user_jefe_stock = models.BooleanField(default=False)
    user_jefe_facturacion = models.BooleanField(default=False)
    user_reporte = models.BooleanField(default=False)
    user_listado_tecnico_avanzado = models.BooleanField(default=False)
    user_cerrar_informe = models.BooleanField(default=False)
    user_unificar_hc = models.BooleanField(default=False)
    user_listado_stock_servicios = models.BooleanField(default=False)
    matricula = models.CharField(max_length=80, null=True, blank=True)
    foto_firma = models.ImageField(upload_to=user_directory_path_fotofirma, verbose_name="Foto Firma", null=True, blank=True) # foto de la firma de los medicos
    iniciales = models.CharField(max_length=5, null=True, blank=True)
    registra_stock = models.BooleanField(default=False)
    user_feriado = models.BooleanField(default=False)
    sexo_prefix = models.CharField(max_length=20, default='Dr.')

    def __str__(self):
        return self.user.username
