"""
    Functions related to Turnos proccessed by the Rad. technician
"""


def get_protocolo_estado(turno):
    """
        Return the protocolo state from a given turno
        :param turno: turno from Turno
        :return: String with the current protocolo state
            states = (Finalizado, Suspendido Completar o Paciente, Cancelado, No realizado)
    """

    estado = 'No Realizado'
    if turno.estudio_finalizado:
        estado = 'Finalizado'
    elif turno.estudio_suspendido_paciente:
        estado = 'Suspendido Paciente'
    elif turno.estudio_suspendido_completar:
        estado = 'Suspendido Completar'
    elif turno.estudio_cancelado or turno.estudio_reprograma:
        estado = 'Cancelado'

    return estado
