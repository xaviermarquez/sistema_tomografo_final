def patiient_identity_label(patient):
    patient_first_name = patient.nombre if patient.nombre else ''
    patient_last_name = patient.apellido if patient.apellido else ''

    if patient.paciente_nn:
        patient_identity = str(patient.id) + '_' + patient.paciente_nnlabel
    else:
        patient_identity = str(patient.id) + '_' + patient.identificacion_tipo + '_' + patient.identificacion_numero

    return patient_identity + '_' + patient_last_name + '_' + patient_first_name
