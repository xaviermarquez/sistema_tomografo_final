import os
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sistema_ris.settings')
import django
django.setup()
from turno.models import *
from informe.models import *
from usuario.models import *
from tecnico.models import *
import csv
from django.db.models import Q


def fix_tecnicos_movimientos_delete(tecnico_insumo):
    """ script fix insumos ajuste para tecnicos"""
    print('Script INIT TECNICOS AJUSTE')
    movimientos_insumo_tecnico = InsumoDetalle.objects.filter(tecnico_insumo=tecnico_insumo)

    for det in movimientos_insumo_tecnico:
        if det.comprobante:
            turno = Turno.objects.filter(id=det.comprobante).first()
            if turno:
                turno_insumo = TurnoInsumo.objects.filter(turno=turno, insumo=tecnico_insumo.insumo).first()
                if not turno_insumo:
                    print('delte mov', det.id, det.comprobante)
                    det.delete()
    print('Script END')


def fix_tecnicos_movimientos(tecnico_insumo):
    """ script fix insumos ajuste para tecnicos"""
    print('Script INIT TECNICOS AJUSTE INSUMO')
    movimientos_insumo_tecnico = InsumoDetalle.objects.filter(tecnico_insumo=tecnico_insumo).order_by('fechahora')

    saldo = 0
    for det in movimientos_insumo_tecnico:
        det.existencia = saldo + det.ingreso - det.egreso
        saldo = det.existencia
        det.save()
        print(det.id)
    tecnico_insumo.stock = saldo
    tecnico_insumo.save()
    print('Script END')

def fix_tecnicos_movimientos_all():
    all_tecnicos = UserProfile.objects.filter(perfil__contains='Tecnico')
    all_insumos = Insumo.objects.all()
    for tecnico in all_tecnicos:
        for insumo in all_insumos:
            tecnico_insumo = TecnicoInsumo.objects.filter(tecnico=tecnico.user, insumo=insumo).first()
            if tecnico_insumo:
                fix_tecnicos_movimientos_delete(tecnico_insumo)
                fix_tecnicos_movimientos(tecnico_insumo)
                print('Tecnico:', tecnico.user.username, 'Insumo:', insumo.nombre)

# __main__
print('iniciar')
#fix_tecnicos_movimientos_all()


tecnico_insumo = TecnicoInsumo.objects.filter(id=23).first()
fix_tecnicos_movimientos(tecnico_insumo)