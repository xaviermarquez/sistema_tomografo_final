# README #

## Project directory structure
     .
     └── tfe_tomografo # Project name
         │
         ├── core # app core
         │     ├── templates # core specific templates
         │     └── static # core specific assets
         │
         ├── apps # (turno, tecnico, informe) 
         │     ├── templates # app specific templates
         │     └── static # app specific assets
         │
         ├── sistema_ris # inner project directory
         |     ├── urls.py
         |     └── settings.py
         |     
         ├── static # top level project assets
         |     
         ├── media # fotos de pm y doc
         |     
         ├── templates # top level template (base)        
  

## Apps (nueva version 2.0, solo 3 apps) ##
- APP TURNO 
    - Obrasocial: Model(Obrasocial) 
    - Pacientes: Model(Pacientes, Paises, Provincias, Localidades)
    - Turnos: Model(Turno, establecimiento derivador, servicios, medicos derivantes)
- APP TECNICO
    - Tecnicos: Model(Protocolos)
    - Farmacia: Model(Insumos) 
- APP INFORME
    - Informe: Generacion de los informes.
