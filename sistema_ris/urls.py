from django.contrib import admin
from django.urls import path, include
from turno.urls import turno_patterns
from informe.urls import informe_patterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('turno/', include(turno_patterns)),
    path('informe/', include(informe_patterns)),
    path('tecnico/', include('tecnico.urls'))

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
admin.site.site_header = 'Tomografia Computada Sociedad del Estado'
admin.site.site_title = "Tomografia Computada Sociedad del Estado"
admin.site.index_title = "Tomografia Computada Sociedad del Estado"
