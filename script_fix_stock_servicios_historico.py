import os
import datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sistema_ris.settings')
import django
django.setup()
from turno.models import *
from informe.models import *
from usuario.models import *
from tecnico.models import *
import csv
from django.db.models import Q


def fix_servicios_movimientos_delete(servicio_insumo):
    """ script fix insumos ajuste para tecnicos"""
    print('Script INIT SERVICIOS DELETE AJUSTE')
    movimientos_insumo_servicio = InsumoMovimientosServicio.objects.filter(servicio_insumo=servicio_insumo)

    for det in movimientos_insumo_servicio:
        if det.comprobante:
            turno = Turno.objects.filter(id=det.comprobante).first()
            if turno:
                turno_insumo = TurnoInsumo.objects.filter(turno=turno, insumo=servicio_insumo.insumo).first()
                if not turno_insumo:
                    print('delte mov', det.id, det.comprobante)
                    det.delete()
    print('Script END')


def fix_servicios_movimientos(servicio_insumo):
    """ script fix insumos ajuste para tecnicos"""
    print('Script INIT TECNICOS AJUSTE')
    movimientos_insumo_servicio = InsumoMovimientosServicio.objects.filter(servicio_insumo=servicio_insumo).order_by('fechahora')

    saldo = 0
    for det in movimientos_insumo_servicio:
        det.existencia = saldo + det.ingreso - det.egreso
        saldo = det.existencia
        det.save()
        print(det.id)
    servicio_insumo.stock = saldo
    servicio_insumo.save()
    print('Script END')


def fix_servicios_movimientos_all():
    all_servicios = Servicio.objects.filter(is_servicio_externo=False)
    all_insumos = Insumo.objects.all()
    for servicio in all_servicios:
        for insumo in all_insumos:
            servicio_insumo = InsumoServicio.objects.filter(servicio=servicio, insumo=insumo).first()
            if servicio_insumo:
                fix_servicios_movimientos_delete(servicio_insumo)
                fix_servicios_movimientos(servicio_insumo)
                print('servicio:', servicio.nombre, 'Insumo:', insumo.nombre)

# __main__
print('iniciar')
fix_servicios_movimientos_all()