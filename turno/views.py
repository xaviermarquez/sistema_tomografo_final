import base64
import datetime
import io
import json
import locale
import os
import uuid

import pytz
import requests
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.db import transaction
from django.db.models import Q
from django.http import FileResponse
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import ListView, DeleteView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, FormView
from reportlab.lib.pagesizes import LEGAL
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, cm
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Image, Frame

from .form import ObrasocialForm, MedicoForm, PacienteForm, TurnoForm, TurnoActualizarForm, TurnoPacienteForm, \
    TurnoPacienteFormModal
from .models import Obrasocial, MedicoDerivante, Pais, Provincia, Localidad, Departamento, Turno, \
    EstudioSolicitado, EstablecimientoDerivador, ServicioEstudio, TurnoNota, TurnoBloqueo, Servicio, Paciente, \
    TurnoReservado, Barrio, Feriados


class ObrasocialListar(ListView):
    model = Obrasocial
    template_name = 'obrasocial/obrasocial_list.html'


class ObrasocialCrear(CreateView):
    template_name = 'obrasocial/obrasocial_create.html'
    form_class = ObrasocialForm

    def get_success_url(self):
        return reverse('turno:obrasocial_list_view') + "?success_add"


class ObrasocialActualizar(UpdateView):
    model = Obrasocial
    form_class = ObrasocialForm
    template_name = 'obrasocial/obrasocial_update.html'

    def get_success_url(self):
        return reverse('turno:obrasocial_list_view') + "?success_update"


class ObrasocialEliminar(DeleteView):
    model = Obrasocial
    template_name = 'obrasocial/obrasocial_delete.html'

    def get_success_url(self):
        return reverse('turno:obrasocial_listar') + "?success_delete"


class MedicoListar(ListView):
    model = MedicoDerivante
    template_name = 'med_der/med_der_list.html'


class MedicoCrear(CreateView):
    template_name = 'med_der/med_der_create.html'
    form_class = MedicoForm

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return redirect(reverse_lazy('turno:med_listar') + "?error=matricula")

    def get_success_url(self):
        return reverse('turno:med_listar') + "?success_add"


class MedicoActualizar(UpdateView):
    model = MedicoDerivante
    form_class = MedicoForm
    template_name = 'med_der/med_der_update.html'

    def get_success_url(self):
        return reverse('turno:med_listar') + "?success_update"


class MedicoEliminar(DeleteView):
    model = MedicoDerivante
    template_name = 'med_der/med_der_delete.html'

    def get_success_url(self):
        return reverse('turno:med_listar') + "?success_delete"


class PacienteListar(ListView):
    model = Paciente
    template_name = 'paciente/paciente_list.html'


class PacienteCrear(CreateView):
    template_name = 'paciente/paciente_create.html'
    form_class = PacienteForm

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            result = 'error_create'
            return redirect(reverse('turno:pac_listar') + '?' + result)

    def get_success_url(self):
        return reverse('turno:pac_listar') + "?success_add"


class PacienteActualizar(UpdateView):
    model = Paciente
    form_class = PacienteForm
    template_name = 'paciente/paciente_update.html'

    def get_success_url(self):
        return reverse('turno:turno_listar') + "?success_update"


class PacienteEliminar(DeleteView):
    model = Paciente
    template_name = 'paciente/paciente_delete.html'

    def get_success_url(self):
        return reverse('turno:pac_listar') + "?success_delete"


class PacienteCorregir(ListView):
    model = Paciente
    template_name = 'paciente/paciente_list_corregir.html'

    def get_queryset(self):
        queryset = Paciente.objects.filter(corregir=True)
        return queryset


class PacienteCorregirPopUp(UpdateView):
    model = Paciente
    form_class = PacienteForm
    template_name = 'paciente/paciente_corregir_popup.html'

    def get_success_url(self):
        return reverse('turno:pac_corregir') + "?success_update"


@login_required
def carga_provincias(request):
    pais_codigo = request.GET.get('pais_codigo', '')
    select_html_provincias = '<option value = ""> --------- </option>'

    if pais_codigo != '':
        pais = Pais.objects.filter(codigo=pais_codigo).first()

        if pais:
            provincias = Provincia.objects.filter(pais=pais).order_by('nombre')
        else:
            provincias = ''

        for prov in provincias:
            select_html_provincias = select_html_provincias + '<option value = "' + str(
                prov.codigo) + '"> ' + prov.nombre + '</option>'

    response_data = {'provincias': select_html_provincias}

    return JsonResponse(response_data)


@login_required
def carga_departamentos(request):
    provincia_codigo = request.GET.get('provincia_codigo', '')
    select_html_departamentos = '<option value = ""> --------- </option>'

    if provincia_codigo != '':
        provincia = Provincia.objects.filter(codigo=provincia_codigo).first()
        if provincia:
            departamentos = Departamento.objects.filter(provincia=provincia).order_by('nombre')
        else:
            departamentos = ''

        for dpto in departamentos:
            select_html_departamentos = select_html_departamentos + '<option value = "' + str(
                dpto.codigo) + '"> ' + dpto.nombre + '</option>'
    response_data = {'departamentos': select_html_departamentos}

    return JsonResponse(response_data)


@login_required
def carga_localidades(request):
    departamento_codigo = request.GET.get('departamento_codigo', '')
    provincia_codigo = request.GET.get('provincia_codigo', '')
    pais_codigo = request.GET.get('pais_codigo', '')
    select_html_localidades = '<option value = ""> --------- </option>'

    if pais_codigo != '' and provincia_codigo != '' and departamento_codigo != '':
        pais = Pais.objects.filter(codigo=pais_codigo).first()
        provincia = Provincia.objects.filter(codigo=provincia_codigo, pais=pais).first()
        departamento = Departamento.objects.filter(codigo=departamento_codigo,provincia=provincia ).first()
        if departamento:
            localidades = Localidad.objects.filter(departamento=departamento).order_by('nombre')
        else:
            localidades = ''

        for loca in localidades:
            select_html_localidades = select_html_localidades + '<option value = "' + str(
                loca.codigo) + '"> ' + loca.nombre + '</option>'
    response_data = {'localidades': select_html_localidades}

    return JsonResponse(response_data)


@login_required
def consulta_barrio_codigoxid(request):
    barriocodigo = request.GET.get('barrio_codigo')
    barrio = Barrio.objects.filter(codigo=barriocodigo).first()
    if barrio:
        barrioid = barrio.id
    else:
        barrioid = 0
    response_data = {'barrioid': barrioid}

    return JsonResponse(response_data)


@login_required
def consulta_barrio_idxcodigo(request):
    barrioid = request.GET.get('barrio_id')
    barrio = Barrio.objects.filter(id=barrioid).first()
    codigo = str(barrio.codigo)
    response_data = {'barrio_codigo': codigo}

    return JsonResponse(response_data)



@login_required
def constancia_turno_imprimir(request):
    turnoid = request.GET.get('turnoid', '')
    obj_turno = Turno.objects.get(id=turnoid)
    paciente_nombre = obj_turno.paciente.nombre if obj_turno.paciente.nombre else ''
    paciente_apellido = obj_turno.paciente.apellido if obj_turno.paciente.apellido else ''
    pacientenombrecompleto = paciente_apellido + '_' + paciente_nombre + '.pdf'
    pacientenombrecompleto2 = paciente_apellido + ', ' + paciente_nombre
    filename = paciente_apellido + '_' + paciente_nombre
    buffer = io.BytesIO()
    w = LEGAL[0]
    h = LEGAL[1]
    ps = (w, h)
    c = canvas.Canvas(buffer, pagesize=ps)
    c.rect(0.4 * cm, 18.21 * cm, w - 0.8 * cm, (h / 2) - 0.8 * cm)
    x = 0.4 * cm
    y = h - 2 * cm
    xfin = w - 0.4 * cm

    logo = 'static/dist/img/tc.png'
    im = Image(logo, 102, 43)
    x_imagen = 0.42 * cm
    y_imagen = h - 1.63 * cm
    im.drawOn(c, x_imagen, y_imagen - 8)

    c.setFont('Helvetica', 16)
    c.drawString((w / 2) - 70, h - 1.2 * cm, "CONSTANCIA DE TURNO")
    c.setFont('Helvetica', 7)
    c.drawString(w - 115, h - 0.7 * cm, "TOMOGRAFIA COMPUTADA")
    c.drawString(w - 110, h - 1 * cm, "SOCIEDAD DEL ESTADO")
    c.drawString(w - 115, h - 1.3 * cm, "Mariano Boedo 151 - 4400-Salta")
    c.drawString(w - 98, h - 1.6 * cm, "Tel: 0387-4215342")
    c.line(x, y + 2, xfin, y + 2)

    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 28, "Servicio:")
    c.setFont('Helvetica-Bold', 20)
    c.drawString(x_imagen + 61, y_imagen - 28, obj_turno.servicio.nombre_completo)

    server_timezone = pytz.timezone("America/Argentina/Salta")
    f = obj_turno.fechahora_inicio.astimezone(server_timezone)
    dias = {"Monday": "Lunes", "Tuesday": "Martes", "Wednesday": "Miércoles", "Thursday": "Jueves", "Friday": "Viernes",
            "Saturday": "Sábado", "Sunday": "Domingo"}

    fe_str = dias[f.strftime("%A")] + ", " + f.strftime("%d/%m/%Y")
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 280, y_imagen - 28, "Fecha: ")
    c.setFont('Helvetica', 15)
    c.drawString(w - 234, y_imagen - 28, fe_str)

    textopresentarse = ''
    if not obj_turno.urgencia:
        if obj_turno.internado:
            textopresentarse = ' (Presentarse 10 minutos antes)'
        elif obj_turno.contrastevo:
            textopresentarse = ' (Presentarse 2 horas antes)'
        else:
            textopresentarse = ' (Presentarse 30 minutos antes)'
    else:
        textopresentarse = ''

    hora = f.strftime("%H:%M") + textopresentarse
    c.setFont('Helvetica-Bold', 13)
    c.drawString(w - 280, y_imagen - 49, "Hora: ")

    c.setFont('Helvetica', 13)
    c.drawString(w - 242, y_imagen - 49, hora)

    c.setFont('Helvetica-Bold', 13)
    c.drawString(x_imagen + 4, y_imagen - 49, 'Dirección: ')
    c.setFont('Helvetica', 12)
    c.drawString(x_imagen + 70, y_imagen - 49, obj_turno.servicio.direccion)

    c.line(x, y_imagen - 58, xfin, y_imagen - 58)

    c.setFont('Helvetica-Bold', 12)
    x_paciente = x_imagen + 3
    y_paciente = y_imagen - 35
    c.drawString(x_paciente, y_paciente - 38, 'Apellido y Nombre: ')

    c.setFont('Helvetica', 12)
    if obj_turno.paciente.paciente_nn:
        c.drawString(x_paciente + 112, y_paciente - 38, obj_turno.paciente.paciente_nnlabel)
    else:
        c.drawString(x_paciente + 112, y_paciente - 38, pacientenombrecompleto2.upper())
    c.setFont('Helvetica-Bold', 10)
    c.drawString(x_paciente, y_paciente - 53, "Obra Social: ")

    c.setFont('Helvetica', 10)
    obra_soc = obj_turno.obrasocial.nombre
    c.drawString(x_paciente + 65, y_paciente - 53, obra_soc.lower().capitalize())

    tipodni = obj_turno.paciente.identificacion_tipo
    if tipodni == 'NN':
        c.setFont('Helvetica-Bold', 11)

        c.drawString(w - 253, y_paciente - 38, tipodni + ": ")
    else:
        c.setFont('Helvetica-Bold', 13)

        c.drawString(w - 130, y_paciente - 38, tipodni + ": ")

    if tipodni == 'NN':
        c.setFont('Helvetica', 11)

        c.drawString(w - 230, y_paciente - 38, obj_turno.paciente.identificacion_numero)
    else:
        c.setFont('Helvetica', 13)

        c.drawString(w - 105, y_paciente - 38, obj_turno.paciente.identificacion_numero)

    c.line(x, y_imagen - 96, xfin, y_imagen - 96)

    c.setFont('Helvetica-Bold', 10)

    if obj_turno.servicio.tipo == 'CT':
        servicio_tipo = 'Tomografias'
        texto_sol = "Solicitadas: "
    elif obj_turno.servicio.tipo == 'MR':
        servicio_tipo = 'Resonancias'
        texto_sol = "Solicitadas: "
    else:
        servicio_tipo = 'Estudios'
        texto_sol = "Solicitados:"

    c.drawString(x_paciente, y_paciente - 80, servicio_tipo)
    c.drawString(x_paciente, y_paciente - 90, texto_sol )

    q_estudiosol = EstudioSolicitado.objects.filter(turno=obj_turno)
    c.setFont('Helvetica', 10)
    lineadd = 0
    linejump = 0
    for estudisol in q_estudiosol:
        if lineadd == 30:
            linejump += 170
            lineadd = 0
        esttexto = '- ' + estudisol.servicio_estudio.nombre
        c.drawString(x_paciente + 65 + linejump, y_paciente - 76 - lineadd, esttexto)
        lineadd += 10

    c.line(x, y_imagen - 138, xfin, y_imagen - 138)

    c.setFont('Helvetica-Bold', 10)
    if obj_turno.urgencia:
        if  obj_turno.servicio.tipo == "MR":
            tipoatencion = "URGENCIA - INTERNADO"
        else:
            tipoatencion = 'URGENCIA (En el caso que el Paciente no se encuentre estable se requiere acompañamiento del Prof. Médico)'
    elif obj_turno.internado:
        tipoatencion = 'PROGRAMADO - INTERNADO'
    else:
        tipoatencion = 'PROGRAMADO - AMBULATORIO'
    c.drawString(x_paciente, y_paciente - 116, 'Atención: ' + tipoatencion)

    c.line(x, y_imagen - 158, xfin, y_imagen - 158)

    band_indicaciones = False

    if obj_turno.anestesia:
        band_indicaciones = True
        hoy = datetime.datetime.now(tz=pytz.utc)
        edad = relativedelta(hoy.date(), obj_turno.paciente.fecnac)
        edadyear = edad.years
        edadmonth = edad.months

        textoedadpaciente = ''
        if edadyear >= 1:
            textoedadpaciente = str(edadyear) + ' años'
        else:
            textoedadpaciente = str(edadmonth) + ' meses'
        if edadyear > 10:
            textoanestesia_ayuno = '- Edad del paciente (' + textoedadpaciente + '): [ mayor a 10 años ] con "8 HORAS DE AYUNO DE LIQUIDOS Y SÓLIDOS"'
        elif edadyear >= 1 or edadyear < 1 and edadmonth > 6:
            textoanestesia_ayuno = '- Edad del paciente (' + textoedadpaciente + '): [ 6 meses a 10 años ] con "6 HORAS DE AYUNO DE LIQUIDOS Y SÓLIDOS"'
        else:
            textoanestesia_ayuno = '- Edad del paciente (' + textoedadpaciente + '): [ 0 a 6 meses ] con "4 HORAS DE AYUNO DE LIQUIDOS Y SÓLIDOS"'

        if obj_turno.contrasteev:
            textoanestesiatitulo = '➢ Anestesia y Contraste Endovenoso:'
        else:
            textoanestesiatitulo = '➢ Anestesia:'

        texto1 = 'Para la realización del estudio que ha sido asignado deberá concurrir'
        texto2 = textoanestesia_ayuno
        texto3 = 'Para este estudio deberá traer indefectiblemente, un ELECTROCARDIOGRAMA con antigüedad no mayor a 6 meses.'
        c.setFont('Helvetica-Bold', 10)
        c.drawString(x_paciente + 5, y_paciente - 152, textoanestesiatitulo)
        c.setFont('Helvetica', 10)
        c.drawString(x_paciente + 14, y_paciente - 164, texto1)
        c.drawString(x_paciente + 14, y_paciente - 176, texto2)
        c.setFont('Helvetica-Bold', 10)
        c.drawString(x_paciente + 14, y_paciente - 188, texto3)

        if obj_turno.contrastevo:
            if obj_turno.internado:
                textocoraltitulo = '➢ Contraste Oral:'
                textocoral1 = 'Tomar el Contraste Oral 2 HORAS antes del turno'
                c.setFont('Helvetica-Bold', 10)
                c.drawString(x_paciente + 5, y_paciente - 202, textocoraltitulo)
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 214, textocoral1)
            else:
                textocoraltitulo = '➢ Contraste Oral:'
                textocoral1 = 'Presentarse 2 horas antes del turno para la preparación con Contraste Oral'
                c.setFont('Helvetica-Bold', 10)
                c.drawString(x_paciente + 5, y_paciente - 202, textocoraltitulo)
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 214, textocoral1)
        elif obj_turno.tomaagua:
            if obj_turno.internado:
                textocoraltitulo = '➢ Preparación oral con Agua:'
                textocoral1 = 'Tomar 1 LITRO de agua 2 HORAS antes del turno'
                c.setFont('Helvetica-Bold', 10)
                c.drawString(x_paciente + 5, y_paciente - 202, textocoraltitulo)
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 214, textocoral1)
            else:
                textocoraltitulo = '➢ Preparación oral con Agua:'
                textocoral1 = 'Tomar 1 LITRO de agua 2 HORAS antes del turno'
                c.setFont('Helvetica-Bold', 10)
                c.drawString(x_paciente + 5, y_paciente - 202, textocoraltitulo)
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 214, textocoral1)
    else:
        if obj_turno.contrasteev or obj_turno.contrastevo or obj_turno.tomaagua:
            if obj_turno.contrastevo and obj_turno.contrasteev:
                textotitulocontrasteev = '➢ Contraste Oral y Endovenoso:'
            elif obj_turno.tomaagua  and obj_turno.contrasteev:
                textotitulocontrasteev = '➢ Preparación oral con Agua y Contraste Endovenoso:'
            elif obj_turno.contrastevo:
                textotitulocontrasteev = '➢ Contraste Oral:'
            elif obj_turno.tomaagua:
                textotitulocontrasteev = '➢ Preparación oral con Agua:'
            elif obj_turno.contrasteev:
                textotitulocontrasteev = '➢ Contraste Endovenoso:'

            band_indicaciones = True
            texto1 = 'Para la realización del estudio que ha sido asignado deberá concurrir'
            texto2 = '- Con 6 (seis) HORAS DE AYUNO DE LÍQUIDOS Y SÓLIDOS'
            c.setFont('Helvetica-Bold', 10)
            c.drawString(x_paciente + 5, y_paciente - 152, textotitulocontrasteev)
            c.setFont('Helvetica', 10)
            c.drawString(x_paciente + 14, y_paciente - 164, texto1)
            c.drawString(x_paciente + 14, y_paciente - 176, texto2)

        if obj_turno.contrastevo:
            band_indicaciones = True
            if obj_turno.internado:
                textocoral1 = '- Tomar el Contraste Oral 2 HORAS antes del turno'
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 190, textocoral1)
            else:
                textocoral1 = '- Presentarse 2 HORAS antes del turno para la preparación con Contraste Oral'
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 190, textocoral1)
        elif obj_turno.tomaagua:
            band_indicaciones = True
            if obj_turno.internado:
                textocoral1 = '- Tomar 1 LITRO de agua 2 HORAS antes del turno'
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 190, textocoral1)
            else:
                textocoral1 = '- Tomar 1 LITRO de agua 2 HORAS antes del turno'
                c.setFont('Helvetica', 10)
                c.drawString(x_paciente + 14, y_paciente - 190, textocoral1)

    if obj_turno.canalizacion:
        band_indicaciones = True
        c.setFont('Helvetica-Bold', 10)
        textotitulocanalizacion = '➢ Canalizacion Previa:'
        c.drawString(x_paciente + 5, y_paciente - 228, textotitulocanalizacion)

        c.setFont('Helvetica', 10)
        texto1 = 'Para el estudio de Tomografía Computada con contraste IV (Iodo), el paciente deberá concurrir, con una vía (Preferentemente'
        texto2 = 'Angiocateter 18G o 20G), rogamos al servicio de enfermería de la sala, comprobar si la vía está en perfectas condiciones'
        texto3 = 'en el momento que sale para el tomógrafo (la administración del medicamento contraste iodado se realiza con bomba inyectora)'
        texto4 = 'Nota: Si el diagnóstico presuntivo del paciente es TEP, la venopuntura deberá realizarse en el brazo derecho'

        c.drawString(x_paciente + 14, y_paciente - 240, texto1)
        c.drawString(x_paciente + 14, y_paciente - 252, texto2)
        c.drawString(x_paciente + 14, y_paciente - 264, texto3)
        c.drawString(x_paciente + 14, y_paciente - 276, texto4)

    if band_indicaciones:
        c.setFont('Helvetica-Bold', 10)
        c.drawString(x_paciente, y_paciente - 136, 'Indicaciones para el turno ')
        c.setFont('Helvetica', 10)
        c.drawString(x_paciente + 130, y_paciente - 136,
                     '(Si no se cumplen con las indicaciones solicitadas, no se podra realizar el estudio):')

    c.line(x, y_paciente - 280, xfin, y_paciente - 280)
    c.setFont('Helvetica-Bold', 10)
    renglon = "IMPORTANTE: LA NO CONCURRENCIA EN EL TIEMPO INDICADO, PUEDE PROVOCAR LA PÉRDIDA DE ESTE TURNO."
    c.drawString(x_paciente, y_paciente - 291, renglon)
    c.setFont('Helvetica', 10)

    renglon = "- Traer estudios anteriores (copias, no originales): Tomografías, Radiografías, Ecografías, Resonancias. etc. "
    c.drawString(x_paciente, y_paciente - 303, renglon)
    renglon = "- Concurrir con ropa sencilla y sin elementos metálicos (aros, piercing, etc.), maquillaje, pelo mojado o gel."
    c.drawString(x_paciente, y_paciente - 315, renglon)

    c.line(x, y_paciente - 318, xfin, y_paciente - 318)


    c.setFont('Helvetica-Bold', 10)
    if obj_turno.entrega_digital and obj_turno.email_entregadigital:
        texto_conformidad_envio = "Autorizó expresamente a TCSE la remisión del informe del presente estudio a mi dirección electrónica personal:"
        texto_conformidad_envio2 = obj_turno.email_entregadigital
        texto_conformidad_envio3 = "Y a la dirección electrónica del Profesional Solicitante."
        c.drawString(x_paciente + 3, y_paciente - 330, texto_conformidad_envio)
        c.drawString(x_paciente + 12, y_paciente - 342, texto_conformidad_envio2)
        c.setLineWidth(0.3)
        c.setDash(array=[1], phase=0)
        c.line(x_paciente + 3, y_paciente - 344, xfin - 50, y_paciente - 344)
        c.drawString(x_paciente + 3, y_paciente - 354, texto_conformidad_envio3)
        c.setLineWidth(1)
        c.setDash(array=[], phase=0)

    c.line(x, y_paciente - 358, xfin, y_paciente - 358)

    c.setFont('Helvetica', 10)
    c.setFont('Helvetica', 8)
    usr = "Atendió: " + obj_turno.usuario_creacion.last_name + ', ' + obj_turno.usuario_creacion.first_name
    c.drawString(x_paciente, y_paciente - 406, usr)
    expedienteid = "N° Orden: " + str(obj_turno.id)
    c.drawString(x_paciente + 260, y_paciente - 406, expedienteid)

    styles = getSampleStyleSheet()
    styleN = styles['Normal']
    story = []
    observacion = "Observaciones: " + obj_turno.observacion
    f = Frame(x_paciente, inch, xfin - x_paciente, 499.795, showBoundary=0)
    story.append(Paragraph(observacion, styleN))
    f.addFromList(story, c)
    c.line(w - 190, y_paciente - 398, w - 30, y_paciente - 398)
    firma = "Firma del paciente"
    c.drawString(w - 147, y_paciente - 406, firma)
    c.showPage()
    c.save()
    buffer.seek(0)
    response = HttpResponse(FileResponse(buffer, as_attachment=True, filename=pacientenombrecompleto),
                            content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename=' + filename + '.pdf'
    return response


class TurnoListar(TemplateView):
    template_name = 'turno/turno_listar.html'


class TurnoBrightspeed(LoginRequiredMixin, ListView):
    model = Turno
    template_name = 'turno/turno_brightspeed.html'

    def get_queryset(self):
        days = -1 * int(self.request.GET.get('days', 32))
        from_date = datetime.datetime.now()
        from_date_timezone = timezone.make_aware(from_date) + datetime.timedelta(days=days)
        servicio_codigo = self.request.GET.get('serviciocod', '')
        turnos_bs = None
        if servicio_codigo:
            turnos_bs = Turno.objects.filter(fechahora_inicio__gte=from_date_timezone, servicio__codigo=servicio_codigo, habilitado=True)

        return turnos_bs

    def get_context_data(self, **kwargs):
        servicio_codigo = self.request.GET.get('serviciocod', '')
        servicio = Servicio.objects.get(codigo=servicio_codigo)
        otrosservicios = Servicio.objects.all().exclude(id=servicio.id)
        turnos_bs = self.object_list
        days = -1 * int(self.request.GET.get('days', 32))
        hoy = datetime.datetime.now()
        hoymenos30 = timezone.make_aware(hoy) + datetime.timedelta(days=days)
        lista_notas = TurnoNota.objects.filter(fecha__gte=hoymenos30, servicio=servicio)
        room_name_json = mark_safe(json.dumps('canal_turno'))
        turnos_bloqueados = TurnoBloqueo.objects.filter(servicio=servicio.codigo)
        turnos_reservados = TurnoReservado.objects.filter(fecha__gte=hoymenos30, servicio=servicio)
        feriados = Feriados.objects.filter(fecha__gte=hoymenos30)

        context = {'turnos_bs': turnos_bs, 'lista_notas': lista_notas,
                   'room_name_json': room_name_json, 'turnos_bloqueados': turnos_bloqueados, 'feriados':feriados,
                   'turnos_reservados': turnos_reservados, 'servicio': servicio, 'otrosservicios':otrosservicios, 'days_limit':days}
        return context


class TurnoCrear(CreateView):
    form_class = TurnoForm
    template_name = 'turno/turno_nuevo_form.html'

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form_edit = form.save(commit=False)
            servicio_id_aux = request.POST.get("servicio_id_aux", "")
            servicio_selected = Servicio.objects.get(codigo=servicio_id_aux)
            form_edit.servicio = servicio_selected
            entregadigital = request.POST.get("entregadigital", "")
            if entregadigital == 'on':
                form_edit.entrega_digital = True

            whatsapp = request.POST.get("whatsapp", "")
            if whatsapp == 'on':
                form_edit.whatsapp = True

            recepcion_suspende = request.POST.get("recepcion_suspende", "")
            if recepcion_suspende == 'on':
                form_edit.turno_recepcion_suspendido = True

            fechaini = request.POST.get("fechahora_inicio", "")
            fechafin = request.POST.get("fechahora_fin", "")
            objdate_fechafin = datetime.datetime.strptime(fechafin, '%Y-%m-%dT%H:%M')
            objdate_fechaini = datetime.datetime.strptime(fechaini, '%Y-%m-%dT%H:%M')
            fechaini_aware = timezone.make_aware(objdate_fechaini, timezone.get_current_timezone())
            fechafin_aware = timezone.make_aware(objdate_fechafin, timezone.get_current_timezone())

            form_edit.fechahora_inicio = fechaini_aware

            if servicio_selected.codigo == 2:
                form_edit.fechahora_fin = fechafin_aware

                fechaini2 = request.POST.get("fechahora2_inicio", "")
                fechafin2 = request.POST.get("fechahora2_fin", "")

                if fechaini2 != '':
                    objdate_fechaini2 = datetime.datetime.strptime(fechaini2, '%Y-%m-%dT%H:%M')
                    fechaini_aware2 = timezone.make_aware(objdate_fechaini2, timezone.get_current_timezone())
                    form_edit.fechahora_inicio2 = fechaini_aware2
                else:
                    form_edit.fechahora_inicio2 = form_edit.fechahora_inicio

                if fechafin2 != '':
                    objdate_fechafin2 = datetime.datetime.strptime(fechafin2, '%Y-%m-%dT%H:%M')
                    fechafin_aware2 = timezone.make_aware(objdate_fechafin2, timezone.get_current_timezone())
                    form_edit.fechahora_fin2 = fechafin_aware2
                else:
                    form_edit.fechahora_fin2 = form_edit.fechahora_fin

            else:
                time_guardia_inicio = form_edit.servicio.hora_guardiaini
                time_guardia_fin = form_edit.servicio.hora_guardiafin
                time_turnoinicio = fechaini_aware.time()
                time_turnofin = fechafin_aware.time()
                dayofweek = fechaini_aware.weekday()

                if not (time_guardia_fin < time_turnoinicio < time_guardia_inicio and dayofweek not in (5, 6)):
                    fechafin_guardia = fechaini_aware + datetime.timedelta(minutes=10)
                    form_edit.fechahora_fin = fechafin_guardia
                else:
                    if not form_edit.turno_enpartes_primero:
                        fechafin_turnopartes = fechaini_aware + datetime.timedelta(minutes=20)
                        form_edit.fechahora_fin = fechafin_turnopartes
                    else:
                        form_edit.fechahora_fin = fechafin_aware
                form_edit.fechahora_inicio2 = form_edit.fechahora_inicio
                form_edit.fechahora_fin2 = form_edit.fechahora_fin
            est1 = request.POST.get("est1", "")
            est2 = request.POST.get("est2", "")
            est3 = request.POST.get("est3", "")
            est4 = request.POST.get("est4", "")
            est5 = request.POST.get("est5", "")
            est6 = request.POST.get("est6", "")
            est7 = request.POST.get("est7", "")
            est8 = request.POST.get("est8", "")
            est9 = request.POST.get("est9", "")
            est10 = request.POST.get("est10", "")
            est11 = request.POST.get("est11", "")

            estudios_solicitados = [est1, est2, est3, est4, est5, est6, est7, est8, est9, est10, est11]

            for item in range (0, len(estudios_solicitados)):
                if estudios_solicitados[item] != "":
                    estudios_solicitados[item] = str(int(estudios_solicitados[item]))

            estudios_solicitados_sinvacio = list(filter(None, estudios_solicitados))

            string_estdios_sol = ''
            string_estdios_sol_codigos = ''
            estudio_prefix = form_edit.servicio.protocolo_prefix

            lengestudios = len(estudios_solicitados_sinvacio)
            lencount = 0
            for estudi in estudios_solicitados_sinvacio:
                lencount += 1
                s_estudi = str(estudi)
                if not len(s_estudi) > 1:
                    s_estudi = '0' + s_estudi

                if not lencount == lengestudios:
                    if estudi != '':
                        string_estdios_sol += estudio_prefix + s_estudi + ' - '
                        string_estdios_sol_codigos += 'cKw' + estudi + 'x '
                else:
                    if estudi != '':
                        string_estdios_sol += estudio_prefix + s_estudi
                        string_estdios_sol_codigos += 'cKw' + estudi + 'x '

            form_edit.estudios_solaux = string_estdios_sol
            form_edit.turno_estudios_soli_backup = string_estdios_sol
            form_edit.estudios_solaux_codigos = string_estdios_sol_codigos

            usuario_creacion = request.user
            form_edit.usuario_creacion = usuario_creacion

            imagen_pm = request.POST.get("img_pm", "")
            imagen_dni1 = request.POST.get("img_dni1", "")
            imagen_dni2 = request.POST.get("img_dni2", "")
            form_edit.save()
            form_imagen_pm = request.FILES.get("foto_pedidomedico", "")
            if form_imagen_pm:
                initial_path = form_edit.foto_pedidomedico.path
                form_edit.foto_pedidomedico.name = 'fotospmdoc/uid_' + str(form_edit.paciente.id) + '/pm/' + 'pm_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_pedidomedico.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_pm != '':
                img_received = imagen_pm
                clear_image_data = img_received.replace('data:image/jpeg;base64,', '')
                image_data = base64.b64decode(clear_image_data)
                imagen_nombre = 'pm_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                setattr(form_edit, 'foto_pedidomedico', ContentFile(image_data, imagen_nombre))

            form_imagen_doc = request.FILES.get("foto_documento", "")
            if form_imagen_doc:
                initial_path = form_edit.foto_documento.path
                form_edit.foto_documento.name = 'fotospmdoc/uid_' + str(form_edit.paciente.id) + '/dni/' + 'dni1_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_documento.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_dni1 != '':
                img_received1 = imagen_dni1
                clear_image_data1 = img_received1.replace('data:image/jpeg;base64,', '')
                image_data1 = base64.b64decode(clear_image_data1)
                imagen_nombre1 = 'dni1_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                setattr(form_edit, 'foto_documento', ContentFile(image_data1, imagen_nombre1))

            form_imagen_doc2 = request.FILES.get("foto_documento2", "")
            if form_imagen_doc2:
                initial_path = form_edit.foto_documento2.path
                form_edit.foto_documento2.name = 'fotospmdoc/uid_' + str(
                    form_edit.paciente.id) + '/dni/' + 'dni2_' + form_edit.paciente.identificacion_numero + '_' + str(
                    form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_documento2.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_dni2 != '':
                img_received2 = imagen_dni2
                clear_image_data2 = img_received2.replace('data:image/jpeg;base64,', '')
                image_data2 = base64.b64decode(clear_image_data2)
                imagen_nombre2 = 'dni2_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                setattr(form_edit, 'foto_documento2', ContentFile(image_data2, imagen_nombre2))


            turnorelacionado_id = request.POST.get("turnorelacionado_id", "")
            if turnorelacionado_id != "":
                turno_padre = Turno.objects.get(id=turnorelacionado_id)
                if turno_padre.tipomodalidad == "En Partes":
                    form_edit.turno_relacionado = turno_padre

            ape = form_edit.paciente.apellido if form_edit.paciente.apellido else ''
            nom = form_edit.paciente.nombre if form_edit.paciente.nombre else ''
            form_edit.backupnombrecompleto = ape + ', ' + nom
            form_edit.backupdni = form_edit.paciente.identificacion_numero

            if form_edit.servicio.is_servicio_externo:
                form_edit.estudio_terminado = True
                form_edit.estudio_finalizado = True

            form_edit.save()
            servicio = form_edit.servicio

            estudio_sol_cantidad = 1
            for index, estudio_sol in enumerate(estudios_solicitados_sinvacio):
                if servicio.tipo == 'CR':
                    est_cant = request.POST.get('est' + str(index + 1) + '_cant', '')
                    estudio_sol_cantidad = int(est_cant) if est_cant != '' else 2

                estudio_servicio = ServicioEstudio.objects.get(servicio=servicio, codigo=estudio_sol)
                obj_estudio_solicitado = EstudioSolicitado(turno=form_edit, servicio=servicio,
                                                           servicio_estudio=estudio_servicio, cantidad=estudio_sol_cantidad)
                obj_estudio_solicitado.save()

            turnoblo = 'turno_reservado_' + usuario_creacion.username
            servicio_id = form_edit.servicio.codigo

            t_id = form_edit.id
            fechaini_string = form_edit.fechahora_inicio.strftime("%Y-%m-%dT%H:%M")
            fechafin_string = form_edit.fechahora_fin.strftime("%Y-%m-%dT%H:%M")

            t_fechaini = fechaini_string
            t_fechafin = fechafin_string

            fechaini_string2 = form_edit.fechahora_inicio2.strftime("%Y-%m-%dT%H:%M")
            fechafin_string2 = form_edit.fechahora_fin2.strftime("%Y-%m-%dT%H:%M")
            t_fechaini2 = fechaini_string2
            t_fechafin2 = fechafin_string2

            t_pres = form_edit.presente
            t_confirmacion = form_edit.confirmacion
            t_estudio_terminado = form_edit.estudio_terminado
            t_anes = form_edit.anestesia
            t_cev = form_edit.contrasteev
            t_cvo = form_edit.contrastevo
            t_agua = form_edit.tomaagua
            t_urg = form_edit.urgencia
            t_int = form_edit.internado
            t_pacnom = form_edit.paciente.nombre.upper() if form_edit.paciente.nombre else ''
            t_pacape = form_edit.paciente.apellido.upper() if form_edit.paciente.apellido else ''
            t_pacdoc = form_edit.paciente.identificacion_numero
            t_estnom = form_edit.establecimiento_derivador.abreviatura
            t_cod = form_edit.estudios_solaux_codigos
            t_whatsapp = form_edit.whatsapp
            t_pacientearm = form_edit.paciente_arm
            t_pacid = form_edit.paciente.id
            t_resofix = form_edit.turno_fixreso
            t_sexo = form_edit.paciente.sexo
            t_codcomp = form_edit.estudios_solaux

            if form_edit.foto_pedidomedico:
                t_fotopm = True
            else:
                t_fotopm = False

            if form_edit.foto_documento:
                t_fotodoc1 = True
            else:
                t_fotodoc1 = False

            if form_edit.foto_documento2:
                t_fotodoc2 = True
            else:
                t_fotodoc2 = False

            t_susp = form_edit.turno_recepcion_suspendido

            if not form_edit.paciente.paciente_nn:
                if t_pacdoc.isdigit():
                    t_pacdoc = int(t_pacdoc)
                    locale.setlocale(locale.LC_NUMERIC, 'es_AR.utf8')
                    t_pacdoc = locale.format('%.0f', t_pacdoc, True)
            else:
                t_pacdoc = 'Paciente NN'
                t_pacnom = ''
                t_pacape = form_edit.paciente.paciente_nnlabel

            eliminarturnobloqueoycrear_calendar(turnoblo, usuario_creacion.username, servicio_id, t_id, t_fechaini,
                                                t_fechafin, t_pres, t_anes, t_cev, t_cvo, t_agua, t_urg, t_int,
                                                t_pacnom, t_pacape, t_pacdoc, t_estnom, t_cod, t_confirmacion,
                                                t_estudio_terminado, t_whatsapp, t_pacientearm, t_fotopm, t_fotodoc1,
                                                t_fotodoc2, t_pacid, t_resofix, t_fechaini2, t_fechafin2, t_codcomp, t_sexo, t_susp)

            if form_edit.servicio.usa_worklist_vm and form_edit.presente:
                crearturno_worklist(t_id)

            band_imprimir = request.GET.get('bandimprimir', '')
            if band_imprimir == '1':
                responsejson = JsonResponse({"turnoid": str(form_edit.id), "imprimir": "1"})
            else:
                responsejson = JsonResponse({"turnoid": str(form_edit.id), "imprimir": "0"})
            return responsejson

        else:
            errores = form.errors
            responsejson = JsonResponse({"errores":errores})
            return responsejson

def crearturno_worklist(turnoid):
    turno = Turno.objects.get(id=turnoid)
    if turno.paciente.sexo == 'Masculino':
        pacientesexo = 'M'
    elif turno.paciente.sexo == 'Femenino':
        pacientesexo = 'F'
    else:
        pacientesexo = 'O'

    if turno.paciente.identificacion_tipo == "DU":
        id_tipo = "DNI"
    else:
        id_tipo = turno.paciente.identificacion_tipo

    if turno.servicio.tipo == 'MR':
        acces_numer_sufx = '-'
    else:
        acces_numer_sufx = ''

    pac_nombre = turno.paciente.paciente_nnlabel if turno.paciente.paciente_nn else turno.paciente.nombrecompleto.upper()
    pac_idtipo = id_tipo + turno.paciente.identificacion_numero
    datosjson = {
        "datosEstudio": {
            "pacientenombre": pac_nombre,
            "pacienteid": pac_idtipo,
            "pacientesexo": pacientesexo,
            "pacienteobrasocial": turno.obrasocial.codigo,
            "estudioaccessionnumber": turno.servicio.prefix_access_number + '-' + str(turno.id) + acces_numer_sufx,
            "estudiomodalidad": turno.servicio.tipo,
            "turnoid": turno.id,
            "turnofecha": turno.fechahora_inicio.strftime('%Y-%m-%d %H:%M:%S'),
            "aetitle": turno.servicio.aetitle,
            "centrolocal": "TSE",
            "pacientepeso": "0",
            "pacientetalla": "0",
        }
    }
    if turno.paciente.fecnac:
        datosjson['datosEstudio']['pacientefechanacimiento'] = turno.paciente.fecnac.strftime('%Y-%m-%d')
    # TODO cambiar urls hardcoded
    r = requests.post("http://172.16.5.1:8081/webservicewl/index.php/acciones/abturno", json=datosjson)
    response = r.json()
    return response


# genera el link imagenes pacs
def generar_linkimagenes(turnoid, sufix_turno_id=0):
    turno = Turno.objects.get(id=turnoid)
    result = {"success": False}

    if not turno:
        return result

    turno_id_pacs = str(turno.id) if sufix_turno_id == 0 else str(turno.id) + '-' + str(sufix_turno_id)
    if turno.estudio_accessnumber:
        access_numbers = [turno.estudio_accessnumber]
    else:
        access_numbers = [turno.servicio.prefix_access_number + '-' + turno_id_pacs]
        access_numbers.append(turno.servicio.prefix_access_number + turno_id_pacs)

    for access_num in access_numbers:
        datosjson = {
            "datosEstudioABuscar": {
                "estudioaccessionnumber": access_num,
                "red": "0"
            }
        }

        #TODO cambiar urls hardcoded
        r_local = requests.post("http://172.16.5.1:8081/webservicewl/index.php/acciones/url1turno", json=datosjson)
        r_externa = requests.post("http://172.16.5.1:8081/webservicewl/index.php/acciones/url2turno", json=datosjson)

        responsejson_local = r_local.json()
        responsejson_externa = r_externa.json()

        if ("mensaje" not in responsejson_local) and ("mensaje" not in responsejson_externa):
            result["url_interna"] = responsejson_local["url"]
            result["url_externa"] = responsejson_externa["url"]
            datosjson_tags = {
                "datosEstudioABuscar": {
                    "estudioaccessionnumber": access_num,
                }
            }
            tags_dicom = requests.post("http://172.16.5.1:8081/webservicewl/index.php/acciones/consultatags", json=datosjson_tags)
            responsejson_tags = tags_dicom.json()

            result["estudio_descripcion"] = responsejson_tags["studyDescrp"]
            result["success"] = True
            break
        else:
            if responsejson_externa['mensaje'] == 'ERROR, ESTUDIO NO EXISTE' or responsejson_local['mensaje'] == 'ERROR, ESTUDIO NO EXISTE':
                result["success"] = False

    return result


class TurnoActualizar(UpdateView):
    form_class = TurnoActualizarForm
    model = Turno
    template_name = 'turno/turno_actualizar_form.html'

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        turno_editar = Turno.objects.get(pk=self.kwargs['pk'])
        form = TurnoActualizarForm(request.POST, request.FILES, instance=turno_editar)
        estudio_tecnico_procesado = turno_editar.estudio_finalizado or turno_editar.estudio_terminado or turno_editar.estudio_suspendido_paciente or turno_editar.estudio_suspendido_completar

        if estudio_tecnico_procesado:
            no_edit_fields = ['paciente', 'contrasteev', 'contrastevo', 'anestesia', 'internado', 'canalizacion', 'habilitado', 'urgencia', 'paciente_arm']
            for field_to_remove in no_edit_fields:
                del form.fields[field_to_remove]

        if form.is_valid():
            form_edit = form.save(commit=False)

            fechaold = request.POST.get("fechaold_ini", '')
            fechanew_ini_datetime = form_edit.fechahora_inicio
            if fechaold != '':
                fechaold_ini_datetime = datetime.datetime.strptime(fechaold, '%Y-%m-%d %H:%M:%S').astimezone(timezone.get_current_timezone())
                if fechaold_ini_datetime != fechanew_ini_datetime:
                    fechadiff = fechanew_ini_datetime - fechaold_ini_datetime
                    form_edit.fechahora_fin = form_edit.fechahora_fin + fechadiff


            servicio_id_aux = request.POST.get("servicio_id_aux2", "")
            servicio_selected = Servicio.objects.get(codigo=servicio_id_aux)
            estudio_prefix = servicio_selected.protocolo_prefix

            recepcion_suspende = request.POST.get("turno_recepcion_suspendido", "")
            if recepcion_suspende == 'on':
                form_edit.turno_recepcion_suspendido = True

            if not estudio_tecnico_procesado or turno_editar.servicio.is_servicio_externo:
                form_edit.fechahora_inicio2 = form_edit.fechahora_inicio
                form_edit.fechahora_fin2 = form_edit.fechahora_fin
                est1 = request.POST.get("est1", "")
                est2 = request.POST.get("est2", "")
                est3 = request.POST.get("est3", "")
                est4 = request.POST.get("est4", "")
                est5 = request.POST.get("est5", "")
                est6 = request.POST.get("est6", "")
                est7 = request.POST.get("est7", "")
                est8 = request.POST.get("est8", "")
                est9 = request.POST.get("est9", "")
                est10 = request.POST.get("est10", "")
                est11 = request.POST.get("est11", "")

                estudios_solicitados = [est1, est2, est3, est4, est5, est6, est7, est8, est9, est10, est11]
                estudios_solicitados_sinvacio = list(filter(None, estudios_solicitados))

                string_estdios_sol = ''
                string_estdios_sol_codigos = ''

                lengestudios = len(estudios_solicitados_sinvacio)
                lencount = 0

                for estudi in estudios_solicitados_sinvacio:
                    lencount += 1
                    s_estudi = str(estudi)
                    if not len(s_estudi) > 1:
                        s_estudi = '0' + s_estudi
                    if not lencount == lengestudios:
                        if estudi != '':
                            string_estdios_sol += estudio_prefix + s_estudi + ' - '
                            string_estdios_sol_codigos += 'cKw' + estudi + 'x '
                    else:
                        if estudi != '':
                            string_estdios_sol += estudio_prefix + s_estudi
                            string_estdios_sol_codigos += 'cKw' + estudi + 'x '

                form_edit.estudios_solaux = string_estdios_sol
                form_edit.turno_estudios_soli_backup = string_estdios_sol
                form_edit.estudios_solaux_codigos = string_estdios_sol_codigos
                servicio = form_edit.servicio
                lista_estudios_delete = EstudioSolicitado.objects.filter(turno=turno_editar)
                lista_estudios_delete.delete()

                estudio_sol_cantidad = 1
                for index, estudio_sol in enumerate(estudios_solicitados_sinvacio):
                    if servicio.tipo == 'CR':
                        est_cant = request.POST.get('est' + str(index + 1) + '_cant', '')
                        estudio_sol_cantidad = int(est_cant) if est_cant != '' else 2

                    estudio_servicio = ServicioEstudio.objects.get(servicio=servicio, codigo=estudio_sol)
                    obj_estudio_solicitado = EstudioSolicitado(turno=form_edit, servicio=servicio,
                                                               servicio_estudio=estudio_servicio, cantidad=estudio_sol_cantidad)
                    obj_estudio_solicitado.save()

            imagen_pm = request.POST.get("img_pm", "")
            imagen_dni1 = request.POST.get("img_dni1", "")
            imagen_dni2 = request.POST.get("img_dni2", "")
            fechainicioturno = form_edit.fechahora_inicio.strftime('%Y-%m-%dT%H:%M')

            form_edit.save()
            form_imagen_pm = request.FILES.get("foto_pedidomedico", "")
            if form_imagen_pm:
                initial_path = form_edit.foto_pedidomedico.path
                form_edit.foto_pedidomedico.name = 'fotospmdoc/uid_' + str(form_edit.paciente.id) + '/pm/' + 'pm_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_pedidomedico.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_pm != '':
                img_received = imagen_pm
                clear_image_data = img_received.replace('data:image/jpeg;base64,', '')
                image_data = base64.b64decode(clear_image_data)
                imagen_nombre = 'pm_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                try:
                    form_edit.foto_pedidomedico.delete(save=True)
                except:
                    pass
                setattr(form_edit, 'foto_pedidomedico', ContentFile(image_data, imagen_nombre))

            form_imagen_doc = request.FILES.get("foto_documento", "")
            if form_imagen_doc:
                initial_path = form_edit.foto_documento.path
                form_edit.foto_documento.name = 'fotospmdoc/uid_' + str(form_edit.paciente.id) + '/dni/' + 'dni1_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_documento.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_dni1 != '':
                img_received1 = imagen_dni1
                clear_image_data1 = img_received1.replace('data:image/jpeg;base64,', '')
                image_data1 = base64.b64decode(clear_image_data1)
                imagen_nombre1 = 'dni1_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                try:
                    form_edit.foto_documento.delete(save=True)
                except:
                    pass
                setattr(form_edit, 'foto_documento', ContentFile(image_data1, imagen_nombre1))

            form_imagen_doc2 = request.FILES.get("foto_documento2", "")
            if form_imagen_doc2:
                initial_path = form_edit.foto_documento2.path
                form_edit.foto_documento2.name = 'fotospmdoc/uid_' + str(
                    form_edit.paciente.id) + '/dni/' + 'dni2_' + form_edit.paciente.identificacion_numero + '_' + str(
                    form_edit.id) + '.jpg'
                new_path = 'media/' + form_edit.foto_documento2.name
                os.rename(initial_path, new_path)
                form_edit.save()
            elif imagen_dni2 != '':
                img_received2 = imagen_dni2
                clear_image_data2 = img_received2.replace('data:image/jpeg;base64,', '')
                image_data2 = base64.b64decode(clear_image_data2)
                imagen_nombre2 = 'dni2_' + form_edit.paciente.identificacion_numero + '_' + str(form_edit.id) + '.jpg'
                try:
                    form_edit.foto_documento2.delete(save=True)
                except:
                    pass
                setattr(form_edit, 'foto_documento2', ContentFile(image_data2, imagen_nombre2))

            form_edit.usuario_recepcion_ultimaedicion = request.user
            ape = form_edit.paciente.apellido if form_edit.paciente.apellido else ''
            nom = form_edit.paciente.nombre if form_edit.paciente.nombre else ''
            form_edit.backupnombrecompleto = ape + ', ' + nom
            form_edit.backupdni = form_edit.paciente.identificacion_numero

            form_edit.save()

            if form_edit.servicio.usa_worklist_vm and form_edit.presente:
                crearturno_worklist(form_edit.id)

            t_id = form_edit.id
            fechaini_string = form_edit.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta")).strftime(
                "%Y-%m-%dT%H:%M")
            fechafin_string = form_edit.fechahora_fin.astimezone(pytz.timezone("America/Argentina/Salta")).strftime(
                "%Y-%m-%dT%H:%M")
            t_fechaini = fechaini_string
            t_fechafin = fechafin_string
            fechaini_string2 = form_edit.fechahora_inicio2.strftime("%Y-%m-%dT%H:%M")
            fechafin_string2 = form_edit.fechahora_fin2.strftime("%Y-%m-%dT%H:%M")
            t_fechaini2 = fechaini_string2
            t_fechafin2 = fechafin_string2

            t_pres = form_edit.presente
            t_confirmacion = form_edit.confirmacion
            t_estudio_terminado = form_edit.estudio_terminado
            t_anes = form_edit.anestesia
            t_cev = form_edit.contrasteev
            t_cvo = form_edit.contrastevo
            t_agua = form_edit.tomaagua
            t_urg = form_edit.urgencia
            t_int = form_edit.internado
            t_pacnom = form_edit.paciente.nombre.upper() if form_edit.paciente.nombre else ''

            if form_edit.paciente.apellido:
                t_pacape = form_edit.paciente.apellido.upper()
            else:
                t_pacape = ''

            t_pacdoc = form_edit.paciente.identificacion_numero
            t_estnom = form_edit.establecimiento_derivador.abreviatura
            t_cod = form_edit.estudios_solaux_codigos
            t_whatsapp = form_edit.whatsapp
            t_user = form_edit.usuario_creacion
            t_servicio_id = form_edit.servicio.codigo
            t_pacientearm = form_edit.paciente_arm
            t_pacid = form_edit.paciente.id
            t_resofix = form_edit.turno_fixreso
            t_sexo = form_edit.paciente.sexo
            t_codcomp = form_edit.estudios_solaux

            if form_edit.foto_pedidomedico:
                t_fotopm = True
            else:
                t_fotopm = False

            if form_edit.foto_documento:
                t_fotodoc1 = True
            else:
                t_fotodoc1 = False

            if form_edit.foto_documento2:
                t_fotodoc2 = True
            else:
                t_fotodoc2 = False

            t_susp = form_edit.turno_recepcion_suspendido
            if not form_edit.paciente.paciente_nn:
                if t_pacdoc.isdigit():
                    t_pacdoc = int(t_pacdoc)
                    locale.setlocale(locale.LC_NUMERIC, 'es_AR.utf8')
                    t_pacdoc = locale.format('%.0f', t_pacdoc, True)
            else:
                t_pacdoc = 'Paciente NN'
                t_pacnom = ''
                t_pacape = form_edit.paciente.paciente_nnlabel

            websocket_actualizarturno(t_id, t_user.username, t_servicio_id, t_id, t_fechaini,
                                      t_fechafin, t_pres, t_anes, t_cev, t_cvo, t_agua, t_urg, t_int,
                                      t_pacnom, t_pacape, t_pacdoc, t_estnom, t_cod, t_confirmacion,
                                      t_estudio_terminado, t_whatsapp, t_pacientearm, t_fotopm, t_fotodoc1, t_fotodoc2,
                                      t_pacid, t_resofix, t_fechaini2, t_fechafin2, t_codcomp, t_sexo, t_susp)

            band_imprimir = request.GET.get('bandimprimir', '')

            if band_imprimir == '1':
                responsejson = JsonResponse({"turnoid": str(form_edit.id), "imprimir": "1"})

            else:
                responsejson = JsonResponse({"turnoid": str(form_edit.id), "imprimir": "0"})

            return responsejson

        else:
            errores = form.errors
            responsejson = JsonResponse({"errores":errores})
            return responsejson

    def get_context_data(self, **kwargs):
        context = super(TurnoActualizar, self).get_context_data(**kwargs)
        turno = self.object
        turno_en_partes = False
        if turno.tipomodalidad == 'En partes':
            turno_padre = True
            turno_en_partes = True
        else:
            turno_padre = False

        if turno.turno_relacionado:
            turno_en_partes = True
        turnos_hijos = {}
        if turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(Q(turno_relacionado=turno) ).exclude(habilitado=False).order_by("fechahora_inicio")
        elif not turno_padre and turno_en_partes:
            turnos_hijos = Turno.objects.filter(Q(turno_relacionado=turno.turno_relacionado) | Q(id=turno.turno_relacionado.id)).exclude(Q(id=turno.id) | Q(habilitado=False)).order_by("fechahora_inicio")

        context['turno_padre'] = turno_padre
        context['turnos_hijos'] = turnos_hijos
        context['turno_en_partes'] = turno_en_partes
        return context


@login_required
def buscar_paciente_dni(request):
    try:
        response_data = {}
        dni_buscar = request.GET['dni_buscar'].strip()
        tipodni = request.GET['tipodni']
        paciente = Paciente.objects.get(identificacion_numero=dni_buscar, identificacion_tipo=tipodni)
        response_data['error_404'] = ''
        sexo = paciente.sexo if paciente.sexo else ''
        fecnac = paciente.fecnac.strftime('%d/%m/%Y') if paciente.fecnac else ''
        telefono = paciente.telefono
        domicilio = paciente.domicilio if paciente.domicilio else ''
        barrio = paciente.barrio.nombre if paciente.barrio else ''
        pacientennlabel = paciente.paciente_nnlabel if paciente.paciente_nnlabel else ''
        response_data['paciente_apellido'] = paciente.apellido if paciente.apellido else ''
        response_data['paciente_nombre'] = paciente.nombre if paciente.nombre else ''
        response_data['paciente_tipoid'] = paciente.identificacion_tipo
        response_data['paciente_id'] = paciente.id
        response_data['paciente_domicilio'] = domicilio
        response_data['paciente_nn'] = paciente.paciente_nn
        response_data['paciente_sexo'] = sexo
        response_data['paciente_fecnac'] = fecnac
        response_data['paciente_tel'] = telefono
        response_data['paciente_pais'] = paciente.pais.nombre if paciente.pais else ''
        response_data['paciente_provincia'] = paciente.provincia.nombre if paciente.provincia else ''
        response_data['paciente_departamento'] = paciente.departamento.nombre if paciente.departamento else ''
        response_data['paciente_localidad'] = paciente.localidad.nombre if paciente.localidad else ''
        response_data['paciente_barrio'] = barrio
        response_data['paciente_obs'] = paciente.observaciones if paciente.observaciones else ''
        pac_os = Obrasocial.objects.filter(paciente=paciente)
        s_obrasocial = ''
        for os in pac_os:
            if pac_os.last() == os:
                s_obrasocial += os.nombre
            else:
                s_obrasocial += os.nombre + ' || '
        response_data['paciente_obraSocial'] = s_obrasocial
        response_data['paciente_datos'] = '<b>Sexo: </b>' + sexo + '<br/>' + '<b>Fecha Nacimiento:  </b>' + fecnac \
                                          + '<br/>' + '<b>Telefono:   </b>' + telefono + '<br/>' + '<b> Domicilio: </b>' \
                                          + domicilio + '<br/>' + '<b> Barrio: </b>' + barrio + '</>'
        response_data['paciente_nnlabel'] = pacientennlabel
        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_paciente_datos(request):
    try:
        response_data = {}
        dni_buscar = request.GET['dni_buscar'].strip()
        tipodni = request.GET['tipodni']
        paciente = Paciente.objects.get(identificacion_numero=dni_buscar, identificacion_tipo=tipodni)
        turnos_paciente = Turno.objects.filter(paciente=paciente, habilitado=True)
        paciente_turnos = True if turnos_paciente.first() else False
        response_data['paciente_turnos'] = paciente_turnos

        if paciente:
            response_data['error_404'] = ''
            response_data['paciente_identificacion_tipo'] = paciente.identificacion_tipo
            response_data['paciente_apellido'] = paciente.apellido
            response_data['paciente_sexo'] = paciente.sexo

            if paciente.nombre:
                response_data['paciente_nombre'] = paciente.nombre
            else:
                response_data['paciente_nombre'] = ''

            if paciente.fecnac:
                response_data['paciente_fecnac'] = paciente.fecnac.strftime('%Y-%m-%d')
            else:
                response_data['paciente_fecnac'] = ''

            if paciente.telefono:
                response_data['paciente_telefono'] = paciente.telefono
            else:
                response_data['paciente_telefono'] = ''

            if paciente.observaciones:
                response_data['paciente_observaciones'] = paciente.observaciones
            else:
                response_data['paciente_observaciones'] = ''

            if paciente.pais:
                response_data['paciente_pais'] = paciente.pais.codigo
            else:
                response_data['paciente_pais'] = ''
            if paciente.provincia:
                response_data['paciente_provincia'] = paciente.provincia.codigo
            else:
                response_data['paciente_provincia'] = ''
            if paciente.departamento:
                response_data['paciente_departamento'] = paciente.departamento.codigo
            else:
                response_data['paciente_departamento'] = ''

            if paciente.localidad:
                response_data['paciente_localidad'] = paciente.localidad.codigo
            else:
                response_data['paciente_localidad'] = ''

            if paciente.barrio:
                response_data['paciente_barrio'] = paciente.barrio.codigo
            else:
                response_data['paciente_barrio'] = ''

            if paciente.domicilio:
                response_data['paciente_domicilio'] = paciente.domicilio
            else:
                response_data['paciente_domicilio'] = ''

            response_data['paciente_nn'] = paciente.paciente_nn
            response_data['paciente_id'] = paciente.id

        else:
            response_data['error_404'] = '404'

        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_obrasocial(request):
    try:
        response_data = {}
        obrasocial_buscar = request.GET['obrasocial_buscar']
        servicio_codigo = request.GET['servicio_codigo']
        servicio = Servicio.objects.get(codigo=servicio_codigo)
        obrasocial = Obrasocial.objects.filter(codigo=obrasocial_buscar, servicio=servicio, habilitada=True, activa=True).first()
        response_data['error_404'] = ''
        response_data['obrasocial_nombre'] = obrasocial.nombre
        response_data['obrasocial_id'] = obrasocial.id
        info = ''
        if obrasocial.observacion:
            obser = obrasocial.observacion.split('\n')
            for item in obser:
                info += item + "<br>"

        response_data['obrasocial_descripcion'] = info
        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_medico(request):
    try:
        response_data = {}
        medico_buscar = request.GET['medico_buscar']
        medico = MedicoDerivante.objects.get(matricula=medico_buscar)
        response_data['error_404'] = ''
        response_data['medico_nombre'] = ''
        response_data['medico_apellido'] = medico.apellido
        response_data['medico_id'] = medico.id
        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_establecimiento(request):
    try:
        response_data = {}
        establecimiento_buscar = request.GET['establecimiento_buscar']
        establecimiento = EstablecimientoDerivador.objects.get(codigo=establecimiento_buscar)
        response_data['error_404'] = ''
        response_data['establecimiento_nombre'] = establecimiento.nombre
        response_data['establecimiento_id'] = establecimiento.id
        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_turno(request):
    try:
        response_data = {}
        turno_buscar = request.GET['turno_buscar']
        if turno_buscar[0] != 'R':
            raise ValueError('codigo de turno invalido')
        else:
            turno_buscar_id = turno_buscar.replace("R", '')

        turno = Turno.objects.get(id=turno_buscar_id)
        fechainicio = turno.fechahora_inicio.astimezone(pytz.timezone("America/Argentina/Salta"))
        response_data['error_404'] = ''
        response_data['paciente_dni'] = turno.paciente.identificacion_numero
        response_data['paciente_tipoid'] = turno.paciente.identificacion_tipo
        response_data['paciente_nn'] = turno.paciente.paciente_nn
        response_data['paciente_sexo'] = turno.paciente.sexo
        response_data['paciente_telefono'] = turno.paciente.telefono
        response_data['paciente_apellido'] = turno.paciente.apellido if turno.paciente.apellido else '---'
        response_data['paciente_nombre'] = turno.paciente.nombre if turno.paciente.nombre else '---'
        response_data['turno_id'] = str(turno.id)
        if turno.paciente.barrio:
            response_data['paciente_barrio'] = turno.paciente.barrio.codigo
        response_data['paciente_domicilio'] = turno.paciente.domicilio
        response_data['medico_derivante_id'] = turno.medico_derivante.matricula
        response_data['establecimiento_derivador_id'] = turno.establecimiento_derivador.codigo
        response_data['obrasocial_id'] = turno.obrasocial.codigo
        response_data['servicio_id'] = turno.servicio.id
        response_data['contrasteev'] = turno.contrasteev
        response_data['contrastevo'] = turno.contrastevo
        response_data['tomaagua'] = turno.tomaagua
        response_data['anestesia'] = turno.anestesia
        response_data['presente'] = turno.presente
        response_data['confirmacion'] = turno.confirmacion
        response_data['pacientearm'] = turno.paciente_arm
        response_data['canalizacion'] = turno.canalizacion
        response_data['urgencia'] = turno.urgencia
        response_data['internado'] = turno.internado
        response_data['observacion'] = turno.observacion
        response_data['observacion_noimpresa'] = turno.observacion_noimpresa
        response_data['fechaini'] = fechainicio
        response_data['fechaini_string'] = fechainicio.strftime('%d/%m/%Y %H:%M')
        response_data['tipomodalidad'] = turno.tipomodalidad
        response_data['whatsapp'] = turno.whatsapp
        response_data['entrega_digital'] = turno.entrega_digital
        response_data['estudio_terminado'] = turno.estudio_terminado or turno.estudio_suspendido_paciente or turno.estudio_suspendido_completar
        response_data['recepcion_suspendido'] = turno.turno_recepcion_suspendido
        response_data['servicio_externo'] = turno.servicio.is_servicio_externo
        if turno.estudios_solaux:
            estudios_sol_string = turno.estudios_solaux.replace(' ', ' - ')
        else:
            estudios_sol_string = '---'
        response_data['estudios_sol_string'] = estudios_sol_string

        if turno.establecimiento_servicio:
            response_data['establecimiento_servicio_id'] = turno.establecimiento_servicio.id

        # envio el listado de estudios como array
        estudios_solicitados = EstudioSolicitado.objects.filter(turno=turno)
        lista_estudios = []

        for est in estudios_solicitados:
            lista_estudios.append((est.servicio_estudio.codigo, est.cantidad))

        response_data['estudios_solicitados'] = lista_estudios

        if turno.estudio_finalizado:
            estudio_estado = 'Estudio Finalizado por Técnico Rad.'
        elif turno.estudio_suspendido_paciente or turno.estudio_suspendido_completar:
            estudio_estado = 'Estudio Suspendido por Técnico Rad.'
        elif turno.estudio_cancelado or turno.estudio_reprograma:
            estudio_estado = 'Estudio Cancelado por Técnico Rad.'
        else:
            estudio_estado = 'Estudio No Realizado'

        response_data['estudio_estado'] = estudio_estado
        response_data['tecnico'] = turno.usuario_tecnico.first_name + ', ' + turno.usuario_tecnico.last_name if turno.usuario_tecnico else '---'
        if turno.informe_finalizado:
            estado_informe = 'Informe Finalizado'
        elif turno.informe_cerrado:
            estado_informe = 'Finalizado sin Informe'
        else:
            estado_informe = 'Sin Informe'

        response_data['estado_informe'] = estado_informe
        return JsonResponse(response_data)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
def buscar_turnosxpacienteid(request):
    try:
        response_data = {}
        response_data['error_404'] = [{''}]
        json_data_list = []
        paciente_id = request.GET['paciente_id']
        paciente = Paciente.objects.get(id=paciente_id)
        turnos = Turno.objects.filter(paciente=paciente).exclude(habilitado=False)
        sexo = paciente.sexo if paciente.sexo else ''
        fecnac = paciente.fecnac.strftime('%d/%m/%Y') if paciente.fecnac else ''
        telefono = paciente.telefono
        domicilio = paciente.domicilio if paciente.domicilio else ''
        barrio = paciente.barrio.nombre if paciente.barrio else ''
        paciente_nombre = paciente.nombre if paciente.nombre else ''
        paciente_apellido = paciente.apellido if paciente.apellido else ''

        for turno in turnos:
            fec_finalizacion_informe = ' (' + turno.informe_fecha_finalizacion.strftime('%d/%m/%Y') + ')' if turno.informe_fecha_finalizacion else ''
            estado = ''
            tecnico_imprime_ficha = True
            if turno.turno_relacionado:
                estado += 'Turno Partes: Origen Nº ' + str(turno.turno_relacionado.id)

            else:
                if (turno.estudio_finalizado and turno.tipomodalidad == 'Normal') or (turno.estudio_partes_finalizado and turno.tipomodalidad == 'En partes'):
                    if turno.informe_finalizado:
                        estado += 'Informe Listo' + fec_finalizacion_informe
                    elif turno.informe_cerrado:
                        estado += 'Finalizado sin Informe'
                    else:
                        estado += 'En proceso de informe'
                else:
                    if turno.estudio_cancelado or turno.estudio_reprograma:
                        estado += 'Cancelado'
                    elif turno.estudio_suspendido_paciente or turno.estudio_suspendido_completar:
                        estado += 'Suspendido'
                    else:
                        tecnico_imprime_ficha = False
                        estado += 'No realizado'

            try:
                fotopm = str(turno.foto_pedidomedico)
            except:
                fotopm = ''

            try:
                fotodoc = str(turno.foto_documento)
            except:
                fotodoc = ''

            try:
                fotodoc2 = str(turno.foto_documento2)
            except:
                fotodoc2 = ''

            informe_pdf_url = turno.informe_firmadopdf.url if turno.informe_firmadopdf else ''

            if turno.informe_facturado and turno.informe_facturado_fecha:
                facturado = turno.informe_facturado_fecha.strftime('%d/%m/%Y')
            else:
                facturado = '---'

            t = {
                'dni': str(turno.paciente.identificacion_numero),
                'apellidoNombre': str(turno.paciente.apellido) + ", " + paciente_nombre,
                'medico': str(turno.medico_derivante.nombre),
                'establecimiento': str(turno.establecimiento_derivador.nombre),
                'obrasocial': str(turno.obrasocial.nombre),
                'servicio': turno.servicio.id,
                'servicio_nombre': turno.servicio.nombre,
                'cev': str(turno.contrasteev),
                'cvo': str(turno.contrastevo),
                'agua': str(turno.tomaagua),
                'anestesia': str(turno.anestesia),
                'urgencia': str(turno.urgencia),
                'internado': str(turno.internado),
                'observacion': turno.observacion,
                'observacion_noimpresa': turno.observacion_noimpresa,
                'presente': str(turno.presente),
                'estudios': turno.estudios_solaux,
                'estado': estado,
                'tipo': turno.tipomodalidad,
                'fecha': turno.fechahora_inicio.astimezone(tz=pytz.timezone('America/Argentina/Salta')).strftime('%d/%m/%y - %H:%M'),
                'fotopm': fotopm,
                'fotodoc': fotodoc,
                'fotodoc2': fotodoc2,
                'turnoId': turno.id,
                'nro_informe': turno.informe_numero,
                'turno_hijo': str(bool(turno.turno_relacionado)),
                'informe_finalizado': str(turno.informe_finalizado),
                'tecnico_imprime_ficha': tecnico_imprime_ficha,
                'informe_pdf_url': informe_pdf_url,
                'facturado': facturado,
                'informe_cerrado_obs': turno.informe_cerrado_observaciones if turno.informe_cerrado_observaciones else '---',
            }
            json_data_list.append(t)

        pacientennlabel = paciente.paciente_nnlabel if paciente.paciente_nnlabel else ''
        pac_os = Obrasocial.objects.filter(paciente=paciente)
        s_obrasocial = ''
        for os in pac_os:
            if pac_os.last() == os:
                s_obrasocial += os.nombre
            else:
                s_obrasocial += os.nombre + ' || '

        t_paciente = {
            'paciente_id': paciente_id,
            'paciente_apellido': paciente_apellido,
            'paciente_nombre': paciente_nombre,
            'paciente_tipoid': paciente.identificacion_tipo,
            'paciente_dni': paciente.identificacion_numero,
            'paciente_domicilio': domicilio,
            'paciente_nn': paciente.paciente_nn,
            'paciente_sexo': sexo,
            'paciente_fecnac': fecnac,
            'paciente_tel': telefono,
            'paciente_pais': paciente.pais.nombre if paciente.pais else '',
            'paciente_provincia': paciente.provincia.nombre if paciente.provincia else '',
            'paciente_departamento': paciente.departamento.nombre if paciente.departamento else '',
            'paciente_localidad': paciente.localidad.nombre if paciente.localidad else '',
            'paciente_barrio': barrio,
            'paciente_obs': paciente.observaciones if paciente.observaciones else '',
            'paciente_obraSocial': s_obrasocial,
            'paciente_nnlabel': pacientennlabel,
            'paciente_datos': "'<b>Sexo: </b>' + sexo + '<br/>' + '<b>Fecha Nacimiento:  </b>' + fecnac \
                                              + '<br/>' + '<b>Telefono:   </b>' + telefono + '<br/>' + '<b> Domicilio: </b>' \
                                              + domicilio + '<br/>' + '<b> Barrio: </b>' + barrio + '</>'",
        }
        json_data_list.append(t_paciente)

        return JsonResponse(json_data_list, safe=False)

    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


@login_required
@transaction.atomic
def actualizar_turno_fecha(request):
    try:
        response_data = {}
        turno_id = request.GET['turno_id']
        turno_fecha_inicio = request.GET['turno_fecha_inicio']
        turno_fecha_fin = request.GET['turno_fecha_fin']
        turno_fecha_inicio2 = request.GET['turno_fecha_inicio2']
        turno_fecha_fin2 = request.GET['turno_fecha_fin2']
        objdate_fechafin = datetime.datetime.strptime(turno_fecha_fin, '%Y-%m-%d %H:%M')
        objdate_fechaini = datetime.datetime.strptime(turno_fecha_inicio, '%Y-%m-%d %H:%M')
        objdate_fechafin2 = datetime.datetime.strptime(turno_fecha_fin2, '%Y-%m-%d %H:%M')
        objdate_fechaini2 = datetime.datetime.strptime(turno_fecha_inicio2, '%Y-%m-%d %H:%M')
        fechaini_aware = timezone.make_aware(objdate_fechaini, timezone.get_current_timezone())
        fechafin_aware = timezone.make_aware(objdate_fechafin, timezone.get_current_timezone())
        fechaini_aware2 = timezone.make_aware(objdate_fechaini2, timezone.get_current_timezone())
        fechafin_aware2 = timezone.make_aware(objdate_fechafin2, timezone.get_current_timezone())

        if turno_id[0] == 'E':
            turno = TurnoReservado.objects.get(id=turno_id[1:])

            if turno.servicio.codigo == 2:
                turno.fecha = fechaini_aware2
                turno.fecha2 = fechafin_aware2
                turno.save()
            else:
                turno.fecha = fechaini_aware
                turno.fecha2 = fechafin_aware
                turno.save()
            layer = get_channel_layer()
            async_to_sync(layer.group_send)('chat_turnobrightspeed',
                                            {
                                                'type': 'mensaje_turno',
                                                'message': turno_id,
                                                'message_type': 'moverreservado',
                                                'message_user': turno.usuario.username,
                                                'message_servicio': turno.servicio.codigo,
                                                'message_body': turno.titulo,
                                                'turno_id': turno_id,

                                                'turno_resofix': 'na',
                                                'turno_fechaini2': 'na',
                                                'turno_fechafin2': 'na',

                                                'turno_fechaini': turno.fecha.strftime('%Y-%m-%d %H:%M'),
                                                'turno_fechafin': turno.fecha2.strftime('%Y-%m-%d %H:%M'),
                                                'turno_presente': 'na',
                                                'turno_confirmacion': 'na',
                                                'turno_pacientearm': 'na',
                                                'turno_estudio_terminado': 'na',
                                                'turno_whatsapp': 'na',
                                                'turno_anestesia': 'na',
                                                'turno_cev': 'na',
                                                'turno_cvo': 'na',
                                                'turno_agua': 'na',
                                                'turno_urgencia': 'na',
                                                'turno_internado': 'na',
                                                'turno_suspendido': 'na',

                                                'turno_nombre': 'na',
                                                'turno_apellido': 'na',
                                                'turno_documento': 'na',
                                                'turno_establecimiento': 'na',
                                                'turno_codigos': 'na',
                                                'turno_codigoscompleto': 'na',
                                                'turno_sexo': 'na',
                                                'turno_fotopm': 'na',
                                                'turno_fotodoc1': 'na',
                                                'turno_fotodoc2': 'na',
                                                'turno_pacienteid': 'na'

                                            })

        else:

            turno = Turno.objects.get(id=turno_id)
            if (turno.estudio_terminado or turno.estudio_suspendido_completar or turno.estudio_suspendido_paciente) and not turno.servicio.is_servicio_externo:
                response_data = {'actualizar_exito': False}
                return JsonResponse(response_data)

            turno.fechahora_inicio = fechaini_aware
            turno.fechahora_fin = fechafin_aware
            turno.fechahora_inicio2 = fechaini_aware2
            turno.fechahora_fin2 = fechafin_aware2
            t_pacdocu = turno.paciente.identificacion_numero
            if t_pacdocu.isdigit():
                t_pacdoc = int(t_pacdocu)
                locale.setlocale(locale.LC_NUMERIC, 'es_AR.utf8')
                t_pacdocu = locale.format('%.0f', t_pacdoc, True)
            if turno.foto_pedidomedico:
                t_fotopm = True
            else:
                t_fotopm = False
            if turno.foto_documento:
                t_doc1 = True
            else:
                t_doc1 = False
            if turno.foto_documento2:
                t_doc2 = True
            else:
                t_doc2 = False

            turno.usuario_recepcion_ultimaedicion = request.user
            turno.save()
            if turno.paciente.paciente_nn:
                paciente_nombre = ''
                paciente_apellido = turno.paciente.paciente_nnlabel
                t_pacdocu = 'Paciente NN'
            else:
                paciente_nombre = turno.paciente.nombre.upper() if turno.paciente.nombre else ''
                paciente_apellido = turno.paciente.apellido.upper() if turno.paciente.apellido else ''
            layer = get_channel_layer()
            async_to_sync(layer.group_send)('chat_turnobrightspeed',
                                            {
                                                'type': 'mensaje_turno',
                                                'message': turno.id,
                                                'message_type': 'mover',
                                                'message_user': turno.usuario_creacion.username,
                                                'message_servicio': turno.servicio.codigo,
                                                'message_body': 'na',

                                                'turno_resofix': turno.servicio.codigo,
                                                'turno_fechaini2': turno_fecha_inicio2,
                                                'turno_fechafin2': turno_fecha_fin2,

                                                'turno_id': turno.id,
                                                'turno_fechaini': turno_fecha_inicio,
                                                'turno_fechafin': turno_fecha_fin,

                                                'turno_presente': turno.presente,
                                                'turno_confirmacion': turno.confirmacion,
                                                'turno_pacientearm': turno.paciente_arm,
                                                'turno_estudio_terminado': turno.estudio_terminado,
                                                'turno_whatsapp': turno.whatsapp,
                                                'turno_anestesia': turno.anestesia,
                                                'turno_cev': turno.contrasteev,
                                                'turno_cvo': turno.contrastevo,
                                                'turno_agua': turno.tomaagua,
                                                'turno_urgencia': turno.urgencia,
                                                'turno_internado': turno.internado,
                                                'turno_suspendido': turno.turno_recepcion_suspendido,
                                                'turno_nombre': paciente_nombre,
                                                'turno_apellido': paciente_apellido,
                                                'turno_documento': t_pacdocu,
                                                'turno_establecimiento': turno.establecimiento_derivador.abreviatura,
                                                'turno_codigos': turno.estudios_solaux_codigos,
                                                'turno_codigoscompleto': turno.estudios_solaux,
                                                'turno_sexo': turno.paciente.sexo,
                                                'turno_fotopm': t_fotopm,
                                                'turno_fotodoc1': t_doc1,
                                                'turno_fotodoc2': t_doc2,
                                                'turno_pacienteid': turno.paciente.id

                                            })

        response_data['actualizar_exito'] = 'true'
        response_data['turnoid'] = turno.id

        return JsonResponse(response_data)

    except:
        response_data = {'actualizar_exito': 'false'}
        return JsonResponse(response_data)


# eliminar turno normal y reservado al pasarlo a deshabilitado
@login_required
@transaction.atomic
def actualizar_turno_estado(request):
    # noinspection PyBroadException
    try:
        response_data = {}
        turno_id = request.GET['turno_id']
        turno_estado = request.GET['turno_estado']
        if turno_id[0] == 'E':
            turno = TurnoReservado.objects.get(id=turno_id[1:])
            if turno_estado == 'Deshabilitado':
                turno.estado = False
            elif turno_estado == 'Habilitado':
                turno.estado = True
            turno.save()
            tipoeliminado = 'eliminarreservado'
            fechaturnoinicio = 'na'
            fechaturnofin = 'na'
            turnosuspendido = 'na'
        else:
            turno = Turno.objects.get(id=turno_id)
            if turno.estudio_terminado or turno.estudio_suspendido_completar or turno.estudio_suspendido_paciente:
                response_data = {'actualizar_exito': False}
                return JsonResponse(response_data)

            if turno_estado == 'Deshabilitado':
                turno.habilitado = False
            elif turno_estado == 'Habilitado':
                turno.habilitado = True
            turno.usuario_recepcion_ultimaedicion = request.user
            turno.save()
            tipoeliminado = 'eliminar'
            fechaturnoinicio = turno.fechahora_inicio.strftime("%Y-%m-%dT%H:%M")
            fechaturnofin = turno.fechahora_fin.strftime("%Y-%m-%dT%H:%M")
            turnosuspendido = turno.turno_recepcion_suspendido

        layer = get_channel_layer()
        async_to_sync(layer.group_send)('chat_turnobrightspeed',
                                        {
                                            'type': 'mensaje_turno',
                                            'message': turno_id,
                                            'message_type': tipoeliminado,
                                            'message_user': request.user.username,
                                            'message_servicio': turno.servicio.codigo,
                                            'message_body': 'na',
                                            'turno_resofix': 'na',
                                            'turno_fechaini2': 'na',
                                            'turno_fechafin2': 'na',

                                            'turno_id': 'na',
                                            'turno_fechaini': fechaturnoinicio,
                                            'turno_fechafin': fechaturnofin,
                                            'turno_presente': 'na',
                                            'turno_confirmacion': 'na',
                                            'turno_pacientearm': 'na',
                                            'turno_estudio_terminado': 'na',
                                            'turno_whatsapp': 'na',
                                            'turno_anestesia': 'na',
                                            'turno_cev': 'na',
                                            'turno_cvo': 'na',
                                            'turno_agua': 'na',
                                            'turno_urgencia': 'na',
                                            'turno_internado': 'na',
                                            'turno_suspendido': turnosuspendido,

                                            'turno_nombre': 'na',
                                            'turno_apellido': 'na',
                                            'turno_documento': 'na',
                                            'turno_establecimiento': 'na',
                                            'turno_codigos': 'na',
                                            'turno_codigoscompleto': 'na',
                                            'turno_sexo': 'na',
                                            'turno_fotopm': 'na',
                                            'turno_fotodoc1': 'na',
                                            'turno_fotodoc2': 'na',
                                            'turno_pacienteid': 'na'
                                        })

        response_data['actualizar_exito'] = True
        return JsonResponse(response_data)

    except:
        response_data = {'actualizar_exito': False}
        return JsonResponse(response_data)


@login_required
@transaction.atomic()
def actualizar_turno_cortado(request):
    response_data = {'actualizar_exito': False}
    turno_id = request.GET['turno_id']
    turno_estado = request.GET['turno_estado']
    turno = Turno.objects.get(id=turno_id)

    if turno.estudio_terminado or turno.estudio_suspendido_completar or turno.estudio_suspendido_paciente:
        response_data = {'actualizar_exito': False}
        return JsonResponse(response_data)

    if turno_estado == 'Deshabilitado':
        turno.habilitado = False
    elif turno_estado == 'Habilitado':
        turno.habilitado = True

    turno.save()
    response_data['actualizar_exito'] = True
    return JsonResponse(response_data)


@login_required
def crearnota_turno(request):
    try:
        response_data = {}
        nota = request.GET['nota']
        fecha = request.GET['fecha']
        fecha2 = request.GET.get('fecha2', '')
        servicio_codigo = request.GET.get('serviciocod')
        servicio = Servicio.objects.get(codigo=servicio_codigo)
        usuario = request.GET['usuario']
        objdate_fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')
        fecha_aware = timezone.make_aware(objdate_fecha, timezone.get_current_timezone())
        user = User.objects.get(id=usuario)

        if fecha2 != '':
            objdate_fecha2 = datetime.datetime.strptime(fecha2, '%Y-%m-%d')
            fecha_aware2 = timezone.make_aware(objdate_fecha2, timezone.get_current_timezone())
            notaobj = TurnoNota(usuario=user, fecha=fecha_aware, fecha2=fecha_aware2, observacion=nota, estado=True, servicio=servicio)
            notaobj.save()
            layer = get_channel_layer()
            async_to_sync(layer.group_send)('chat_turnobrightspeed', {
                'type': 'mensaje_turno',
                'message': 'N' + str(notaobj.id),
                'message_type': 'crearnota',
                'message_user': user.username,
                'message_servicio': servicio.codigo,
                'message_body': nota,

                'turno_resofix': 'na',

                'turno_id': 'N' + str(notaobj.id),
                'turno_fechaini': fecha,
                'turno_fechafin': fecha2,
                'turno_fechaini2': 'na',
                'turno_fechafin2': 'na',
                'turno_presente': 'na',
                'turno_confirmacion': 'na',
                'turno_estudio_terminado': 'na',
                'turno_whatsapp': 'na',
                'turno_pacientearm': 'na',
                'turno_anestesia': 'na',
                'turno_cev': 'na',
                'turno_cvo': 'na',
                'turno_agua': 'na',
                'turno_urgencia': 'na',
                'turno_internado': 'na',
                'turno_suspendido': 'na',

                'turno_pacienteid': 'na',
                'turno_nombre': 'na',
                'turno_apellido': 'na',
                'turno_documento': 'na',
                'turno_establecimiento': 'na',
                'turno_codigos': 'na',
                'turno_codigoscompleto': 'na',
                'turno_sexo': 'na',
                'turno_fotopm': 'na',
                'turno_fotodoc1': 'na',
                'turno_fotodoc2': 'na'

            })

        else:
            notaobj = TurnoNota(usuario=user, fecha=fecha_aware, observacion=nota, estado=True, servicio=servicio)
            notaobj.save()
            layer = get_channel_layer()
            async_to_sync(layer.group_send)('chat_turnobrightspeed', {
                'type': 'mensaje_turno',
                'message': notaobj.id,
                'message_type': 'crearnota',
                'message_user': user.username,
                'message_servicio': servicio.codigo,
                'message_body': nota,
                'turno_id': notaobj.id,
                'turno_fechaini': fecha,
                'turno_fechafin': 'na',
                'turno_resofix': 'na',

                'turno_presente': 'na',
                'turno_confirmacion': 'na',
                'turno_estudio_terminado': 'na',
                'turno_whatsapp': 'na',
                'turno_pacientearm': 'na',
                'turno_anestesia': 'na',
                'turno_cev': 'na',
                'turno_cvo': 'na',
                'turno_agua': 'na',
                'turno_urgencia': 'na',
                'turno_internado': 'na',
                'turno_suspendido': 'na',

                'turno_pacienteid': 'na',
                'turno_nombre': 'na',
                'turno_apellido': 'na',
                'turno_documento': 'na',
                'turno_establecimiento': 'na',
                'turno_codigos': 'na',
                'turno_codigoscompleto': 'na',
                'turno_sexo': 'na',
                'turno_fotopm': 'na',
                'turno_fotodoc1': 'na',
                'turno_fotodoc2': 'na'

            })
        response_data['crearnota_exito'] = True
        response_data['nota_id'] = notaobj.id

        return JsonResponse(response_data)

    except:
        response_data = {'crearnota_exito': False}
        return JsonResponse(response_data)


@login_required
def actualizarnota_turno(request):
    try:
        response_data = {}
        nota_id = request.GET['nota_id']
        nota = TurnoNota.objects.get(id=nota_id[1:])
        nota.estado = False
        nota.save()
        layer = get_channel_layer()
        async_to_sync(layer.group_send)('chat_turnobrightspeed',
                                        {
                                            'type': 'mensaje_turno',
                                            'message': nota_id,
                                            'message_type': 'eliminarnota',
                                            'message_user': 'na',
                                            'message_servicio': nota.servicio.codigo,
                                            'message_body': 'na',
                                            'turno_id': 'na',
                                            'turno_fechaini': 'na',
                                            'turno_fechafin': 'na',
                                            'turno_resofix': 'na',
                                            'turno_fechaini2': 'na',
                                            'turno_fechafin2': 'na',
                                            'turno_presente': 'na',
                                            'turno_confirmacion': 'na',
                                            'turno_estudio_terminado': 'na',
                                            'turno_whatsapp': 'na',
                                            'turno_pacientearm': 'na',
                                            'turno_anestesia': 'na',
                                            'turno_cev': 'na',
                                            'turno_cvo': 'na',
                                            'turno_agua': 'na',
                                            'turno_urgencia': 'na',
                                            'turno_internado': 'na',
                                            'turno_suspendido': 'na',

                                            'turno_pacienteid': 'na',
                                            'turno_nombre': 'na',
                                            'turno_apellido': 'na',
                                            'turno_documento': 'na',
                                            'turno_establecimiento': 'na',
                                            'turno_codigos': 'na',
                                            'turno_codigoscompleto': 'na',
                                            'turno_sexo': 'na',
                                            'turno_fotopm': 'na',
                                            'turno_fotodoc1': 'na',
                                            'turno_fotodoc2': 'na'

                                        })
        response_data['actualizarnota_exito'] = True
        return JsonResponse(response_data)

    except:
        response_data = {'actualizarnota_exito': False}
        return JsonResponse(response_data)


@login_required
def consulta_tipoturno(request):
    try:
        response_data = {}
        turno_id = request.GET['turno_id']
        turno = Turno.objects.get(id=turno_id)
        tipoturno = turno.tipomodalidad
        response_data['tipoturno'] = tipoturno
        response_data['consulta_exito'] = True
        return JsonResponse(response_data)

    except:
        response_data = {'consulta_exito': False}
        return JsonResponse(response_data)


class TurnoPacienteCrear(FormView):
    model = Paciente
    form_class = TurnoPacienteForm
    template_name = 'turno/turno_crearpaciente_form.html'


@login_required
def crearpaciente_turno(request):
    try:
        response_data = {}
        pais = request.GET.get('pais', '')
        provincia = request.GET.get('provincia', '')
        departamento = request.GET.get('departamento', '')
        localidad = request.GET.get('localidad', '')
        barrio = request.GET.get('barrio', '')
        domicilio = request.GET['domicilio']
        identificacion_tipo = request.GET['identificacion_tipo']
        identificacion_numero = request.GET['identificacion_numero'].strip()
        apellido = request.GET.get('apellido','').upper().strip()
        nombre = request.GET.get('nombre','').upper().strip()
        sexo = request.GET['sexo']
        fecnac = request.GET['fecnac']
        telefono = request.GET['telefono']
        observaciones = request.GET['observaciones']
        pacientenn_label = request.GET['i_nnlabel']

        if pais != '':
            obj_pais = Pais.objects.get(codigo=pais)
            if provincia != '':
                obj_provincia = Provincia.objects.get(codigo=provincia, pais=obj_pais)
                if departamento != '':
                    obj_dept = Departamento.objects.get(codigo=departamento, provincia=obj_provincia)
                    if localidad != '':
                        obj_loca = Localidad.objects.get(codigo=localidad, departamento=obj_dept)
                    else:
                        obj_loca = None
                else:
                    obj_dept = obj_loca = None
            else:
                obj_provincia = obj_dept = obj_loca = None
        else:
            obj_pais = obj_provincia = obj_dept = obj_loca = None
        if barrio != '':
            obj_barrio = Barrio.objects.get(codigo=barrio)
        else:
            obj_barrio = None

        if fecnac != '':
            objdate_fecha = datetime.datetime.strptime(fecnac, '%Y-%m-%d')
            fechanacimiento = objdate_fecha
        else:
            fechanacimiento = None

        if identificacion_tipo == 'NN' and identificacion_numero == '0':
            paciente_uuid = str(uuid.uuid4())
            pac = Paciente(identificacion_numero=paciente_uuid, identificacion_tipo=identificacion_tipo,
                           paciente_nn=True, paciente_nnlabel=pacientenn_label,
                           barrio=obj_barrio, domicilio=domicilio, apellido=apellido, nombre=nombre, sexo=sexo,
                           pais=obj_pais, provincia=obj_provincia, departamento=obj_dept, localidad=obj_loca,
                           fecnac=fechanacimiento, telefono=telefono, observaciones=observaciones)
            pac.save()
            response_data['paciente_nn'] = True
            response_data['paciente_dni'] = paciente_uuid
        else:
            Paciente.objects.update_or_create(identificacion_numero=identificacion_numero,
                                              identificacion_tipo=identificacion_tipo,
                                              defaults={"pais": obj_pais, "provincia": obj_provincia,
                                                        "departamento": obj_dept, "localidad": obj_loca,
                                                        "barrio": obj_barrio, "domicilio": domicilio,
                                                        "identificacion_tipo": identificacion_tipo,
                                                        "identificacion_numero": identificacion_numero,
                                                        "apellido": apellido, "nombre": nombre, "sexo": sexo,
                                                        "fecnac": fechanacimiento, "telefono": telefono,
                                                        "observaciones": observaciones})
            response_data['paciente_nn'] = False

        response_data['crearpaciente_exito'] = True

        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'crearpaciente_exito': False}
        return JsonResponse(response_data)


@login_required
def actualizarpaciente_turno(request):
    try:
        response_data = {}
        paciente_id = request.GET.get('paciente_id', '')
        pais = request.GET.get('pais', '')
        provincia = request.GET.get('provincia', '')
        departamento = request.GET.get('departamento', '')
        localidad = request.GET.get('localidad', '')
        barrio = request.GET.get('barrio', '')
        domicilio = request.GET['domicilio']
        identificacion_tipo = request.GET['identificacion_tipo']
        identificacion_numero = request.GET['identificacion_numero']
        apellido = request.GET.get('apellido','').upper()
        nombre = request.GET.get('nombre','').upper()
        sexo = request.GET['sexo']
        fecnac = request.GET['fecnac']
        telefono = request.GET['telefono']
        observaciones = request.GET['observaciones']
        fullname = apellido + ' ' + nombre

        if pais != '':
            obj_pais = Pais.objects.get(codigo=pais)
            if provincia != '':
                obj_provincia = Provincia.objects.get(codigo=provincia, pais=obj_pais)
                if departamento != '':
                    obj_dept = Departamento.objects.get(codigo=departamento, provincia=obj_provincia)
                    if localidad != '':
                        obj_loca = Localidad.objects.get(codigo=localidad, departamento=obj_dept)
                    else:
                        obj_loca = None
                else:
                    obj_dept = obj_loca = None
            else:
                obj_provincia = obj_dept = obj_loca = None
        else:
            obj_pais = obj_provincia = obj_dept = obj_loca = None

        obj_barrio = Barrio.objects.get(codigo=barrio) if barrio else None
        fechanacimiento = datetime.datetime.strptime(fecnac, '%Y-%m-%d') if fecnac else None
        paciente = Paciente.objects.get(id=paciente_id)
        paciente_duplicated = Paciente.objects.filter(identificacion_tipo=identificacion_tipo, identificacion_numero=identificacion_numero).exclude(id=paciente.id)

        if paciente_duplicated.first():
            response_data['crearpaciente_exito'] = False
            response_data['paciente_duplicado'] = True
            response_data['message'] = 'Error, Paciente Duplicado'

            return JsonResponse(response_data)

        paciente_to_update = Paciente.objects.filter(id=paciente.id)

        turnos_paciente = Turno.objects.filter(paciente=paciente_to_update.first(), informe_finalizado=True, habilitado=True)

        if turnos_paciente:
            paciente_to_update.update(pais=obj_pais, provincia=obj_provincia,departamento= obj_dept, localidad= obj_loca,
                                      barrio= obj_barrio, domicilio= domicilio, fecnac= fechanacimiento, telefono= telefono, observaciones= observaciones)
        else:
            paciente_to_update.update(pais=obj_pais, provincia=obj_provincia,departamento= obj_dept, localidad= obj_loca,
                                      barrio= obj_barrio, domicilio= domicilio, identificacion_tipo= identificacion_tipo, nombrecompleto=fullname,
                                      identificacion_numero= identificacion_numero, apellido= apellido, nombre= nombre, sexo= sexo,
                                      fecnac=fechanacimiento, telefono=telefono, observaciones=observaciones)

        response_data['paciente_nn'] = False
        response_data['crearpaciente_exito'] = True

        return JsonResponse(response_data)

    except Exception as e:
        print(e)
        response_data = {'crearpaciente_exito': False}
        return JsonResponse(response_data)

def eliminarturnobloqueoycrear_calendar(turnobloqueo_id, usu, servi, t_id, t_fechaini, t_fechafin, t_pres, t_anes,
                                        t_cev, t_cvo, t_agua, t_urg, t_int, t_pacnom, t_pacape, t_pacdoc, t_estnom,
                                        t_cod, t_confirmacion, t_estudio_terminado, t_whatsapp, t_pacientearm, t_fotopm,
                                        t_fotodoc1, t_fotodoc2, t_pacid, t_resofix, t_fechaini2, t_fechafin2, t_codigoscompleto, t_sexo, t_susp):

    layer = get_channel_layer()
    async_to_sync(layer.group_send)('chat_turnobrightspeed', {
        'type': 'mensaje_turno',
        'message': turnobloqueo_id,
        'message_type': 'eliminarycrear',
        'message_user': usu,
        'message_servicio': servi,
        'message_body': 'na',
        'turno_resofix': t_resofix,
        'turno_fechaini2': t_fechaini2,
        'turno_fechafin2': t_fechafin2,

        'turno_id': t_id,
        'turno_fechaini': t_fechaini,
        'turno_fechafin': t_fechafin,
        'turno_presente': t_pres,
        'turno_confirmacion': t_confirmacion,
        'turno_estudio_terminado': t_estudio_terminado,
        'turno_whatsapp': t_whatsapp,
        'turno_pacientearm': t_pacientearm,
        'turno_anestesia': t_anes,
        'turno_cev': t_cev,
        'turno_cvo': t_cvo,
        'turno_agua': t_agua,
        'turno_urgencia': t_urg,
        'turno_internado': t_int,
        'turno_suspendido': t_susp,

        'turno_pacienteid': t_pacid,
        'turno_nombre': t_pacnom,
        'turno_apellido': t_pacape,
        'turno_documento': t_pacdoc,
        'turno_establecimiento': t_estnom,
        'turno_codigos': t_cod,
        'turno_codigoscompleto': t_codigoscompleto,
        'turno_sexo': t_sexo,
        'turno_fotopm': t_fotopm,
        'turno_fotodoc1': t_fotodoc1,
        'turno_fotodoc2': t_fotodoc2

    })
    return True

@login_required
def turnobloqueado_eliminar(request):

    try:
        servicio_codigo = request.GET.get('servicio_codigo', '')
        turnos_bloqueados = TurnoBloqueo.objects.filter(usuario=request.user, servicio=servicio_codigo)
        if turnos_bloqueados:
            turnos_bloqueados.delete()

        layer = get_channel_layer()
        async_to_sync(layer.group_send)('chat_turnobrightspeed', {
            'type': 'mensaje_turno',
            'message': 'na',
            'message_type': 'eliminar_turnobloqueado',
            'message_user': str(request.user.username),
            'message_servicio': servicio_codigo,
            'message_body': 'na',
            'turno_resofix': 'na',
            'turno_fechaini2': 'na',
            'turno_fechafin2': 'na',

            'turno_id': 'na',
            'turno_fechaini': 'na',
            'turno_fechafin': 'na',
            'turno_presente': 'na',
            'turno_confirmacion': 'na',
            'turno_estudio_terminado': 'na',
            'turno_whatsapp': 'na',
            'turno_pacientearm': 'na',
            'turno_anestesia': 'na',
            'turno_cev': 'na',
            'turno_cvo': 'na',
            'turno_agua': 'na',
            'turno_urgencia': 'na',
            'turno_internado': 'na',
            'turno_suspendido': 'na',

            'turno_pacienteid': 'na',
            'turno_nombre': 'na',
            'turno_apellido': 'na',
            'turno_documento': 'na',
            'turno_establecimiento': 'na',
            'turno_codigos': 'na',
            'turno_codigoscompleto': 'na',
            'turno_sexo': 'na',
            'turno_fotopm': 'na',
            'turno_fotodoc1': 'na',
            'turno_fotodoc2': 'na'
        })
        response_data = {'turnobloq_eliminar': True}
        return JsonResponse(response_data)
    except:
        response_data = {'turnobloq_eliminar': False}
        return JsonResponse(response_data)


def websocket_actualizarturno(turnoid, usu, servi, t_id, t_fechaini, t_fechafin, t_pres, t_anes,
                              t_cev, t_cvo, t_agua, t_urg, t_int, t_pacnom, t_pacape, t_pacdoc, t_estnom,
                              t_cod, t_confirmacion, t_estudio_terminado, t_whatsapp, t_pacientearm, t_fotopm,
                              t_fotodoc1, t_fotodoc2, t_pacid, t_resofix, t_fechaini2, t_fechafin2, t_codigoscompleto, t_sexo, t_susp):
    layer = get_channel_layer()
    async_to_sync(layer.group_send)('chat_turnobrightspeed', {
        'type': 'mensaje_turno',
        'message': turnoid,
        'message_type': 'eliminaryactualizar',
        'message_user': usu,
        'message_servicio': servi,
        'message_body': 'na',
        'turno_resofix': t_resofix,
        'turno_fechaini2': t_fechaini2,
        'turno_fechafin2': t_fechafin2,

        'turno_id': t_id,
        'turno_fechaini': t_fechaini,
        'turno_fechafin': t_fechafin,
        'turno_presente': t_pres,
        'turno_confirmacion': t_confirmacion,
        'turno_estudio_terminado': t_estudio_terminado,
        'turno_whatsapp': t_whatsapp,
        'turno_pacientearm': t_pacientearm,
        'turno_pacienteid': t_pacid,
        'turno_anestesia': t_anes,
        'turno_cev': t_cev,
        'turno_cvo': t_cvo,
        'turno_agua': t_agua,
        'turno_urgencia': t_urg,
        'turno_internado': t_int,
        'turno_suspendido': t_susp,

        'turno_nombre': t_pacnom,
        'turno_apellido': t_pacape,
        'turno_documento': t_pacdoc,
        'turno_establecimiento': t_estnom,
        'turno_codigos': t_cod,
        'turno_codigoscompleto': t_codigoscompleto,
        'turno_sexo': t_sexo,
        'turno_fotopm': t_fotopm,
        'turno_fotodoc1': t_fotodoc1,
        'turno_fotodoc2': t_fotodoc2
    })
    return True


def websocket_crearturnoreservado(turno_res, usuario, servicio, nota, fechaini, fechafin):
    fechaini_string = fechaini.strftime("%Y-%m-%dT%H:%M")
    fechafin_string = fechafin.strftime("%Y-%m-%dT%H:%M")

    layer = get_channel_layer()
    async_to_sync(layer.group_send)('chat_turnobrightspeed', {
        'type': 'mensaje_turno',
        'message': fechaini_string,
        'message_type': 'reservar',
        'message_user': str(usuario.username),
        'message_servicio': str(servicio.codigo),
        'message_body': nota,
        'turno_resofix': 'na',
        'turno_fechaini2': 'na',
        'turno_fechafin2': 'na',

        'turno_id': 'E' + str(turno_res.id),
        'turno_fechaini': 'na',
        'turno_fechafin': fechafin_string,
        'turno_presente': 'na',
        'turno_confirmacion': 'na',
        'turno_estudio_terminado': 'na',
        'turno_whatsapp': 'na',
        'turno_pacientearm': 'na',
        'turno_pacienteid': 'na',
        'turno_anestesia': 'na',
        'turno_cev': 'na',
        'turno_cvo': 'na',
        'turno_agua': 'na',
        'turno_urgencia': 'na',
        'turno_internado': 'na',
        'turno_suspendido': 'na',

        'turno_nombre': 'na',
        'turno_apellido': 'na',
        'turno_documento': 'na',
        'turno_establecimiento': 'na',
        'turno_codigos': 'na',
        'turno_codigoscompleto': 'na',
        'turno_sexo': 'na',
        'turno_fotopm': 'na',
        'turno_fotodoc1': 'na',
        'turno_fotodoc2': 'na'
    })
    return True


@login_required
def paciente_listar(request):
    mimetype = "application/json"

    if request.is_ajax:
        palabra = request.GET.get('term', '')
        if len(palabra) < 7:
            data_json = 'fail'
            return HttpResponse(data_json, mimetype)

        result_search = 'empty'
        for term in palabra.split():
            if len(term) >= 3:
                paciente_list = Paciente.objects.filter(Q(nombre__icontains=term) | Q(apellido__icontains=term) | Q(
                    identificacion_numero__iexact=term)).exclude(identificacion_tipo='NN')
                if result_search == 'empty':
                    result_search = paciente_list
                else:
                    result_search = result_search & paciente_list

        results = []
        for pac in result_search.order_by('apellido', 'nombre'):
            paciente_nombre = str(pac.nombre).capitalize() if pac.nombre else ''
            paciente_apellido = str(pac.apellido).capitalize() if pac.apellido else ''
            art_json = {'label': '(' + pac.identificacion_tipo + ' ' + str(pac.identificacion_numero) + ') ' + paciente_apellido + ', ' + paciente_nombre , 'value': pac.id}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    return HttpResponse(data_json, mimetype)


@login_required
def listarpaciente_turnoid(request):
    if request.is_ajax:
        palabra = request.GET.get('term', '')
        turno = Turno.objects.filter(id=palabra).first()
        results = []
        if turno:
            pac = turno.paciente
            paciente_nombre = str(pac.nombre).capitalize() if pac.nombre else ''
            paciente_apellido = str(pac.apellido).capitalize() if pac.apellido else ''
            if pac.paciente_nn:
                paciente_nombre = ''
                paciente_apellido = '- Paciente NN'

            art_json = {'label': '(' + str(pac.identificacion_numero) + ') ' + paciente_apellido + ', ' + paciente_nombre, 'value': pac.id}
            results.append(art_json)

        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def paciente_listarnormal(request):
    if request.is_ajax:
        palabra = request.GET.get('term', '')
        if palabra == '*':
            paciente_list = Paciente.objects.all().order_by('apellido')
        else:
            paciente_list = Paciente.objects.all()
            for term in palabra.split():
                paciente_list = paciente_list.filter(Q(nombre__icontains=term) | Q(apellido__icontains=term)).exclude(
                    identificacion_tipo='NN')
        results = []
        for pac in paciente_list:
            paciente_nombre = str(pac.nombre).capitalize() if pac.nombre else ''
            paciente_apellido = str(pac.apellido).capitalize() if pac.apellido else ''
            art_json = {'label': '(' + str(pac.identificacion_numero) + ') ' + paciente_apellido + ', ' + paciente_nombre, 'value': pac.identificacion_numero}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def obrasocial_listar(request):
    if request.is_ajax:
        palabra = request.GET.get('term', '')
        servicio_codigo = request.GET.get('servcod', '')
        servicio = Servicio.objects.get(codigo=servicio_codigo)

        if palabra == '*':
            obrasocial_list = Obrasocial.objects.filter(servicio=servicio).order_by('nombre')
        else:
            obrasocial_list = Obrasocial.objects.all()
            for term in palabra.split():
                obrasocial_list = obrasocial_list.filter(
                    Q(nombre__icontains=term) | Q(abreviatura__icontains=term), servicio=servicio).order_by('nombre')
        results = []
        for obs in obrasocial_list:
            habilitada = ''
            if not obs.habilitada or not obs.activa:
                habilitada = '*DESHAB. '
            nombre = habilitada + "(" + str(obs.codigo) + ") " + obs.nombre
            art_json = {'label': nombre, 'value': obs.codigo}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def medicosderivantes_listar(request):
    if request.is_ajax:
        palabra = request.GET.get('term', '')
        if palabra == '*':
            medicoder_list = MedicoDerivante.objects.all().order_by('nombre')
        else:
            medicoder_list = MedicoDerivante.objects.all()
            for term in palabra.split():
                medicoder_list = medicoder_list.filter(Q(apellido__icontains=term) | Q(nombre__icontains=term))
        results = []
        for med in medicoder_list:
            art_json = {'label': str(med.apellido).capitalize(), 'value': med.matricula}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)


@login_required
def establecimientos_listar(request):
    if request.is_ajax:
        palabra = request.GET.get('term', '')
        if palabra == '*':
            establecimientos_list = EstablecimientoDerivador.objects.all().order_by('nombre')
        else:
            establecimientos_list = EstablecimientoDerivador.objects.all()
            for term in palabra.split():
                establecimientos_list = establecimientos_list.filter(
                    Q(nombre__icontains=term) | Q(abreviatura__icontains=term))
        results = []
        for est in establecimientos_list:
            art_json = {'label': str(est.nombre).capitalize(), 'value': est.codigo}
            results.append(art_json)
        data_json = json.dumps(results)
    else:
        data_json = 'fail'

    mimetype = "application/json"
    return HttpResponse(data_json, mimetype)

@login_required
@transaction.atomic
def turnoreservado_crear(request):
    try:
        response_data = {}
        # servicio
        servicio_codigo = request.GET['servicio']
        servicio = Servicio.objects.get(codigo=servicio_codigo)

        # titulo nota
        titulo = request.GET.get('titulo', '')
        repeat = int(request.GET.get('repeat', '1'))
        size = int(request.GET.get('size', '1'))


        for rep in range(int(repeat)):
            # fecha inicio turno
            fecha = request.GET['fecha']
            objdate_fecha = datetime.datetime.strptime(fecha, '%d-%m-%YT%H:%M')
            fecha_aware = timezone.make_aware(objdate_fecha, timezone.get_current_timezone())
            fecha_aware = fecha_aware + datetime.timedelta(days=rep)
            # fecha fin turno
            fecha_fin = request.GET['fecha_fin']
            objdate_fechafin = datetime.datetime.strptime(fecha_fin, '%d-%m-%YT%H:%M')
            fechafin_aware = timezone.make_aware(objdate_fechafin, timezone.get_current_timezone())
            turno_tamano_servicio = int(servicio.default_event_duration[3:5]) * (int(size) - 1)
            fechafin_aware = fechafin_aware + datetime.timedelta(minutes=turno_tamano_servicio, days=rep)

            turnobj = TurnoReservado(usuario=request.user, fecha=fecha_aware, fecha2=fechafin_aware,  titulo=titulo, estado=True, servicio=servicio)
            turnobj.save()

            websocket_crearturnoreservado(turnobj,turnobj.usuario,turnobj.servicio,turnobj.titulo,turnobj.fecha,turnobj.fecha2)

        response_data['crearturno_exito'] = True

        return JsonResponse(response_data)

    except Exception as e:
        response_data = {'crearturno_exito': False}
        return JsonResponse(response_data)


@login_required
def marcar_paciente_corregido(request):
    try:
        paciente_id = request.GET['paciente_id']
        paciente = Paciente.objects.get(id=paciente_id)
        paciente.corregir = False
        paciente.corregir_texto = ""
        paciente.save()
        response_data = {"exito", "Marcado como corredigo"}
        return JsonResponse(response_data)
    except:
        response_data = {'error_404': '404'}
        return JsonResponse(response_data)


class TurnoPacienteCrearModal(FormView):
    model = Paciente
    form_class = TurnoPacienteFormModal
    template_name = 'paciente/paciente_form.html'


@login_required
@transaction.atomic()
def crear_feriado(request):
    try:
        response_data = {'eliminar_feriado': False, 'crear_feriado': False}

        fecha = request.GET.get('fecha', '')
        crear = request.GET.get('crear_feriado', False)

        if fecha != '':
            objdate_fecha = datetime.datetime.strptime(fecha, '%d/%m/%Y')
            date_feriado = objdate_fecha.date()
        else:
            return JsonResponse(response_data)
        if crear == 'true':

            feriado_nombre = request.GET.get('feriado_nombre', '')
            new_feriado, created = Feriados.objects.get_or_create(
                fecha=date_feriado,
            )

            new_feriado.nombre = feriado_nombre
            new_feriado.save()
            response_data['crear_feriado'] = True
        else:

            feriado_delete = Feriados.objects.filter(fecha=date_feriado)
            feriado_delete.delete()
            response_data['eliminar_feriado'] = True

        return JsonResponse(response_data)

    except Exception as e:
        print(e)
        response_data = {'crear_feriado': False}
        return JsonResponse(response_data)


@login_required
@transaction.atomic
def medicoderivante_crear(request):
    try:
        response_data = {'crearmedico_exito': False, 'medico_existe': False}

        matricula = request.GET.get('matricula', '')
        nombrecompleto = request.GET.get('nombrecompleto', '')

        medico_der, created = MedicoDerivante.objects.get_or_create(
            matricula=matricula,
        )
        if not created:
            response_data['medico_existe'] = True
            return JsonResponse(response_data)

        medico_der.apellido = nombrecompleto
        medico_der.save()
        response_data['crearmedico_exito'] = True
        return JsonResponse(response_data)

    except Exception as e:
        print(e)
        response_data = {'crearmedico_exito': False}
        return JsonResponse(response_data)