import datetime
import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from django.contrib.auth.models import User
from django.utils import timezone

from .models import TurnoBloqueo


class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.room_name = 'turnobrightspeed'
        self.room_group_name = 'chat_%s' % self.room_name

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        message_type = text_data_json['message_type']
        message_user = text_data_json['message_user']
        message_servicio = text_data_json['message_servicio']
        message_body = text_data_json['message_body']

        turno_resofix = text_data_json['turno_resofix']
        turno_fechaini2 = text_data_json['turno_fechaini2']
        turno_fechafin2 = text_data_json['turno_fechafin2']

        turno_id = text_data_json['turno_id']
        turno_fechaini = text_data_json['turno_fechaini']
        turno_fechafin = text_data_json['turno_fechafin']

        turno_presente = text_data_json['turno_presente']
        turno_confirmacion = text_data_json['turno_confirmacion']
        turno_estudio_terminado = text_data_json['turno_estudio_terminado']
        turno_whatsapp = text_data_json['turno_whatsapp']
        turno_pacientearm = text_data_json['turno_pacientearm']
        turno_pacienteid = text_data_json['turno_pacienteid']

        turno_anestesia = text_data_json['turno_anestesia']
        turno_cev = text_data_json['turno_cev']
        turno_cvo = text_data_json['turno_cvo']
        turno_agua = text_data_json['turno_agua']
        turno_urgencia = text_data_json['turno_urgencia']
        turno_internado = text_data_json['turno_internado']
        turno_suspendido = text_data_json['turno_suspendido']

        turno_nombre = text_data_json['turno_nombre']
        turno_apellido = text_data_json['turno_apellido']
        turno_documento = text_data_json['turno_documento']
        turno_establecimiento = text_data_json['turno_establecimiento']
        turno_codigos = text_data_json['turno_codigos']

        turno_codigoscompleto = text_data_json['turno_codigoscompleto']
        turno_sexo = text_data_json['turno_sexo']

        turno_fotopm = text_data_json['turno_fotopm']
        turno_fotodoc1 = text_data_json['turno_fotodoc1']
        turno_fotodoc2 = text_data_json['turno_fotodoc2']

        if message_type not in ['eliminar','mover','eliminarycrear']:
            await self.crearturno_bloqueo(message_user, message, message_servicio, message_type)

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'mensaje_turno',
                'message': message,
                'message_type': message_type,
                'message_user': message_user,
                'message_servicio': message_servicio,
                'message_body': message_body,

                'turno_resofix': turno_resofix,
                'turno_fechaini2': turno_fechaini2,
                'turno_fechafin2': turno_fechafin2,

                'turno_id': turno_id,
                'turno_fechaini': turno_fechaini,
                'turno_fechafin': turno_fechafin,

                'turno_presente': turno_presente,
                'turno_confirmacion': turno_confirmacion,
                'turno_estudio_terminado': turno_estudio_terminado,
                'turno_whatsapp': turno_whatsapp,
                'turno_anestesia': turno_anestesia,
                'turno_cev': turno_cev,
                'turno_cvo': turno_cvo,
                'turno_agua': turno_agua,
                'turno_urgencia': turno_urgencia,
                'turno_internado': turno_internado,
                'turno_pacientearm': turno_pacientearm,
                'turno_pacienteid': turno_pacienteid,
                'turno_suspendido': turno_suspendido,

                'turno_nombre': turno_nombre,
                'turno_apellido': turno_apellido,
                'turno_documento': turno_documento,
                'turno_establecimiento': turno_establecimiento,
                'turno_codigos': turno_codigos,
                'turno_codigoscompleto': turno_codigoscompleto,
                'turno_sexo': turno_sexo,

                'turno_fotopm': turno_fotopm,
                'turno_fotodoc1': turno_fotodoc1,
                'turno_fotodoc2': turno_fotodoc2

            }
        )

    async def mensaje_turno(self, event):
        message = event['message']
        message_type = event['message_type']
        message_user = event['message_user']
        message_servicio = event['message_servicio']
        message_body = event['message_body']

        turno_resofix = event['turno_resofix']
        turno_fechaini2 = event['turno_fechaini2']
        turno_fechafin2 = event['turno_fechafin2']
        turno_id = event['turno_id']
        turno_fechaini = event['turno_fechaini']
        turno_fechafin = event['turno_fechafin']

        turno_presente = event['turno_presente']
        turno_confirmacion = event['turno_confirmacion']
        turno_estudio_terminado = event['turno_estudio_terminado']
        turno_whatsapp = event['turno_whatsapp']
        turno_pacientearm = event['turno_pacientearm']
        turno_pacienteid = event['turno_pacienteid']

        turno_anestesia = event['turno_anestesia']
        turno_cev = event['turno_cev']
        turno_cvo = event['turno_cvo']
        turno_agua = event['turno_agua']
        turno_urgencia = event['turno_urgencia']
        turno_internado = event['turno_internado']
        turno_suspendido = event['turno_suspendido']

        turno_nombre = event['turno_nombre']
        turno_apellido = event['turno_apellido']
        turno_documento = event['turno_documento']
        turno_establecimiento = event['turno_establecimiento']
        turno_codigos = event['turno_codigos']
        turno_codigoscompleto = event['turno_codigoscompleto']
        turno_sexo = event['turno_sexo']

        turno_fotopm = event['turno_fotopm']
        turno_fotodoc1 = event['turno_fotodoc1']
        turno_fotodoc2 = event['turno_fotodoc2']


        await self.send(text_data=json.dumps({
            'message': message,
            'message_type': message_type,
            'message_user': message_user,
            'message_servicio': message_servicio,
            'message_body': message_body,

            'turno_resofix': turno_resofix,
            'turno_fechaini2': turno_fechaini2,
            'turno_fechafin2': turno_fechafin2,
            'turno_id': turno_id,
            'turno_fechaini': turno_fechaini,
            'turno_fechafin': turno_fechafin,

            'turno_presente': turno_presente,
            'turno_confirmacion': turno_confirmacion,
            'turno_estudio_terminado': turno_estudio_terminado,
            'turno_whatsapp': turno_whatsapp,
            'turno_anestesia': turno_anestesia,
            'turno_cev': turno_cev,
            'turno_cvo': turno_cvo,
            'turno_agua': turno_agua,
            'turno_urgencia': turno_urgencia,
            'turno_internado': turno_internado,
            'turno_pacientearm': turno_pacientearm,
            'turno_pacienteid': turno_pacienteid,
            'turno_suspendido': turno_suspendido,
            'turno_nombre': turno_nombre,
            'turno_apellido': turno_apellido,
            'turno_documento': turno_documento,
            'turno_establecimiento': turno_establecimiento,
            'turno_codigos': turno_codigos,

            'turno_codigoscompleto': turno_codigoscompleto,
            'turno_sexo': turno_sexo,

            'turno_fotopm': turno_fotopm,
            'turno_fotodoc1': turno_fotodoc1,
            'turno_fotodoc2': turno_fotodoc2

        }))

    @database_sync_to_async
    def crearturno_bloqueo(self, usuario, fecha, servicio, tipo):
        servicio_bloq = servicio
        usuario_obj = User.objects.get(username=usuario)
        fecha_obj = datetime.datetime.strptime(fecha, '%Y-%m-%dT%H:%M')
        fecha_aware = timezone.make_aware(fecha_obj, timezone.get_current_timezone())

        if tipo == 'bloquear':
            turno = TurnoBloqueo.objects.get_or_create(usuario=usuario_obj, fecha=fecha_aware, servicio=servicio_bloq)
            obj_turno = turno[0]
            obj_turno.save()
        else:
            TurnoBloqueo.objects.filter(usuario=usuario_obj, servicio=servicio_bloq).delete()

class ChatConsumer2(AsyncWebsocketConsumer):

    async def connect(self):
        self.room_name = 'informes'
        self.room_group_name = 'chat_%s' % self.room_name

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        turno_id = text_data_json['turno_id']
        tipo_actualizacion = text_data_json['tipo_actualizacion']
        servicio = text_data_json['servicio']
        fecha = text_data_json['fecha']
        identificacion_numero = text_data_json['identificacion_numero']
        apellido_nombre = text_data_json['apellido_nombre']
        atencion = text_data_json['atencion']
        medico_matricula = text_data_json['medico_matricula']
        medico_iniciales = text_data_json['medico_iniciales']
        tipeo_finalizado_tipo = text_data_json['tipeo_finalizado_tipo']

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'turno_id': turno_id,
                'tipo_actualizacion': tipo_actualizacion,
                'servicio': servicio,
                'fecha': fecha,
                'identificacion_numero': identificacion_numero,
                'apellido_nombre': apellido_nombre,
                'atencion': atencion,
                'medico_matricula': medico_matricula,
                'medico_iniciales': medico_iniciales,
                'tipeo_finalizado_tipo' : tipeo_finalizado_tipo,

            }
        )

    async def mensaje_turno(self, event):
        turno_id = event['turno_id']
        tipo_actualizacion = event['tipo_actualizacion']
        servicio = event['servicio']
        fecha = event['fecha']
        identificacion_numero = event['identificacion_numero']
        apellido_nombre = event['apellido_nombre']
        atencion = event['atencion']
        medico_matricula = event['medico_matricula']
        medico_iniciales = event['medico_iniciales']
        tipeo_finalizado_tipo = event['tipeo_finalizado_tipo']

        await self.send(text_data=json.dumps({
            'turno_id': turno_id,
            'tipo_actualizacion': tipo_actualizacion,
            'servicio': servicio,
            'fecha': fecha,
            'identificacion_numero': identificacion_numero,
            'apellido_nombre': apellido_nombre,
            'atencion': atencion,
            'medico_matricula': medico_matricula,
            'medico_iniciales': medico_iniciales,
            'tipeo_finalizado_tipo': tipeo_finalizado_tipo,
        }))
