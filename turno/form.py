from django import forms
from django.forms.widgets import CheckboxSelectMultiple

from .models import Obrasocial, MedicoDerivante, Turno
from .models import Paciente, Pais, Provincia, Departamento, Localidad, Barrio


class ObrasocialForm(forms.ModelForm):
    class Meta:
        model = Obrasocial
        fields = ('nombre', 'abreviatura', 'codigo', 'servicio', 'direccion', 'habilitada', 'activa', 'informacion_contacto', 'observacion')
        labels = {
            'nombre': 'Nombre - Razón Social',
            'observacion': 'Observaciones - Requerimientos',
            'servicio': 'Servicios Habilitados',
        }
        widgets = {
            'nombre': forms.TextInput(
                attrs={'class': 'form-control',  }),
            'abreviatura': forms.TextInput(attrs={'class': 'form-control', }),
            'codigo': forms.NumberInput(attrs={'class': 'form-control', }),
            'servicio': forms.SelectMultiple(
                attrs={'class': 'form-control', }),

            'direccion': forms.TextInput(
                attrs={'class': 'form-control', }),
            'habilitada': forms.Select(
                attrs={'class': 'form-control'}),
            'activa': forms.Select(
                attrs={'class': 'form-control'}),
            'informacion_contacto': forms.Textarea(attrs={'class': 'form-control',  'rows': '3'}),
            'observacion': forms.Textarea(attrs={'class': 'form-control', 'rows': '8'}),
        }


class MedicoForm(forms.ModelForm):
    class Meta:
        model = MedicoDerivante
        fields = ('matricula', 'apellido', 'observacion', 'telefono')

        widgets = {
            'matricula': forms.NumberInput(
                attrs={'class': 'form-control input-bg-color', 'min': '0', }),
            'apellido': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', }),
            'observacion': forms.TextInput(
                attrs={'class': 'form-control input-bg-color',}),
            'telefono': forms.TextInput(
                attrs={'class': 'form-control input-bg-color',}),
        }


class PacienteForm(forms.ModelForm):
    class Meta:
        model = Paciente
        fields = (
            'identificacion_tipo',
            'identificacion_numero',
            'apellido',
            'nombre',
            'sexo',
            'fecnac',
            'telefono',
            'estadocivil',
            'paciente_nn',
            'observaciones',
            'obrasocial',
            'pais',
            'provincia',
            'departamento',
            'localidad',
            'barrio',
            'domicilio',
        )

        widgets = {
            'pais': forms.Select(attrs={'class': 'form-control input-bg-color select'}),
            'provincia': forms.Select(attrs={'class': 'form-control input-bg-color select'}),
            'departamento': forms.Select(attrs={'class': 'form-control input-bg-color select'}),
            'localidad': forms.Select(attrs={'class': 'form-control input-bg-color select'}),
            'barrio': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Barrio', 'placeholder': 'Barrio del paciente', }),
            'domicilio': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Domicilio', 'placeholder': 'Domicilio del paciente', }),

            'identificacion_tipo': forms.Select(
                attrs={'class':'form-control input-bg-color select', }),
            'identificacion_numero': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'placeholder': 'N° de identificacion',}),

            'apellido': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Apellido', 'placeholder': 'Apellido del paciente', }),
            'nombre': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Nombre', 'placeholder': 'Nombre del paciente', }),
            'sexo': forms.Select(attrs={'class':'form-control input-bg-color select'}),
            'fecnac': forms.DateInput(
                attrs={'class': 'form-control input-bg-color select','placeholder':'Fecha de nacimiento del paciente'}),
            'telefono': forms.NumberInput(
                attrs={'class': 'form-control input-bg-color', 'min': '0', 'label': 'Teléfono', 'placeholder': 'N° de teléfono del paciente',}),
            'estadocivil': forms.Select(attrs={'class':' form-control input-bg-color select'}),
            'paciente_nn': forms.Select(attrs={'class': ' form-control input-bg-color select'}),
            'observaciones': forms.Textarea(
                attrs={'class': 'form-control input-bg-color', 'rows': '3', 'style': 'resize: none;', }),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['pais'].queryset = Pais.objects.filter().order_by('nombre')
        self.fields['provincia'].queryset = Provincia.objects.none()
        self.fields['departamento'].queryset = Departamento.objects.none()
        self.fields['localidad'].queryset = Localidad.objects.none()
        self.fields['obrasocial'].widgets = CheckboxSelectMultiple()
        self.fields['obrasocial'].queryset = Obrasocial.objects.all()

        if 'pais' in self.data:
            try:
                idpais = int(self.data.get('pais'))
                self.fields['provincia'].queryset = Provincia.objects.filter(pais=idpais).order_by('nombre')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['provincia'].queryset = Provincia.objects.filter(pais=self.instance.pais).order_by('nombre')

        if 'provincia' in self.data:
            try:
                idprovin = int(self.data.get('provincia'))
                self.fields['departamento'].queryset = Departamento.objects.filter(provincia=idprovin).order_by('nombre')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['departamento'].queryset = Departamento.objects.filter(provincia=self.instance.provincia).order_by('nombre')
        if 'departamento' in self.data:
            try:
                iddepar = int(self.data.get('departamento'))
                self.fields['localidad'].queryset = Localidad.objects.filter(departamento=iddepar).order_by('nombre')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['localidad'].queryset = Localidad.objects.filter(departamento=self.instance.departamento).order_by('nombre')


class TurnoForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = ('paciente', 'medico_derivante', 'establecimiento_derivador', 'obrasocial', 'establecimiento_servicio', 'turno_numero', 'turno_fixreso_inicio', 'turno_fixreso_fin',
                  'contrasteev', 'contrastevo', 'tomaagua', 'anestesia', 'urgencia', 'paciente_arm', 'turno_recepcion_suspendido',
                  'internado', 'presente', 'presentefecha', 'observacion', 'tipomodalidad', 'canalizacion', 'paciente_nn', 'foto_pedidomedico', 'foto_documento', 'foto_documento2',
                  'tiporeprogramado', 'habilitado', 'turno_relacionado','turno_enpartes_primero' ,'turnotrabajo', 'confirmacion', 'observacion_noimpresa', 'telefono_whatsapp', 'email_entregadigital')
        widgets = {
            'telefono_whatsapp': forms.TextInput(
                attrs={'class': 'form-control', 'id': "input_telefono_whatsapp",
                       'name': "input_telefono_whatsapp"}),
            'email_entregadigital': forms.TextInput(
                attrs={'class': 'form-control', 'id': "input_email_entregadigital",
                       'name': "input_email_entregadigital", 'type': "email"}),
            'paciente': forms.TextInput(
                attrs={'class':'form-control', 'id':"input_paciente_id", 'style': "display: none", 'name': "paciente_id"}),
            'turno_numero': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_numero_id", 'style': "display: none", 'name': "turno_numero_id"}),
            'turno_fixreso_inicio': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_fixreso_inicio", 'style': "display: none", 'name': "turno_fixreso_inicio"}),
            'turno_fixreso_fin': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_fixreso_fin", 'style': "display: none",
                       'name': "turno_fixreso_fin"}),
            'medico_derivante': forms.TextInput(
                attrs={'class': 'form-control', 'id': "input_medico_id", 'style': "display: none", 'name': "medico_id"}),
            'establecimiento_derivador': forms.TextInput(
                attrs={'class': 'form-control', 'id':"establecimiento_id", 'style': "display: none", 'name': "establecimiento_id"}),
            'establecimiento_servicio': forms.Select(
                attrs={'class': 'form-control', 'id': "establecimiento_servicio_id",
                       'name': "establecimiento_servicio_id", 'disabled':'disabled'}),
            'obrasocial': forms.TextInput(attrs={'class': 'form-control', 'id':"input_obrasocial_id", 'style': "display: none", 'name': "obrasocial_id"}),
            'contrasteev': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id': "f_contrasteev", 'name': "f_contrasteev"}),
            'contrastevo': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id': "f_contrastevo", 'name': "f_contrastevo"}),
            'tomaagua': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id': "f_tomaagua", 'name': "f_tomaagua"}),
            'turno_recepcion_suspendido': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_recepcion_suspendido", 'name': "f_recepcion_suspendido"}),

            'canalizacion': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_canalizacion", 'name': "f_canalizacion"}),
            'anestesia': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_anestesia", 'name':"f_anestesia"}),
            'urgencia': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_urgencia", 'name':"f_urgencia"}),
            'paciente_arm': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_pacientearm", 'name': "f_pacientearm"}),
            'paciente_nn': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_pacientenn",
                       'name': "f_pacientenn"}),
            'internado': forms.TextInput(
                attrs={'class': 'form-control', 'style':"display: none", 'id': "f_internado", 'name': "f_internado"}),
            'presente': forms.TextInput(
                attrs={'class': 'form-control', 'style':"display: none", 'id': "f_presente", 'name': "f_presente"}),
            'confirmacion': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_confirmacion", 'name': "f_confirmacion"}),

            'presentefecha': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "presentefecha", 'name': "presentefecha"}),
            'observacion': forms.Textarea(
                attrs={'class': 'form-control', 'rows': '2', 'id': 'observacion', 'style': 'resize: none;'}),
            'observacion_noimpresa': forms.Textarea(
                attrs={'class': 'form-control', 'rows': '2', 'id': 'observacion_noimpresa', 'maxlength': '280', 'style': 'resize: none;'}),
            'tipomodalidad': forms.Select(attrs={'class': 'form-control'}),
            'tiporeprogramado': forms.Select(attrs={'class': 'form-control', 'style':"display: none" }),
            'habilitado': forms.Select(attrs={'class': 'form-control', 'style':"display: none" }),
            'turno_enpartes_primero': forms.Select(attrs={'class': 'form-control', 'id': 'tipo_turno_enpartes'}),

            'turno_relacionado': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turnorelacionado_id", 'style': "display: none",
                       'name': "turnorelacionado_id"}),
            'turnotrabajo': forms.TextInput(
                attrs={'class': 'form-control', 'id':"crearturno-turnoguardia",  'name': "crearturno-turnoguardia", 'value': "Normal"}),
        }


class TurnoActualizarForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = ('paciente', 'medico_derivante', 'establecimiento_derivador', 'obrasocial', 'establecimiento_servicio',
                  'entrega_digital','whatsapp', 'turno_fixreso_inicio', 'turno_fixreso_fin', 'turno_numero', 'foto_pedidomedico',
                  'contrasteev', 'contrastevo', 'tomaagua', 'anestesia', 'urgencia', 'paciente_arm', 'turno_recepcion_suspendido',
                  'internado', 'presente', 'presentefecha', 'observacion', 'tipomodalidad', 'canalizacion', 'paciente_nn',
                  'tiporeprogramado', 'habilitado', 'turno_relacionado','turno_enpartes_primero' ,'turnotrabajo',
                  'confirmacion', 'observacion_noimpresa', 'telefono_whatsapp', 'email_entregadigital', 'fechahora_inicio',
                  'fechahora_fin', 'fechahora_inicio2', 'fechahora_fin2', 'foto_documento', 'foto_documento2')
        widgets = {
            'telefono_whatsapp': forms.TextInput(
                attrs={'class': 'form-control', 'id': "input_telefono_whatsapp2",
                       'name': "input_telefono_whatsapp"}),
            'email_entregadigital': forms.TextInput(
                attrs={'class': 'form-control', 'id': "input_email_entregadigital2",
                       'name': "input_email_entregadigital", 'type': "email"}),
            'paciente': forms.TextInput(attrs={'class':'form-control', 'id':"input_paciente_id2", 'style':"display: none", 'name':"paciente_id"}),
            'medico_derivante': forms.TextInput(attrs={'class': 'form-control', 'id':"input_medico_id2", 'style':"display: none", 'name':"medico_id"}),
            'establecimiento_derivador': forms.TextInput(attrs={'class': 'form-control', 'id':"establecimiento_id2", 'style':"display: none", 'name':"establecimiento_id"}),
            'establecimiento_servicio': forms.Select(
                attrs={'class': 'form-control', 'id': "establecimiento_servicio_id2",
                       'name': "establecimiento_servicio_id", 'disabled':'disabled'}),
            'obrasocial': forms.TextInput(attrs={'class': 'form-control', 'id':"input_obrasocial_id2", 'style':"display: none", 'name':"obrasocial_id"}),
            'contrasteev': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_contrasteev2", 'name':"f_contrasteev"}),
            'contrastevo': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_contrastevo2", 'name':"f_contrastevo"}),
            'tomaagua': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_tomaagua2", 'name':"f_tomaagua"}),
            'canalizacion': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_canalizacion2", 'name': "f_canalizacion"}),
            'anestesia': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_anestesia2", 'name':"f_anestesia"}),
            'urgencia': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_urgencia2", 'name':"f_urgencia"}),
            'whatsapp': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_whatsapp2", }),
            'entrega_digital': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_engregadigital2",}),
            'turno_recepcion_suspendido': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_recepcion_suspendido2",
                       'name': "f_recepcion_suspendido"}),
            'paciente_arm': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_pacientearm2", 'name': "f_pacientearm"}),
            'paciente_nn': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_pacientenn2",
                       'name': "f_pacientenn"}),
            'internado': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_internado2", 'name':"f_internado"}),
            'presente': forms.TextInput(attrs={'class': 'form-control', 'style':"display: none", 'id':"f_presente2", 'name':"f_presente"}),
            'confirmacion': forms.TextInput(
                attrs={'class': 'form-control', 'style': "display: none", 'id': "f_confirmacion2", 'name': "f_confirmacion"}),

            'presentefecha': forms.TextInput(attrs={'class': 'form-control', 'style': "display: none", 'id':"presentefecha2", 'name':"presentefecha"}),
            'observacion': forms.Textarea(attrs={'class': 'form-control', 'rows': '2', 'id': 'observacion2'}),
            'observacion_noimpresa': forms.Textarea(attrs={'class': 'form-control', 'rows': '2', 'id': 'observacion_noimpresa2', 'maxlength':'280'}),
            'tipomodalidad': forms.Select(attrs={'class': 'form-control', 'id': 'tipomodalidad2'}),
            'tiporeprogramado': forms.Select(attrs={'class': 'form-control', 'style': "display: none"}),
            'habilitado': forms.Select(attrs={'class': 'form-control', 'style': "display: none"}),
            'turno_enpartes_primero': forms.Select(attrs={'class': 'form-control', 'id': 'tipo_turno_enpartes2'}),

            'turno_relacionado': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turnorelacionado_id2", 'style': "display: none" ,
                       'name': "turnorelacionado_id2"}),
            'turnotrabajo': forms.TextInput(
                attrs={'class': 'form-control', 'id': "crearturno-turnoguardia2",  'name': "crearturno-turnoguardia", 'value':"Normal"}),

            'turno_numero': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_numero_id2", 'style': "display: none",
                       'name': "turno_numero_id"}),
            'turno_fixreso_inicio': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_fixreso_inicio2", 'style': "display: none",
                       'name': "turno_fixreso_inicio"}),
            'turno_fixreso_fin': forms.TextInput(
                attrs={'class': 'form-control', 'id': "turno_fixreso_fin2", 'style': "display: none",
                       'name': "turno_fixreso_fin"}),

            'fechahora_inicio': forms.TextInput(
                attrs={'class': 'form-control',  'id': 'fechahora_inicio2', "name":"fechahora_inicio"}),
            'fechahora_fin': forms.TextInput(
                attrs={'class': 'form-control', 'id': 'fechahora_fin2', "name":"fechahora_fin"}),

            'fechahora_inicio2': forms.TextInput(
                attrs={'class': 'form-control',  'id': 'fechahora2_inicio2',
                       "name": "fechahora2_inicio"}),
            'fechahora_fin2': forms.TextInput(
                attrs={'class': 'form-control',  'id': 'fechahora2_fin2',
                       "name": "fechahora2_fin"}),

        }


class TurnoPacienteForm(forms.ModelForm):
    class Meta:
        model = Paciente
        fields = (
            'pais',
            'provincia',
            'departamento',
            'localidad',
            'barrio',
            'domicilio',
            'identificacion_tipo',
            'identificacion_numero',
            'apellido',
            'nombre',
            'sexo',
            'fecnac',
            'telefono',
            'observaciones',
        )

        widgets = {
            'pais': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_pais', 'tabindex':'-1'}),
            'provincia': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_provincia', 'name': 'provincia', 'tabindex':'-1'}),
            'departamento': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_departamento', 'name': 'departamento', 'tabindex':'-1'}),
            'localidad': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_localidad', 'name': 'localidad', 'tabindex':'-1'}),
            'barrio': forms.Select(
                attrs={'class': 'form-control input-bg-color select', 'label': 'Barrio',  'name':'barrio', 'id': 'id_barrio',}),
            'domicilio': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Domicilio',  'name':'domicilio', 'id': 'id_domicilio'}),
            'identificacion_tipo': forms.Select(
                attrs={'class': 'form-control input-bg-color select', 'name':'identificacion_tipo', 'id':'id_identificacion_tipo', 'onChange':'clearform()'}),
            'identificacion_numero': forms.TextInput(
                attrs={'class': 'form-control input-bg-color',  'name':'identificacion_numero', 'id':'id_identificacion_numero', 'autofocus':"autofocus", 'onfocusout':'buscarpaciente_datos()'}),
            'apellido': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Apellido',  'name':'apellido', 'id':'id_apellido', 'required': 'true'}),
            'nombre': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Nombre',  'name':'nombre', 'id':'id_nombre', 'required': 'true'}),
            'sexo': forms.Select(attrs={'class':'form-control input-bg-color select', 'name':'sexo', 'id':'id_sexo', 'onchange':'pacientennlabel_create()'}),
            'fecnac': forms.DateInput(
                attrs={'class': 'form-control input-bg-color', 'type':'date' , 'name':'fecnac', 'id':'id_fecnac',}),
            'telefono':forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'min': '0', 'label': 'Teléfono',  'name':'telefono', 'id':'id_telefono'}),
            'observaciones': forms.Textarea(
                attrs={'class': 'form-control input-bg-color', 'rows':'3', 'style':'resize: none;', 'name':'observaciones', 'id':'id_observaciones'}),
        }


    def __init__(self, *args, **kwargs):
        super(TurnoPacienteForm, self).__init__(*args, **kwargs)
        barrios = Barrio.objects.all()
        lista_barrios = [(i.codigo, i.nombre) for i in barrios]
        lista_barrios.insert(0, ('' , '-------'))
        self.fields['barrio'] = forms.ChoiceField(choices=lista_barrios, widget=forms.Select(attrs={'class':'form-control input-bg-color select', 'label': 'Barrio',  'name':'barrio', 'id':'id_barrio',}))
        paises = Pais.objects.all()
        lista_paises = [(i.codigo, i.nombre) for i in paises]
        lista_paises.insert(0, ('' , '-------'))
        self.fields['pais'] = forms.ChoiceField(choices=lista_paises, widget=forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_pais', 'tabindex':'-1'}))

class TurnoPacienteFormModal(forms.ModelForm):
    class Meta:
        model = Paciente
        fields = (
            'pais',
            'provincia',
            'departamento',
            'localidad',
            'barrio',
            'domicilio',
            'identificacion_tipo',
            'identificacion_numero',
            'apellido',
            'nombre',
            'sexo',
            'fecnac',
            'telefono',
            'observaciones',
        )

        widgets = {
            'pais': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_pais', 'tabindex':'-1'}),
            'provincia': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_provincia', 'name': 'provincia', 'tabindex':'-1'}),
            'departamento': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_departamento', 'name': 'departamento', 'tabindex':'-1'}),
            'localidad': forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_localidad', 'name': 'localidad', 'tabindex':'-1'}),
            'barrio': forms.Select(
                attrs={'class': 'form-control input-bg-color select', 'label': 'Barrio',  'name':'barrio', 'id': 'id_barrio',}),
            'domicilio': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Domicilio',  'name':'domicilio', 'id': 'id_domicilio'}),
            'identificacion_tipo': forms.Select(
                attrs={'class': 'form-control input-bg-color select', 'name':'identificacion_tipo', 'id':'id_identificacion_tipo',}),
            'identificacion_numero': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'name':'identificacion_numero', 'id':'id_identificacion_numero',}),
            'apellido': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Apellido',  'name':'apellido', 'id':'id_apellido', 'required': 'true'}),
            'nombre': forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'label': 'Nombre',  'name':'nombre', 'id':'id_nombre', 'required': 'true'}),
            'sexo': forms.Select(attrs={'class':'form-control input-bg-color select', 'name':'sexo', 'id':'id_sexo',}),
            'fecnac': forms.DateInput(
                attrs={'class': 'form-control input-bg-color', 'type':'date' , 'name':'fecnac', 'id':'id_fecnac',}),
            'telefono':forms.TextInput(
                attrs={'class': 'form-control input-bg-color', 'min': '0', 'label': 'Teléfono',  'name':'telefono', 'id':'id_telefono'}),
            'observaciones': forms.Textarea(
                attrs={'class': 'form-control input-bg-color', 'rows':'3', 'style':'resize: none;', 'name':'observaciones', 'id':'id_observaciones'}),
        }


    def __init__(self, *args, **kwargs):
        super(TurnoPacienteFormModal, self).__init__(*args, **kwargs)
        barrios = Barrio.objects.all()
        lista_barrios = [(i.codigo, i.nombre) for i in barrios]
        lista_barrios.insert(0, ('' , '-------'))
        self.fields['barrio'] = forms.ChoiceField(choices=lista_barrios, widget=forms.Select(attrs={'class':'form-control input-bg-color select', 'label': 'Barrio',  'name':'barrio', 'id':'id_barrio',}))
        paises = Pais.objects.all()
        lista_paises = [(i.codigo, i.nombre) for i in paises]
        lista_paises.insert(0, ('' , '-------'))
        self.fields['pais'] = forms.ChoiceField(choices=lista_paises, widget=forms.Select(attrs={'class':'form-control input-bg-color select', 'id':'id_pais', 'tabindex':'-1'}))