from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords

from tecnico.models import Insumo, InsumoLote


def user_directory_path_pm(instance, filename):
    return 'fotospmdoc/uid_{0}/pm/{1}'.format(instance.paciente.id, filename)


def user_directory_path_dni(instance, filename):
    return 'fotospmdoc/uid_{0}/dni/{1}'.format(instance.paciente.id, filename)


def user_directory_path_informe(instance, filename):
    return 'informes/uid_{0}/{1}'.format(instance.id, filename)


def user_directory_path_informe_audio(instance, filename):
    return 'audios/uid_{0}/informe_audio_{0}.wav'.format(instance.id)


class Pais(models.Model):
    class Meta:
        ordering = ['nombre']
    nombre = models.CharField(max_length=120)
    codigo = models.IntegerField()

    def __str__(self):
        return self.nombre


class Provincia(models.Model):
    class Meta:
        ordering = ['nombre']
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=40)
    codigo = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    class Meta:
        ordering = ['nombre']
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=120)
    codigo = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Localidad(models.Model):
    class Meta:
        ordering = ['nombre']
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=120)
    codigo = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Barrio(models.Model):
    class Meta:
        ordering = ['nombre']
    nombre = models.CharField(max_length=160)
    codigo = models.IntegerField(unique=True)

    def __str__(self):
        return self.nombre


class MedicoDerivante(models.Model):
    class Meta:
        ordering = ['apellido', 'nombre']
    matricula = models.CharField(max_length=40, unique=True)
    nombre = models.CharField(max_length=80, null=True, blank=True)
    apellido = models.CharField(max_length=200)
    observacion = models.CharField(max_length=600, null=True, blank=True)
    telefono = models.CharField(max_length=40, null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        nombre_completo = ''
        if self.apellido:
            nombre_completo += self.apellido + ', '
        if self.nombre:
            nombre_completo += self.nombre
        return nombre_completo


class EstablecimientoDerivador(models.Model):
    ENABLE = (
        (True, 'Habilitado'),
        (False, 'Deshabilitado'),
    )
    TYPES = {
        ('Hospital', 'Hospital'),
        ('Clinica', 'Clinica')
    }

    class Meta:
        ordering = ['nombre']
    nombre = models.CharField(max_length=200, verbose_name='Nombre del Establecimiento')
    tipo = models.CharField(choices=TYPES, max_length=40, verbose_name='Tipo de institución', null=True)
    abreviatura = models.CharField(max_length=10, verbose_name='Abreviatura', null=True, blank=True)
    codigo = models.PositiveSmallIntegerField(verbose_name='Codigo del Establecimiento Derivador', unique=True)
    direccion = models.CharField(max_length=200,verbose_name='Dirección', null=True, blank=True)
    informacion_contacto = models.TextField(max_length=300, verbose_name='Información de contacto', null=True, blank=True)

    def __str__(self):
        return self.nombre


class EstablecimientoServicio(models.Model):
    ENABLE = (
        (True, 'Habilitado'),
        (False, 'Deshabilitado'),
    )

    class Meta:
        ordering = ['nombre']
    nombre = models.CharField(max_length=200, verbose_name='Nombre del Servicio ', null=True)
    observaciones = models.TextField(max_length=300, verbose_name='Información de contacto del Servicio', null=True, blank=True)

    def __str__(self):
        return self.nombre


class Servicio(models.Model):
    TYPES = {
        ('CT', 'CT'),
        ('MR', 'MR'),
        ('CR', 'CR')
    }
    SUCURSAL = {
        ('Salta', 'Salta'),
        ('Oran', 'Oran'),
        ('Cafayate', 'Cafayate'),
        ('Rosario de la Frontera', 'Rosario de la Frontera'),
        ('Embarcacion', 'Embarcacion'),
        ('Tartagal', 'Tartagal'),
    }

    class Meta:
        ordering = ['nombre']
    nombre = models.CharField(max_length=200, verbose_name='Nombre del Establecimiento', unique=True)
    nombre_completo = models.CharField(max_length=200, verbose_name='Nombre Completo', null=True, blank=True)
    direccion = models.CharField(max_length=200, verbose_name='Direccion sucursal', null=True, blank=True)
    tipo = models.CharField(choices=TYPES, max_length=40, verbose_name='Tipo de Equipo', null=True)
    sucursal = models.CharField(choices=SUCURSAL, max_length=40, verbose_name='Sucursal')
    observacion = models.TextField(max_length=400, verbose_name='Observacion', null=True, blank=True)
    aetitle = models.CharField(max_length=80, verbose_name='AE TITLE', null=True, blank=True)
    protocolo_count = models.IntegerField(null=True, blank=True)
    protocolo_prefix = models.CharField(max_length=50, null=True, blank=True)
    protocolo_label = models.CharField(max_length=50, null=True, blank=True)
    codigo = models.IntegerField(null=True, blank=True, unique=True)
    codigo_sistema_ant = models.IntegerField(null=True, blank=True, unique=True)
    protocolo_mesactual = models.SmallIntegerField(default=1) # aux para el reseteo del count de mes en protocolos
    hora_guardiaini = models.TimeField(null=True, blank=True)
    hora_guardiafin = models.TimeField(null=True, blank=True)
    informe_encabezado = models.CharField(max_length=300, null=True, blank=True) # texto info por servicio que se agrega al informe final
    prefix_access_number = models.CharField(max_length=25, null=True, blank=True) # prefijo para los access number de este servicio
    nro_informe_prefix = models.CharField(max_length=50, null=True, blank=True)
    nro_informe_count = models.IntegerField(null=True, blank=True, default=0)
    stock_insumo_general = models.BooleanField(default=False) # gestion del inusmo en forma general por servicio o en caso false seria por tecnico caso boedo
    encargado_stock = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    usa_worklist_vm = models.BooleanField(default=False)
    usa_access_number = models.BooleanField(default=True)
    habilitado = models.BooleanField(default=True)
    is_servicio_externo = models.BooleanField(default=False)
    slot_duration = models.CharField(max_length=50, default='00:20:00')
    slot_labelinteval = models.CharField(max_length=50, default='00:20:00')
    snap_duration = models.CharField(max_length=50, default='00:20:00')
    default_event_duration = models.CharField(max_length=50, default='00:20:00')
    slot_duration_sabado = models.CharField(max_length=50, default='00:20:00')
    slot_labelinteval_sabado = models.CharField(max_length=50, default='00:20:00')
    snap_duration_sabado = models.CharField(max_length=50, default='00:20:00')
    default_event_duration_sabado = models.CharField(max_length=50, default='00:20:00')
    slot_duration_domingo = models.CharField(max_length=50, default='00:20:00')
    slot_labelinteval_domingo = models.CharField(max_length=50, default='00:20:00')
    snap_duration_domingo = models.CharField(max_length=50, default='00:20:00')
    default_event_duration_domingo = models.CharField(max_length=50, default='00:20:00')
    maxtime_gridweek = models.CharField(max_length=50, default='24:00:00')
    mintime_gridweek = models.CharField(max_length=50, default='00:00:00')
    maxtime_gridday = models.CharField(max_length=50, default='24:00:00')
    mintime_gridday = models.CharField(max_length=50, default='00:00:00')
    maxtime_sabado = models.CharField(max_length=50, default='24:00:00')
    mintime_sabado = models.CharField(max_length=50, default='00:00:00')
    maxtime_domingo = models.CharField(max_length=50, default='24:00:00')
    mintime_domingo = models.CharField(max_length=50, default='00:00:00')
    gridweek_weekends = models.CharField(max_length=50, default='false')

    def __str__(self):
        return self.nombre


class Obrasocial(models.Model):
    OS_ENABLE = (
        (True, 'Habilitada'),
        (False, 'Deshabilitada'),
    )
    OS_ACTIVA = (
        (True, 'SI'),
        (False, 'NO'),
    )

    class Meta:
        ordering = ['nombre']

    nombre = models.CharField(max_length=200, verbose_name='Nombre - Razon Social')
    cuit = models.CharField(max_length=40, verbose_name='Numero de CUIT', null=True, unique=True)
    servicio = models.ManyToManyField(Servicio)
    abreviatura = models.CharField(max_length=120, verbose_name='Abreviatura', null=True)
    codigo = models.IntegerField(verbose_name='Codigo interno Obra Social', null=True, unique=True)
    direccion = models.CharField(max_length=200,verbose_name='Dirección', null=True, blank=True)
    informacion_contacto = models.CharField(max_length=300, verbose_name='Información de contacto', null=True, blank=True)
    observacion = models.CharField(max_length=1000, verbose_name='Observacion', null=True, blank=True)
    habilitada = models.BooleanField(choices=OS_ENABLE, verbose_name='Estado', default=True, null=True)
    activa = models.BooleanField(choices=OS_ACTIVA, verbose_name='Activa', default=True)
    fecregistro = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación', null=True)
    fecactualizacion = models.DateTimeField(auto_now=True, verbose_name='Ultima actualización', null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre


class ServicioEstudio(models.Model):
    servicio = models.ForeignKey(Servicio,on_delete=models.PROTECT)
    numero = models.IntegerField(null=True, blank=True)
    nombre = models.CharField(max_length=200, verbose_name='Nombre del Estudio')
    nombre_abreviado = models.CharField(max_length=200, verbose_name='abreviado', null=True, blank=True)
    codigo = models.CharField(max_length=40)

    def __str__(self):
        return self.nombre


class ServicioAnestesia(models.Model):
    servicio = models.ForeignKey(Servicio,on_delete=models.PROTECT)
    numero = models.IntegerField(null=True, blank=True)
    nombre = models.CharField(max_length=200, verbose_name='Descripcion Anestesia')
    codigo = models.CharField(max_length=40)

    def __str__(self):
        return self.nombre


class Feriados(models.Model):
    nombre = models.CharField(max_length=200, verbose_name='Descripcion Feriado', blank=True, null=True)
    fecha = models.DateField(unique=True)
    is_guardia = models.BooleanField(default=True)


class Paciente(models.Model):
    class Meta:
        ordering = ['apellido','nombre']
        unique_together = ('identificacion_numero', 'identificacion_tipo',)

    TIPO_ID = (('DU', 'DU'), ('LC','LC'), ('LE', 'LE'), ('PA','PA'), ('CI','CI'), ('NN','NN'))
    SEXO = (('Masculino','Masculino'), ('Femenino', 'Femenino'), ('No Identificado', 'No Identificado'))
    ESTADO_CIVIL = (('Casado','Casado'),('Soltero','Soltero'), ('Otro','Otro'))
    NN = ((True, 'Verdadero'), (False, 'Falso'),)

    obrasocial = models.ManyToManyField(Obrasocial)
    pais = models.ForeignKey(Pais, on_delete=models.PROTECT, null=True, blank=True)
    provincia = models.ForeignKey(Provincia, on_delete=models.PROTECT, null=True, blank=True)
    departamento = models.ForeignKey(Departamento, on_delete=models.PROTECT, null=True, blank=True)
    localidad = models.ForeignKey(Localidad, on_delete=models.PROTECT, null=True, blank=True)
    barrio = models.ForeignKey(Barrio, on_delete=models.PROTECT, null=True, blank=True)
    domicilio = models.CharField(max_length=200, null=True, blank=True)
    identificacion_tipo = models.CharField(choices=TIPO_ID, max_length=10)
    identificacion_numero = models.CharField(max_length=80)
    nombre = models.CharField(max_length=120, null=True, blank=True)
    apellido = models.CharField(max_length=120,null=True, blank=True )
    nombrecompleto = models.CharField(max_length=200)
    sexo = models.CharField(choices=SEXO, null=True, max_length=20)
    fecnac = models.DateField(null=True, blank=True)
    telefono = models.TextField(max_length=200, null=True, blank=True)
    telefono_whatsapp = models.TextField(max_length=200, null=True, blank=True)
    email = models.TextField(max_length=200, null=True, blank=True)
    estadocivil = models.CharField(choices=ESTADO_CIVIL, null=True, max_length=20, blank=True)
    paciente_nn = models.BooleanField(default=False, choices=NN) # define si paciente sin identificacion
    observaciones = models.CharField(max_length=200, null=True, blank=True)
    paciente_nnlabel = models.CharField(max_length=200, null=True, blank=True)
    fecregistro = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()
    corregir = models.BooleanField(default=False)
    corregir_texto = models.CharField(max_length=240, default='', blank=True)

    def __str__(self):
        return '{0} {1} {2}'.format( self.apellido, self.nombre, self.paciente_nnlabel )

    def save( self, *args, **kw ):
        self.nombrecompleto = '{0} {1}'.format( self.apellido, self.nombre )
        super( Paciente, self ).save( *args, **kw )


class Turno(models.Model):
    class Meta:
        ordering = ['-id']
    CONTRASTEEV = {
        (False, 'Sin Contraste EV'),
        (True, 'Con Contraste EV')
    }
    CONTRASTEVO = {
        (False, 'Sin Contraste Oral'),
        (True, 'Con Contraste Oral')
    }
    AGUA = {
        (False, 'No Toma Agua'),
        (True, 'Toma Agua')
    }
    CANALIZACION = {
        (False, 'Sin Canalizar'),
        (True, 'Canalización Previa')
    }
    ANESTESIA = {
        (False, 'Sin Anestesia'),
        (True, 'Con Anestesia')
    }
    RETIROCONTRASTEVO = {
        (True, 'Retiro contraste oral'),
        (False, 'No retiro contraste oral')
    }
    TURNOTRABAJO = {
        ('Normal', 'Normal'),
        ('Guardia', 'Guardia')

    }
    TIPOMODALIDAD = [
        ('Normal', 'Normal'),
        ('En partes', 'En partes'),
    ]
    TIPOREPROGRAMADO = {
        ('Normal', 'Normal'),
        ('Re-Programado', 'Re-Programado'),
    }
    URGENCIA = {
        (True, 'Urgente'),
        (False, 'Programado'),
    }
    INTERNADO = {
        (True, 'Internado'),
        (False, 'Ambulatorio'),
    }
    HABILITADO = {
        (True, 'Habilitado'),
        (False, 'Deshabilitado'),
    }
    ANESTESISTA = (('ANESTESISTA UNICA', 'ANESTESISTA UNICA'),
                   ('ANESTESISTA UNICA', 'ANESTESISTA UNICA'))
    TIPO_ENPARTES = {
        (True, 'Principal'),
        (False, 'Secundario'),
    }

    usuario_creacion = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_creacion')
    usuario_recepcion_ultimaedicion = models.ForeignKey(User, on_delete=models.PROTECT, related_name='recepcion_ultima_edicion', null=True, blank=True)
    usuario_tecnico = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_tecnico', null=True, blank=True)
    usuario_tecnico_ultimaedicion = models.ForeignKey(User, on_delete=models.PROTECT, related_name='tecnico_ultima_edicion', null=True, blank=True)
    usuario_tipeo = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_tipeo', null=True, blank=True)
    usuario_tipeo_actual = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_tipeo_actual', null=True, blank=True)
    usuario_tipeo_ultimaedicion = models.ForeignKey(User, on_delete=models.PROTECT, related_name='tipeo_ultima_edicion', null=True, blank=True)
    usuario_tipeo_registra = models.ForeignKey(User, on_delete=models.PROTECT, related_name='tipeo_registra_informe', null=True, blank=True)
    usuario_medico = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_medico', null=True, blank=True)
    usuario_medico_ultimaedicion = models.ForeignKey(User, on_delete=models.PROTECT, related_name='medico_ultima_edicion', null=True, blank=True)
    paciente = models.ForeignKey(Paciente, on_delete=models.PROTECT)
    medico_derivante = models.ForeignKey(MedicoDerivante, on_delete=models.PROTECT)
    establecimiento_derivador = models.ForeignKey(EstablecimientoDerivador, on_delete=models.PROTECT, null=True, blank=True)
    establecimiento_servicio = models.ForeignKey(EstablecimientoServicio, on_delete=models.PROTECT, null=True, blank=True)
    obrasocial = models.ForeignKey(Obrasocial, on_delete=models.PROTECT)
    turno_relacionado = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT, null=True)
    entrega_digital = models.BooleanField(default=False, null=True, blank=True)
    whatsapp = models.BooleanField(default=False, null=True, blank=True)
    telefono_whatsapp = models.CharField(max_length=80, null=True, blank=True)
    email_entregadigital = models.CharField(max_length=80, null=True, blank=True)
    contrasteev = models.BooleanField(choices=CONTRASTEEV, default=False, null=True, blank=True)
    contrastevo = models.BooleanField(choices=CONTRASTEVO, default=False, null=True, blank=True)
    tomaagua = models.BooleanField(choices=AGUA, default=False, null=True, blank=True)
    canalizacion = models.BooleanField(choices=CANALIZACION, default=False, null=True, blank=True)
    anestesia = models.BooleanField(choices=ANESTESIA, default=False, null=True, blank=True)
    anestesia_codigo = models.CharField(max_length=80, null=True, blank=True)
    retirocontrastevo = models.BooleanField(choices=RETIROCONTRASTEVO, default=False)
    urgencia = models.BooleanField(choices=URGENCIA, default=False, null=True, blank=True)
    internado = models.BooleanField(choices=INTERNADO, default=False, null=True, blank=True)
    confirmacion = models.BooleanField(default=False)
    paciente_arm = models.BooleanField(default=False)
    paciente_nn = models.BooleanField(default=False)
    presente = models.BooleanField(default=False)
    presentefecha = models.DateTimeField(null=True, blank=True)
    observacion = models.TextField(max_length=280, verbose_name='Observacion', null=True, blank=True)
    observacion_noimpresa = models.TextField(max_length=1000, verbose_name='Observacion no impresa', null=True, blank=True)
    tipomodalidad = models.CharField(max_length=20, choices=TIPOMODALIDAD, default='Normal')
    tiporeprogramado = models.CharField(max_length=40, choices=TIPOREPROGRAMADO, default='Normal')
    turno_enpartes_primero = models.BooleanField(choices=TIPO_ENPARTES, default=True)
    turnotrabajo = models.CharField(max_length=40, choices=TURNOTRABAJO, null=True, blank=True)
    turno_orden = models.IntegerField(null=True, blank=True, default=0) # campo variable ajuste turnos reso horafecha inicio
    turno_recepcion_suspendido = models.BooleanField(default=False)
    turno_estudios_soli_backup = models.CharField(max_length=300, null=True, blank=True)
    turno_fixreso_inicio = models.IntegerField(null=True, blank=True, default=0) # campo variable ajuste turnos reso horafecha fin
    turno_fixreso_fin = models.IntegerField(null=True, blank=True, default=0) # campo variable ajuste turnos reso horafecha fin
    turno_fixreso = models.BooleanField(default=False)
    turno_numero = models.IntegerField(null=True, blank=True)
    fechahora_inicio = models.DateTimeField(null=True)
    fechahora_fin = models.DateTimeField(null=True)
    fechahora_inicio2 = models.DateTimeField(null=True,blank=True) #hora para vizualizar
    fechahora_fin2 = models.DateTimeField(null=True,blank=True)
    habilitado = models.BooleanField(choices=HABILITADO, default=True)
    estudios_solaux = models.CharField(max_length=200, null=True, blank=True)
    estudios_solaux_codigos = models.CharField(max_length=200, null=True, blank=True)
    estudios_solaux_inicial = models.CharField(max_length=200, null=True, blank=True)
    fecregistro = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    fecactualizacion = models.DateTimeField(auto_now=True, verbose_name='Ultima actualización')
    foto_pedidomedico = models.ImageField(upload_to=user_directory_path_pm, verbose_name="Foto PM", null=True, blank=True)
    foto_documento = models.ImageField(upload_to=user_directory_path_dni, verbose_name="Foto DOC1", null=True, blank=True)
    foto_documento2 = models.ImageField(upload_to=user_directory_path_dni, verbose_name="Foto DOC2", null=True, blank=True)
    backupnombrecompleto = models.CharField(max_length=400, null=True, blank=True)
    backupdni = models.CharField(max_length=100, null=True, blank=True)
    edadregistro_manual = models.CharField(max_length=20, null=True, blank=True)
    turno_observaciones_sistema = models.TextField(max_length=1000, null=True, blank=True) # para el caso de modificaciones/correciones manuales dejar constancia en el registro
    insumo = models.ManyToManyField(Insumo, through='TurnoInsumo')
    estudio_count = models.PositiveSmallIntegerField(null=True, blank=True) # cantidad de estudios realizados
    estudio_protocolocount = models.SmallIntegerField(null=True, blank=True) # cuenta actual de numero de estudios del servicio para el estudio realizado
    estudio_protocoloprefix = models.CharField(max_length=20, null=True, blank=True) # prefix del protocolo Año (2) Mes (2) label de servicio (2) eje: 2001A5
    estudio_accessnumber = models.CharField(max_length=80, null=True, blank=True)
    estudio_fechahora_ingreso = models.DateTimeField(null=True, blank=True)
    estudio_fechahora_manual = models.DateTimeField(null=True, blank=True)
    estudio_medicoanestesia = models.CharField(max_length=120, choices=ANESTESISTA, null=True, blank=True)
    estudio_terminado = models.NullBooleanField(null=True, blank=True) # si ya se cierra el estudio, cancelado
    estudio_finalizado = models.NullBooleanField(null=True, blank=True) # si finalizo con exito, continua el proceso a tipeo
    estudio_cancelado = models.NullBooleanField(null=True, blank=True)
    estudio_reprograma = models.NullBooleanField(null=True, blank=True)
    estudio_suspendido_paciente = models.NullBooleanField(null=True, blank=True)
    estudio_suspendido_completar = models.NullBooleanField(null=True, blank=True)
    estudio_finalizado_sistema = models.BooleanField(default=False)
    estudio_observacion_nota = models.TextField(max_length=1000, null=True, blank=True)
    estudio_observacion_medico = models.TextField(max_length=1000, null=True, blank=True)
    estudio_corregir = models.BooleanField(default=False)
    estudio_corregir_texto = models.CharField(max_length=80, null=True, blank=True)
    estudio_imagenes_url = models.CharField(max_length=3000, null=True, blank=True) # ya no se usa porque se cambio a multiples link por turno se mantiene el campo para los estudios viejos antes del csambio
    estudio_imagenes_url_externa = models.CharField(max_length=3000, null=True, blank=True)# idem upside
    estudio_link_imagenes_generado = models.BooleanField(default=False)
    estudio_partes_finalizado = models.NullBooleanField(null=True, blank=True)
    informe_fecha_apertura = models.DateTimeField(null=True, blank=True)
    informe_fecha_finalizacion = models.DateTimeField(null=True, blank=True) # fecha finalizacion medico
    informe_utltimafecha_actualizacion = models.DateTimeField(null=True, blank=True)
    informe_fecha_finalizacion_a = models.DateTimeField(null=True, blank=True) # fecha finalizacion del audio
    informe_fecha_apertura_t = models.DateTimeField(null=True, blank=True) # fecha finalizacion tipeo
    informe_fecha_finalizacion_t = models.DateTimeField(null=True, blank=True) # fecha finalizacion tipeo
    informe_finalizado = models.NullBooleanField(null=True, blank=True)
    informe_finalizado_tipeo = models.NullBooleanField(default=False)
    informe_texto = RichTextField(null=True, blank=True)
    informe_observaciones = models.TextField(max_length=1000, null=True, blank=True)
    informe_pre_observaciones = models.TextField(max_length=1000, null=True, blank=True)
    informe_observaciones_tipeo = models.TextField(max_length=1000, null=True, blank=True)
    informe_entregado = models.NullBooleanField(default=False)
    informe_audio = models.FileField(upload_to=user_directory_path_informe_audio, null=True, blank=True)
    informe_medicomatricula = models.CharField(max_length=50, null=True, blank=True)
    informe_numero = models.CharField(max_length=200, null=True, blank=True, default='No asignado')
    informe_firmadopdf = models.FileField(upload_to=user_directory_path_informe, verbose_name="Informe Firmado", null=True, blank=True)
    informe_cerrado = models.BooleanField(default=False) # informe cerrado = sin informe y sacado de pendientes
    informe_cerrado_fecha = models.DateTimeField(null=True, blank=True)
    informe_cerrado_usuario = models.ForeignKey(User, on_delete=models.PROTECT, related_name='usuario_informe_cierre', null=True, blank=True)
    informe_cerrado_observaciones = models.TextField(max_length=1000, null=True, blank=True)
    informe_no_firmadigital = models.NullBooleanField(default=False) # si no requiere firma digital
    informe_firmadodigital = models.BooleanField(default=False, null=True, blank=True) # si esta firmado o no digitalmente
    informe_datos_finalizado = models.CharField(max_length=2000, null=True, blank=True)
    informe_para_tipear = models.NullBooleanField(default=False)
    informe_facturado = models.BooleanField(default=False)
    informe_pasado_facturacion = models.BooleanField(default=False)
    informe_pasado_facturacion_fec = models.DateTimeField(null=True, blank=True)
    infomme_pasado_recepcion = models.BooleanField(default=False)
    informe_facturado_fecha = models.DateTimeField(null=True, blank=True)
    informe_tec_corregir = models.BooleanField(default=False)
    informe_tec_corregir_completo = models.BooleanField(default=False)
    informe_tec_corregir_ini = models.DateTimeField(null=True, blank=True)
    informe_tec_corregir_fin = models.DateTimeField(null=True, blank=True)
    history = HistoricalRecords()


class EstudioSolicitado(models.Model):
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT)
    servicio_estudio = models.ForeignKey(ServicioEstudio, on_delete=models.PROTECT)
    turno = models.ForeignKey(Turno, on_delete=models.PROTECT)
    numero_protocolo = models.CharField(max_length=30, null=True, blank=True,)
    cantidad = models.PositiveSmallIntegerField(default=1)
    history = HistoricalRecords()


class TurnoLinkImagen(models.Model):
    turno = models.ForeignKey(Turno, on_delete=models.PROTECT)
    numero_orden = models.PositiveSmallIntegerField()
    descripcion = models.CharField(max_length=500, null=True, blank=True)
    estudio_imagenes_url = models.CharField(max_length=3000, null=True, blank=True)
    estudio_imagenes_url_externa = models.CharField(max_length=3000, null=True, blank=True)


class TurnoNota(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    fecha = models.DateTimeField(null=True)
    fecha2 = models.DateTimeField(null=True)
    observacion = models.TextField(max_length=400, null=True, blank=True)
    fecregistro = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    estado = models.BooleanField(default=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT)


class TurnoBloqueo(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    fecha = models.DateTimeField(null=True)
    observacion = models.TextField(max_length=400, null=True, blank=True)
    servicio = models.PositiveIntegerField()
    estado = models.BooleanField(default=False)


class TurnoInsumo(models.Model): # insumos utilizados en el estudio
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    turno = models.ForeignKey(Turno, on_delete=models.PROTECT)
    fechahora = models.DateTimeField(auto_now_add=True)
    cantidad_usada = models.FloatField(default=0)
    usado_completo = models.BooleanField(default=True)
    history = HistoricalRecords()


class InsumoServicio(models.Model):
    class Meta:
        ordering = ['id']

    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT)
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT)
    habilitado_listado = models.BooleanField(default=False)
    stock = models.FloatField(default=0)
    history = HistoricalRecords()


class RemitoEntregaServicio(models.Model):
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    observaciones = models.CharField(max_length=400, null=True, blank=True)
    comprobante_numero = models.CharField(max_length=120, default='---')


class InsumoRemitoEntregaServicio(models.Model):
    remito_entrega_servicio = models.ForeignKey(RemitoEntregaServicio, on_delete=models.PROTECT, null=True)
    insumo_servicio = models.ForeignKey(InsumoServicio, on_delete=models.PROTECT)
    lote = models.ForeignKey(InsumoLote, on_delete=models.PROTECT, null=True)
    cantidad = models.FloatField()


class InsumoMovimientosServicio(models.Model):
    insumo = models.ForeignKey(Insumo, on_delete=models.PROTECT, null=True, blank=True)
    lote = models.ForeignKey(InsumoLote, on_delete=models.PROTECT, null=True, blank=True)
    servicio_insumo = models.ForeignKey(InsumoServicio, on_delete=models.PROTECT, null=True, blank=True)
    insumo_remito_entrega_serv = models.ForeignKey(InsumoRemitoEntregaServicio, on_delete=models.PROTECT, null=True, blank=True)
    comprobante = models.CharField(max_length=120, null=True, blank=True)
    ingreso = models.FloatField(default=0, null=True, blank=True)
    egreso = models.FloatField(default=0, null=True, blank=True)
    existencia = models.FloatField(default=0, null=True, blank=True)
    fechahora = models.DateTimeField(auto_now_add=True)
    observaciones = models.CharField(max_length=350, null=True, blank=True)


class TurnoReservado(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)
    fecha = models.DateTimeField(null=True) # fecha visual
    fecha2 = models.DateTimeField(null=True) # fecha visual
    titulo = models.TextField(max_length=400, null=True, blank=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.PROTECT)
    estado = models.BooleanField(default=True)


class ConfiguracionGeneral(models.Model):
    is_db_production = models.BooleanField(default=True)
    remito_egreso_numeracion = models.IntegerField(default=0)
    informe_email_body = models.TextField(max_length=1000, null=True, blank=True)
    informe_email_asunto = models.CharField(max_length=30, null=True, blank=True)
    informe_email_from = models.EmailField(null=True, blank=True)
    informe_email_envio_automatico = models.BooleanField(default=False)


class PacienteDBTCSE(models.Model):
    documento = models.CharField(max_length=400, null=True, blank=True)
    tipo_doc = models.CharField(max_length=400, null=True, blank=True)
    nombre = models.CharField(max_length=400, null=True, blank=True)
    fecha_nac = models.CharField(max_length=400, null=True, blank=True)
    e_civil = models.CharField(max_length=400, null=True, blank=True)
    sexo = models.CharField(max_length=400, null=True, blank=True)
    f_ingreso = models.CharField(max_length=400, null=True, blank=True)
    domicilio = models.CharField(max_length=400, null=True, blank=True)
    barrio = models.CharField(max_length=400, null=True, blank=True)
    dep = models.CharField(max_length=400, null=True, blank=True)
    telefono = models.CharField(max_length=400, null=True, blank=True)
    c_postal = models.CharField(max_length=400, null=True, blank=True)
    localidad = models.CharField(max_length=400, null=True, blank=True)
    depto = models.CharField(max_length=400, null=True, blank=True)
    provincia = models.CharField(max_length=400, null=True, blank=True)
    pais = models.CharField(max_length=400, null=True, blank=True)


class PaisImport(models.Model):
    nombre = models.CharField(max_length=120)
    codigo = models.CharField(max_length=120, null=True, blank=True)


class ProvinciaImport(models.Model):
    nombre = models.CharField(max_length=120)
    codigo = models.CharField(max_length=120, null=True, blank=True)


class DepartamentoImport(models.Model):
    provincia = models.CharField(max_length=120)
    nombre = models.CharField(max_length=120)
    codigo = models.CharField(max_length=120, null=True, blank=True)


class LocalidadImport(models.Model):
    departamento = models.CharField(max_length=120)
    nombre = models.CharField(max_length=120)
    codigo = models.CharField(max_length=120, null=True, blank=True)


class ObrasocialobsImport(models.Model):
    nombre = models.CharField(max_length=1000, null=True, blank=True)
    observaciones = models.CharField(max_length=1000, null=True, blank=True)
    abreviatura = models.CharField(max_length=200, null=True, blank=True)
    direccion = models.CharField(max_length=1000, null=True, blank=True)
    telefono = models.CharField(max_length=1000, null=True, blank=True)
    codigo = models.CharField(max_length=120, null=True, blank=True)
