from simple_history.admin import SimpleHistoryAdmin
from django.contrib import admin
from .models import *


admin.site.register(Pais)
admin.site.register(Provincia)
admin.site.register(Departamento)
admin.site.register(Localidad)
admin.site.register(Barrio)
admin.site.register(EstablecimientoDerivador)
admin.site.register(EstablecimientoServicio)
admin.site.register(Servicio)
admin.site.register(ServicioEstudio)
admin.site.register(TurnoNota)
admin.site.register(TurnoBloqueo)
admin.site.register(TurnoInsumo)
admin.site.register(TurnoReservado)
admin.site.register(ServicioAnestesia)
admin.site.register(TurnoLinkImagen)
admin.site.register(ConfiguracionGeneral)
admin.site.register(Feriados)
admin.site.register(InsumoServicio)
admin.site.register(InsumoMovimientosServicio)
admin.site.register(InsumoRemitoEntregaServicio)
admin.site.register(RemitoEntregaServicio)

admin.site.register(Turno, SimpleHistoryAdmin)
admin.site.register(MedicoDerivante, SimpleHistoryAdmin)
admin.site.register(EstudioSolicitado, SimpleHistoryAdmin)
admin.site.register(Paciente, SimpleHistoryAdmin)
admin.site.register(Obrasocial, SimpleHistoryAdmin)