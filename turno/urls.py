from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views
from .views import *

app_name = 'turno'


turno_patterns = ([
                      url(r'^obrasociallistar/', login_required(ObrasocialListar.as_view()), name='obrasocial_list_view'),
                      url(r'^obrasocialcrear/$', login_required(ObrasocialCrear.as_view()), name='obrasocial_crear'),
                      url(r'^obrasocialeliminar/(?P<pk>\d+)/$', login_required(ObrasocialEliminar.as_view()), name='obrasocial_eliminar'),
                      url(r'^obrasocialactualizar/(?P<pk>\d+)/$', login_required(ObrasocialActualizar.as_view()), name='obrasocial_actualizar'),
                      url(r'^medicolistar/', login_required(MedicoListar.as_view()), name='med_listar'),
                      url(r'^medicocrear/$', login_required(MedicoCrear.as_view()), name='med_crear'),
                      url(r'^medicoactualizar/(?P<pk>\d+)/$', login_required(MedicoActualizar.as_view()), name='med_actualizar'),
                      url(r'^medicoeliminar/(?P<pk>\d+)/$', login_required(MedicoEliminar.as_view()), name='med_eliminar'),
                      url(r'^pacientelistar/', login_required(PacienteListar.as_view()), name='pac_listar'),
                      url(r'^pacientecrear/$', login_required(PacienteCrear.as_view()), name='pac_crear'),
                      url(r'^pacienteactualizar/(?P<pk>\d+)/$', login_required(PacienteActualizar.as_view()), name='pac_actualizar'),
                      url(r'^pacienteactualizarcorregir/(?P<pk>\d+)/$', login_required(PacienteCorregirPopUp.as_view()), name='pac_corregir_popup'),
                      url(r'^pacienteeliminar/(?P<pk>\d+)/$', login_required(PacienteEliminar.as_view()), name='pac_eliminar'),
                      url(r'^pacientecorregir/', login_required(PacienteCorregir.as_view()), name='pac_corregir'),
                      url(r'^pacientemarcarcorregido/', marcar_paciente_corregido, name='pac_marcar_corregido'),
                      path('ajax/carga_provincias/', carga_provincias, name='correr_prov'),
                      path('ajax/carga_departamentos/', carga_departamentos, name='correr_dpto'),
                      path('ajax/carga_localidades/', carga_localidades, name='correr_loc'),
                      url(r'^turnolistar/$', login_required(TurnoListar.as_view()), name='turno_listar'),
                      url(r'^turnobrightspeed/', login_required(TurnoBrightspeed.as_view()), name='turno_brightspeed'),
                      url(r'^turnocrear/', login_required(TurnoCrear.as_view()), name='turno_crear'),
                      url(r'^turnoactualizar/(?P<pk>\d+)/$', login_required(views.TurnoActualizar.as_view()), name='turno_actualizar'),
                      url(r'^turnocrearpaciente/', views.crearpaciente_turno, name='turnopaciente_crear_ajax'),
                      url(r'^turnoactualizarpaciente/', views.actualizarpaciente_turno, name='turnopaciente_actualizar_ajax'),
                      url(r'^turnocrearpacientetemplate/', login_required(TurnoPacienteCrear.as_view()), name='turnopaciente_crear'), # para turno crear paciente
                      url(r'^turnocrearpacientetemplate2/', login_required(TurnoPacienteCrearModal.as_view()), name='turnopaciente_crear2'), # para el listdao de pacientes y historial
                      path('buscarpacientedatos/', views.buscar_paciente_datos, name='buscar_paciente_datos'),
                      path('buscarpacientedni/', views.buscar_paciente_dni, name='buscar_paciente_dni'),
                      path('buscarobrasocial/', views.buscar_obrasocial, name='buscar_obrasocial'),
                      path('buscarmedico/', views.buscar_medico, name='buscar_medico'),
                      path('buscarestablecimiento/', views.buscar_establecimiento, name='buscar_establecimiento'),
                      path('buscarturno/', views.buscar_turno, name='buscar_turno'),
                      path('buscarturnos_paciente/', views.buscar_turnosxpacienteid, name='buscar_turnoxpaciente'),
                      path('listapacientes/', views.paciente_listar, name='paciente_listar'),
                      path('listapacientesturnoid/', views.listarpaciente_turnoid, name='paciente_listarturnoid'),
                      path('listapacientesnormal/', views.paciente_listarnormal, name='paciente_listarnormal'),
                      path('listaobrasocial/', views.obrasocial_listar, name='obrasocial_listar'),
                      path('listamedicosderivantes/', views.medicosderivantes_listar, name='medicosderivantes_listar'),
                      path('listaestablecimientos/', views.establecimientos_listar, name='establecimientos_listar'),
                      path('actualizarturno/', views.actualizar_turno_fecha, name='actualizar_turno'),
                      path('actualizarturnoestado/', views.actualizar_turno_estado, name='actualizar_turno_estado'),
                      path('actualizarturnocortado/', views.actualizar_turno_cortado, name='actualizar_turno_cortado'),
                      path('consultatipoturno/', views.consulta_tipoturno, name='consulta_tipoturno'),
                      path('crearnota/', views.crearnota_turno, name='crear_nota'),
                      path('actualizarnota/', views.actualizarnota_turno, name='actualizar_nota'),
                      path('crearturnoreservado/', views.turnoreservado_crear, name='turnoreservado_crear'),
                      path('eliminarturnobloqueado/', views.turnobloqueado_eliminar, name='turnobloqueado_eliminar'),
                      url(r'^constanciaturnoimprimir/', constancia_turno_imprimir, name='turno_constanciaimprimir'),
                      url(r'^generar_linkimagenes/', generar_linkimagenes, name='generar_linkimagenes'),
                      path('crearferiado/', views.crear_feriado, name='crear_feriado'),
                      path('crearmedicoderivante/', views.medicoderivante_crear, name='crear_medicoderivante'),
                  ], 'turno')
