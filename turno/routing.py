from django.conf.urls import url
from . import consumers

websocket_urlpatterns = [
    url(r'^ws/turno/turnobrightspeed/$', consumers.ChatConsumer),
    url(r'^ws/turno/informes/$', consumers.ChatConsumer2),
]